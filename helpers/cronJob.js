const cron = require('node-cron');
const corte = require('../models/corteServiciosSedes/corteServiciosSedes');

// Cron job que se ejecuta a las 00:00 del primer día de cada mes
cron.schedule('0 0 15 * *', async () => {
  console.log('Este cron job se ejecuta cada 10 días a la medianoche');
  // Datos a guardar
  let data = {
    sede: 'TLYC01',
    texto: 'Este cron job se ejecuta cada 10 días a la medianoche'
  };
  // Crear una nueva instancia del modelo de corte
  const corteSaved = new corte(data);
  try { 
    await corteSaved.save();
    console.log('Corte guardado con éxito');
  } catch (error) {
    console.error('Error al guardar el corte:', error);
  }
});

