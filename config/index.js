const PORT = process.env.PORT || 3200;

require('dotenv').config()
 

  let URI = '';
    //! Esta es para la url de produccion cambiar el ENV_PRODUCTION por que esta en FALSE
    if (process.env.ENV_PRODUCTION == 'true') {
        URI = process.env.URL_DB_PRODC;
    //! Esta es para la url de testing cambiar el ENV_TEST por que esta en FALSE
    } else if (process.env.ENV_TEST == 'true') {
       URI = process.env.URL_DB_TEST;      
    //! Esta es para la url de desarrollo en el local
    } else {
      URI = process.env.URL_DB_DEV
    }
    //URI = "mongodb://localhost:27017"
   // URI = "mongodb+srv://juanito:eltodasmias-16@cluster0-3lxr1.mongodb.net/test"
module.exports = {
    PORT,
    URI
}
// para entrar a heroku
// comando para entrar al proyecto
//! URL PROD https://backendpruebashorizonte.herokuapp.com