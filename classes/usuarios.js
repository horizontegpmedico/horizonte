module.exports = class Usuario {


    constructor() {
        this.personas = [];

    }


    // agregamso una persona al chat 
    agregarPersona(usuario) {


        let persona = {
            nombre: usuario.nombre,
            role: usuario.role,
            id: usuario.id
        }


        this.personas.push(persona);
        // regresamos a todas las personas del chat 

        return this.personas;

    }


    getPersona(id) {


        let persona = this.personas.filter(persona => persona.id === id)[0];

        // devuelve a la persona
        return persona;


    }



    getPersonas() {
        // todas las personas del chat 
        this.personas;
    }


    deletePersona(id) {

        let personaBorrada = this.getPersona(id);

        this.personas = this.personas.filter(persona => {

            // devolvemos a las personas que no tienen ese id

            return persona.id != id;

        });

        return personaBorrada;


    }


}