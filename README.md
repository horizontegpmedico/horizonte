# Dependencias

## Express  
- npm install --save express

## Socket io
- npm install --save socket.io

## nodemon

- npm install --save-dev nodemon

## express-fileupload
- npm i express-fileupload

## jsonwebtoken
- npm i jsonwebtoken


## dotenv
- npm i dotenv