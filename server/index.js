//=======================================
//          LIBS
//=======================================

const http = require('http');
const express = require('express');
const socketIo = require('socket.io');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const jwt = require('jsonwebtoken');

const multer = require('multer');

//=======================================
//          CONFIG
//=======================================

const { PORT, URI } = require('../config/index');
const modules = require('../models/modules');
// const { patch, path } = require('../routes');
const path = require('path');


// inicio de express
const app = express();

// var pedidoLaboratorio;
// Estamos habilitando el cors dentro de
// la apliacion



// update to match the domain you will make the request from
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

// midlewares de expresss
// el bodyparser va antes de usar las rutas

app.use(bodyParser.urlencoded({limit: '50mb', parameterLimit: 100000, extended: true}));
app.use(bodyParser.json({limit: '50mb', parameterLimit: 100000, extended: true}));

/* app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
 */
app.use(require('../routes/index'));
 
    // const a = require('../contratos/img')
app.use(express.static(path.join(__dirname, '../contratos/img/' )));

app.use(express.static(path.join(__dirname, '../public')));
// server configurado para los sockets
let Server = http.createServer(app);

// imagenes para las sedes

// conexion con la BD
console.log(URI)
mongoose.connect(`${URI}`, { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true, useUnifiedTopology: true })
    .then(msg => console.log("DB online"))
    .catch(error => console.log(error));

 
// esta es la comunicación con el backend

var io = socketIo.listen(Server);

require('../sockets/login')(io);

//Cron job 
require('../helpers/cronJob');

Server.listen(PORT, (err) => {
    if (err) throw new Error(err + "aqui hubopedo");
});

console.log(`Server on port: ${PORT}`);

//==========================================================
//         This code was written by:
// Team members
// Jose Juan Patrón Guerrero
// Guillermo Urzúa Sánchez
// Ulises Jaime Suarez
// Nelson alday
// Katia Amaro
// Ilmers Michelle
// Manuel Aragon
//==========================================================

