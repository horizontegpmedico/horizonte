const express= require('express');
//const tabuladorMaternidad = require('../../models/maternidad/tabuladorPagos');
const PaquetesPagos = require('../../models/maternidad/Pagos');
const serveResp = require('../../functions/functions');
const PaquetesPacientes = require('../../models/paquetes-paciente');

const ventasServicios = require('../../models/ventasServicios/serviciosVentas');

const app = express();


app.get('/tabulador/maternidad', (req, resp) => {

  tabuladorMaternidad.find()
  .exec( (error, pagosDB) => {

    if(error){
      return resp.status(500)
      .json({
        ok: false,
        massage: 'No se pudo conectar',
        error
      });
    }

    if( !pagosDB | pagosDB === [] ){
      return resp.status(400)
      .json({
        ok:false,
        message: 'No se encontraron los pagos',
        pagos:pagosDB
      })
    }

    return resp.status(200)
    .json({
      ok:true,
      message: 'Se encontraron los pagos',
      pagos: pagosDB
    })

  });

});

app.get('/tabulador/maternidad/:semanaDeGestacion', ( req, resp ) => {


  let semanaDeGestacion = req.params.semanaDeGestacion;

  tabuladorMaternidad.find({ semanaGestacion:semanaDeGestacion })

    .exec( (error, pagos ) => {


        if(error){
          return resp.status(500)
          .json({
            ok: false,
            massage: 'Algo pasó',
            error
          })
        }


        if( !pagos ){
          return resp.status(400)
          .json({
            ok:false,
            message: 'No se encontró el pago'

        })

        }


        return resp.status(200)
        .json({
          ok: true,
          message:'El pago es:',
          pagos
        });

    });


});

app.post('/tabulador/maternidad', (req, resp) => {

      let body = req.body;

      let tabulador = new tabuladorMaternidad({
        semanaGestacion: body.semanaGestacion,
        semanaPago: body.semanaPago,
        pago: body.pago
      });

      tabulador.save( (error, pago) =>{

        if(error){
          return resp.status(500)
          .json({
            ok: false,
            massage: 'Algo pasó',
            error
          })
        }



        return resp.status(200)
        .json({
          ok: true,
          message:'El pago es:',
          pago
        });

});

});

//============================================================
//                   Paquete de maternidad
//============================================================

app.get('/pagar/maternidad',(req,resp)=>{
  PaquetesPacientes.find({paciente: id})
  .populate('paquete')
  .exec()
  .then(data => serveResp(data, null, 'Se encontraron los paquetes', resp))
  .catch(error => serveResp(null, error, 'No se encontraron paquetes al usuario', resp));


});

//============================================================
//               Paquete de maternidad por paciente
//============================================================

app.get('/pagar/maternidad/:id',(req,resp)=>{
  let id = req.params.id;
  console.log(id);

  PaquetesPagos.find({_id: id})
  .populate('Paciente')
  .populate('paquete')
  .exec()
  .then(data => serveResp(data, null, 'Se encontraron los paquetes', resp))
  .catch(error => serveResp(null, error, 'No se encontraron paquetes al usuario', resp));

});

//============================================================
//                   Paquete de maternidad Pago semanal
//============================================================

app.put('/pagar/maternidad/:semana',(req,resp)=>{
  let semanaActualizar = req.params.semana;
  let body = req.body;


  pagosmaternidad
  .findByIdAndUpdate(semanaActualizar,body)
  .exec()
  .then((data)=>{
    return resp.status(200)
    .json({
        ok: true,
        message: 'Se actualizaron los pagos de los paquetes',
        data
    });
  })
  .catch((error)=>{
    return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo actualizar la semana de pago',
                        error
                    });
  });
});



module.exports = app;
