const express = require('express');
const serviciosPacientes = require('../../models/enfermeria/visitas');


let app = express();


app.get('/vistas/enfermeria', (req, resp) => {

    serviciosPacientes
        .find()
        .exec()
        .then(visitas => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Visitas encontradas',
                    visitas
                });
        })
        .catch(error => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo conectar',
                    error
                })
        })

});



app.post('/nueva/visita', (req, resp) => {



    let body = req.body;

    let nuevoServicio = new serviciosPacientes({

        horallegada: body.horallegada,
        horaSalida: body.horaSalida,
        servicio: body.servicio,
        pacienteId: body.pacienteId,
        atendio: body.atendio

    })



    nuevoServicio.save()
        .then(servicio => {


            return resp.status(201)
                .json({
                    ok: true,
                    message: 'Nuevo servicio agregado',
                    servicio
                })

        })

    .catch(
        error => {


            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo guardar',
                    error
                });

        }
    )
})



module.exports = app;