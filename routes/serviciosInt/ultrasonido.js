const express = require('express');
const ultrasonido = require('../../models/servicioIntegrados/modelUltrasonido');

let app = express();



//========================================================================
//                  SERVICIO QUE CREA UN NUEVO ULTRASONIDO
//========================================================================

app.post('/ultrasonido', (req, resp) => {

    let body = req.body;

    let Ultrasonid = new ultrasonido({



        ESTUDIO: body.ESTUDIO,
        INDICACIONES: body.INDICACIONES,
        PRECIO_PUBLICO: body.PRECIO_PUBLICO,
        PRECIO_PUBLICO_URGENCIA: body.PRECIO_PUBLICO_URGENCIA,
        PRECIO_PUBLICO_HOSPITALIZACION: body.PRECIO_PUBLICO_HOSPITALIZACION,
        PRECIO_PUBLICO_HOSPITALIZACIO_URGENCIA: body.PRECIO_PUBLICO_HOSPITALIZACIO_URGENCIA,
        PRECIO_MEMBRESIA: body.PRECIO_MEMBRESIA,
        PRECIO_MEMBRESIA_URGENCIA: body.PRECIO_MEMBRESIA_URGENCIA,
        PRECIO_MEMBRESIA_HOSPITALIZACION: body.PRECIO_MEMBRESIA_HOSPITALIZACION,
        PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA: body.PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA


    });


    Ultrasonid.save()
        .then((data) => {
            return resp.status(201)
                .json({
                    ok: true,
                    message: 'se creo el ultrasonido',
                    ultrasonido: data
                })
        })
        .catch(error => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'no se pudo crear',
                    error
                })
        })

});





//========================================================================
//                  SERVICIO QUE MUESTRA TODOS LOS ULTRASONIDOS
//========================================================================


app.get('/ultrasonido', (req, resp) => {

    ultrasonido.find()
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontraron los estudios',
                    ultrasonido: data
                })
        })
        .catch(error => {

            return resp.status(400)
                .json({
                    ok: false,
                    message: 'no se encontraron',
                    error
                })
        })


});

//========================================================================
//                SERVICIO QUE MUESTRA UN ULTRASONIDO POR EL ID
//========================================================================


app.get('/ultrasonido/:id', (req, resp) => {


    let id = req.params.id;


    ultrasonido.findById({ _id: id })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontraron los estudios',
                    ultrasonido: data
                })
        })
        .catch(error => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo encontrar',
                    error
                })
        })


});



//========================================================================
//                SERVICIO QUE ACTUALIZA UN ULTRASONIDO POR EL ID
//========================================================================

app.put('/ultrasonido/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;

    ultrasonido.findByIdAndUpdate({ _id: id }, body, (error, ultrasonidoUpdated) => {

        if (error) {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo conectar',
                    error
                })
        }

        if (!ultrasonidoUpdated) {
            return resp.status(400)
                .json({
                    ok: false,
                    message: `No se econtró el ultrasonido con el id: ${id}`,
                    ultrasonido: ultrasonidoUpdated
                })

        }


        return resp.status(200)
            .json({
                ok: true,
                message: 'Se encontró el ultrasonid',
                ultrasonido: ultrasonidoUpdated
            });


    });

});




//========================================================================
//                SERVICIO QUE ELIMINA UN ULTRASONIDO POR EL ID
//========================================================================


app.delete('/ultrasonido/:id', (req, resp) => {


    let id = req.params.id;

    ultrasonido.findByIdAndDelete({ _id: id }, (error, ultrasonidoDeleted) => {


        if (error) {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo conectar',
                    error
                })
        }

        if (!ultrasonidoDeleted) {
            return resp.status(400)
                .json({
                    ok: false,
                    message: `No se econtró el ultrasonido con el id: ${id}`,
                    ultrasonido: ultrasonidoDeleted
                })

        }


        return resp.status(200)
            .json({
                ok: true,
                message: 'Se encontró el ultrasonid',
                ultrasonido: ultrasonidoDeleted
            });
    });


});

module.exports = app;