const express = require('express');
const Ambulancia = require('../../models/servicioIntegrados/modelAmbulancia');
const serveResp = require('../../functions/functions');

let app = express();

app.get('/ambulancia', async(req, resp) => {

    console.log('Servicio de ambulancia')

    await Ambulancia.find()
        .sort('DESTINO')
        .exec()
        .then(data => respuestaServer(data, null, 'Se encotraron los destinos', resp))
        .catch(error => respuestaServer(null, error, 'No se encontraron los destinos', resp));


});


app.post('/ambulancia', (req, resp) => {

    let body = req.body;

    let newDestino = new Ambulancia({

        DESTINO: body.DESTINO,
        PRECIO_PUBLICO_DIA: body.PRECIO_PUBLICO_DIA,
        PRECIO_PUBLICO_REDONDO_DIA: body.PRECIO_PUBLICO_REDONDO_DIA,
        PRECIO_PUBLICO_NOCHE: body.PRECIO_PUBLICO_NOCHE,
        PRECIO_PUBLICO_REDONDO_NOCHE: body.PRECIO_PUBLICO_REDONDO_NOCHE,
        PRECIO_MEMBRESIA_DIA: body.PRECIO_MEMBRESIA_DIA,
        PRECIO_MEMBRESIA_REDONDO_DIA: body.PRECIO_MEMBRESIA_REDONDO_DIA,
        PRECIO_MEMBRESIA_NOCHE: body.PRECIO_MEMBRESIA_NOCHE,
        PRECIO_MEMBRESIA_REDONDO_NOCHE: body.PRECIO_MEMBRESIA_REDONDO_NOCHE

    });



    newDestino.save()
        .then(data => respuestaServer(data, null, 'Se creo el nuevo servicio', resp))
        .catch(error => respuestaServer(null, error, 'No se pudo crear el nuevo servicio', resp))

});

app.get('/obetener/ambulancia/membresia', (req, resp) => {


    Ambulancia
        .find({}, "DESTINO  PRECIO_MEMBRESIA_DIA PRECIO_MEMBRESIA_REDONDO_DIA PRECIO_MEMBRESIA_NOCHE PRECIO_MEMBRESIA_REDONDO_NOCHE name")
        .exec()
        .then(data => respuestaServer(data, null, 'Se encontraron los servicios con membresia', resp))
        .catch(error => respuestaServer(null, error, 'No se pudo conectar', resp))

});

app.get('/ambulancia/:id', (req, resp) => {


    let id = req.params.id;

    Ambulancia
        .findById({ _id: id })
        .exec()
        .then(data => respuestaServer(data, null, 'Se encontro el destino', resp))
        .catch(error => respuestaServer(null, error, 'No se encontro el destino', resp))

});


//==============================================================
//     update de la ambulancia
//==============================================================

app.put('/ambulancia/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;

    Ambulancia
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then(data => respuestaServer(data, null, 'Se actualizaron los datos', resp))
        .catch(error => respuestaServer(null, error, 'No se pudieron actualizar los datos', resp))

});






//=============================================================
//    ESTE SERVICIO ELIMINA LOS DATOS DE AMBULANCIA
//=============================================================


app.delete('/ambulancia/:id', async(req, resp) => {

    let id = req.params.id;

    Ambulancia
        .findByIdAndDelete(id)
        .exec()
        .then(data => respuestaServer(data, null, 'Se elimino el destino', resp))
        .catch(error => respuestaServer(null, error, 'No se pudo eliminar el destino', resp))

});



const respuestaServer = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;