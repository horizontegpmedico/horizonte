const express = require('express');
const sedes = require('../../models/sedes/sedes');

const serveResp = require('../../functions/functions')

const app = express();

app.post('/crear/sede', (req, resp) => {

    const body = req.body;

    const newSede = new sedes({

        nomenclatura: body.nomenclatura,
        localizacion: body.localizacion,
        encargado: body.encargado,
        rfc: body.rfc
    });

    newSede
        .save()
        .then(data => serveResp(data, null, 'Se creo la nueva sede', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear la nueva sede', resp));

});


app.post('/agregar/utilidad/:id', (req, resp) => {

    const id = req.params.id;

    sedes
        .findOneAndUpdate(id)
        .exec()
        .then(data => serveResp(data, null, 'Se guardar', resp))
        .catch(error => serveResp(null, error, 'No se guardaron los datos', resp));

});

app.get('/ver/sedes', (req, resp) => {

    sedes
        .find()
        .sort({ encargado: 1 })
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron las sedes', resp))
        .catch(error => serveResp(null, error, 'No se encontraron sedes', resp))
});


app.get('/ver/sede/:id', (req, resp) => {

    const { id } = req.params;

    console.log(id);
    sedes
        .findById(id)
        .exec()
        .then(data => serveResp(data, null, 'Se encontro la utilidad', resp))
        .catch(error => serveResp(null, error, 'No se encontro la utilidad', resp));
});

app.get('/ver/nome/sede/:nom', (req, resp) => {

    const { nom } = req.params;
    sedes
        .find({nomenclatura:nom})
        .exec()
        .then(data => serveResp(data, null, 'Se encontro la utilidad', resp))
        .catch(error => serveResp(null, error, 'No se encontro la utilidad', resp));
});

module.exports = app