const express = require('express');
const serveResp = require('../../functions/functions');
const medicos = require('../../models/medicos/modelmedicos');

let app = express();


//============================================================
//                   Crea un nuevo medico
//============================================================

app.post('/nuevo/medico', (req, resp) => {

    let body = req.body;

    let newMedico = new medicos({
        nombre: body.nombre,
        cedulaProfesional: body.cedula,
        sucursal: body.sucursal
    });

    newMedico.save( (error, medicoSaved) => {

        if(error){
            return resp.status(500)
            .json({
                ok:false,
                message: 'No se pudo conectar',
                error
            })
        }


        if( !medicoSaved ){
            return resp.status(400)
            .json({
                ok: false,
                message:'No se pudo guardar el medico'
            })
        }


        return resp.status(200)
            .json({
                ok: true,
                message: 'Se guardo el medico',
                medico: medicoSaved
            });
    });

});



//============================================================
//                   listado de los medicos
//============================================================


app.get('/medicos', async (req, resp ) => {


    try {
       const medicosDB = await medicos.find().sort({"NOMBRE" : 1});
       serveResp(medicosDB, null, 'Se encontraron los medicos', resp);
       
    } catch (error) {

        console.log(error); 
        serveResp(null, error, 'No se pudo conectar', resp)  
    }

});

module.exports = app;