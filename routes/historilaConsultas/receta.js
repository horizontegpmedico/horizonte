const express = require('express');

const receta = require('../../models/consultasGeneralesBitacoras/recetaGRal');
const serveResp = require('../../functions/functions');
const personal = require('../../models/personal');

let app = express();


app.post('/agregar/receta', async(req, resp) => {

    let body = req.body;

    const counter = await receta.find({origen: body.origen});
    countLength = counter.length + 1;

    let newReceta = new receta({
        medicamentos: body.medicamentos,
        otrasIndicaciones: body.otrasIndicaciones,
        idPaciente: body.idPaciente,
        idConsulta: body.idConsulta,
        fechaReceta: body.fechaReceta,
        horaReceta: body.horaReceta,
        estudios: body.estudios,
        medicoQueAtendio: body.medicoQueAtendio,
        idMedico: body.idMedico,
        cedula: body.cedula,
        firma: body.firma,
        origen: body.origen,
        horaEgreso: body.horaEgreso,
        prioridad: body.prioridad,
        folio:countLength+1
    });

    newReceta
        .save()
        .then(data => serveResp(data, null, 'Se creo la receta', resp))
        .catch(error => serveResp(null, error, 'No se pudo guadar la receta del paciente', resp))

});


app.get('/ver/recetas/:medico', async(req, resp) => {

    const{ medico } = req.params;

    try {

        const medicoResp = await personal.findById( medico.id );

        if( medicoResp ) {
            serveResp( null, 'El ID no es valido', 'Ingresa un ID valido', resp );
        }

        const dataRecetas = await receta.find({ idMedico: medico })
                                            .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero')
                                            .sort({fechaReceta: -1});
        serveResp(dataRecetas, null, 'Se encontraron las recetas', resp);
    
        
    } catch (error) {
        console.error(error);
        serveResp( null, error, 'No se pudo conectar', resp);
    }


});


// listado de las recetas con estudios
app.get('/ver/recetas/con/estudios', (req, resp) => {

    receta
        .find()
        .populate('idPaciente')
        .limit(56)
        .sort({_id:-1})
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los estudidos pendientes', resp))
        .catch(error => serveResp(null, error, 'No se en contraron estudios', resp))

});

// listado de las recetas con estudios
app.get('/ver/recetas/con/estudios/por/sede/:sede', (req, resp) => {
    const{ sede } = req.params;
    receta
        .find({origen:sede})
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los estudidos pendientes', resp))
        .catch(error => serveResp(null, error, 'No se en contraron estudios', resp))

});

app.get('/ver/receta/:id', async(req, resp) => {

    try {

        const { id } = req.params;
        
        const dataId = await receta.findById(id).populate('idPaciente','nombrePaciente apellidoPaterno apellidoMaterno fechaNacimientoPaciente genero telefono curp edad')
                                                .populate('idMedico', "Especialidad cedulaProfesional cedulaEsp nombre" )
        serveResp(dataId, null, 'Se encontro al receta', resp);

    } catch (error) {
        console.error(error);
        serveResp(null, error,'No se pudo conectar', resp )
    }

});

app.get('/ver/consulta/receta/:id', (req, resp) => {



    const { id } = req.params;


    receta
        .findById(id)
        .populate('idConsulta')
        .populate('idMedico')
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontro la receta y la consulta', resp))
        .catch(error => serveResp(null, error, 'No se econtro', resp))
});
app.get("/ver/consultas/anteriores/paciente/:id", (req, resp) => {
    const { id } = req.params;
    receta
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las recetas', resp))
        .catch(error => serveResp(null, error, 'No se encontraron las recetas', resp))
})

module.exports = app;