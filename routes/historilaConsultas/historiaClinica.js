let express = require('express');
let historiaClinica = require('../../models/signosVitales/historiaClinica')

let app = express();


app.post('/agregar/hoja/evolucion', (req, resp) => {

    let body = req.body;

    let newHistoriaClinica = new historiaClinica({
        idPaciente: body.idPaciente,
        diagnostico: body.diagnostico,
        fecha: body.fecha,
        hora: body.hora,
        motivoDeConsulta: body.motivoDeConsulta,
        evolucionDelPadecimiento: body.evolucionDelPadecimiento,
        medicoTrante: body.medicoTrante,
        plan: body.plan,
        piel: body.piel,
        cabezaCuello: body.cabezaCuello,
        torax: body.torax,
        abdomen: body.abdomen,
        genitales: body.genitales,
        extremidades: body.extremidades,
        sistemaNervioso: body.sistemaNervioso,
        respiratorioCardiovascular: body.respiratorioCardiovascular,
        digestivo: body.digestivo,
        endocrino: body.endocrino,
        musculoEsqueletico: body.musculoEsqueletico,
        genitourinario: body.genitourinario,
        hematopoyeticoLinfatico: body.hematopoyeticoLinfatico,
        pielAnexos: body.pielAnexos,
        neurologicoPsiquiatricos: body.neurologicoPsiquiatricos
    });


    newHistoriaClinica
        .save()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se guardo la historia clinica',
                    data
                });
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo guardar',
                    error
                });
        });

});

app.get('/ver/historia/paciente/:id', (req, resp) => {

    let id = req.params.id;


    historiaClinica
        .findById({ _id: id })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontro la historia del paciente',
                    data
                });
        })
        .catch((error) => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo encontrar la historia del paciente',
                    error
                });
        });

});

module.exports = app;