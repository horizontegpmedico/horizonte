const express = require('express');
const hospitalizacionSchema = require('../../models/hospitalizacion/hospitalizacion');
const serveResp = require('../../functions/functions');

const app = express();

app.post('/register/nota/indicaciones', async (req, resp) => {

    const { body } = req;

    if( body.paciente !== '' &&  body.indicaciones.length > 0 && body.hora !== ''){
        try{
        const notaIndicacioesnew = new  hospitalizacionSchema;
        const notaSaved = await notaIndicacioesnew.save();
        serveResp( notaSaved, null, 'Se creo la nueva nota' , resp);

        }catch(error) {
            console.log(error);
            serveResp(null, error, 'Hubo un error', resp);
        }
    }
    

});


app.get('/list/notas/indicaciones', async (req, resp) => {

    try{

        const listNotas = await hospitalizacionSchema.find().sort({ paciente: -1 });
        serveResp(listNotas, null, 'Listado de las notas', resp);
    }catch (error ) {
        console.log(error);
        serveResp(null, error, 'Hubo un error', resp);
    }
    
});

module.exports = app;