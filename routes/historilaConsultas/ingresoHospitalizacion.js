const hospitalizacion = require('../../models/consultasGeneralesBitacoras/ingresoHospitalizacion');
const express = require('express')

const app = express();




app.post('/ingreso/hospitalizacion', async(req, res) => {


    let body = req.body;


    let newSchemaHospitalizacion = new hospitalizacion({

        fechaIngreso: body.fechaIngreso,
        horaIngreso: body.horaIngreso,
        enfermeraAtendio: body.enfermeraAtendio,
        diagnosticoInicial: body.diagnosticoInicial,
        diagnosticoActual: body.diagnosticoActual,
        medicoTrante: body.medicoTrante,
        genero: body.genero,
        nombre: body.nombre,
        edad: body.edad,
        apellidoPaterno: body.apellidoPaterno,
        apellidoMaterno: body.apellidoMaterno,
        tipoDeServicio: body.tipoDeServicio,
        membretesLegible: body.membretesLegible,
        notificacionDeIdentificacion: body.notificacionDeIdentificacion,
        solicitudesDeEstudioDeGabinete: body.solicitudesDeEstudioDeGabinete,
        cuentaConBrazaleteDeIdentificacion: body.cuentaConBrazaleteDeIdentificacion,
        elMedicamentoDelPacienteCuantaConLaIdentificion: body.elMedicamentoDelPacienteCuantaConLaIdentificion,
        solucionIntravenosaCuentaConIdentificacioCompleta: body.solucionIntravenosaCuentaConIdentificacioCompleta,
        cuentaConMembreteLaCabecera: body.cuentaConMembreteLaCabecera,
        elHemoDervidadoSeencuentraConLosDatosDelPaciente: body.elHemoDervidadoSeencuentraConLosDatosDelPaciente,
        lasSolucionesDeDotacionDeDietaEstanIdentificada: body.lasSolucionesDeDotacionDeDietaEstanIdentificada,
        cuentaConAlgunaTipoDeSondaOdrenage: body.cuentaConAlgunaTipoDeSondaOdrenage,
        laSondaODrenageSeEncuntraConLosDatosDelPaciente: body.laSondaODrenageSeEncuntraConLosDatosDelPaciente,
        solicitudesDeEstudio: body.solicitudesDeEstudio,
        estado: body.estado,
        tipoConsulta: body.tipoConsulta,
        idPaciente: body.idPaciente,
        excedentes: body.excedentes,
        montoAcumulado: body.montoAcumulado,
        abono: body.abono,
    })

    await newSchemaHospitalizacion
        .save()
        .then((data) => {
            return res.status(200)
                .json({
                    ok: true,
                    message: 'Se agrego paciente a hospitalizacion',
                    data
                })
        })

    .catch((error) => {
        console.log(error);
        return res.status(500)
            .json({
                ok: false,
                message: 'No se pudo hospitalizar',
                error
            })
    })



});


app.get('/ver/pacientes/hospitalizados', async(req, resp) => {


    await hospitalizacion
        .find({})
        .populate('idPaciente')
        .exec()
        .then((data) => {

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Pacientes hospitalizados',
                    data
                })
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'Hubo un error',
                    error
                })
        })


});




app.get('/paciente/hospitalizado/:id', async(req, resp) => {

    let id = req.params.id;

    // console.log(  id );

    await hospitalizacion
        .findById(id)
        .populate('idPaciente')
        .exec()
        .then(
            (data) => {
                return resp.json({
                    data
                })
            })
        .catch((error) => {
            console.log(error);
        })


});

// este servicio agrega un paquete de squirofano a un apciente ya exixtente
// para eso hace uso de que se le pase el id del paciente y el id del paquete de quirofano

app.put('/agregar/paquete/quirofano/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;

    hospitalizacion
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Paquete agregado',
                        data
                    })
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo agregar',
                        error
                    });
            }
        )


});


app.put('/agregar/excedentes/:id', (req, resp) => {

    let id = req.params.id;

    console.log(req.body.excedentes)

    hospitalizacion
        .findByIdAndUpdate({ _id: id }, { $push: req.body.excedentes })
        .exec()
        .then((data) => {

            // console.log(data);
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se agrego el excedente',
                    data
                })

        })
        .catch(
            error => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo agregar',
                        error
                    })
            }
        )

})

module.exports = app;