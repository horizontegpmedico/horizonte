const express = require('express');
// models
const consultaHistorico = require('../../models/consultasGeneralesBitacoras/consultaHistorial');
const serveResp = require('../../functions/functions');
const sedes = require('../../models/sedes/sedes');
const modelPersonal  = require('../../models/personal');
// app
let app = express();

// SE CREA EL SEGUIMIENTO DE LA CONSULTA

app.post('/consultas/general/identificacion',async (req, resp) => {


    const {body} = req;

    const newConsultaSeguimiento = new consultaHistorico({

        fechaIngreso: body.fechaIngreso,
        horaIngreso: body.horaIngreso,
        enfermeraAtendio: body.enfermeraAtendio,
        diagnosticos: body.diagnosticos,
        diagnosticoActual: body.diagnosticoActual,
        medicoTrante: body.medicoTrante,
        paciente: body.id,
        consultorio: body.consultorio,
        motivoIngreso: body.motivoIngreso,
        notaRecepcion: body.notaRecepcion,
        doctorAPasar: body.doctorAPasar,
        tipoDeServicio: body.servicio,
        turno: body.turno,
        // certificacion
        importtanciaIdentificacio: body.notificacionDeIdentificacion,
        membretesLegible: body.membretesLegible,
        solicitudesDeEstudioDeGabinete: body.solicitudesDeEstudioDeGabinete,
        solicitudesDeEstudio: body.solicitudesDeEstudio,
        sede: body.sede
    });

    try {
      const dataConssulta = await newConsultaSeguimiento.save();
      serveResp(dataConssulta, null, 'Se creo la consulta', resp);

    } catch (error) {
        
        console.log(error);
        serveResp(null, error, 'No se pudo guadar la consulta', resp)
    }
});

// SE FINALIZA LA FUNICON QUE AGREGA LA CONSULTA



app.get('/ver/consultas/:sede', async(req, resp) => {

    try {

        const { sede} = req.params;

        const dataSede = await sedes.find({ nomenclatura: sede });
        
            if(!dataSede){
            serveResp(null, 'Sede no valida', 'Sede invalida', resp);
        }


        const dataConsultas = await consultaHistorico.find({ sede}).populate('paciente').limit(56)
        .sort({_id:-1})
        serveResp(dataConsultas, null, 'Consultas encontradas', resp)

    } catch (error) {

        console.log(error);        
        serveResp(null, error, 'Algo fallo para ver las consultas', resp)
    }
});




app.get('/ver/consultas/pendietes/enfemeria', (req, resp) => {
    consultaHistorico
        .find({ status: 'En espera' })
        .populate('paciente')
        .exec()
        .then((data) => serveResp(data, null, 'Se encontro la bitacora de enfemeria', resp))
        .catch((error) => serveResp(null, error, 'No se encontraron consultas pendientes', resp));
});

app.get('/ver/consultas/pendietes/enfemeria/sede/:sede', (req, resp) => {
    const { sede} = req.params;
    consultaHistorico
        .find({ status: 'En espera', sede: sede })
        .populate('paciente')
        .exec()
        .then((data) => serveResp(data, null, 'Se encontro la bitacora de enfemeria', resp))
        .catch((error) => serveResp(null, error, 'No se encontraron consultas pendientes', resp));
});

// ESTA FUNCION NOS DEVULVE TODAS LAS CONSULTAS

app.get('/ver/consultas/identificacion', (req, resp) => {

    consultaHistorico
        .find()
        .populate('paciente')
        .exec()
        .then((data) => serveResp(data, null, 'Se encontraron consultas pendientes', resp))
        .catch((error) => serveResp(null, error, 'No se puede mostart', resp));

});


app.get('/ver/historia/consultas', (req, resp) => {
    consultaHistorico
        .find()
        .populate('paciente')
        .sort('consultaHistorico.fechaIngreso')
        .exec()
        .then((data) => serveResp(data, null, 'Se encontraron consultas pendientes', resp))
        .catch((error) => serveResp(null, error, 'No se puede mostrar', resp));
});

// FINALIZA LA FUNCION QUE NOS MUESTRAS TODAS LAS CONSULTAS


// INCIA LA CONSULTA QUE CAMBIA EL ESTADO

app.put('/actualizar/estado/consulta/:id', async (req, resp) => {


    try {
        const id = req.params.id;
        const body = req.body;
    
      const respUpdatedData =  await  consultaHistorico.findOneAndUpdate({ _id: id }, { status: body.status }, { new: true });       
        console.log( respUpdatedData )
      serveResp(respUpdatedData, null, 'Se actualizo el estado de la consulta', resp)
   
    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se pudo actualizar el estado de la consulta')
    }
    // console.log('Se actualizo el estado de la consulta a enfermeria');

});

// TERMINA LA CONSULTA QUE CAMBIA EL ESTADP

//actualiza la sede a todas las consultas 
app.get('/actualizar/sede/consluta', async(req, resp)=>{


    try {

        const consultaRegistered =  await consultaHistorico.find();

        
        consultaRegistered.forEach( async(element) => {
            
            if(element.sede == undefined){
              const sedeUpdated = await consultaHistorico.findByIdAndUpdate( element._id,  { sede:"TLYC01" },  {new: true});

            }
        });
        serveResp( consultaRegistered, null, 'Se encontro', resp);
    } catch (error) {
        console.log(error);
        serveResp( null, error, 'No se pudo conectar', resp);
    }

});


// ESTA FUNICON NOS RETORNA UNA CONSULTA POR EL ID


app.get('/consulta/general/identificacion/:id', (req, resp) => {

    let id = req.params.id;

    consultaHistorico
        .findById(id)
        .populate('paciente')
        .populate('idHojaEvolucion')
        .exec()
        .then((data) => serveResp(data, null, 'Se encontro la consulta', resp))
        .catch((error) => serveResp(null, error, 'No se pudo encontrar la consulta', resp));
});

// FINALIZA LA FUNCION QUE NOS RETORNA LA CONSULTA POR EL ID

// INICIA EL PROCESO DE LA HOSPITALIZACION


app.get('/ver/hospitalizacion', async(req, resp) => {

    await consultaHistorico
        .find({ status: 'hospitalizacion' })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Pacientes hospitalizados',
                    data
                })
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: true,
                    message: 'No se encontraron pacientes',
                    error
                });
        })
});



app.put('/ingreso/hospitalizacion/:id', async(req, resp) => {


    let id = req.params.id;
    let body = req.body;


    await consultaHistorico
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => serveResp(data, null, 'Se encontraron las consultass', resp))
        .catch((error) => serveResp(null, error, 'No se encontraon consultas', resp));

});

app.put('/agregar/receta/consulta/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;

    console.log(body);

    consultaHistorico
        .findById({ id }, { idRecetaMedica: body.idReceta })
        .exec()
        .then(data => serveResp(data, null, 'Se guardo la receta', resp))
        .catch(error => serveResp(null, error, 'no se pudo guadar', resp))
});



// INICIA EL CODIGO QUE GUARDA EN LA DB LOS INDICADORES 

app.put('/agregar/indicadores/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;


    consultaHistorico.findByIdAndUpdate({ _id: id }, {

            notificacionDeIdentificacion: body.notificacionDeIdentificacion1,
            membretesLegible: body.notificacionDeIdentificacion2,
            cuentaConMembreteLaCabecera: body.notificacionDeIdentificacion3,
            cuentaConBrazaleteDeIdentificacion: body.notificacionDeIdentificacion4,
            elMedicamentoDelPacienteCuantaConLaIdentificion: body.notificacionDeIdentificacion5,
            solucionIntravenosaCuentaConIdentificacioCompleta: body.notificacionDeIdentificacion6,
            elHemoDervidadoSeencuentraConLosDatosDelPaciente: body.notificacionDeIdentificacion7,
            solicitudesDeEstudioDeGabinete: body.notificacionDeIdentificacion8,
            solicitudesDeEstudio: body.notificacionDeIdentificacion9,
            lasSolucionesDeDotacionDeDietaEstanIdentificada: body.notificacionDeIdentificacion10,
            cuentaConAlgunaTipoDeSondaOdrenage: body.notificacionDeIdentificacion11,
            laSondaODrenageSeEncuntraConLosDatosDelPaciente: body.notificacionDeIdentificacion12,
            tipoConsulta: body.tipoConsulta,
            status: body.status,
        })
        .exec()
        .then((data) => serveResp(data, null, 'Se agregaron los datos', resp))
        .catch((error) => serveResp(null, error, 'No se pudierron agregar los datos', resp));
});


// este servicio nos actualiza los signos vitales dentro del modelo de consultas

app.put('/agregar/signos/consulta/:id', async(req, resp) => {

    let id = req.params.id;
    let body = req.body;

    console.log("SV", body);
    await consultaHistorico
        .findOneAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => serveResp(data, null, 'Se agregaron los signos', resp))
        .catch((error) => serveResp(null, error, 'No se pudo agregar los signos vitales', resp));

});

// TERMINA EL CODIGO QUE GUARDA EN LA DB LOS indicadores

// INICIAI EL PROCESO QUE ACTUALIZA EL MTIVO DE LA CONSULTA

app.put('/agregar/hoja/evolucion/modificado/:id', async(req, resp) => {
    let id = req.params.id;
    let body = req.body;

    await consultaHistorico
        .findByIdAndUpdate({ _id: id }, { motivoDeConsulta: body.motivoDeConsulta, evolucionDelPadecimiento: body.evolucionDelPadecimiento, medicoTrante: body.medicoTrante, diagnosticos: body.diagnosticos, idHistoriaClinica: body.idHistoriaClinica, status:body.status },{new:true})
        .exec()
        .then((data) => serveResp(data, null, 'Se agregaron los datos de la hoja de evolucion', resp))
        .catch((error) => serveResp(null, error, 'No se pudo guadar la iformacion', resp));
});

// TERMINA EL PROCESO QUE ACTUALIZA EL MTIVO DE LA CONSULTA


// Incia el codigo que lista los sv por paciente 

app.get('/ver/signos/paciente/:id', (req, resp) => {

    let id = req.params.id;

    consultaHistorico
        .find({ paciente: id }, 'talla peso sistolica diastolica imc fc fr temp pc pa pt apgar SaO pao glucosa fechaIngreso')
        .exec()
        .then((data) => serveResp(data, null, 'Se enontraron las consultas anteriores y sus SV', resp))
        .catch((error) => serveResp(null, error, 'No se encontraron mas consulta', resp));

});


app.post('/ver/consultas/medico', (req, resp) => {

    // let nombre = req.params.nombre 

    let body = req.body;

    consultaHistorico
        .find({ status: 'Medico', doctorAPasar: body.nombre })
        .populate('paciente')
        .exec()
        .then((data) => serveResp(data, null, 'Consultas pendientes', resp))
        .catch((error) => serveResp(null, error, 'No se econtraron consultas pendientes', resp))
});

app.put('/agregar/exploracion/fisica/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;

    consultaHistorico
        .findByIdAndUpdate({ _id: id }, {
            piel: body.piel,
            cabezaCuello: body.cabezaCuello,
            torax: body.torax,
            abdomen: body.abdomen,
            genitales: body.genitales,
            extremidades: body.extremidades,
            sistemaNervioso: body.sistemaNervioso
        })
        .exec()
        .then((data) => serveResp(data, null, 'Se agrego la exploracion fisica', resp))
        .catch((error) => serveResp(null, error, 'No se pudo guardar la exploracion fisica', resp))

});

// Termina el codigo que lista los sv por paciente 
app.put('/agregar/aparatos/sistemas/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;


    consultaHistorico
        .findByIdAndUpdate({ _id: id }, {
            respiratorioCardiovascular: body.respiratorioCardiovascular,
            digestivo: body.digestivo,
            endocrino: body.endocrino,
            musculoEsqueletico: body.musculoEsqueletico,
            genitourinario: body.genitourinario,
            hematopoyeticoLinfatico: body.hematopoyeticoLinfatico,
            pielAnexos: body.pielAnexos,
            neurologicoPsiquiatricos: body.neurologicoPsiquiatricos
        })
        .exec()
        .then((data) => serveResp(data, null, 'Se agrego la interrogacion por aparatos y sistemas', resp))
        .catch((error) => serveResp(null, error, 'No se pudo agregar la inerrogacion por aparatos y sistemass', resp));
});

//INICIA LA SECCION QUE AGREGA LA HOJA DE EVOLUCION A LA CONSULTA DEL PACIENTE
// ENVIAR EL ID DE LA CONSULTA

app.put('/agregar/hoja/evolucion/consulta/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;

    consultaHistorico
    // enviar de esta forma el id de la hoja de evolucion - idHojaEvolucion  
        .findByIdAndUpdate({ _id: id }, { idHojaEvolucion: body.idHojaEvolucion })
        .exec()
        .then((data) => serveResp(data, null, 'Se guardo la hoja de evolucion', resp))
        .catch((error) => serveResp(null, error, 'No se pudo guardar la hoja de evolucion', resp));
});


app.get('/ver/hoja/evolucion/paciente/:id', (req, resp) => {

    let id = req.params.id;
    // enviar el id de la consulta
    consultaHistorico
        .findByI({ _id: id })
        .exec()
        .then((data) => serveResp(data, null, 'Se encontro la hoja de evolucion', resp))
        .catch((error) => serveResp(null, error, 'no se pudo encontrar la hoja de evolucion', resp));
});


app.get('/ver/consultas/jefa/enfermeria', async (req, resp) => {

    try {
        
        const consultasHistorico = await consultaHistorico.find()
                                                            .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno')
                                                            .limit(56)
                                                            .sort({_id:-1})
        /* .sort({fechaIngreso: 1}) */
        serveResp(consultasHistorico, null, 'Se econtraron los datos', resp);
    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se encontrarons los datos', resp);
    }


});


//ruta para obtener las consultas de los medicos por el nombre del dr y la sede
app.post('/ver/historial/consultas/:id' ,async(req, resp)=> {

    try {
        
        const { id } = req.params;
        const { body } = req;

        const validMedic = await modelPersonal.findById( id );
        // console.log(  validMedic  )
        if( validMedic == null ) {

            serveResp( null, 'El usuario no tiene acceso', 'Usuario no valido', resp);            
        }else {
            if(validMedic.role == 'Admin' ||  validMedic.role == 'Medico' ) {
                const respConsultasHistorial = await consultaHistorico.find({doctorAPasar: body.medicoTrante}).populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno genero edad')
                // console.log( respConsultasHistorial )
                serveResp( respConsultasHistorial, null, 'Se econtraro las consultas', resp );
            }else {
                
                serveResp( null, 'El usuario no tiene acceso', 'Usuario no valido', resp);
                // console.log( respConsultasHistorial );
            }
        }



    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se pudo conectar', resp);
    }

});

module.exports = app;