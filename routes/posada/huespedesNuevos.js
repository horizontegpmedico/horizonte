const express = require('express');
const huesped = require('../../models/posada/nuevoHuesped');
const serveResp = require('../../functions/functions');
const app = express();

app.post("/huesped", (req, resp) => {
    const body = req.body;
    const huespedN = new huesped({
        nombre: body.nombre,
        apellidoPaterno: body.apellidoPaterno,
        apellidoMaterno: body.apellidoMaterno,
        genero: body.genero,
        edad: body.edad,
        telefono: body.telefono,
        correoHuesped: body.correoHuesped,
        domicilio: body.domicilio,
        claveIne: body.claveIne,
    });
    huespedN
    .save()
    .then((huesped) =>serveResp(huesped, null, "Se creo el huesped", resp))
    .catch((error) => console.log(error));
});

app.get("/obtener/huesped", (req, resp) => {
    huesped.find()
    .then((data) =>serveResp(data, null, "Se encontraron los pacientes", resp))
    .catch((error) =>serveResp(null, error, "No se encotraron pacientes", resp));
});

module.exports = app;

