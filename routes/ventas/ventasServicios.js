//libs
const express = require('express')

// models DB
const ventasServicios = require('../../models/ventasServicios/serviciosVentas');
const cancelVentas = require('../../models/ventasServicios/cancelVentas');
const sedes = require('../../models/sedes/sedes');
const {verificaToken} = require('../../middlewares/auth');
// functions
const serveResp = require('../../functions/functions');
const { logMemory } = require('../../config/logs');

// express app
const app = express();


app.post('/venta/servicios', async(req, resp) => {
    
    const {  body } = req;

    try {

        //sacamos el count de las ventas para crear el folio de las ventas y los tickets

        // const counter = await ventasServicios.find({sede: body.sede});
        // countLength = counter.length + 1;
        const counter = await ventasServicios.countDocuments();

        
        const newVentaServicios = new ventasServicios({

            paciente: body.paciente,
            vendedor: body.vendedor,
            fecha: body.fecha,
            hora: body.hora,
            estudios: body.estudios,
            efectivo: body.efectivo,
            montoEfectivo: body.montoEfectivo,
            tarjetCredito: body.tarjetCredito,
            montoTarjteCredito: body.montoTarjteCredito,
            tarjetaDebito: body.tarjetaDebito,
            montoTarjetaDebito: body.montoTarjetaDebito,
            doctorQueSolicito: body.doctorQueSolicito,
            transferencia: body.transferencia,
            montoTranferencia: body.montoTranferencia,
            facturacion: body.facturacion,
            factura: body.factura,
            totalCompra: body.totalCompra,
            sede: body.sede,
            status:body.status,
            compraConMembresia: body.compraConMembresia,
            recibe: body.recibe,
            folio : counter,
            montoAnticipo : body.montoAnticipo,
            ganancia: body.ganancia,
            gananciaPromedio: body.gananciaPromedio,
            socioSede: body.socioSede
        });
        
        const ventaSevd = await newVentaServicios.save();

        console.log(ventaSevd,"ventaSevd");
        
        const pacientFind = await ventasServicios.findById(ventaSevd._id).populate('paciente');

        console.log("Venta find", pacientFind)


        return resp.json({
            ok: true,
            message: 'Venta exitosa',
            data: pacientFind
        });

    } catch (error) {
        
        logMemory('/venta/servicios', false, error);
        console.error(error);
        serveResp(null, error, 'No se pudo crear', resp);
    }
});

//  2161

app.get('/ventas/all/pacientes', async ( req, resp) => {

    try {

    const ventas = await ventasServicios.find().populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno').populate('vendedor', 'nombre role')
     serveResp(ventas, null, 'Se encontron las ventas', resp);                       
    
    } catch (error) {
    console.log(error);
    serveResp(null, error, 'No se encontron las ventas', resp);
}

});

app.get('/ventas/por/usuario/:id', (req, resp) => {

    const { id } = req.params;

    ventasServicios
        .findById(id)
        .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las ventas', resp))
        .catch(error => serveResp(null, error, 'No se pudo encontrar', resp));
});


app.get('/app/ventas/usuario/:id', (req, resp) => {

    let id = req.params.id;

    ventasServicios
        .find({ paciente: id })
        .exec()
        .then(data => {
            serveResp(data, null, 'Historial de ventas', resp)
        })
        .catch(error => serveResp(null, error, 'No se encontraron ventas', resp));

});

// este servcio me devuelve las ventas para la hoja de orden de servicios

app.post('/ver/totalesventas/servicios', async(req, resp) => {



    const { fecha, sede } = req.body;

    ventasServicios
        .aggregate([
            {$match: {   "fecha": {'$eq' :  fecha}  }},
            {$group: {
                _id:"",
                efectivo : {$sum : '$montoEfectivo'},
                tarjetaCredito : { $sum: '$montoTarjteCredito' },
                tarjetaDebito: { $sum: '$montoTarjetaDebito' },
                transferencia: {$sum : "$montoTranferencia" },
            },
            
        },


        ])
        .exec()
        .then(
            (data) => {
                console.log(data);
                serveResp(data, null, 'Se encotraron las ventas', resp)
        })
        .catch(error => serveResp(null, error, 'No se encontraron ventas', resp));
});

app.post('/ver/ventas/fecha/sede', (req, resp) => {

    const { sede, fecha } = req.body;

    ventasServicios
        .find({ fecha, sede })
        .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las ventas', resp))
        .catch(error => serveResp(null, error, 'No se encontraron las ventas', resp));
});


app.put('/actualizar/estado/ventas/:id', (req, resp) => {

    const { id } = req.params;
    const { status } = req.body;

    ventasServicios
        .findOneAndUpdate({ id }, status)
        .exec()
        .then(data => serveResp(data, null, 'Se actualizo el estado', resp))
        .catch(error => serveResp(null, error, 'No se pudo actualizar el estado', resp));

});



app.post('/actualizar/estado/ticket/:id/:token', async(req, resp) => {

    const {  id }  = req.params;
    const { body } = req;


    // verificaToken(req, resp);

        try {   


            // if( req._id ) {
    
                const updateData = await ventasServicios.findOneAndUpdate({_id:id}, { status: 'CANCELADO', motivoCancelacio: body.motivo }, {new: true});        

                const dataSavedVentas = new cancelVentas({ 
                    idVenta: updateData._id,
                    idUser: req.id,
                })

               const dataUserCancel = await dataSavedVentas.save();
                serveResp(updateData, null, 'Se actulizo la informacion', resp);
    
            // }else {
            //     serveResp(null, 'No puedes cancelar el ticket', 'No tienes los permisos', resp);
            // }
            

        } catch (error) {
            console.log( error);
            serveResp(null, error, 'No se pudo conectar', resp);
        }

    

});


app.get('/ver/tickets/corte', async (req, resp) => {

    try {
        
        const ventasRealizadas = await ventasServicios.find()
                                        .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad')
                                         .populate('vendedor', 'nombre')
                                        .sort({ folio: -1 })
                                        .limit( 50 )


        serveResp( ventasRealizadas, null, 'Se encontraron las ventas', resp );

    } catch (error) {
        
        console.log( error);
        serveResp(null, error, 'No se pudo conectar', resp)
    }

});


app.post('/ver/ventas/por/fecha/sedes', async (req, resp) => {

    const { body}  = req;

    try {

       const dataDatesSells = await ventasServicios.find({
           fecha: {'$gte' :  body.fecha1 ,  '$lte':   body.fecha2    },
           socioSede: body.sede
        })
        .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero sede fechaNacimientoPaciente')
       

        
    serveResp( dataDatesSells, null, 'Se encontraron las ventas', resp );
        
    } catch (error) {
        console.log( error );
        serveResp(null, error , 'No se pudo conectar', resp);
    }


    
});


// funcion para la bitacora de las ganancias de los socios
app.get('/ver/ventas/pagadas/sedes/:sede', async (req, resp) => {
    try {
        const { sede } = req.params;

        // Verificar si la sede existe
        const validSede = await sedes.find({ nomenclatura: sede });

        if (validSede.length === 0) {
            serveResp(null, 'No existe esa sede', 'Ingresa una sede válida', resp);
            return; // Detener ejecución si no existe la sede
        }

        // Buscar ventas pagadas en la sede
        const ventasSedesValidate = await ventasServicios
            .find({ socioSede:sede, status: 'Pagado' })
            .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero sede curp')
            .sort({ fecha: 1 });

        if (ventasSedesValidate.length === 0) {
            serveResp(null, 'No se encontraron ventas pagadas para esta sede', 'Intenta con otra sede', resp);
        } else {
            serveResp(ventasSedesValidate, null, 'Ventas encontradas exitosamente', resp);
        }
    } catch (error) {
        console.error('Error en la solicitud:', error.message);
        serveResp(null, error.message, 'No se pudo procesar la solicitud', resp);
    }
});


// funcion para la bitacora de las ganancias de los socios
app.get('/ver/ventas/pagadas/desglose/sedes/:sede/:id', async (req, resp) => {
    try {
        const { sede, id } = req.params;
        const validSede = await sedes.find({ nomenclatura: sede });

        if (validSede.length === 0) {
            serveResp(null, 'No existe esa sede', 'Ingresa una sede válida', resp);
            return;
        }

        const ventasSedesValidate = await ventasServicios
            .find({ status: 'Pagado', _id: id })
            .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero sede curp')
            .sort({ fecha: 1 });

        if (ventasSedesValidate.length === 0) {
            serveResp(null, 'No se encontraron ventas pagadas con el ID proporcionado', 'Verifica los datos', resp);
            return;
        }

        serveResp(ventasSedesValidate, null, 'Se encontró la venta', resp);
    } catch (error) {
        console.error('Error en la solicitud:', error.message);
        serveResp(null, error.message, 'No se pudo procesar la solicitud', resp);
    }
});


app.get('/ver/pacientes/ventas/personal', async (req, resp) => {
    
    try{

        const ventas = await ventasServicios.find().populate('paciente').populate('vendedor').sort({fecha:-1});
        serveResp(ventas, null, 'Se encontraron las ventas', resp);

   }catch(error){
       console.log(error); 
       serveResp(null, error, 'No se encontraron las ventas', resp)
    }
});

app.get('/ver/pacientes/ventas/detalles/:id', async (req, resp) => {
    
    try{

        const {id} = req.params;

        const ventas = await ventasServicios.findById(id).populate('paciente').populate('vendedor').sort({fecha:-1});
        serveResp(ventas, null, 'Se actualizo el estado', resp);

   }catch(error){
       console.log(error); 
       serveResp(error, null, 'Hubo un error', resp);
    }
});

app.post("/busqueda/tickets", async(req, resp) => {
  try{
    const { folio } = req.body;
    if (folio == 0) {
        return resp.status(400)
            .json({
                ok: false,
                message: 'No se ha introducido un ticket'
            })
    }
    const ventasRealizadasFilter = await ventasServicios.find({folio:folio})
                                        .populate('paciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad')
                                        .populate('vendedor', 'nombre')
                                        .sort({ folio: -1 });
    serveResp(ventasRealizadasFilter, null , 'Se encontraron los tickets', resp);
  }catch ( error) {
    console.log( error);
    serveResp(null, error, 'No se econtraron pacientes', resp);
  }
});

module.exports = app;