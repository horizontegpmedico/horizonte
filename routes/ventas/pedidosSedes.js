const express = require('express');
const pedidosSedes = require('../../models/ventasServicios/pedidosSedes');
const serveResp = require('../../functions/functions')


const app = express();



app.get('/compras/sedes', (req, resp) => {

    pedidosSedes
        .find()
        .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los pedidos de las sedes', resp))
        .catch(error => serveResp(null, error, 'No se encontraron pedidos', resp));

});


app.get('/compras/pendientes/sedes', (req, resp) => {

    pedidosSedes
        .find({ status: 'Pendiente' })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los pedidos de las sedes', resp))
        .catch(error => serveResp(null, error, 'No se encontraron pedidos', resp));

});



app.post('/nueva/compra/sede', async  (req, resp) => {

    const { body } = req;

    const folio = await pedidosSedes.countDocuments();


    const nuevaCompra = new pedidosSedes({
        paciente: body.paciente,
        vendedor: body.vendedor,
        fecha: body.fecha,
        hora: body.hora,
        estudios: body.estudios,
        efectivo: body.efectivo,
        montoEfectivo: body.montoEfectivo,
        doctorQueSolicito: body.doctorQueSolicito,
        sede: body.sede,
        totalCompra: body.totalCompra,
        compraConMembresia: body.compraConMembresia,
        anticipo: body.anticipo,
        montoAnticipo: body.montoAnticipo,
        status: body.status,
        folio: folio,
        montoTarjteCredito:body.montoTarjteCredito,
        montoTarjetaDebito:body.montoTarjetaDebito,
        montoTranferencia:body.montoTranferencia,
        tarjetCredito:body.tarjetCredito,
        tarjetaDebito:body.tarjetaDebito,
        transferencia:body.transferencia,
        sinpago:body.sinpago,
        montoSinpago:body.montoSinpago,
        ganancias:body.ganancias
    });

    nuevaCompra
        .save()
        .then(data =>{
            console.log(data)
            serveResp(data, null, 'Se encontraron los pedidos de las sedes', resp)
        })
        .catch(error => serveResp(null, error, 'No se encontraron pedidos', resp));

});

app.put('/actualizar/estado/compra/:id', (req, resp) => {



    const id = req.params.id;
    const body = req.body;
    console.log(body);

    pedidosSedes
        .findByIdAndUpdate(id, body)
        .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se actualizo el estado', resp))
        .catch(error => serveResp(null, error, 'No se pudo actualizar el estado', resp));

});

app.get('/ver/pedidos/pagados/:sede', (req, resp) => {
    const { sede } = req.params;

    pedidosSedes
        .find({ sede, status: 'Pagado' })
        .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los pagos', resp))
        .catch(error => serveResp(null, error, 'No se encotraron pagos', resp));
});



app.post('/ver/pedidos/pagados/fecha/sede', (req, resp) => {

    console.log(req.body);
    const { fecha1, fecha2, sede } = req.body

    // console.log(sede);
    pedidosSedes
        .find({
            status: 'Pagado',
            sede: sede,
            fecha: {
                $gte: (fecha1),
                $lte: (fecha2)
            }
        })

    .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los pagos :P', resp))
        .catch(error => serveResp(null, error, 'No se encotraron pagos', resp));
});


app.get('/ver/pedido/sede/:id', (req, resp) => {



    const { id } = req.params;
    pedidosSedes
        .findById(id)
        .populate('paciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontró el pedido', resp))
        .catch(error => serveResp(null, error, 'No encontro el pedido', resp))

});


module.exports = app;