const endoscopiaPedido = require("../../models/endoscopias/endoscopias");
const endoscopiaregresos = require("../../models/endoscopias/regresosEndoscopias");

const express = require("express");
const serveResp = require("../../functions/functions");
const app = express();

// creacion de los pedidos que se hacen por medio de las ventas de los servicios
app.post("/agregar/pedido/endoscopia", async (req, resp) => {
  const {body} = req;

  try {
    const pedidoToSave = new endoscopiaPedido(body);
    const pedidoSaved = await pedidoToSave.save();

    serveResp(pedidoSaved, null, "Se guardo el pedido", resp);
  } catch (error) {
    console.error(error);
    serveResp(null, error, "Algo paso", resp);
  }
});

//editar pedido endoscopia
app.put("/ver/pedido/endoscopia/:id", async (req, res) => {
  const pedidoId = req.params.id;
  const body = req.body;
  try {
    const pedidoEdit = await endoscopiaPedido
      .findByIdAndUpdate(pedidoId, body)
      .exec();
    serveResp(pedidoEdit, null, "Se actualizó con éxito", res);
  } catch (error) {
    console.error(error);
    serveResp(null, error, "Algo paso", resp);
  }
});

//eliminar pedido endoscopia
app.delete("/ver/pedido/endoscopia/:id", async (req, res) => {
  const pedidoId = req.params.id;

  try {
    const pedidoExiste = await endoscopiaPedido.findById(pedidoId);

    if (pedidoExiste) {
      const pedidoDelete = await endoscopiaPedido.findByIdAndDelete(pedidoId);
      serveResp(pedidoDelete, null, "Se eliminó el pedido", res);
    }
  } catch (error) {
    serveResp(null, error, "Algo pasó", res);
  }
});
//obtenmos los pedidos que estan pendientes

app.get("/ver/pedidos/:status", async (req, resp) => {
  const {status} = req.params;

  if (status == "PEDIDO" || status == "FINALIZADO") {
    try {
      const pedidosSaved = await endoscopiaPedido
        .find({status})
        .populate(
          "idPaciente",
          "nombrePaciente apellidoPaterno apellidoMaterno edad genero"
        )
        .sort({fechaDePedidoDeLosExamenes: 1});

      serveResp(pedidosSaved, null, "Se encontraron los pedidos", resp);
    } catch (error) {
      console.error(error);
      serveResp(null, error, "Algo paso", resp);
    }
  } else {
    serveResp(null, "No es valido el status", "Status no valido", resp);
  }
});

app.get("/ver/pedido/:id", async (req, resp) => {
  try {
    const {id} = req.params;

    const pedidosFinded = await endoscopiaPedido
      .findById(id)
      .populate(
        "idPaciente",
        "nombrePaciente apellidoPaterno apellidoMaterno edad genero curp telefono fechaNacimientoPaciente"
      );

    if (pedidosFinded) {
      serveResp(pedidosFinded, null, "Se encontro el pedido", resp);
    } else {
      const errorResp = "El id no existe";
      serveResp(null, errorResp, "No encontró el ID", resp);
    }
  } catch (error) {
    console.error(error);
    serveResp(null, error, "Algo paso", resp);
  }
});

app.post("/agregar/regresos/endoscopia", async (req, resp) => {
  const {body} = req;

  try {
    const regresosEndoscopiaSaved = new endoscopiaregresos(body);
    const regresos = await regresosEndoscopiaSaved.save();

    if (!regresos) {
      serveResp(null, "No se pudo guardar", "No se pudo guardar", resp);
    }

    const endoscopiaStatus = await endoscopiaPedido.findByIdAndUpdate(
      body.idPedido,
      {status: "FINALIZADO", idObtenido: regresosEndoscopiaSaved},
      {new: true}
    );

    serveResp(endoscopiaStatus, null, "Se creo el regreso", resp);
  } catch (error) {
    console.error(error);
    serveResp(null, error, "No se pudo conectar", resp);
  }
});

//editar regresos endoscopia
app.put("/ver/regresos/endoscopia/:id", async (req, res) => {
  const regresoId = req.params.id;
  const body = req.body;
  try {
    const regresoEdit = await endoscopiaregresos
      .findByIdAndUpdate(regresoId, body)
      .exec();
    serveResp(regresoEdit, null, "Se actualizó con éxito", res);
  } catch (error) {
    console.error(error);
    serveResp(null, error, "Algo paso", resp);
  }
});

//eliminar regresos endoscopia
app.delete("/ver/regresos/endoscopia/:id", async (req, res) => {
  const regresoId = req.params.id;

  try {
    const regresoExiste = await endoscopiaregresos.findById(regresoId);

    if (regresoExiste) {
      const regresoDelete = await endoscopiaregresos.findByIdAndDelete(
        regresoId
      );
      serveResp(regresoDelete, null, "Se eliminó el regreso", res);
    }
  } catch (error) {
    serveResp(null, error, "Algo pasó", res);
  }
});

app.get("/ver/obtenidos/endoscopia/:id", async (req, resp) => {
  const {id} = req.params;

  try {
    if (id == "") {
      serveResp(null, "No es in ID valido", "No es un ID valido", resp);
    }

    const dataObtenido = await endoscopiaPedido
      .findById(id)
      .populate(
        "idPaciente",
        "nombrePaciente apellidoPaterno apellidoMaterno edad genero curp telefono fechaNacimientoPaciente religion"
      )
      .populate("idObtenido");

    if (!dataObtenido) {
      serveResp(null, "No se enoctro el regreso", "No es un ID valido", resp);
    }

    serveResp(dataObtenido, null, "Se encontró el regreso", resp);
  } catch (error) {
    console.error(error);
    serveResp(null, error, "No se pudo ver", resp);
  }
});
module.exports = app;
