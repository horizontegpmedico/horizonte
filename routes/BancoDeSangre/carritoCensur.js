const express = require('express');
const carritoCensur = require('../../models/almacen/stock/carritoCensur');
const BancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const serveResp = require('../../functions/functions');
const app = express();


// ruta del carrito 

// creaccion del carrito
app.post('/agergar/carrito/censur', async(req, resp) => {


    const { body } = req;


    try {

        const counter = await carritoCensur.countDocuments();

        const carritoSaved = new carritoCensur({
            idBanco: body.idBanco,
            product: body.idPaciente,
            total: body.total,
            fechaDeCreacion: body.fechaDeCreacion,
            TarjetaCredito: body.TarjetaCredito,
            TarjetaDebito: body.TarjetaDebito,
            Efectivo: body.Efectivo,
            Transferencia: body.Transferencia,
            horaDeCreacion: body.horaDeCreacion,
            montoCredito: body.montoCredito,
            montoDebito: body.montoDebito,
            montoEfectivo: body.montoEfectivo,
            montoTranseferencia: body.montoTranseferencia,
            quienRecibe: body.quienRecibe,
            sede: body.sede,
            vendedor: body.vendedor,
            folio: counter + 1
        });
        const newCarrito = await carritoSaved.save()
        const prueba = await BancoSangreSocios
            .findOneAndUpdate({ '_id': body.idBanco }, {
                '$set': {
                    "ventaCensur": newCarrito._id
                }
            }).exec()

        /* console.log(prueba); */
        return resp.json({
            ok: true,
            message: 'Venta realizada',
            folio: counter + 1,
            data: newCarrito
        });

    } catch (error) {
        console.error(error);
        serveResp(null, error, 'No se pudo conectar', resp);
    }

});


app.get('/ver/carritos/cesnur', async(req, resp) => {

    try {
        const carritosResp = await carritoCensur.find();
        serveResp(carritosResp, null, 'Se encotraron los carritos', resp);

    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se encontraron', resp);
    }
});


app.get('/carrito/censur/unico/:id', async(req, resp) => {
    const { id } = req.params;

    try {

        const carritoResp = await carritoCensur.findById(id);

        serveResp(carritoResp, null, 'Se encontró el carrito', resp);
    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se encontró', resp);
    }
});

app.delete('/eliminar/carrito/censur/:id', async(req, resp) => {
    const { id } = req.params;

    try {
        const deletedCarrito = await carritoCensur.findByIdAndRemove(id);
        serveResp(deletedCarrito, null, 'Se elimno el carrito', resp);
    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se pudo eliminar', resp)
    }
});

app.put('/actualizar/carrito/products/:id', async(req, resp) => {

    const { id } = req.params;
    const { body } = req;

    try {

        const updatedCarrito = await carritoCensur.findByIdAndUpdate({ _id: id }, body);
        serveResp(updatedCarrito, null, 'Se actualizo el carrito', resp);

    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se pudo actualizar el carrito', resp)
    }

});

module.exports = app;