const proveedoresBS = require('../../models/almacen/proveedores/porveedorModel');

const express = require('express');
const serveResp = require('../../functions/functions');
const app = express();

// creracio de proveedores nuevos 
app.post('/registro/proveedor', async(req, resp) => {
    const {  body } = req;
    try {
        const proveedorSaved = new proveedoresBS(body);
        const proveedorSavedDb = await proveedorSaved.save();
        serveResp(proveedorSavedDb, null, 'Se creo el proveedor', resp);
    } catch (error) {
        serveResp(null, error, 'Hubo un error'+error, resp );
    }
});

app.get('/ver/proveedores', async(req, resp) => {
    try {
        const proveedoresDB = await proveedoresBS.find({ status: 'ACTIVO' }).sort({_id:-1});
        serveResp(proveedoresDB, null, 'Se encontraron los proveedores', resp);
    } catch (error) {
        serveResp(null, error, 'Hubo un error', resp);
    }
});

app.get("/ver/proveedor/por/id/:id", async(req, resp)=> {
    const {id} = req.params;
    try {
        const proveedor = await proveedoresBS.findById( id);
        serveResp(proveedor, null, 'Se encontro el proveedor', resp);

    } catch (error) {
        serveResp( null, error, 'Hubo un error', resp);
    }
});

app.put('/actualizar/proveedor/:id', async (req, resp) => {
    const  { id } = req.params;  
    const { body } = req;
    const proveedor = await proveedoresBS.findById( id );
    if(!proveedor) {
        serveResp(null, 'El proveedor no existe', 'Proveedor no encontardo', resp);
    }else {
        try {  
            const proveedorUpdated = await proveedoresBS.findByIdAndUpdate( id, body, { new: true } );
            serveResp( proveedorUpdated, null,'Producto actulizado', resp);
        } catch (error) {
            serveResp(null, error, 'No se pudo actualizar', resp);
        }
    }
});

app.get("/desactivar/proveedor/por/id/:id", async(req, resp)=> {
    const {id} = req.params;
    try {
        await proveedoresBS.findOneAndUpdate({_id: id},  {status: 'INACTIVO'}, { new: true });
        const proveedoresDB = await proveedoresBS.find({ status: 'ACTIVO' }).sort({_id:-1});
        serveResp(proveedoresDB, null, 'Se elimino el proveedor', resp);

    } catch (error) {
        serveResp( null, error, 'Hubo un error', resp);
    }
});

module.exports = app;