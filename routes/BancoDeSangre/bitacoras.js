const express = require('express')
const app = express()
//modelos requeridos
const paciente = require('../../models/nuevoPaciente')
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const bancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const laboratorio = require('../../models/bancoDeSangre/modelLaboratorio')
const almacen =require('../../models/almacen/modelminiAlmacen')
//funciones de terceros
const serveResp = require('../../functions/functions')
//bitacora de enfermeria
app.get('/bitacora/enfermeria', (req, resp)=>{
    bancoDeSangre
    .find({proceso: 'enfermeria'})
    .populate('paciente')
    .populate('cuestionarioautoexclusion')
    .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
    .catch((error)   => serveResp(null,error, 'No se puede encontro el disponente', resp));
})
//bitacora de doctor
app.get('/bitacora/doctor', (req, resp)=>{
    bancoDeSangre
    .find({proceso: 'doctor'})
    .populate('paciente')
    .populate('cuestionarioautoexclusion')
    .populate('signosvitales')
    .populate('laboratorio')
    .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
    .catch((error)   => serveResp(null,error, 'No se puede encontro el disponente', resp));
})
//bitacora de enfermeria
app.get('/bitacora/flebotomia', (req, resp)=>{
    bancoDeSangre
    .find({proceso: 'FLEBOTOMIA'})
    .populate('paciente')
    .populate('cuestionarioautoexclusion')
    .populate('signosvitales')
    .populate('laboratorio')
    .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
    .catch((error)   => serveResp(null,error, 'No se puede encontro el disponente', resp));
})

//bitacora de laboratorio
app.get('/bitacora/laboratorio', (req, resp)=>{
    bancoDeSangre
    .find()
    .populate('paciente')
    .then((data) =>{
        /* console.log(data); */
        let data2 = Array()
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            if (element.paciente !== undefined && element.laboratorio !== undefined && element.proceso === 'DOCTOR' || element.proceso === 'SEROLOGIA') {
                 data2.push(element)
            }
            
        }
        serveResp(data2, null, 'Se encontraron los disponentes del banco', resp)
             
    })
    .catch((error)  => serveResp(null,error, 'No se puede encontro el disponente', resp));
})

//bitacora de almacen
app.get('/bitacora/almacen', (req, resp)=>{
    bancoDeSangre
    .find()
    .populate('paciente')
    .populate('almacen')
    .then((data) => {
/*         let data2 = Array()
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            if (element.paciente !== undefined && element.almacen !== undefined) {
                 data2.push(element)
            }
            
        }
        serveResp(data2, null, 'Se encontraron los disponentes del banco', resp) */
        serveResp(data, null, 'Se encontraron los disponentes del banco', resp)
    })
    .catch((error)   => serveResp(null,error, 'No se puede encontro el disponente', resp));
})

module.exports = app