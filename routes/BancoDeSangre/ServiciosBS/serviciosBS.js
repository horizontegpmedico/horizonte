// libs
const express = require('express');
const serveResp = require('../../../functions/functions');
const serviciosCensur = require('../../../models/almacen/modelServicios');
const app = express();


// creamos un nuevo serivicios en Censur
app.post('/agregar/servicio/censur', async (req, resp) => {
    
    const { body } = req;
console.log(body);
    try {
        const newServiceCensur = new serviciosCensur(body);
        const serviceSaved = await newServiceCensur.save();
        
        serveResp(serviceSaved, null, 'Se creo el servicio' , resp);
    } catch (error) {

        console.error(error);
        serveResp(null, error, 'No se pudo crear el servicio');
    }

});

// obtenemos todos los servicios que hay en censur para la bitacora 
app.get('/ver/servicios/censur', async(req, resp) => {
    
    try {
        const data = await serviciosCensur.find();
        serveResp(data, null, 'Se encontaron los servicios', resp);
    } catch (error) {
        console.error(error);
    }
})

// obtenemos el counter de los documentos registrados de los serivicios 

app.get('/ver/folio/servicios', async (req, resp) => {
    
    try {
    
        const conuterServices = await serviciosCensur.countDocuments();
        serveResp(conuterServices, null, 'Se encontro el folio', resp);
    
    } catch (error) {
            console.error(error);
            serveResp(null, error, 'No se pudo encontrar el folio' , resp);
    }
});


app.get('/ver/servicio/banco/sangre/:id', async(req, resp)=> {

        const { id } = req.params;

        try {
            const serviciosDb = await serviciosCensur.find( {_id:id, status: 'ACTIVADO'} );
            console.log(serviciosDb);
            serveResp( serviciosDb, null, 'Se encontro el servicio', resp);
        } catch (error) {
            console.error(error);
            serveResp(null, error, 'No se encontro el servicio', resp);
        }
});

module.exports = app;
