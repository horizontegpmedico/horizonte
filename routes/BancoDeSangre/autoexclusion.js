const express = require('express');

//modelos requeridos
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre');
const cuestioExclusion = require('../../models/bancoDeSangre/modelCuestioExclusion');
const serveResp = require('../../functions/functions');

const app = express()
//se agregar el cuestionario de autoexclusion junto con el id del banco de sangre del disponente
app.post('/cuestionario/autoexclusion', (req,resp)=>{
    const body = req.body
    const autoexclusion = new cuestioExclusion({
        cuestionarioautoexclusion: body.idbancosangre,
        autoexclusion0: body.preguntacero,
        autoexclusion1: body.preguntauno,
        autoexclusion2: body.preguntados,
        autoexclusion3: body.preguntatres,
        autoexclusion4: body.preguntacuatro,
        autoexclusion5: body.preguntacinco,
        autoexclusion6: body.preguntaseis,
        autoexclusion7: body.preguntasiete,
        autoexclusion8: body.preguntaocho,
        autoexclusion9: body.preguntanueve,
        autoexclusion10: body.preguntadiez,
        autoexclusion11: body.preguntaonce,
        autoexclusion12: body.preguntadoce,
    })
    autoexclusion
    .save()
    .then((data) => 
    {
        if (data) {
            const cuestionarioautoexclusion = {
             cuestionarioautoexclusion: data._id
            }
            bancoDeSangre
            .findByIdAndUpdate(body.idbancosangre, { $push: cuestionarioautoexclusion })
            .exec()
            .then(data => serveResp(data, null, 'Se agrego el cuestionario de autoexclusion al banco de sangre del disponente', resp))
            .catch(error => serveResp(null, error, 'No se agrego el cuestionario de autoexclusion al banco de sangre del disponente', resp));
        } else {
            serveResp(data, null, 'El cuestionario de autoexclusion se guardo correctamente', resp)
        }
    })
    .catch((error)   => serveResp(null,error, 'No se pudo guardar el cuestionario de autoexclusion', resp));
})

module.exports = app;