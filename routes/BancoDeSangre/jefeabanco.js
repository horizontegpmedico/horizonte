const express = require('express')
const app = express()
    //modelos requeridos
const paciente = require('../../models/nuevoPaciente')
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const BancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const productosAlmacenStock = require('../../models/almacen/stock/modelProductosStock')
    //funciones de terceros
const serveResp = require('../../functions/functions')


//obtiene historico de banco de un disponente 
app.get('/gethistorico/:id', async(req, resp) => {
    let id = req.params.id
    /* console.log(id) */

    try{
        const historico = await paciente.findById(id).populate('modelBancoSangre')
        for (let a = 0; a < historico[0].modelBancoSangre.length; a++) {
            const element = historico[0].modelBancoSangre
            bancoDeSangre
            
            .findById(element._id)
            .populate('paciente')
            .populate('cuestionarioautoexclusion')
            .populate('signosvitales')
            .populate('historiaclinica')
            .populate('hojaevolucion')
            .populate('reaccionesderivadas')
            .populate('flebotomia')
            .populate('laboratorio')
            .populate('almacen')
            .then((data) => serveResp(data, null, 'Se encontro el disponente del banco', resp))
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp));
        }

    }catch(error){
        serveResp(null, error, 'No se puede encontro el disponente', resp)
    }    
})




module.exports = app;