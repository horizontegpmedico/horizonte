//libs
const express = require('express')
const QRcode = require('qrcode');
const app = express();

//modelos requeridos
const paciente = require('../../models/nuevoPaciente')
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const BancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const laboratorio = require('../../models/bancoDeSangre/modelLaboratorio')
const EstudiosLabBS = require('../../models/bancoDeSangre/modelEstudios')
const productosAlmacenStock = require('../../models/almacen/stock/modelProductosStock');
//modelo de los laboratorios

//Funcion de terceros
const serveResp = require('../../functions/functions')

// variables
const id_lab =  ''
const data2 = Array()
//se crea un nuevo estudio de laoratorio
app.post('/nuevo/estudio/banco', (req, resp) => {

    const body = req.body

    const estudios = new EstudiosLabBS({

        nombre: body.nombreEstudio,
        tipo_de_examen: body.tipoExamen,
        valoresDeReferencia: body.valores,
        observaciones : body.observaciones

        
    })
    estudios
    .save()
    .then((data) => serveResp(data, null, 'Se guardaron los estudios correctamente', resp))
    .catch((error) => serveResp(null, error, 'No se guardaron los estudios' + error, resp));
})

// Se obtiene un estudio de laboratorio por el id del mismo
app.get('/obtener/estudio/banco/:id', async (req, resp) => {

    const { id} = req.params;

    try{
        const estudio = await EstudiosLabBS.findById(id);
        serveResp(estudio, null, 'Se encontaron los estudios', resp);
    }catch(err){
        serveResp(err, null, 'No se encontro el estudio', resp)
    }


});

//actualizar valores de referencia
app.put('/actualizar/estudios',( req, resp) => {
    const body = req.body
    estudios
    .updateOne({'_id':body.idestudios},{$set: body})
    .then((data) => {
        serveResp(data, null, 'Se actualizaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se actualizaron los estudios' + error, resp) 
    });

})

//cambiar el proceso del laboratorio estatus hoja de servicios
app.put('/proceso/laboratorio',( req, resp) => {
        const body = req.body;
    laboratorio
    .findOneAndUpdate({'laboratorios':body.idbancodesangre},
    {'$set':{
        "proceso":body.proceso
    }
    })
    .exec()
    .then(data => serveResp(data, null, 'Se modifico el proceso de laboratorio', resp))
    .catch(error => serveResp(null, error, 'No se modifico el proceso de laboratorio', resp));
})

//todos los estudios vista historico de estudios
app.get('/todos/estudio', (req, resp) => {
    const body = req.body
    EstudiosLabBS
    .find()
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})

//analisis de estudios biometria hematica completa  obtenidos: body.obtenidos[0],
app.post('/analisis/estudios',( req, resp) => {
     const body = req.body;

     const laboratorios = new laboratorio({
                 laboratorio: body.idbancodesangre,
                 obtenidos: body.obtenidos,
                 pedido: body.pedido,
                 metodo: body.metodo,
                 quimico: body.quimico,
                 tecnico: body.tecnico,
                 cedula: body.cedula,
                 proceso: body.proceso,
            })
            laboratorios
            .save()
            .then((data) => {
                /* console.log('resultado del guardar datos',data) */

                const laboratorios = { 
                    laboratorio: data._id 
                    }
                    
                bancoDeSangre
                .findByIdAndUpdate(body.idbancodesangre, { $push: laboratorios })
                .then(async (data) => {
                    /* console.log('asignacion lab a banco',data) */
                    const bslab = await bancoDeSangre.find({'_id':body.idbancodesangre}).populate('laboratorio')
                    /* console.log(data) */
                    serveResp(bslab, null, 'Se guardaron los laboratorios', resp)
                }).catch((error) => {
                    serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
                });
            }).catch((error) => {
                serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
            });

})

//analisis de grupo y rh
app.post('/analisis/grupo',( req, resp) => {
     const {body} = req;
    const data2 = []
        const laboratorios = new laboratorio({
                 laboratorio: body.idbancodesangre,
                 obtenidos: body.grupo,
                 pedido: body.pedido,
                 metodo: body.metodo,
                 quimico: body.quimico,
                 tecnico: body.tecnico,
                 cedula: body.cedula,
                 proceso: body.proceso,
        })
        laboratorios
        .save()
        .then((data) => {
            data2.push(data)
           const laboratorio = { 
                    laboratorio: data._id 
                    }
            bancoDeSangre
            .findByIdAndUpdate(body.idbancodesangre, { $push: laboratorio })
            .then(async (data) => {
                console.log('asignacion de lab GR a banco',data)
                data2.push(data)
                const GR = await paciente
                .findByIdAndUpdate(body.idpaciente,{$set: {tipoDeSangre:body.grupo}})
                .exec()
                data2.push(GR)
                const bdlab = await bancoDeSangre.find({'_id':body.idbancodesangre}).populate('laboratorio')
                serveResp(bdlab, null, 'Se guardaron los estudios correctamente en el banco de sangre', resp)
            }).catch((error) => {
                console.log(error)
                serveResp(null, error, 'No se asigno el id del laboratorio al banco de sangre' + error, resp)
            });
        }).catch((error) => {
            serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
        });
})

//analisis de estudio serologia
app.post('/analisis/serologia', async( req, resp) => {
   
        const {body} = req;
        console.log(body, "req")
        const data2 = [];
     const laboratorios = new laboratorio({
                 laboratorio: body.idbancodesangre,
                 obtenidos: body.obtenidos[0],
                 pedido: ['Serologia'],
                 metodo: body.metodo,
                 quimico: body.quimico,
                 tecnico: body.tecnico,
                 cedula: body.cedula,
                 proceso: body.proceso,
            })

       try {
       const laboratoioId =  await laboratorios.save();
       const bacoUpdate = await bancoDeSangre.findByIdAndUpdate(body.idbancodesangre ,{$push: laboratoioId._id });
         for (let i = 0; i < body.productos.length; i++) {
            if (body.productos[i] != undefined) {
                const productosGuardados = new productosAlmacenStock({
                    productosFI: body.productos[i].productosFI,
                    lote: body.productos[i].lote,
                    proveedor: body.productos[i].proveedor,
                    factura: body.productos[i].factura,
                    fecha_caducidad: body.productos[i].fecha_caducidad,
                    lote_unitario: body.productos[i].lote_unitario,
                    costoReal: body.productos[i].costoReal,
                    precioVenta:body.productos[i].precioVenta,
                    seDesecha:body.productos[i].seDesecha,
                    motivoDesecho:body.productos[i].motivoDesecho,
                    procesoDesecha:body.productos[i].procesoDesecha,
                });
                const productosEnStock = await productosGuardados.save();
                const idproductos = productosEnStock._id;
                const qr = await QRcode.toDataURL("https://backendpruebashorizonte.herokuapp.com/vender/producto/stock/"+idproductos);
                const productosConQr = await productosAlmacenStock.findOneAndUpdate({'_id':idproductos}, {codeQr_img: qr}, {new: true} );
                data2.push( qr );
                const almacen = { almacen: idproductos }
                const alm = await bancoDeSangre.findByIdAndUpdate(body.idbancodesangre,{ $push: almacen });
            } else {
                
            }
 
         }
            serveResp( data2, null, "Se guardo el producto" , resp);
       } catch (error) {
           console.log(error);
           serveResp(null, error, "No se pudo crear", resp);
       }



        
})

//asignar estudios a laboratorio tipe y cruce
app.post('/analisis/tipeycruce',( req, resp) => {
    const {body} = req;
    const data2 = [];
    const laboratorios = new laboratorio({
                 tiparycruzar: body.idCensur,
                 obtenidos: body.obtenidos,
                 pedido: body.pedido,
                 metodo: body.metodo,
                 quimico: body.quimico,
                 tecnico: body.tecnico,
                 cedula: body.cedula,
                 proceso: body.proceso,
            })
        laboratorios
        .save()
        .then(async (data) => {
           const tiparycruzar = { 
                    tiparycruzar: data._id 
                }
            const tipe = await BancoSangreSocios.findByIdAndUpdate(body.idCensur, { $push: tiparycruzar })
            serveResp(tipe, null, 'Se guardaron los estudios correctamente en el banco de sangre', resp)
        }).catch((error) => {
            serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
        });
})
 

//pedido de laboratorio por el id
app.post('/pedido/laboratorio/:id', (req, resp) => {
    let id = req.params.id
    laboratorio
    .findById({'laboratorio':id})
    .populate('paciente')
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})

// Se obtiene la tabla de banco de sangre
app.get('/obtener/banco/banco/:id', async (req, resp) => {

    const { id } = req.params;

    try{
        const data = await bancoDeSangre
        .findById(id)
        .populate('paciente');
        
        serveResp(data, null, 'Se encontraron los datos', resp);
    }catch(error) {
        serveResp(null, error, 'No se encontraron los datos', resp);
    }


});

//hoja de servicio 
app.get('/hoja/servicio/laboratorio/:id', (req, resp) => {
    let id = req.params.id
    bancoDeSangre
    .findById({'_id' : id })
    .populate('paciente')
    .populate('laboratorio')
    .populate('flebotomia')
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})

//obtener estudios por idbanco de sangre 
app.post('/laboratorios/doctor', (req, resp) => {
    const body = req.body
    laboratorio
    .find({'laboratorio': body.idbancodesangre})
    .populate('resultados')
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})
//obtener el estatus de los estudios 
app.post('/laboratorios/servicios/proceso', (req, resp) => {
    const body = req.body
    laboratorio
    .find({'laboratorio': body.idbancodesangre})
    .populate('resultados')
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})
//actualizar los analisis de biometria por el doctor
app.put('/actualizar/analisis/estudios', (req, resp) => {
    const body = req.body;
    laboratorio
    .findOneAndUpdate({ '_id' : body.idlaboratorio },
    {
        '$set': { "obtenido" : body.obtenidos }
    })
    .exec()
    .then(data => serveResp(data, null, 'Se actualizaron la biometria hematica', resp))
    .catch(error => serveResp(null, error, 'No se pudo actualizar actualizar la biometria hematica', resp));
});
//obtener estudios por idbanco de sangre 
app.post('/pedido/laboratorio', (req, resp) => {
    const body = req.body
    laboratorio
    .find({'laboratorio': body.idbancodesangre})
    .then((data) => {
        serveResp(data, null, 'Se guardaron los estudios correctamente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se guardaron los estudios' + error, resp)
    });
})


app.get('/lab', (req, resp)=>{
    bancoDeSangre
    .find()
    .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
    .catch((error)   => {
        console.log(error)
        serveResp(null,error, 'No se puede encontro el disponente', resp)
    });
});

module.exports = app




//id estudios : 60cd1a33dcee51336c5f6c86 , 60cd1c165325e54d3c712e58
