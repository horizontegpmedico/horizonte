const express = require('express');

const paciente = require('../../models/nuevoPaciente');
const BancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const BancoSangreSociosProcesos = require('../../models/bancoDeSangre/modelBancoSangreTerceroProcesos')
const documentosCensur = require('../../models/bancoDeSangre/modelDocumentos')
const pedidosSedesBanco = require('../../models/bancoDeSangre/modelPedidosBanco')
const tipeyCruce = require('../../models/bancoDeSangre/modelTipeyCruce')
const resultados = require('../../models/bancoDeSangre/modelResultadoLaboratorio')
//const almacenProductos = require('../../models/bancoDeSangre/modelProductos')
const almacenProductos = require('../../models/almacen/modelProductos')
const productosAlmacenStock = require('../../models/almacen/stock/modelProductosStock');
const documentosReaccionesReceptor = require('../../models/bancoDeSangre/modeldocumetosReceptor');

const serveResp = require('../../functions/functions');

const app = express();
const multer = require('multer')
const upload = multer({dest: 'upload/'})
const { uploadFile } =require('../../functions/s3')

//se genera el id de censur y se agrega el id del receptor
app.post('/censur', (req, resp ) => { 
    const body = req.body
    const bancosangre = new BancoSangreSocios({
        paciente         : body.idpaciente,
        tipo_producto    : body.tipo_producto,
    })
    bancosangre
    .save()
    .then((data) => {
        if (data) {
            const idCensur = data._id
             const censur = {
                censur: idCensur
            }
            paciente
            .findByIdAndUpdate(body.idpaciente, { $push: censur })
            .exec()
            .then(data => {
                const procesoscensur = new BancoSangreSociosProcesos({
                proceso_censur: idCensur, 
                })
                .save()
                .then((data) => {
                    const procesos_censur = {
                        proceso_censur: data._id
                    }
                    BancoSangreSocios
                    .findByIdAndUpdate(idCensur, { $push: procesos_censur })
                    .exec()
                    .then((data) => {
                        BancoSangreSocios
                    .find({paciente: body.idpaciente})
                    .then((data) => serveResp(data, null, 'Se encontraron los siguientes produtos :)', resp))
                    .catch((error)   => serveResp(null,error, 'No se encontraron productos3 ', resp))
                    }).catch((error) => {
                        serveResp(null,error, 'No se encontraron productos3 ', resp)
                    });
                }).catch((error) => {
                    serveResp(null,error, 'No se encontraron productos3 ', resp)    
                });
                    
                /* serveResp(data, null, 'Se le asigno un receptor de censur:)', resp) */})
            .catch(error => serveResp(null, error, 'No se pudo asignar el receptor de censur2', resp));
        } else {
           serveResp(data, null, 'Se le asigno un usuario a receptor censur', resp) 
        }
    })
    .catch((error)   => serveResp(null,error, 'No se pudo asignar un usuario al receptor censur1', resp));
})
app.post('/receptor/documentos', async(req, resp) => {
    
    // para el formData se necesita que se use multer
    const { body } = req;
    try {
        
        //console.log(body)
        const documentosResp = new documentosCensur({
            receptor: body.idcensur,
            documentosreceptor: body.documentos
        });
        console.log(documentosResp)
        const respDocs = await documentosResp.save();

        const DocumentosFleboRes = {
            receptor: respDocs
        }


        const dataBanco = await BancoSangreSocios
            .findByIdAndUpdate( body.idcensur,  {$push: DocumentosFleboRes}).exec();

            serveResp(dataBanco, null, 'Se guardaron los datos', resp);

    } catch (error) {
        console.log(error)
        serveResp(null, error , 'Error de servidor '+ error, resp);

    } 

});

app.put('/actualizar/receptor/documentos/:id', async(req, resp) => {
    // para el formData se necesita que se use multer
    const id = req.params.id;
    const body = req.body;
    body.forEach(async element => {
        insertData = { documentosreceptor: element }
        respuesta = await PaquetesPacientes.findByIdAndUpdate({ _id: id }, { $push: insertData },{new:true});
    });
    if(respuesta != null){
        serveResp( respuesta, null, 'Se encontro el paquete', resp);
    }else{
        serveResp( null, 'no se encontro el paquete', 'Accion no valida', resp);
    }
});

//agregar los procesos de censur a horizonte
app.post('/documentos/censur', (req, resp) => {
    const body = req.body
    console.log(body);
    const documentoscensur = new documentosCensur({
        receptor: body.idcensur,
        documentosreceptor: body.documentosreceptor
    })
    documentoscensur
    .save()
    .then((data) => {
        console.log(data);
        if (data) {
            const receptor = {
                receptor: data._id
            }
            BancoSangreSocios
            .findByIdAndUpdate(body.idcensur, { $push: receptor })
            .exec()
            .then(data =>{
                console.log(data);
                 serveResp(data, null, 'Se le asigno los documentos al receptor', resp)
             } )
            .catch(error => serveResp(null, error, 'No se pudo asignar los documentos al receptor', resp));
        } else {
          serveResp(data, null, 'Se encontraron los siguientes produtos', resp)  
        }
    })
    .catch((error)   => serveResp(null,error, 'No se encontraron productos ', resp))
})
//cargar los documentos a aws
app.post('/cargar/documentos/censur', upload.array('file',12), async (req, resp) => {
    const file = req.file
    let data2 = Array()
    for (let i = 0; i < req.files.length; i++) {
        const element = req.files[i];
        const result = await uploadFile(element)
        data2.push({path: `/images/${result.Key}`})
        console.log(element)
    }
    serveResp(data2, null, 'Los documentos son', resp)
})
//agregar la orden de pedido al receptor y se agreg el id de la orden a censur 
app.post('/ordenarpedido/censur', (req, resp) => {
    const {body} = req
    const pedidosedeBanco = new pedidosSedesBanco({
        pedido: body.idcensur,
        vendedor: body.vendedor,
        estudios: body.estudios,
        sede_banco: body.sede_banco,
        totalCompra: body.totalCompra
    });

    console.log( body )
    pedidosedeBanco
    .save()
    .then((data) => {
        if (data) {
            const pedido = {
                pedido: data._id
            }
            BancoSangreSocios
            .findByIdAndUpdate(body.idcensur, { $push: pedido })
            .exec()
            .then(data => serveResp(data, null, 'Se le asigno una orden de compra al receptor', resp))
            .catch(error => serveResp(null, error, 'No se pudo asignar una orden de compra al receptor', resp));
        } else {
            serveResp(data, null, 'Se encontraron los siguientes produtos', resp) 
        }
    }) 
    .catch((error)=> {
        console.log(error);
        serveResp(null,error, 'No se encontraron productos ', resp)
    })
})
//obtener los productos del banco de sangre
app.get('/productos/censur',async (req, resp)=>{
    let ids = [
        "61611aad8598bf035496e74f",
        "61611abf8598bf035496e750",
        "61611ac88598bf035496e751",
        "61611add8598bf035496e752",
    ] 
    const array_productos = []
    try {
        for (let i = 0; i < ids.length; i++) {
            const existencia =  await productosAlmacenStock.find({ productosFI: ids[i], status: 'STOCK' })
            array_productos.push(existencia)
        }
        //console.log(array_productos)

    } catch (error) {
        console.log(error)
    }
    
    //console.log(productosAlmacenados)
    //const productosAlmacenados = await productos.find({'_id':ids},{ estado : 'ACTIVO' });
    //array_productos.push(productosAlmacenados)
    //console.log(array_productos)
/*     productos
    .find()
    .then((data) => serveResp(data, null, 'Se encontraron los siguientes produtos', resp))
    .catch((error)   => serveResp(null,error, 'No se encontraron productos ', resp)) */
})
//bitacora de receptores por sede
app.post('/receptores', (req, resp) => {
    const body = req.body
    paciente
    .find({sede: body.sede})
    .populate('paciente')
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))
    .catch(error => serveResp(null, error, 'Aun no hay pacientes', resp))
})
//informacion de un paciente en censur
app.post('/informacion/receptor/pedido',(req, resp) => {
    let productos = {
    idDonante: '',
    idProductoC: '',
    precioVenta: '',
    idProducto:'',
    producto:'',
    }
    array = []
    BancoSangreSocios
    .find({ _id: req.body.idbancosangre})
    .populate('censur')
    .populate('paciente')
    .populate('receptor')
    .populate('pedido')
    .populate('tiparycruzar')
    .exec()
    .then((data) => {   
        for (let a = 0; a < data.length; a++) {
            //console.log(data)
            for (let c = 0; c < data[a].tiparycruzar[a].obtenidos.length; c++) {
                        //console.log(element2)
                        const element = data[a].tiparycruzar[a].obtenidos[c];
                        console.log(element)
/*                          let productos = {
                            idDonante:   element.idDonante,
                            idProductoC: element.idProductoC,
                            precioVenta: element.precioVenta,
                            idProducto: element2.idProducto,
                            producto:element2.producto
                        }
                        array.push(productos)  */
                        array.push(element)
                    //console.log(element)
            }   
        }
        //console.log(array)
        serveResp(array, null, 'Se encontró el paciente', resp)
    })
    .catch(error => serveResp(null, error, 'No se encontró paciente', resp))
})
app.post('/informacion/receptor',(req, resp) => {

    BancoSangreSocios
    .findOne({ _id: req.body.idbancosangre})
    .populate('censur')
    .populate('paciente')
    .populate('receptor')
    .populate('pedido')
    .then((data) => {
        serveResp(data, null, 'Se encontró el paciente', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se encontró paciente', resp)
    });
})
//se agrego el laboratorio en el banco de sangre del disponente
app.post('/ordenar/tipeycruce',(req, resp) => {
    const body = req.body
    const TipeyCruce = new tipeyCruce({
        laboratorios: body.idcensur,
        pedido: body.pedido
    })
    .save()
    .then((data) => {
        if (data) {
            const laboratorio = { tipeycruce: data._id}
            BancoSangreSocios
            .findByIdAndUpdate(body.idcensur, { $push: laboratorio })
            .exec()
            .then((data) => {
                serveResp(data, null, 'Se agregaron los laboratorio al banco de sangre', resp)
            }).catch((error) => {
                serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
            });
        } else {
        //serveResp(data, null, 'No se le agrego los laboratorios al paciente', resp)
        }
        })
    .catch((error)   => serveResp(null,error, 'No se pudo agregar los laboratorios al paciente', resp));
})
//obtener todos los procesos de la orden de censur
app.post('/procesos/orden/censur',(req, resp) => {
    const body = req.body
    console.log(body)
    BancoSangreSociosProcesos
    .find({proceso_censur: body.idcensur})
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))   
    .catch((error) => {
        serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
    });
})
//actualizar el proceso de documentos
app.post('/procesos/documentos/receptor',(req, resp) => {
    const body = req.body
    console.log(body)
    BancoSangreSociosProcesos
    .find({proceso_censur: body.idcensur})
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))   
    .catch((error) => {
        serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
    });
})
//actualiar el proceso de tipar y cruzar 
app.post('/procesos/tipar/cruzar',(req, resp) => {
    const body = req.body
    console.log(body)
    BancoSangreSociosProcesos
    .find({proceso_censur: body.idcensur})
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))   
    .catch((error) => {
        serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
    });
})
//actualizar el proceso de historico
app.post('/procesos/historico/documento',(req, resp) => {
    const body = req.body
    console.log(body)
    BancoSangreSociosProcesos
    .find({proceso_censur: body.idcensur})
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))   
    .catch((error) => {
        serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
    });
})
//actualizar el proceso de entrega
app.post('/procesos/entrega/productos',(req, resp) => {
    const body = req.body
    BancoSangreSociosProcesos
    .find({proceso_censur: body.idcensur})
    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))   
    .catch((error) => {
        serveResp(null, error, 'No se agregaron los laboratorio al banco de sangre', resp)
    });
})
app.get('/productos/materiales/servicios/censur',async (req, resp)=>{
    try{
        const productos = await almacenProductos.find({laboratorio: 'CENSUR'})
        serveResp(productos, null, 'Se encontro el disponente del banco', resp)
    }catch(error){
        console.log(error)
    }
})
app.post('/receptores', (req, resp) => {
    const body = req.body
    paciente
    .find({sede: body.sede})
    .populate('paciente')

    .then(data => serveResp(data, null, 'Se encontraron los pacientes de la sede', resp))
    .catch(error => serveResp(null, error, 'Aun no hay pacientes', resp))
});

app.put('/actulizar/estado/pedido/censur/:id', async(req, resp) => {


    try{

        const {id} = req.params;
        const {body } = req;
        console.log(body)
        const pedido = await BancoSangreSocios.findById(id);
        console.log(pedido, "pedido cambio estado".toLocaleUpperCase());
        
        if( pedido != null ){
            await BancoSangreSocios.updateOne({_id:id}, { estatus: body.estado.toUpperCase() });
            await pedido.save();

            const pedidoUpdated = await BancoSangreSocios.findById(id);
            // console.log(pedidoUpdated);
            serveResp( pedidoUpdated, null, 'Se pudo actulizar el estado', resp);
        }else{
            serveResp( null, 'No es un ID valido', 'No se actulizo el estado', resp);
        }

    }catch(error) {
        console.log(error );
        serveResp( null, error, 'Se pudo actulizar el estado', resp);
    }

});

app.put('/actulizar/estado/recpetor/censur/:id', async(req, resp) => {


    try{

        const {id} = req.params;
        const {body } = req;

        const bancoSangre = await BancoSangreSocios.findById(id);
        if( bancoSangre != null ){

            if(bancoSangre.status != 'REACCIONES TRANSFUCIONALES'){

                //update pedido 
                /*
                console.log( pedido.pedido, "idPEDIDO" )
                const pedidos = await pedidosSedesBanco.findOne({_id: pedido.pedido });
                console.log( pedidos,  "pedidos - find" )
                await pedidosSedesBanco.updateOne({_id:pedido.pedido}, { status: "finalizado 1 - prueba".toUpperCase() });
                await pedidos.save();
                
                console.log(pedidos, "pedido")
                */


                const pedido = await BancoSangreSocios.findById(id);
                await BancoSangreSocios.updateOne({_id:id}, { estatus: body.estado.toUpperCase()});
                await pedido.save();

                const pedidos = await BancoSangreSocios.findById(id);
                console.log(pedidos)
                serveResp( pedidos, null, 'Se actulizo el estado', resp);
            }
        }

    }catch(error) {
        console.log(error );
        serveResp( null, error, 'Se pudo actulizar el estado', resp);
    }

});

app.get('/ver/pedidos/detalle/:id', async( req, resp) => {

    const  { id } = req.params;
    console.log(id)
    
    try {
        const filterPedido = await pedidosSedesBanco.findById(id).populate('pedido');
        console.log(filterPedido)
        if( filterPedido == null  ){
            serveResp( null, 'No es un ID valido', 'Se pudo actulizar el estado', resp);
        }else {
            const pacinteDetalle = await paciente.findById( filterPedido.pedido.paciente );
            console.log(pacinteDetalle);
            serveResp( pacinteDetalle,null , 'Se encontró el pedido', resp);
        }
        
    }catch(error){
        console.log(error );
        serveResp( null, error, 'Se pudo actulizar el estado', resp);
    }
});

app.get('/reacciones/transfucionales', async (req, resp) => {
    
    try{

        const reaccionesFilter = await BancoSangreSocios.find({estatus:"REACCIONES TRANSFUCIONALES"}).populate('paciente');

        if( reaccionesFilter == null || reaccionesFilter == undefined ){
            serveResp(reaccionesFilter, null, 'Se encontraron reacciones transfucionales', resp);
        }else{
            serveResp(reaccionesFilter, null, 'No hay reacciones transfucionales', resp);
        }

    }catch(error ){
        console.log(error);
        serveResp( null, error, 'Se encotraron reacciones transfucionales', resp);
    }

});

app.put('/agregar/documentos/banco/sangres/socios/:id', async(req, resp) => {

    try{

        const { id} = req.params;
        const { body} = req;

        const pedidosFound = await BancoSangreSocios.findById(id);
        if(pedidosFound){   

            const pedidoUpdate = await BancoSangreSocios.findById(id);
            await BancoSangreSocios.updateOne({_id:id}, body);
            await pedidoUpdate.save();

            const bancoSangreResp = await BancoSangreSocios.findOne({_id:id});
            serveResp( bancoSangreResp , null, 'Se actulizo el paciente', resp);         
        }else{
            console.log('No se encontro el paciente');
            serveResp( null, 'No se encontro el paiciente', 'Hubo un error', resp);           
        }

    }catch(error){
        console.log(error);
        serveResp( null, error, 'Hubo un error', resp);
    }

});

// ** Aca se agrega el endpoint para agregar los documetos del recpetor, es otro modelo para

app.post('/agregar/documentos/receptor', async (req, resp) =>{

    const { body } = req;
    console.log( body )

    try{

        const documentosSaved = new documentosReaccionesReceptor(body);
        const documentSaved  = await  documentosSaved.save();

        if( documentSaved ){
            serveResp( documentSaved, null, 'Se agregaron los docuemtos', resp);
        }else{
            serveResp( null ,'No se pudo agregar' , 'Hubo un error', resp)         
        }

    }catch(error){
        console.log(error);
        serveResp( null, error, 'Hubo un error', resp);
    }

});


app.get('/ver/documentos/receptor/:idCensur', async (req, resp ) => {
    try{

        const {idCensur} = req.params;

        const validateCensurId = await documentosReaccionesReceptor.find({idCensur});
        
        if( validateCensurId == null ){
            serveResp( null, 'No es un ID valido', 'Hubo un error', resp);
        }else {

            serveResp( validateCensurId  , null, 'Se encontraron los docuemtos', resp);
        }

    }catch (error){
        console.log(error);
        serveResp( null, error, 'Hubo un error', resp);
    }
});

module.exports = app; 


/**\
 * 
 * 
 *             let docSaved = await hospitalizacion.findOne( {_id:id});
            await hospitalizacion.updateOne( {_id:id}, body );

            await docSaved.save()
          const savedHospitalizacion =  await hospitalizacion.findOne( {_id:id})
 */