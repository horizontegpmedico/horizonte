const express = require('express');
const cSeguimiento = require('../../models/almacen/modelSeguimiento');

let app = express();

app.post('/crear/nuevo/seguimiento', (req, resp) => {
    let body = req.body;
    // console.log("Seguimiento Perrucho:", body);

    let newSchemaSeguimiento = new cSeguimiento({
        dptoSolicitante: body.dptoSolicitante,
        dptoSolicitado: body.dptoSolicitado,
        nombre: body.nombre,
        // id: body.id,
        categoria: body.categoria,
        servicios: body.servicios,
        solicitante: body.solicitante,
        estado: body.estado,
        descripcion: body.descripcion
    });

    newSchemaSeguimiento
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se ha creado el Seguimiento, guapo ;)',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'Ooops, algo salió mal',
                        err
                    })
            }
        );
});

app.get('/ver/seguimientos/creados', (req, resp) => {
    cSeguimiento
        .find()
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Seguimiento Encontrado',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontraron Seguimientos',
                        error
                    });
            }
        )
});

// app.get('/ver/seguimientos/creados/:id', (req, resp) => {
//     let _id = req.params.id;

//     cSeguimiento
//         .find({ _id })
//         .exec()
//         .then(
//             (data) => {
//                 return resp.status(200)
//                     .json({
//                         ok: true,
//                         message: 'Seguimiento Encontrado',
//                         data
//                     });
//             }
//         )
//         .catch(
//             (error) => {
//                 return resp.status(500)
//                     .json({
//                         ok: false,
//                         message: 'No se encontraron Seguimientos',
//                         error
//                     });
//             }
//         )
// });

app.put('/actualizar/seguimiento/:id', (req, resp) => {
    let _id = req.params.id
    let body = req.body;

    cSeguimiento
        .findByIdAndUpdate(_id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se ha actualizado el Seguimiento',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se ha actualizado el Seguimiento',
                        data
                    });
            });
});

app.delete('/eliminar/seguimiento/:id', (req, resp) => {
    let _id = req.params.id;
    cSeguimiento
        .findByIdAndDelete({ _id })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se ha ELIMINADO el Seguimiento',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se ha eliminado el Seguimiento',
                        data
                    });
            });
});

module.exports = app;