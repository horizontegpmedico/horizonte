const express = require('express')
const app = express()
//modelos requeridos
const paciente = require('../../models/nuevoPaciente')
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const signosVitales = require('../../models/bancoDeSangre/modelSignosVitales')
//Funcion de terceros 
const serveResp = require('../../functions/functions')
//se agregan los signos vitales en el banco de sangre del disponente
app.post('/agregar/signosvitales', (req, resp) => {
    const body = req.body
    const signosvitales = new signosVitales({
        signosvitales: body.idbancosangre,
        talla: body.talla,
        peso: body.peso,
        imc: body.imc,
        temp:body.temp,
        sistolica: body.sistolica,
        diastolica: body.diastolica,
        huellasMultiples: body.multiples,
        huellasRecientes: body.recientes
    })
    signosvitales
    .save() 
    .then((data) => {
        if (data) {
            const signosvitales = {
                signosvitales: data._id
            }
            bancoDeSangre
            .findByIdAndUpdate(body.idbancosangre, { $push: signosvitales })
            .exec()
            .then(data => serveResp(data, null, 'Se agregaron los signos vitales al banco de sangre del disponente', resp))
            .catch(error => serveResp(null, error, 'No se pudo agregar los signos vitales al banco de sangre del disponente', resp))
        } else {
                serveResp(data, null, 'Se le agrego los signos vitales al paciente', resp)
        }
    })
    .catch((error)   => serveResp(null,error, 'No se pudo agregar los signos vitales al paciente', resp))
})
//actualizar la religion del paciente
app.put('/actualizar/religion', (req, resp) => {
    const body = req.body;
    paciente
    .findOneAndUpdate({'_id':body.idpaciente},
    {'$set':{
        "religion":body.religion
    }
    })
    .exec()
    .then(data => serveResp(data, null, 'Se actualizo la religion del disponente', resp))
    .catch(error => serveResp(null, error, 'No se pudo agregar el lugar de origen del disponente', resp));
});

//actualizar la religion del paciente
app.put('/actualizar/tipoSangre/paciente', (req, resp) => {
    const body = req.body;
    paciente
    .findOneAndUpdate({'_id':body.idpaciente},
    {'$set':{
        "tipoDeSangre":body.tipoDeSangre
    }
    })
    .exec()
    .then(data => serveResp(data, null, 'Se actualizo el tipo de sangre del disponente', resp))
    .catch(error => serveResp(null, error, 'No se pudo agregar el tipo de sangre del paciente', resp));
});
module.exports = app