const express = require('express')
const app = express()
    //modelos requeridos
const paciente = require('../../models/nuevoPaciente')
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const BancoSangreSocios = require('../../models/bancoDeSangre/modelBancoSangreTercero')
const productosAlmacenStock = require('../../models/almacen/stock/modelProductosStock')
const flebotomia = require('../../models/bancoDeSangre/modelFlebotomia')
    //funciones de terceros
const serveResp = require('../../functions/functions')
const sortByDate = require('../../functions/sortByDate')
    //se genera el id de banco de sangre y se agrega para el disponente

app.post('/bancodesangre', (req, resp) => {
    const body = req.body
    const bancosangre = new bancoDeSangre({
        paciente: body.idpaciente
    })
    bancosangre
        .save()
        .then((data) => {
            if (data) {
                const modelBancoSangre = {
                    modelBancoSangre: data._id
                }
                paciente
                    .findByIdAndUpdate(body.idpaciente, { $push: modelBancoSangre })
                    .then((data) => {
                        bancoDeSangre
                            .find({ paciente: body.idpaciente })
                            .then((data) => {
                                serveResp(data, null, 'todos los datos del banco de sangre son', resp)
                            }).catch((error) => {
                                serveResp(null, error, 'No se pudo agregar el banco de sagre', resp)
                            });
                    })
                    .catch(error => serveResp(null, error, 'No se pudo agregar el banco de sagre', resp));
            } else {
                serveResp(data, null, 'Se le asigno un usuario al banco de sangre', resp)
            }
        })
        .catch((error) => serveResp(null, error, 'No se pudo asignar un usuario al banco de sangre', resp));
});

//actulizar el proceso al disponente
app.put('/agregar/procesobancodesangre', (req, resp) => {
    const body = req.body;
    bancoDeSangre
        .findOneAndUpdate({ '_id': body.idbancosangre }, {
            '$set': {
                "proceso": body.proceso,
                "fecha_termino":body.fecha_termino,
                "estatus":body.estatus,
                "motivo":body.motivo,
                "tiempo_rechazo":body.tiempo_rechazo
            }
        })
        .exec()
        .then(data => serveResp(data, null, 'Se agrego el lugar de origen del disponente', resp))
        .catch(error => serveResp(null, error, 'No se pudo agregar el lugar de origen del disponente', resp));
});

//obtiene los datos de un disponente 
app.get('/getdisponente/:id', async(req, resp) => {
        let id = req.params.id
        bancoDeSangre
            .findOne({paciente:id})
            .populate('paciente')
            .populate('cuestionarioautoexclusion')
            .populate('signosvitales')
            .populate('historiaclinica')
            .populate('hojaevolucion')
            .populate('reaccionesderivadas')
            .populate('flebotomia')
            .populate('laboratorio')
            .populate('almacen')
            .then((data) => {
                console.log(data);
                serveResp(data, null, 'Se encontro el disponente del banco', resp)})
            .catch((error) => {
                console.log(error);
                serveResp(null, error, 'No se puede encontro el disponente', resp)});
        /*     try {
                 const disponente = await bancoDeSangre.find({'_id':id}).populate('paciente').exec()
                 console.log(disponente)
                 serveResp(disponente, null, 'Se encontro el disponente del banco', resp)
            } catch (error) {
                console.log(error)
            } */
    })
    //obtener todos los disponentes

    //obtiene los datos de un disponente 
app.get('/getdisponente/por/paciente/:id', async(req, resp) => {
    let id = req.params.id
    /* console.log(id) */
    bancoDeSangre
        .findOne({paciente:id})
        .populate('paciente')
        .populate('cuestionarioautoexclusion')
        .populate('signosvitales')
        .populate('historiaclinica')
        .populate('hojaevolucion')
        .populate('reaccionesderivadas')
        .populate('flebotomia')
        .populate('laboratorio')
        .populate('almacen')
        .then((data) => serveResp(data, null, 'Se encontro el disponente del banco', resp))
        .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp));
    /*     try {
             const disponente = await bancoDeSangre.find({'_id':id}).populate('paciente').exec()
             console.log(disponente)
             serveResp(disponente, null, 'Se encontro el disponente del banco', resp)
        } catch (error) {
            console.log(error)
        } */
})
//obtener todos los disponentes
app.get('/disponentes', (req, resp) => {
    bancoDeSangre
        .find()
        .populate('paciente')
        .populate('cuestionarioautoexclusion')
        .populate('signosvitales')
        .populate('historiaclinica')
        .populate('hojaevolucion')
        .populate('reaccionesderivadas')
        .populate('flebotomia')
        .populate('tipeycruce')
        .populate('laboratorio')
        .populate('serologia')
        .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
        .catch((error) => {
            /* console.log(error) */
            serveResp(null, error, 'No se puede encontro el disponente', resp)
        });
});

app.get('/disponentes', (req, resp) => {
    bancoDeSangre
        .find()
        .populate('paciente')
        .populate('cuestionarioautoexclusion')
        .populate('signosvitales')
        .populate('historiaclinica')
        .populate('hojaevolucion')
        .populate('reaccionesderivadas')
        .populate('flebotomia')
        .populate('tipeycruce')
        .populate('laboratorio')
        .populate('serologia')
        .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
        .catch((error) => {
            /* console.log(error) */
            serveResp(null, error, 'No se puede encontro el disponente', resp)
        });
});
//diferir el disponente
app.post('/diferir/disponente', (req, resp) => {

        const { body } = req;
        bancoDeSangre
            .findByIdAndUpdate(body.idbanco, {
                "proceso": body.proceso,
                "estatus": body.estatus,
                "motivo": body.motivo,
                "tiempo_rechazo": body.rechazo,
                "fecha_termino": body.fechaRechazo
            })
            .exec()
            .then(data => {
                /* console.log(data); */
                serveResp(data, null, 'Se diferio el paciente', resp)
            })
            .catch(error => serveResp(null, error, 'No se puede diferir', resp));
    })
    //listado de ordenes de receptores
app.get('/orden/receptores', (req, resp) => {

        BancoSangreSocios
            .find({ estatus: "RECEPTOR" })
            .populate('paciente')
            .populate('receptor')
            .populate('pedido')
            .then((data) => {
                // console.log(data);
                let arrayfinal = []
                for (let a = 0; a < data.length; a++) {
                    if (data[a].ventaCensur.length > 0) {
                        /* console.log(data[a].ventaCensur.length); */
                    } else {
                        arrayfinal.push(data[a])
                    }
                }
                serveResp(data, null, 'Se encontraron los disponentes del banco', resp)
            })
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp));
    })
    
    app.get('/orden/receptor/:id', (req, resp) => {
        let id = req.params.id
        BancoSangreSocios
            .findOne({_id:id})
            .populate('paciente')
            .populate('receptor')
            .populate('pedido')
            .then((data) => {
                serveResp(data, null, 'Se encontraron los disponentes del banco', resp)
            })
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp));
    })

    //Total de personas que han realizado el proceso de banco de sangre
app.get('/disponentes/contar', (req, resp) => {
        bancoDeSangre.find().countDocuments()
            .then((data) => {
                serveResp(data, null, 'Contador del banco de sangre', resp)
            }).catch((err) => {
                serveResp(null, err, 'No se puedo contar los documentos de banco de sangre', resp)
            });
    })
    //Total de visitas por persona que han realizado el proceso de banco de sangre
app.post('/disponente/visitas', (req, resp) => {
        const body = req.body
        /* console.log(body) */
        bancoDeSangre.find({ estatus: "FINALIZADO", paciente: body.idPaciente }).countDocuments()
            .then((data) => {
                serveResp(data, null, 'Contador del banco de sangre', resp)
            }).catch((err) => {
                serveResp(null, err, 'No se puedo contar los documentos de banco de sangre', resp)
            });
    })
    //obtener las visitas del disponente
app.post('/getdisponente/visitas', (req, resp) => {
        const body = req.body
        /* console.log(body) */
        bancoDeSangre.find({ paciente: body.idPaciente }).countDocuments()
            .then((data) => {
                serveResp(data, null, 'Contador visitas del paciente', resp)
            }).catch((err) => {
                serveResp(null, err, 'No se puedo contar los documentos de banco de sangre', resp)
            });
    })
    //obtener disponentes por grupo y RH
app.post('/disponentes/grupoyrh', (req, resp) => {
        const body = req.body
        const data2 = []
        const array = []
        let almacen = []
        let disponente = {
            idDonante: '',
            nombrePaciente: '',
            apellidoPaterno: '',
            apellidoMaterno: '',
            lote_interno: '',
            fecha_caducidad: '',
            tipoDeSangre: '',
        }
        const donadoresfinal = []
        const elitrocitario = []
        const plaquetario = []
        const plasma = []
        const plaquetas = []

        paciente.find({ tipoDeSangre: body.tiposangre }, { _id: { $not: { _id: body.idbancodesangre } } }).populate('modelBancoSangre')
            .then(async(data) => {
                console.table(data);
                for (let a = 0; a < data.length; a++) {
                    if (data[a].modelBancoSangre.length != 0) {
                        for (let b = 0; b < data[a].modelBancoSangre.length; b++) {
                            const pivo = data[a].modelBancoSangre[b]._id
                            const donantes = await bancoDeSangre.find({ _id: pivo }).populate('paciente').populate('almacen')
                            for (let c = 0; c < donantes.length; c++) {
                                if (donantes[c].almacen.length > 0) {
                                    for (let d = 0; d < donantes[c].almacen.length; d++) {

                                        var ce = donantes[c].almacen[d].lote_unitario.search(/CE/i)
                                        var cp = donantes[c].almacen[d].lote_unitario.search(/CP/i)
                                        var pl = donantes[c].almacen[d].lote_unitario.search(/PL/i)
                                        var pt = donantes[c].almacen[d].lote_unitario.search(/PT/i)
                                        let donadores = {
                                            idDonante: donantes[c].paciente._id,
                                            idProducto: donantes[c].almacen[d].productosFI,
                                            tipoDeSangre: donantes[c].paciente.tipoDeSangre,
                                            nombreCompleto: donantes[c].paciente.nombrePaciente +' '+ donantes[c].paciente.apellidoPaterno +' '+ donantes[c].paciente.apellidoMaterno,
                                            tipoDeSangre: donantes[c].paciente.tipoDeSangre,
                                            lote_interno: donantes[c].almacen[d].lote_unitario,
                                            fecha_caducidad: donantes[c].almacen[d].fecha_caducidad,
                                            precioVenta: donantes[c].almacen[d].precioVenta
                                        }
                                        if (ce !== -1) {
                                            elitrocitario.push(donadores)
                                          } else if (cp !== -1) {
                                            plaquetario.push(donadores)
                                          } else if (pl !== -1) {
                                            plasma.push(donadores)
                                          } else if (pt !== -1) {
                                            plaquetas.push(donadores)
                                          }
                                    }
                                } else {
                                    //console.log('almacen vacio')
                                }

                            }

                        }
                    } else {
                        //console.log('data vacia')
                    }

                }
                const e = elitrocitario.sort(sortByDate)
                const p = plaquetario.sort(sortByDate)
                const l = plasma.sort(sortByDate)
                const t = plaquetas.sort(sortByDate)
                let final = {
                    ce: {idProducto: e[0]?.idProducto, donadores: e.reverse()},
                    cp: {idProducto: p[0]?.idProducto, donadores: p.reverse()},
                    pl: {idProducto: l[0]?.idProducto, donadores: l.reverse()},
                    pt: {idProducto: t[0]?.idProducto, donadores: t.reverse()},
                }
                donadoresfinal.push(final)
                /* console.log(donadoresfinal); */
                serveResp(donadoresfinal, null, 'Se encontraron los disponentes del banco con tipo y rh', resp)
            }).catch((error) => {
                /* console.log(error) */
                serveResp(null, error, 'No se puede encontro el disponente', resp)
            });
    })
    //listado de ordenes de receptores
app.post('/disponentes/diferidos', (req, resp) => {
        const body = req.body
        bancoDeSangre
            .find({ estatus: "DIFERIDO", paciente: body.idpaciente })
            .exec()
            .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp))
    })
//listado de diferidos
app.get('/historico/disponentes/diferidos', async (req, resp) => {
        const body = req.body
        const array = []
        try {
            const estatus = await bancoDeSangre.find({ estatus: "DIFERIDO" })
            .populate('paciente')
            .exec()
            for (let a = 0; a < estatus.length; a++) {
                const element = estatus[a]
                let nombre = element.paciente.nombrePaciente + " " + element.paciente.apellidoPaterno + " " + element.paciente.apellidoMaterno 
                let genero = ''
                if (element.paciente.genero == 'femenino') {
                    genero = 'F'
                } else {
                    genero = 'M'
                }
                let DIFERIDO = {
                    idDonante: element.paciente._id,
                    idbanco: element._id,
                    nombreCompleto: nombre ,
                    genero: genero,
                    edad: element.paciente.edad,
                    tipoDeSangre: element.paciente.tipoDeSangre,
                    estatus: element.estatus,
                    proceso: element.proceso,
                    motivo: element.motivo,
                    fecha_diferido: element.tiempo_rechazo,
                    
                }
                array.push(DIFERIDO)
                /* console.log(element); */
                
            }
            serveResp(array, null, 'Se encontraron los disponentes del banco', resp)
        } catch (error) {
            serveResp(null, error, 'No se puede encontro el disponente', resp)
            /* console.log(error); */
        }

            //.then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
            //.catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp))
    })
    //Para traer el historico de banco de sangre del paciente
app.post('/historico/banco/disponente', (req, resp) => {
        const body = req.body
        let resultado = {
            feblotomia:{},
            disp:{}
        };
        /* console.log(body) */
        final = []
        paciente
            .find({ _id: body.idpaciente })
            .populate('modelBancoSangre')
            .exec()
            .then(async (data) => {
                for (let a = 0; a < data.length; a++) {
                    for (let b = 0; b < data[a].modelBancoSangre.length; b++) {
                        const element = data[a].modelBancoSangre[b];
                        if(element.flebotomia != null || element.flebotomia != undefined){
                            const disponente = await flebotomia.findById(element.flebotomia).exec()
                            // resultado.disp = disponente 
                            element.flebotomia = disponente
                            // resultado = Object.assign(element,{feblotomiaDisp:disponente})
                        }
                        // resultado.feblotomia = element
                        final.push(element)
                    }
                }
                serveResp(final, null, 'El historial del banco de sangre', resp)
            })
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp))
        })
    /// traer el disponente 
app.get('/historico/disponente/bs/:id', async(req, resp) => {
        let id = req.params.id
        /* console.log(id) */
         try {
                 const disponente = await bancoDeSangre.find({'_id':id}).populate('paciente').exec()
                 /* console.log("es",disponente) */
                 serveResp(disponente, null, 'Se encontro el disponente del banco', resp)
            } catch (error) {
                console.log(error)
            }   
    })
//Verificar si el disponente en la tabla de diferidos 
app.post('/verificar/disponente', (req, resp) => {
        const body = req.body
        bancoDeSangre
            .find({ paciente: body.idpaciente })
            .populate('paciente')
            .then((data) => serveResp(data, null, 'El historial del banco de sangre', resp))
            .catch((error) => serveResp(null, error, 'No se puede encontro el disponente', resp))
    })
    //obtiene los datos de un disponente 
app.get('/bancos', async(req, resp) => {
    try {
        const disponente = await bancoDeSangre.find().exec()
        /* console.log(disponente) */
        serveResp(disponente, null, 'Se encontro el disponente del banco', resp)
    } catch (error) {
        console.log(error)
    }
})  
    //paciente diferido por termino de proceso en serologia
app.post('/bancos/usuario/nodonacion', async(req, resp) => {
        const body = req.body
        const nueDisponente = {proceso : 'NUEVO'}
        try {
            const disponente = await bancoDeSangre.find({paciente: body.idpaciente}).exec()
            if(disponente.length != 0)
             for (let i = 0; i < disponente.length; i++) {
                const element = disponente[i];
                if (element.proceso == 'FINALIZADO') {
                    serveResp(disponente[i], null, 'Se encontro el disponente del banco', resp) 
                }else if(element.estatus == 'DISPONIBLE'){
                    serveResp(nueDisponente, null, 'Disponente de primera vez', resp)  
                }
            }else{
                serveResp(nueDisponente, null, 'Disponente de primera vez', resp)  
            } 
            /* console.log(disponente) */
            
        } catch (error) {
            console.log(error)
        }
    })     
module.exports = app;