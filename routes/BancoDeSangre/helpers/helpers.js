const express = require('express');
const serveResp = require('../../../functions/functions');
const app = express();
const Pacientes = require('../../../models/nuevoPaciente');
const Antecedentes = require('../../../models/signosVitales/antecedentes');
const ventas = require('../../../models/ventasServicios/serviciosVentas');
const consultas = require('../../../models/consultasGeneralesBitacoras/consultaHistorial');


app.get('/delete/user/prueba/:id', async(req, resp) => {
    
    try{
   
        var arrData = [];

        const { id  } = req.params;

        const paciente = await Pacientes.findById( id );

        
        if( paciente != null  ){
            
            arrData.push( paciente );
        }

        const ventasData = await ventas.find({paciente: id });
        

        if( ventasData.length >= 1 ) {
            arrData.push( ventasData );
        }


        const consultasDB = await consultas.find({paciente: id});


        if( consultasDB.length >= 1 ) {

            arrData.push( consultasDB );
        }


        console.log( arrData )

    }catch(error){

        console.log(error)

    }
    


    serveResp(arrData, null, 'se encontro', resp);

});

module.exports = app;