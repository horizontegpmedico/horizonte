const express = require('express')
const app = express()

//modelos requeridos
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const flebotomia = require('../../models/bancoDeSangre/modelFlebotomia')
const documentos = require('../../models/bancoDeSangre/modelDocumentos')
const paciente = require('../../models/nuevoPaciente');

//Funcion de terceros 
const serveResp = require('../../functions/functions');
const multer = require('multer')
const upload = multer({dest: 'upload/'})
const { uploadFile } =require('../../functions/s3')
//se agregan los documentos de flebotomia en el banco de sangre del disponente

// get de flebotomiaResp

var contador = 0;

app.get('/flebotomia/:id', (req, resp) =>{
    
    const {id} = req.params;

    bancoDeSangre
        .find( {_id: id }  )
        .populate('paciente')
        .populate('cuestionarioautoexclusion')
        .populate('signosvitales')
        .populate('laboratorios')
        .exec()
        .then( (result) =>serveResp(result,null ,"Se encontro flebotomia", resp))
        .catch( (err) => serveResp(null, err, "No se encontro flebotomia", resp) );
});

// esta es una ruta de prueba
// app.post('/flebotomia/prueba/algo',  async (req, resp ) => {


//     const data = await flebotomia.find();  
    
//     flebotomia.watch().on('change', async (event) =>{
//         console.log( event );
        
//         if( event.operationType === 'update' ){
            
//             if(  event.updateDescription.updatedFields.fecha !== null){
              
//                 if(contador < 3) {

//                     const respData =  await flebotomia.findOneAndUpdate( event.documentKey, { fecha: Date.now() } );
//                     console.log(respData);
//                    contador += 1
                   

//                 }
//         }
//         }

//     });

//     serveResp(data, null,"Se encontro flebotomia", resp);
// });

// esta cinsulta se encarga de actualizar a los disponentes
app.get('/flebotomia/prueba/algo',  async (req, resp ) => {
    
    const date = new Date();

    try {

        const dataUpdate = await bancoDeSangre.aggregate([
            {
                $project:{
                    
                   estatus: {
                   
                       $cond: { 
                           
                        if: {
                            $gte: ["$fecha_termino", date.toISOString() ], 
                           },

                           then : "TERMINADO", 
                            else : 'AUN NO' 
                               
                           }
                   }
                 } 
            }
           ]);

           console.log(dataUpdate)
           
           for(var x = 0; dataUpdate.length >= x; x++ ){
                
                if( dataUpdate[x]?.estatus == "TERMINADO" ||  dataUpdate[x]?.estatus == "FINALIZADO" ){

                    const dataResp =  await bancoDeSangre.findByIdAndUpdate( {_id:  dataUpdate[x]._id}, { "estatus": "DISPONIBLE" }, {new: true});
                    console.log(dataResp);
    
                   }
            

           }
           /*
           dataUpdate.forEach( async(idBs) => {

                console.log( idBs, "idBs" )
            // const dataRespFind = await bancoDeSangre.find( idBs._id );
                // console.log( dataRespFind)

               if(idBs.status == "TERMINADO" || idBs.status == "FINALIZADO" ){

                const dataResp =   await bancoDeSangre.findByIdAndUpdate( {_id: idBs._id}, { "estatus": "DISPONIBLE" }, {new: true});
                console.log(dataResp);

               } 
               
           });*/

           return resp.status(200).json({ 
               ok:true,
               message: 'Actualizar campos'
           });
    
        } catch (error) {
            console.log( error) 
            serveResp( null, error, 'Se actualizaron los registros', resp);
        }
   

});

// esta bitacora devulve a los disponentes disponibles que hay en la db
app.get('/ver/lista/disponentes/disponibles', async (req, res) => {

    try {
        
        const dataDiponentesDisponibles = await bancoDeSangre.find({ estatus: "DISPONIBLE"});


        serveResp(dataDiponentesDisponibles, null, 'Se ecnotraron los disponibles', res);

    } catch (error) {
        /* console.log(error); */
        serveResp(null,error, 'No se pudo conectar', res);

    }
    
});

/*
try {

    const data = await bancoDeSangre.updateMany({estatus:'DIFERIDO'}, { "$set": { "fecha" : Date.now()  } });

    
} catch (error) {
    console.error(error);
    serveResp(null, error, 'Hubo un error', resp);
}
*/

// post de flebotomia
app.post('/flebotomia', async(req, resp) => {
    
    // para el formData se necesita que se use multer
    const { body } = req;
    try {
        
        const flebotomiaResp = new flebotomia({

            bancodesangre : body.idbancosangre,
            tipo_de_sangreado : body.tipo_de_sangreado,
            tipo_de_donador : body.tipo_de_donador,
            notas : body.notas,
            tiempo: body.tiempo,
            volumen : body.volumen,
        });

        const data = await flebotomiaResp.save();

        const documentosResp = new documentos({
            disponente: data._id,
            documentosdisponente: body.archivos
        });

        const respDocs = await documentosResp.save();
        const DocumentosFleboRes = {
            disponente: respDocs
        }

        const dataBanco2 = await flebotomia
            .findByIdAndUpdate( data._id,  {$push: DocumentosFleboRes}).exec();

        const flebotomiaRespDocs = {
                flebotomia: data._id
            }

        const dataBanco = await bancoDeSangre
            .findByIdAndUpdate( body.idbancosangre,  {$push: flebotomiaRespDocs}).exec();

            serveResp(dataBanco, null, 'Se guardaron los datos', resp);

    } catch (error) {
        serveResp(null, error , 'Error de servidor '+ error, resp);

    } 

});
// cargar documentos
app.post('/cargar/documentos/flebotomia', upload.array('fileUploadTosend',12), async (req, resp, next) => {
    
    //const file = req.file    
    //console.log( req );

    let data2 = Array()
    for (let i = 0; i < req.files.length; i++) {
        const element = req.files[i];
        const result = await uploadFile(element)
        data2.push({path: `/images/${result.Key}`})
        // console.log(element)
    }

    serveResp(data2, null, 'Los documentos son', resp)
});

module.exports = app