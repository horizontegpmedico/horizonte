
// libs
const express = require('express');
const QRcode = require('qrcode');
const app = express();

//modelos requeridos
const almacenProductos = require('../../models/almacen/modelProductos');
// este modelo es de la tabal admin de los prodctos

// modelo del bs
const productosAlmacenStock = require('../../models/almacen/stock/modelProductosStock');
// modelo de los produictos ya en stock

const {logMemory} = require('../../config/logs');
const serveResp = require('../../functions/functions');
const { verificaToken } = require('../../middlewares/auth');

//obtener todos los productos

// ver si esta ruta se va a eliminar 
app.get('/minialmacen', (req, resp)=>{
    almacenProductos
    .find({ estado: 'ACTIVO' })
    .then((data) => serveResp(data, null, 'Se encontraron los disponentes del banco', resp))
    .catch((error)   => serveResp(null,error, 'No se puede encontro el disponente', resp));
});

app.get('/count/total/productos', async (req, resp) => {

    try {
        const countTotalProducts = await almacenProductos.find().countDocuments();
        serveResp(countTotalProducts, null, 'Conteo de productos', resp);
    } catch (error) {
        /* console.log(error); */ 
        serveResp(null,error, 'No se pudo contar', resp);
    }

});

app.get('/actualizar/status/etiqueta/:id', async(req, resp)=> {
    
    const { id } = req.params;

    try {
        const dataLabeUpdated = await productosAlmacenStock.findOneAndUpdate( {_id: id}, { downloaded:  true  } );
        serveResp(dataLabeUpdated, null, 'Se actulizo la etiqueta', resp);
    } catch (error) {
        /* console.log(error);  */
        serveResp(error, null, 'No se pudo actualizar', resp);
        
    }
});

app.post('/agregar/productos/almacen', async(req, resp) => {

    const {body} = req;
    const almacenSaved = new almacenProductos(body);

    try { 
        
       const productosAlmacenGuardado = await almacenSaved.save();
        serveResp(productosAlmacenGuardado, null, 'Producto creado correctamente', resp);

    }catch(error) {
        /* console.log(error);  */
        serveResp(null, error, 'Error al crear el producto'+error, resp );    
    }
    
});



// este servicio obtiene todos los productos que hay en el almacen registrados en la tabla admin

app.get('/ver/todos/productos/almacen', async(req, resp) => {
    const productsWithExistence =[]
    try{ 
        const productosAlmacenados = await almacenProductos.find({ estado : 'ACTIVO',  productoMatriales : 'PRODUCTO'}).sort({_id:-1});
        /* for (let index = 0; index < productosAlmacenados.length; index++) {
            const existencia =  await productosAlmacenStock.find({ productosFI: productosAlmacenados[index]._id, status: 'STOCK' }).countDocuments();
            let valores = {
                nombre: productosAlmacenados[index].nombre,
                nombre_comercial: productosAlmacenados[index].nombre_comercial,
                proveedor:productosAlmacenados[index].proveedor,
                existencia:existencia
            }
            productsWithExistence.push( valores );
        } */
        /* productosAlmacenados.forEach( async(producto) => {
            
            const existencia =  await productosAlmacenStock.find({ productosFI: producto._id, status: 'STOCK' }).countDocuments();
            let valores = {
                nombre: producto.nombre,
                nombre_comercial: producto.nombre_comercial,
                proveedor:producto.proveedor,
                existencia:existencia
            }
            console.log(valores);
            productsWithExistence.push( valores ); */
            /* console.log(existencia);
            producto.existencia = existencia;
        

            productJson = {...producto.toJSON(), existencia: existencia} 
            productsWithExistence.push( productJson );*/
        /* }); */
        /* console.log(productsWithExistence); */
        /* const dataFunction = setExistencesData( productsWithExistence ) */
        
        
        serveResp(productosAlmacenados, null, 'Productos encontrados', resp);
        
    }catch ( error) {
        /* console.log(error); */ 
        serveResp(null, error, 'Error al cargar los productos', resp);
    }

});



const setExistencesData = (dataArg) => { 
    return dataArg;
}

// este servicio obtiene los productos registrados en la tabla admin por el ID
app.get('/ver/producto/almacen/por/id/:id', async(req, resp) => {
    
    const { id } = req.params;
    try {

        const productoAlmacenAdmin = await almacenProductos.findById( id );
        serveResp(productoAlmacenAdmin, null, 'Se encontro el producto', resp);
    } catch (error) {
        /* console.log(error);  */
        serveResp( null, error, `Error ${error}`, resp );
    }
    
});

/// servicio que se encarga de actualizar los precios de los productos

app.put("/actualizar/productos/costo/:id", async (req, resp) => {

    const { id } = req.params;
    const { body } = req;

    try {
        const productFind = await almacenProductos.findById(id);
        console.log(productFind);
        const updateArray = [];

        productFind.proveedor.forEach( async (Element) => {
            // console.log(Element);
            
            if(Element.proveedor == body.proveedor){
                Element.costo = body.costo;
            }            
            
            updateArray.push(Element);
        });

        const respuesta = await almacenProductos.findByIdAndUpdate(id, { $set: {proveedor: updateArray} },{ new: true}) 

        serveResp(respuesta ,null, 'Se actulizo el producto', resp);    
        
    } catch (error) {
        console.error(error);
        serveResp(null, error, 'No se pudo actualizar', resp);
    }
});


app.put('/actualizar/productos/stock/almacen/:id', async (req, resp ) => {
   
    const {  id} =req.params;
    const { body } =req;

    console.log( id, "req actualizar/productos/stock/almacen/" )
    const respStock = await productosAlmacenStock.findById( id );
    
    if( !respStock ) {
        serveResp(null, 'No se encontro el ID', 'No se encontraron los productos', resp);        
    }

    const responseUpdated = await productosAlmacenStock.findByIdAndUpdate(id, body, { new: true});
    
    if(responseUpdated){
        serveResp(responseUpdated, null, 'Se actualizo el producto', resp);
    }

});
// obtiene los productos de almacen que hay en el stock por el ID del producto de la tabla adminstration de productos

app.get('/ver/productos/almacen/por/id/:id', async (req, resp) => {
        
    const { id } = req.params;
    
    //.populate('proveedor')
    try {

        const productosStock = await productosAlmacenStock.find( {productosFI: id, status: 'STOCK', seDesecha: ""});
        console.log(productosStock);
        serveResp(productosStock, null, 'Se encontraron los productos', resp);
        
    } catch (error) {
        /* console.log(error);  */
        serveResp(error, null,'Hubo un error'+ error, resp);

    }

});

app.get('/ver/productos/salidas', async (req, resp) => {

    try{

        console.log("/ver/productos/desechados", req);
        const productosStockDesechados = await productosAlmacenStock.find( { status: 'STOCK', seDesecha: "SI"}).populate('productosFI', 'nombre nombre_comercial productoMateriales');
        
        if( !productosStockDesechados ){
            serveResp(null, 'No hay salidas', 'No se encontraron las salidas', resp);            
        }
        
        if(productosStockDesechados){
            
            console.log(productosStockDesechados, "resp /ver/productos/salidas")
            serveResp(productosStockDesechados, null, 'Se encontraron los productos', resp);
            // throw new Error('Algo paso')
        }


    }catch( error) {
        console.log(error);
        serveResp(error, null,'Hubo un error'+ error, resp);
    }

});

// actualizamos los datos  de los productos

app.put('/actualizar/productos/almacen/:id', async (req, resp) => {

    const  { id } = req.params;  
    const { body } = req;

    const producto = await almacenProductos.findById( id );

    if(!producto) {
        serveResp(null, 'El producto no existe', 'Producto no encontardo', resp);
   
    }else {

        try {
            
        const productUpdated = await almacenProductos.findByIdAndUpdate( id, body, { new: true } );
        serveResp( productUpdated, null,'Producto actulizado', resp);

        } catch (error) {
            /* console.log(error); */
            serveResp(null, error, 'No se pudo actualizar', resp);
        }

    }


});

// Este servicio guarda en la DB los productos de almacen, los pone en el stock
app.post('/agregar/productos/almacen/stock',  async (req, resp) => {
    

    const { body } = req;
    const { cantidad } = body;

    const cantidadNumber =  Number( cantidad );    
    // convertimos la cantidad a un tipo number
    
    // es for agrega la cantidad de productos que se han ingresado de forma individual
    for( i = 1; i <= cantidadNumber; i++){
        
        const numeroProductos = await productosAlmacenStock.find({productosFI: body.productosFI});
        // obtenemos el total de los productos registrados, de ahi sacamos lo que es el lote unitario que

        const productosGuardados = new productosAlmacenStock({
            // productFI es el id del modelo de la tabla de productos admin
 
            productosFI: body.productosFI,
            lote: body.lote,
            proveedor: body.proveedor,
            factura: body.factura,
            fecha_caducidad: body.fecha_caducidad,
            lote_unitario: numeroProductos.length +1,
            costoReal: body.costoReal,
            precioVenta:body.precioVenta,
            seDesecha: body.seDesecha ,
            motivoDesecho: body.motivoDesecho ,
            procesoDesecha:body.procesoDesecha,
            //lote_unitario: body.lote_unitario,
            // agregamos el lote unitario 
        });
    
        try {            
            const productosEnStock = await productosGuardados.save();
            // guardamos cada item de los productos de forma individual
            // /vender/producto/stock/:id
            // generamos el codigo Qr del producto
            QRcode.toDataURL( "https://backendpruebashorizonte.herokuapp.com/vender/producto/stock/"+productosEnStock._id, async(error, code) => {
                
                if(error) {
                    serveResp(null, error, 'No se crearon los productos', resp);
                }
                // actualizamos el campo de QRs con la imagen que se genra en el paso anterior
                const productosConQr = await productosAlmacenStock.findOneAndUpdate({_id: productosEnStock._id}, {codeQr_img: code} );
                // console.log(productosConQr);
                
                // return resp.status(200)
                // .json({
                //     ok: true,
                //     message: 'Se guardaron los productos',
                // });
                
                const lastPrice = await productosAlmacenStock.find({ productosFI: body.productosFI });

                if( lastPrice[0].precioVenta < body.precioVenta ){
                   await productosAlmacenStock.updateMany({ }, { $set: {  precioVenta: body.precioVenta } });

                }else if (lastPrice[0].precioVenta > body.precioVenta) {
                     await productosAlmacenStock.updateMany({ }, { $set: {  precioVenta: lastPrice[0].precioVenta } });
                }

            });

        } catch (error) {       
             console.log(error); 
             logMemory('/agregar/productos/almacen/stock', true, error);
            // Si falla mandamos el error al front
            serveResp(null, error, 'No se crearon los productos', resp);
            
        }
    }

    
    const respUpdatePrice = await productosAlmacenStock.find({ productosFI: body.productosFI });

    serveResp(respUpdatePrice, null, 'Se actualizo el stock', resp  );
                
});

// Este servicio se encarga de cambiar el estado de los productos a vendido para quitar del stock
app.get('/vender/producto/stock/:id', async (req, resp) => {
    // TODO: SI SE IMPLEMENTA LA APP QUITAR ESTE SERVICIO
    const { id } = req.params;
    
    // console.log(id, "Req ID" + Date.now());
    try {
        
        const productoEncontrado = await productosAlmacenStock.findById( id )
        
        if(productoEncontrado){
            const productoVendido = await productosAlmacenStock.findByIdAndUpdate( id, {status: 'VENDIDO'},  { new: true } );
            console.log(productoVendido, "producto vendido" + Date.now())
            serveResp(productoVendido, null, 'Producto vendido', resp);
        }else {

            serveResp(null, 'ID NO VALIDO', 'El ID NO ES CORRECTO', resp);  
        }

        

    } catch (error) {

         console.log(error);   
        serveResp(null, error, 'Error de servidor', resp);  
    }
});


app.post('/vender/producto/stock/:id', async (req, resp) => {
    
    const { id } = req.params;
    
    // console.log(id, "Req ID" + Date.now());
    try {
        
        const productoEncontrado = await productosAlmacenStock.findById( id )
        
        if(productoEncontrado){
            const productoVendido = await productosAlmacenStock.findByIdAndUpdate( id, {status: 'VENDIDO'},  { new: true } );
            console.log(productoVendido, "producto vendido" + Date.now())
            serveResp(productoVendido, null, 'Producto vendido', resp);
        }else {

            serveResp(null, 'ID NO VALIDO', 'El ID NO ES CORRECTO', resp);  
        }

        

    } catch (error) {

         console.log(error);   
        serveResp(null, error, 'Error de servidor', resp);  
    }
});
// obtenemos  todos los productos que hay en el stock
app.get('/obtener/todos/produtos/stock', async(req, resp) => {
    
    try {
        const productos = await productosAlmacenStock
                                .find()
                                .sort({fecha_caducidad: 1})
        serveResp(productos,null, 'Se encontraron los productos', resp);        
    } catch (error) {
        /* console.log(error);  */
        serveResp(null,error, 'No se encontraron los productos', resp);
    }

});


app.get('/desactivar/producto/almacen/:id', async (req, resp) => {

    const { id } = req.params;
    try {
        const productoDisabled = await almacenProductos.findOneAndUpdate( {_id: id},  {estado: 'INACTIVO'} );
    
        serveResp(productoDisabled, null, 'Se desactivo el producto', resp);
    } catch (error) {
    
        /* console.log(error); */
        serveResp(null,error, 'No se pudo desactivar el producto', resp);
    }
    
});



/**
 * RUTAS DE MATERIALES EN BS
 * 
 */

app.post('/agregar/nuevo/material/banco/sangre', async (req, resp) => {

    const { body } = req;

    try {

        const productNew = new almacenProductos(body);
        const productsSaved = await productNew.save();

        serveResp(productsSaved, null, 'Se agrego el producto', resp);

    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se agrego el producto', resp);

    }
});

/**
 * 
 * OBTENER TODOS LOS MATERIALES
 */

app.get('/ver/materiales/all',async (req, resp) => {

    try {
            
        const materialesAll = await almacenProductos.find({ productoMatriales: 'MATERIAL' });
        serveResp(materialesAll, null, 'Se encontraron los materiales', resp);


    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se encontraron materiales', resp);
    }
});

/**
 *  BUSCAR POR NOMBRE ENTRE MATERIALES Y PRODUCTOS
*/


app.post('/buscar/nombre/productos/materiales', async (req, resp) => {

    const {nombre} = req.body;

    try {
        // let regexp = new RegExp(name.trim(), 'i');
        // let regexp = new RegExp(nombre.trim(), 'i');
        // const productsAndMaterials = await almacenProductos.find({ nombre: {$regex:  regexp}, estado: 'ACTIVO'});
        const productsAndMaterials = await almacenProductos.find({ $or:[ {nombre:{ $regex: '.*' + nombre + '.*'}}, {nombre_comercial:{ $regex: '.*' + nombre + '.*'}}] , estado: 'ACTIVO'});
        serveResp(productsAndMaterials, null, 'Se encontraron los produtos y materiales', resp);
    } catch (error) {

        console.error(error);
        serveResp(null, error, 'No se encontraron', resp);
    }
});


/**
 * Buscamos un material por el ID
 */

app.get('/buscar/materiales/banco/sangre/:id', async(req, resp) => {
    const { id } = req.params;
    
    try {
        const materialResp = await almacenProductos.find( {_id: id, status: 'ACTIVO'} );
        serveResp(materialResp, null, 'Se encotro el material', resp);
    } catch (error) {

        /* console.log( error ); */
        serveResp(null, error, 'No se pudo conectar', resp);
    }
});


/**
 * Buscamos todos los materiale para la bitacora
 */

app.get('/materiales/banco/sangre', async(req, resp) => {

    try {
        
        const materialesResp = await almacenProductos.find({ productoMatriales: "MATERIAL", estado: 'ACTIVO' }).sort({_id:-1});
        serveResp(materialesResp, null, 'Se encontraron los materiales', resp);

    } catch (error) {
        /* console.log( error ); */
        serveResp(null, error, 'No se pudo conectar', resp);
    }

});


app.get('/count/all/materiales/:token', async(req, resp) => {

    //* Se comenta esta fucnion en lo que se crea el endpoint del token renew, porque daba conflictos
   // verificaToken(req, resp)
    
    if( req.usuario  ) {
        
        try {

            const materialesCount  = await almacenProductos.find({ estado : 'ACTIVO',  productoMatriales : 'MATERIAL'} );
          
            const countJson = { count: materialesCount.length }
        
            serveResp(countJson, null, 'Count de los materiales', resp);
            
        } catch (error) {
            /* console.log( error); */
            serveResp(null, error, 'No se pudo hacer el count', resp);
        }

    }

   
});

module.exports = app