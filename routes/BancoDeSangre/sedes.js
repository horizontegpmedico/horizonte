const express = require('express');
const app = express()
const serveResp = require('../../functions/functions');
const Bancosedes = require('../../models/bancoDeSangre/modelSedes');
const CarritoCensur = require('../../models/almacen/stock/carritoCensur');
app.post('/crear/sede/bs', (req, resp) => {

    const body = req.body

    const sedes = new Bancosedes({
        imagencli: body.imagencli,   
        clinica: body.clinica,
            razon: body.razon,
            rfc: body.rfc,
            contacto: body.contacto,
            genero: body.genero,
            fecha: body.fecha,
            telefono: body.telefono,
            usuario: body.usuario,
            contrasena: body.contrasena,
            correo: body.correo,
            responsable_sa: body.responsable_sa,


            calle: body.calle,
            no_exterior: body.no_exterior,
            postal: body.postal,

            pais: body.pais,
            estado: body.estado,
            municipio: body.municipio,
            ref_1: body.ref_1,
            ref_2: body.ref_2,  
            razSoc: body.razon_em,
            rfc_cem: body.rfc_em,
            postal_em: body.postal_em,
            estado_em: body.estado_em,
            municipio_em: body.municipio_em,
            calle_em: body.calle_em,
            exterior_em: body.exterior_em,
            correo_em: body.correo_em,
            archivos: body.archivos,   
            codsede: body.codsede
         

    })
    sedes
    .save()
    .then((data) => serveResp(data, null, 'Se guardó la sede correctamente', resp))
    .catch((error) => serveResp(null, error, 'No se guardó la sede' + error, resp));
})
app.get('/listado/sedes/banco/sangre/', (req, resp) => {
    const body = req.body
    Bancosedes
    .find()
    .then((data) => {
        serveResp(data, null, 'todos las sedes', resp)
    }).catch((error) => {
        serveResp(null, error, 'No se encontraron las sedes' + error, resp)
    });
})
app.get('/detalle/sede/:id', (req, resp) =>{
    
    const {id} = req.params;

    Bancosedes
        .find( {_id: id }  )
        .exec()
        .then( (result) =>serveResp(result,null ,"Se encontro la sede", resp))
        .catch( (err) => serveResp(null, err, "No se encontro la sede", resp) );
});
app.get('/ver/sede', (req, resp) => {
    Bancosedes
        .find()
        .sort({ responsable_sa: 1 })
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron las sedes', resp))
        .catch(error => serveResp(null, error, 'No se encontraron sedes', resp))
});

app.get('/ver/sedes/:id', (req, resp) => {
    const {id} = req.params;
    CarritoCensur
        .find({idBanco: id})
        .exec()
        .then(data => serveResp(data, null, 'Se encotró pedidos de la sede', resp))
        .catch(error => serveResp(null, error, 'No se encontraron pedidos de la sede', resp))
});


module.exports = app
