const express = require('express')
const app = express()
//modelos requeridos
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const hojaEvolucion = require('../../models/bancoDeSangre/modelHojaEvolucion')
//Funcion de terceros 
const serveResp = require('../../functions/functions')
//se agrega la flebotomia en el banco de sangre del disponente
app.post('/agregar/hojadeevolucion', (req, resp) => {
    const body = req.body
    const hojaevolucion = new hojaEvolucion({
        hojaevolucion: body.idbancosangre,
        piel: body.piel,
        mucosas: body.mucosas,
        torax: body.torax,
        abdomen: body.abdomen,
        higado: body.higado,
        bazo: body.bazo,
        observaciones_explofisi: body.observaciones_explofisi,
        ganglios: body.ganglios,
        observaciones_explofisidos: body.observaciones_explofisidos,
        candidato_donacion: body.candidato_donacion,
        observaciones_diagnostico: body.observaciones_diagnostico,
        cie10: body.cie10 
    })
    hojaevolucion
    .save()
    .then((data) => {
        if (data) {
            const hojaevolucion = {
                hojaevolucion: data._id
            }
            bancoDeSangre
            .findByIdAndUpdate(body.idbancosangre, { $push: hojaevolucion })
            .exec()
            .then(data => serveResp(data, null, 'Se agregaro la hoja de evolucion al banco de sangre del disponente', resp))
            .catch(error => serveResp(null, error, 'No se pudo agregar la hoja de evolucion al banco de sangre del disponente', resp))
        } else {
                serveResp(data, null, 'Se le agrego la hoja de evolucion se guardo correctamente', resp)
        }
    })
    .catch((error)   => serveResp(null,error, 'No se pudo guardar la hoja de evolucion', resp));

})
module.exports = app