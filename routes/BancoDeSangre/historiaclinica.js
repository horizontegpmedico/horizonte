const express = require('express')
const app = express()
//modelos requeridos
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const historiaClinica = require('../../models/bancoDeSangre/modelHistoriaClinica');
//Funcion de terceros 
const serveResp = require('../../functions/functions')
//se agrega la historia clinica junto con el id del banco de sangre del disponente
app.post('/agregar/historiaclinica', (req, resp) => {
    const body = req.body
    const historiaclinica = new historiaClinica({
        historiaclinica: body.idbancosangre,
        nacionalidad: body.nacionalidad,
        viajes: body.viajes,
        enfermos_hepatitis: body.enfermos_hepatitis,
        vih_deteccion: body.vih_deteccion,
        hepatitisb_deteccion: body.hepatitisb_deteccion,
        hepatitisc_deteccion: body.hepatitisc_deteccion,
        enfermedad_de_transmision: body.enfermedad_de_transmision,
        odinofagia: body.odinofagia,
        alcohol_consumido: body.alcohol_consumido,
        medicamento_regular: body.medicamento_regular,
        consume_etretinato: body.consume_etretinato,
        tratamiento_dental: body.tratamiento_dental,
        cirugia_mayor: body.cirugia_mayor,
        cirugia_menor: body.cirugia_menor,
        alergias: body.alergias,
        inmunizaciones: body.inmunizaciones,
        cardiopatias: body.cardiopatias,
        efermedades_renales: body.efermedades_renales,
        coagulopatias:  body.coagulopatias,
        cancer:      body.cancer,
        neoplasia_hematologica: body.neoplasia_hematologica,
        anemia:         body.anemia,
        infecciones_bacteo: body.infecciones_bacteo,
        lepra: body.lepra,
        paludismo: body.paludismo,
        brucelosis: body.brucelosis,
        diabetes: body.diabetes,
        hipertension_arte: body.hipertension_arte,
        tuberculosis:  body.tuberculosis,
        epilepsia: body.epilepsia,
        hepatitis: body.hepatitis,
        icteria:  body.icteria,
        transtormental: body.transtormental,
        toxoplasmosis: body.toxoplasmosis,
        transplantes: body.transplantes,
        covid: body.covid,
        otros_patologicos: body.otros_patologicos,
        ultima_relacion: body.ultima_relacion,
        gestas: body.gestas,
        partos: body.partos,
        cesareas: body.cesareas,
        abortos: body.abortos,
        ultimo_parto: body.ultimo_parto,
        ultimo_aborto: body.ultimo_aborto,
        isoinmunizacioes: body.isoinmunizacioes,
        globulina: body.globulina,
        transfusiones_prev: body.transfusiones_prev,
        pago_por_donar_sangre: body.pago_por_donar_sangre,
        uso_drogas: body.uso_drogas,
        heterosecual_promiscuo: body.heterosecual_promiscuo,
        homoxesual: body.homoxesual,
        bisexual: body.bisexual,
        pago_por_relaciones: body.pago_por_relaciones,
        contacto_con_hemofilicos: body.contacto_con_hemofilicos,
        instituciones_pentales: body.instituciones_pentales,
        acupunturas: body.acupunturas,
        tatuajes: body.tatuajes,
        enfermedades_itc: body.enfermedades_itc,
        parejas_sexuales: body.parejas_sexuales,
        ultimo_año_de_parejas: body.ultimo_año_de_parejas,
        ultimos_cinco_años_de_parejas: body.ultimos_cinco_años_de_parejas,
        tos_disnea: body.tos_disnea,
        perdida_de_peso: body.perdida_de_peso,
        diarrea_cronica: body.diarrea_cronica,
        diaforesis_cronica: body.diaforesis_cronica,
        diaforesis_profusa: body.diaforesis_profusa,
        astenia: body.astenia,
        adenomegalias: body.adenomegalias,
        herpes_mucocutaneo: body.herpes_mucocutaneo,
        fiebre_continua: body.fiebre_continua,
        odinofagia_diez_dias: body.odinofagia_diez_dias,
        sindrome_diarreico: body.sindrome_diarreico,
        isotretinoina_ultimo_mes: body.isotretinoina_ultimo_mes,
        covid_ultimo_mes: body.covid_ultimo_mes,
        medicamentos_ultima_semana: body.medicamentos_ultima_semana,
        infecciones_agudas_ultima_semana: body.infecciones_agudas_ultima_semana,
        fiebre_ultimos_dos_dias: body.fiebre_ultimos_dos_dias,
        ejercicio_intenso_ultimos_dos_dias: body.ejercicio_intenso_ultimos_dos_dias,
        ayuno_ultimos_dos_dias: body.ayuno_ultimos_dos_dias,
        vigilia_ultimos_dos_dias: body.vigilia_ultimos_dos_dias,
        ingesta_de_alcohol_dos_dias: body.ingesta_de_alcohol_dos_dias,
        medicamento_acitretina: body.medicamento_acitretina,
        medicamento_tamoxifeno: body.medicamento_tamoxifeno,
        medicamento_dutasterida: body.medicamento_dutasterida,
        medicamento_finasterida: body.medicamento_finasterida,
        medicamento_isotretinoina: body.medicamento_isotretinoina,
        medicamento_tertraciclina: body.medicamento_tertraciclina,
        medicamento_tretinoina: body.medicamento_tretinoina,
        medicamento_talidoida: body.medicamento_talidoida,
        medicamento_acido_acetil_salicilico: body.medicamento_acido_acetil_salicilico,
        medicamento_clopidogrel: body.medicamento_clopidogrel,
        medicamento_diflunisal: body.medicamento_diflunisal,
        medicamento_fenilbutazona: body.medicamento_fenilbutazona,
        medicamento_meloxicam: body.medicamento_meloxicam,
        medicamento_nabumetona: body.medicamento_nabumetona,
        medicamento_naproxeno: body.medicamento_naproxeno,
        medicamento_sulindaco: body.medicamento_sulindaco,
        medicamento_tenoxicam: body.medicamento_tenoxicam,
        medicamento_aceclofenaco: body.medicamento_aceclofenaco,
        medicamento_acetamicin: body.medicamento_acetamicin,
        medicamento_acido_mefenamico: body.medicamento_acido_mefenamico,
        medicamento_diclofenaco: body.medicamento_diclofenaco,
        medicamento_dexibuprofen: body.medicamento_dexibuprofen,
        medicamento_flubiprofeno: body.medicamento_flubiprofeno,
        medicamento_ibuprofeno: body.medicamento_ibuprofeno,
        medicamento_indometacina: body.medicamento_indometacina,
        medicamento_ketoprofeno: body.medicamento_ketoprofeno,
        medicamento_ketorolaco: body.medicamento_ketorolaco,
    })
    .save()
    .then((data) => {
        if (data) {
            const historiaclinica = {
                historiaclinica: data._id
            }
            bancoDeSangre
            .findByIdAndUpdate(body.idbancosangre, { $push: historiaclinica })
            .exec()
            .then(data => serveResp(data, null, 'Se agregaro la historia clinica al banco de sangre del disponente', resp))
            .catch(error => serveResp(null, error, 'No se pudo agregar la historia clinica al banco de sangre del disponente', resp))
        } else {
          serveResp(data, null, 'Se le agrego la historia clinica al paciente', resp)  
        }
    })
    .catch((error)   => serveResp(null,error, 'No se pudo agregar la historia clinica al paciente', resp));
})
module.exports = app