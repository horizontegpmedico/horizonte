const express = require('express');
//modelos requeridos
const paciente = require('../../models/nuevoPaciente');
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre');
const laboratorioBS = require('../../models/bancoDeSangre/modelLaboratorio');
const resultadosLaboratorio = require('../../models/bancoDeSangre/modelResultadoLaboratorio')
    //Funcion de terceros 
const app = express();


const serveResp = require('../../functions/functions');
//se agrega el lugar de origen al disponente
app.put('/agregar/lugardeorigen', (req, resp) => {
    const body = req.body;
    paciente
        .findOneAndUpdate({ '_id': body.idpaciente }, {
            '$set': {
                "lugardeorigen": body.lugarorigen
            }
        })
        .exec()
        .then(data => serveResp(data, null, 'Se agrego el lugar de origen del disponente', resp))
        .catch(error => serveResp(null, error, 'No se pudo agregar el lugar de origen del disponente', resp));
});
//se actualiza el producto al disponente
app.put('/agregar/productodeorigen', (req, resp) => {
    const body = req.body;
    paciente
        .findOneAndUpdate({ '_id': body.idpaciente }, {
            '$set': {
                "disponente": body.disponente
            }
        })
        .exec()
        .then(data => serveResp(data, null, 'Se agrego el lugar de origen del disponente', resp))
        .catch(error => serveResp(null, error, 'No se pudo agregar el lugar de origen del disponente', resp));
});
//se agrego el laboratorio en el banco de sangre del disponente
app.post('/ordenar/laboratorios', (req, resp) => {
    const body = req.body
    /* console.log(body) */
    const laboratorios = new laboratorioBS({
        laboratorio: body.idbancodesangre,
        obtenidos: body.obtenidos,
        pedido: body.pedido,
        metodo: body.metodo,
        quimico: body.quimico,
        tecnico: body.tecnico,
        cedula: body.cedula,
        proceso: body.proceso,
    })
    laboratorios
        .save()
        .then((data) => {
            const laboratorio = {
                laboratorio: data._id
            }
            bancoDeSangre
                .findByIdAndUpdate(body.idbancodesangre, { $push: laboratorio })
                .then(async(data) => {
                    const bslab = await bancoDeSangre.find({ _id: body.idbancodesangre })
                    /* console.log(data)
                    console.log("laboratorios agregados al banco", bslab) */
                    serveResp(bslab, null, 'Se guardaron los laboratorios', resp)
                }).catch((error) => {
                    serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
                });
        }).catch((error) => {
            serveResp(null, error, 'No se guardaron los valores obtenidos' + error, resp)
        });
})
module.exports = app;