const express = require('express')
const app = express()
//modelos requeridos
const bancoDeSangre = require('../../models/bancoDeSangre/modelBancoSangre')
const reaccionesDerivadas = require('../../models/bancoDeSangre/modelReaccionesDerivadas');
//Funcion de terceros 
const serveResp = require('../../functions/functions')
//se gregan las reacciones derivadas en el banco de sangre del disponente
app.post('/agregar/reaccionesderivadas', (req, resp) =>{
    const body = req.body;
    const reaccionesDerivadas = new reaccionesDerivadas({
        paciente:   body.idpaciente,
        tiposangreado: body.tiposangreado,
        contenidoporaferesis: body.contenidoporaferesis,
        iniciodereaccion: body.iniciodereaccion,
        tipodonante: body.tipodonante,
        presentacionreacciones: body.presentacionreacciones,
        antecedentesreacciones: body.antecedentesreacciones,
        reaccionvasovagalleve: body.reaccionvasovagalleve,
        reaccionvasovagalmoderado: body.reaccionvasovagalmoderado,
        reaccionvasovagalsevera: body.reaccionvasovagalsevera,
        reaccionvasovagalleveotros: body.reaccionvasovagalleveotros,
        reaccionvasovagalmoderadootros: body.reaccionvasovagalmoderadootros,
        reaccionvasovagalseveraotros: body.reaccionvasovagalseveraotros,
        reaccionvenopuncionleve: body.reaccionvenopuncionleve,
        reaccionvenopuncionmoderado: body.reaccionvenopuncionmoderado,
        reaccionvenopuncionsevera: body.reaccionvenopuncionsevera,
        reaccionvenopuncionleveotros: body.reaccionvenopuncionleveotros,
        reaccionvenopuncionmoderadootros: body.reaccionvenopuncionmoderadootros,
        reaccionvenopuncionseveraotros: body.reaccionvenopuncionseveraotros,
        reacciontoxicidadleve: body.reacciontoxicidadleve,
        reacciontoxicidadmoderado: body.reacciontoxicidadmoderado,
        reacciontoxicidadsevera: body.reacciontoxicidadsevera,
        reacciontoxicidadleveotros: body.reacciontoxicidadleveotros,
        reacciontoxicidadmoderadootros: body.reacciontoxicidadmoderadootros,
        reacciontoxicidadseveraotros: body.reacciontoxicidadseveraotros,
        tiempodepresentacion: body.tiempodepresentacion,
        resultadosLabCalcio:body.resultadosLabCalcio,
        resultadosLabMagnesio:body.resultadosLabMagnesio,
        tratamiento: body.tratamiento,
    })
    reaccionesDerivadas
    .save()
    .then(data => {
        if (data) {
        const reaccionesderivadas = {
            reaccionesderivadas: data._id
        }
        bancoDeSangre
        .findByIdAndUpdate(body.idbancosangre, {$push: reaccionesderivadas})
        .exec()
        .then(data => serveResp(data, null, 'Se agregaron las reacciones derivadas al banco de sangre del disponente', resp))
        .catch(error => serveResp(null, error, 'No se pudo agregar las reacciones derivadas al banco de sangre del disponente', resp))
        } else {
            serveResp(data, null, 'Se agregaro las reacciones derivadas', resp)
        }
    })
    .catch(error => serveResp(null, error, 'No se pudo agregar las reacciones derivadas', resp));

})

module.exports = app