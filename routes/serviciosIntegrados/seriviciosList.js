
const serveResp = require('../../functions/functions');
const serviciosFi = require('../../models/almacen/servicios/serviciosFi');
const sedes = require('../../models/sedes/sedes')

const express = require('express');
const app = express();



app.post('/crear/servicio/almacen', async (req, res) => {

    const { body } = req;

    try {

        const serviceNew = new serviciosFi(body);
        const servicioSaved  = await serviceNew.save();

        serveResp(servicioSaved, null, 'Se creo el servicio', res);

    } catch (error) {
        
        console.log(error);
        serveResp( null, error, 'No se pudo conectar', res);
    
    
    }
});


app.get('/ver/servicios/almacen', async (req, res) => {

    try {

        const serviciosSaved = await serviciosFi.find();
        const sedesSaved = await sedes.find();

        const pedidosServicios = [ ...serviciosSaved, ...sedesSaved]

        serveResp( pedidosServicios, null, 'Se encontaron los servicios y sedes', res );

    } catch (error) {

        console.log(error);
        serveResp( null, error, 'No se pudo conectar', res);
    }
    
});

module.exports = app;