//libs
const express = require('express');

//models 
const modeloServicios = require('../../models/servicios/modeloServicios');
const modeloHorarios = require('../../models/servicios/horariosServicios');
const ambulancia = require('../../models/servicioIntegrados/modelAmbulancia');
const posadaServices = require('../../models/servicios/posadaServices');

//fucntions 
const serveResponse = require('../../functions/functions');
const { logMemory  } = require('../../config/logs');

//app
const app = express();

app.post('/nuevo/servicio', async (req, resp ) => {

    const {body} = req;        

    console.log( '/nuevo/servicio' , body );

    try{

        // const newService = new modeloServicios(body);
        // const respService = await  newService.save();

        //se crea el servicio de horario 

        const newService = new modeloServicios(body);
        const respService = await  newService.save();

        if( respService ){

                return resp.status(201).json({
                    ok:true,
                    message: "Se creo el servicio",
                    data: respService
                });
        }
        
        return resp.status(500).json({
            ok:false,
            message: 'Hubo un error',
        });

    }catch (error) {

        console.log( error );
        logMemory('/nuevo/servicio', false, error);
        
        return resp.status(501).json({
            ok: false,
            message:"Hubo un error", 
            error
        });
    }

});

//obtenemos tofdos los servicios para los procesos que ya se llevan dentro de horizonte 
app.get('/servicio', async (req, resp) => {

    
    try{

        const serviciosDB = await  modeloServicios.find().populate('tipoDeServicio');

        if(serviciosDB.length > 0){
            return resp.status(201).json({
                ok: true,
                message: 'Se encontraron los servicios',
                data: serviciosDB[0]
            });
        
        }else {
            return resp.status(500).json({
                ok: false,
                message: 'No hay servicios'
            })
        }


    }catch (error) {

        console.log(error);   
        logMemory('/servicio', false, error);

        return resp.status(500).json({
            ok: false,
            message: 'Hubo un error',
            error
        })
    }
        


});




app.get('/servicio/:names', (req, resp) => {
    const {names} = req.params;
    modeloServicios
        .getName({name: names})
        .exec()
        .then(data => serveResp(data, null, 'Se encontró lista de servicios de ', resp))
        .catch(error => serveResp(null, error, 'No se encontró lista de servicios de ', resp))
});

app.get('/servicio/uno/:id', (req, resp) => {


    let id = req.params.id;

    modeloServicios
        .findById({ _id: id })
        .exec()
        .then((data) => serveResp(data, null, 'Se encontro el servicio', resp))
        .catch((error) => serveResp(null, error, 'No se enontro el servicio', resp));

});


app.delete('/servicio/:id', (req, resp) => {
    let id = req.params.id;
    modeloServicios
        .findByIdAndDelete({ _id: id })
        .exec()
        .then((data) => serveResp(data, null, 'Se elimino el servicio', resp))
        .catch((error) => serveResp(null, error, 'No se pudo eliminar el servicio', resp));

});


app.put('/editar/servicio/:id', async(req, resp) => {
    
    const {id} = req.params;
    const {body} = req;

    console.log(`/editar/servicio/${id}`, body);

    try {
        
        const servicioToUpdate = await modeloServicios.findByIdAndUpdate(id, body, { new:  true } );

        if(servicioToUpdate) {

            //TODO: ESTE SERVICIO SE DEBE DE MODIFICAR. QUITAR EL CREATE DEL HORARIO POR EL UPDATRE DEL MISMO
            const  horarioServiciosSaved = new modeloHorarios( body.horario );
            const  horarioSaved  = await horarioServiciosSaved.save();
            //*LINEAS A MODIFICAR


            //TODO: VER EL REQUEST DE LAS FECHAS Y LAS HORAS PARA VER COMO LAS USA LA LIBRERIA 
           

           if(horarioSaved){
                return resp.status(201).json({
                    ok: true,
                    message: 'Se actualizo el servicio',
                    data: horarioSaved
                })
           }

        }


        return resp.status(500).json({
            ok: false,
            message: 'No se pudo crear el servicio'
        });



    } catch (error) {

        console.log( `/editar/servicio/${id}`, error) 
        logMemory(`/editar/servicio/${id}`,false, error );
        serveResp(null, error, 'No se actualizo', resp);
    
    }
        
    
    // modeloServicios
    //     .findByIdAndUpdate({ _id: id }, body)
    //     .then((data) => {
    //         return resp.status(200)
    //             .json({
    //                 ok: true,
    //                 message: 'Se actualizo la informacion de servicio',
    //                 data
    //             })
    //     })
    //     .catch(
    //         (error) => {
    //             return resp.status(500)
    //                 .json({
    //                     ok: false,
    //                     message: 'No se pudo actualizar',
    //                     error
    //                 })
    //         }
    //     )
    //     .then((data) => serveResp(data, null, 'Se actualizo el servicio'),resp)
    //     .catch((error) => serveResp(null, error, 'No se pudo actualizar el servicio', resp))

});


app.put('/actualizar/elementos/:id', (req, resp) => {

    const id = req.params.id;
    const body = req.body;
    // console.log(body);


    modeloServicios
        .findByIdAndUpdate({ _id: id }, { ELEMENTOS: body.grupos })
        .exec() 
        .then(data => serveResp(data, null, 'Se actualizaron los estudios', resp))
        .catch(error => serveResp(null, error, 'No se actualizaron los elementos', resp))
});

app.get('/ver/categoria/servicios/:servicio', (req, resp) => {

    const servicio = req.params.servicio;

    modeloServicios
        .find({ name: servicio }).sort({ ESTUDIO: 1  })
        .exec()
        .then((data) => serveResp(data, null, 'Se encintro la categoria', resp))
        .catch((error) => serveResp(null, error, 'No se pudo encontrar la categoria', resp))

});


// busqueda de todos los serivicioas

app.post('/buscar/servicio', (req, resp) => {

    const body = req.body;

    const regexp = new RegExp(body.estudio, 'i');

    Promise.all([
            buquedaAmbulancia(regexp),
            busquedaServicios(regexp)
        ])
        .then(data => serveResp(data, null, 'Se encontraron servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontraron servicios', resp))
});


app.get('/ver/precios/membresia/app', (req, resp) => {


    modeloServicios
        .find({}, "ESTUDIO PRECIO_MEMBRESIA")
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron precios de la app', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los precios', resp))


});

app.get('/ver/precios/app', (req, resp) => {

    modeloServicios
        .find({}, "ESTUDIO PRECIO_PUBLICO PRECIO_MEMBRESIA")
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron precios de la app', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los precios', resp))

});


// busqueda de todos los serivicioas

app.post('/buscar/servicio', (req, resp) => {

    const body = req.body;
    const regexp = new RegExp(body.estudio, 'i');

    modeloServicios
        .find({ name: servicio }, "ESTUDIO PRECIO_MEMBRESIA  PRECIO_MEMBRESIA_URGENCIA PRECIO_MEMBRESIA_HOSPITALIZACION  PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA name")
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los servicios con membresia', resp))
        .catch(error => serveResp(null, error, 'No se pudo conectar', resp))

    Promise.all([
            buquedaAmbulancia(regexp),
            busquedaServicios(regexp)
        ])
        .then(data => serveResp(data, null, 'Se encontraron servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontraron servicios', resp))


});


app.get('/obtener/servicios/membresia/:servicio', (req, resp) => {


    const servicio = req.params.servicio;
    modeloServicios
        .find({ name: servicio }, "ESTUDIO PRECIO_MEMBRESIA PRECIO_MEMBRESIA_HOSPITALIZACION PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA PRECIO_MEMBRESIA_URGENCIA name")
        .exec()
        .then(data => serveResp(data, null, 'Se enonctraron los servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontraron servicios', resp));

});


// obtenemos todos los servicios con su precio de membresia
app.post('/obtener/servicios/membresia', (req, resp) => {

    const body = req.body;
    const regx = new RegExp(body.estudio, 'i');

    Promise.all([
            busquedaAmbulanciaMembresia(regx),
            busquedaServiciosMembresia(regx)

        ])
        .then(data => serveResp(data, null, 'Servicios con precios de membresia', resp))
        .catch(error => serveResp(null, error, 'No se encontraron servicios con membresia', resp));



}); 

app.post('/ver/precios/membresia/receta', (req, resp) => {

    const body = req.body;

    const regx = new RegExp(body.estudio, 'i');

    modeloServicios
        .find({ ESTUDIO: regx }, "ESTUDIO PRECIO_MEMBRESIA name")
        .exec()
        .then(data => {
            // console.log(data);
            serveResp(data, null, 'Se encontro el servicio desde la receta', resp)
        })
        .catch(error => serveResp(null, error, 'No se encontro el servicio', resp));
}); 

app.get('/ver/servicios/posada/:name', async (req, resp) => {

    try {
        const name  = req.params.name
        const servicesPosada = await posadaServices.find({name: name})

        if( servicesPosada != [] ){
            serveResp( servicesPosada, null, 'Se encontraron los servicios', resp);

        }else {
            serveResp(null, error, 'No hay servicios', resp);
        }
        
    } catch (error) {
        // console.log(error);
        serveResp(null, error, 'No se econtraron servicios', resp);
        
    }
});

// habitaciones disponibles en la posada
app.get('/posada/habitaciones/disponibles', async (req, resp)=>{

    try {
    
        //buscamos por los 2 parametros de busqueda
        const habitacionesDisponibles = await modeloServicios.find({ name:"posada", status:"DISPONIBLE" });

        // validamos que haya una respuesta valida
        if( habitacionesDisponibles.length === 0  ) {
       
            serveResponse(null,'No hay habitaciones' ,'No hay habitaciones disponibles', resp);
            
        }else{
            serveResponse(habitacionesDisponibles, null,'Habitaciones disponibles', resp);
        }

    } catch (error) {
        // console.log(error);
        serverResponse(null, error, 'No se pudo conectar', resp);
    }
});


/*
    RUTAS DE LOS SERVICIOS DE LA POSADA
*/

//cambiar el status de la habitacion de la posada


app.post('/nuevo/servicio/posada', async(req, resp) => {

    const { body } = req;

    try {

        const createdNewServices = new posadaServices(body);
        const createdServices = await createdNewServices.save();
        // console.log(createdServices)
        
        if(createdServices != null || createdServices != []) {
            
            serveResp(createdServices, null, 'Se creo el servico', resp);
        
        }else {
            serveResp(null , 'No se pudo crear', 'Servicio no creado', resp);
       
        }

    } catch (error) {
        // console.log(error);
        serveResponse(null, error, 'No se pudo conectar', resp);
    }
});


// cambio de estado del servicio
app.get('/cambiar/estados/habitacion/:id', async(req, resp)=> {

    const { id} = req.params;
    
    try {
        

        const responseChangeStatusRoom = await posadaServices.findById(id);
        
        if( responseChangeStatusRoom == [] || responseChangeStatusRoom == null){
            serveResponse( null, 'No es un ID valido de haitacion', resp );
        }else { 


            if( responseChangeStatusRoom.status !== 'DISPONIBLE'){

                // console.log("if")
              const changeStatusAvalible = await posadaServices.findByIdAndUpdate(id, { status: 'DISPONIBLE' }, { new: true });
            //   console.log(changeStatusAvalible)
                serveResponse( changeStatusAvalible, null, 'Nuevo estado', resp );

            }else {
                // console.log("else")
                const changeStatusNotAvalible  = await  posadaServices.findByIdAndUpdate(id, { status: 'OCUPADO' }, { new: true});
                // console.log( changeStatusNotAvalible )
                serveResponse( changeStatusNotAvalible, null,  'Se actualizo el estado', resp );
            
            }

        }
        

    } catch (error) {
        console.log(error);
        serveResponse(null, error, 'Algo paso', resp);
    }

});





//* Obtenemos los horarios del modelo de los horarios 
app.post('/horario/servicios', async(req, resp) => {

    const { busqueda } = req.body;

    
    try{
        
        if(  busqueda  ){
            
            console.log( busqueda.toUpperCase(), '/horario/servicios'  );
            const horarios = await modeloHorarios.find({ nombreDeServicio: busqueda.toUpperCase(), status: 'ACTIVO' });

            if(horarios){
                return resp.status(200).json({
                    ok: true,
                    message: 'Se encontraron los hoarios',
                    data: horarios  
                });

            }   else {

                return resp.status(400).json({
                    ok: false,
                    message: 'No hay un servicio activo con el termino de buqueda',
                    busqueda
                })
            }

        }


    }catch (error) {
        console.log('/horario/servicos', error );

        return resp.status(400).json({
            ok: false,
            message: 'Hubo un error',
            error
        })
    }
    
});



//? SE OBTIENE SOLO EL NOMBRE DEL DEPARTAMENTO PARA QUE SE PINTE DE FORMA DINAMICA
app.get('/servicios/listado/departamento', async(req, resp) => {

    try{
        const serviciosDB = await  modeloHorarios.find({ status: 'ACTIVO' }, 'nombreDeServicio');

        if( serviciosDB.length > 0) {

            return resp.status(201).json({
                ok: true,
                message:'Se encontraron los servicios activos',
                data: serviciosDB
            });

        }  else {
            return resp.status(201).json({
                ok: false,
                message:'No hay servicios activos',
                data: serviciosDB
            });
        } 

    }catch(error) {
        
        console.log(error);   
        logMemory('/servicio', false, error);

        return resp.status(500).json({
            ok: false,
            message: 'Hubo un error',
            error
        });
    }

    });

app.get('/listado/servicios/horarios', async(req, resp) => {

  try {
    const  horarios = await modeloHorarios.find({ status: 'ACTIVO' });

    if(!horarios){
        return resp.status(500).json({
            ok: false,
            message: 'No se encontaron servicios'
        });
    }

    return resp.status(200).json({
        ok: true,
        message: 'Se encontraron los servicios',
        data: horarios
    });

  } catch (error) {

    console.log(error);

    logMemory('/listado/servicios/horarios', false, error);

    return resp.status(500).json({
        ok: false,
        message: 'Hubo un error',
        error
    });

  }

});

//? UPDATE DE LOS DATOS POR ID DE LOS SERVICIOS AL MODLEO DE HORARIO
app.put('/update/horario/servicios/:id', async(req, resp) => {

        const {id} = req.params;
        const {  body } = req;

        try{
            if( id.length > 5 ){
                const validateId = await modeloHorarios.findById(id);
                
                if( validateId == null ){
     
                     return resp.status(400).json({
                         ok: false,
                         message: 'ID no valido',
                         error: 'No valid ID'
                     });
                }
     
                const dataUpdated = await modeloHorarios.findByIdAndUpdate( id, body, { new: true } );
     
                if(!dataUpdated){
                     return resp.status(500).json({
                         ok: false,
                         message: 'No se pudo actualizar'
                     });
                     
                }

                return resp.status(201).json({
                    ok: true, 
                    message: 'Se actualizo la informacion',
                    data: dataUpdated
                })

     
             }
        }catch (error) {
            console.log(error);
            logMemory( `/update/horario/servicios/${id}`, false, error );
            return resp.status(500).json({
                ok: false,
                message: 'Hubo un error',
                error
            })
        }

});

//se pasa al modelo de horarios 
// app.get('/ver/servicios/departamento', async(req, resp )=> {


//     try{

//         const departamentos = await serviciosDepartamento.find();

//         if(!departamentos){
//             return resp.status(500).json({
//                 ok: false,
//                 message: 'No se pudo obtener el servicio'
//             })
//         }


//         return resp.status(200).json({
//             ok:true,
//             message: 'Se encontro el servicio',
//             data: departamentos
//         })

//     }catch( error ){
//         console.log( '/ver/servicios/departamento', error );
//         return resp.status(500).json({
//             ok: false,
//             message: 'Hubo un error',
//             error
//         })
//     }


// });



// funciones para hacer  la busqueda entre difrerentes tablas
const buquedaAmbulancia = (regexp) => {
    return new Promise((resolve, reject) => {
        ambulancia
            .find({ DESTINO: regexp })
            .then(data => resolve(data))
            .catch(error => reject(error));
    })
}


const busquedaServicios = (regexp) => {
    return new Promise((resolve, reject) => {
        modeloServicios
            .find({ ESTUDIO: regexp })
            .then((data) => resolve(data))
            .catch(error => reject(error))
    })
}

const busquedaAmbulanciaMembresia = (regex) => {
    return new Promise((resvolve, reject) => {

        ambulancia
            .find({ DESTINO: regex }, "DESTINO PRECIO_MEMBRESIA_DIA PRECIO_MEMBRESIA_REDONDO_DIA PRECIO_MEMBRESIA_NOCHE PRECIO_MEMBRESIA_REDONDO_NOCHE name")
            .exec()
            .then((data) => resvolve(data))
            .catch((error) => reject(error));

    });
}

const busquedaServiciosMembresia = (regex) => {

    return new Promise((resolve, reject) => {

        modeloServicios
            .find({ ESTUDIO: regex }, "ESTUDIO PRECIO_MEMBRESIA PRECIO_MEMBRESIA_URGENCIA PRECIO_MEMBRESIA_HOSPITALIZACION PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA name")
            .exec()
            .then(data => resolve(data))
            .catch(error => reject(error));
    });
}




const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}

//TODO: hacer el refactor de los funciones
module.exports = app;