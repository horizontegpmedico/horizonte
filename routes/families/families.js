const familias = require('../../models/families/familiesModel');
const express = require('express');
const serveResp = require('../../functions/functions');
const app = express();



app.post('/nueva/familia', (req, resp) => {

    const body = req.body;
    console.log(body.integrantes);

    const newFamilie = new familias({
        nombre: body.nombre,
        integrantes: body.integrantes
    });

    newFamilie
        .save()
        .then(data => serveResp(data, null, 'Se creo la familia', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear', resp));
});


app.get('/ver/familias', (req, resp) => {

    familias
        .find()
        .populate('integrantes')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las familias', resp))
        .catch(error => serveResp(null, error, 'No se econtraron familias', resp));
});


app.get('/ver/familia/uno/:id', (req, resp) => {

    const id = req.params.id;
    familias
        .findById(id)
        .populate('integrantes')
        .exec()
        .then(data => serveResp(data, null, 'Se encontro la familia', resp))
        .catch(error => serveResp(null, error, 'No se enonctro la familia', resp));

});

app.get('/ver/familia/:nombre', (req, resp) => {

    const id = req.params.nombre;
    familias
        .find({nombre: id})
        .populate('integrantes')
        .exec()
        .then(data => serveResp(data, null, 'Se encontro la familia', resp))
        .catch(error => serveResp(null, error, 'No se enonctro la familia', resp));

});


app.put('/agregar/familiar/:id', (req, resp) => {
    const id = req.params.id;
    const body = req.body;

    familias
        .findByIdAndUpdate({ _id: id }, { $push: { integrantes: body.integrantes } })
        .exec()
        .then(data => serveResp(data, null, 'Se agrego el familiar', resp))
        .catch(error => serveResp(null, error, 'No se pude agregar el familiar', resp))
});

module.exports = app;