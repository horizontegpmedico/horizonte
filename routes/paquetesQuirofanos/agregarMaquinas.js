const express = require('express');
const maquinasQuirofano = require('../../models/crearPaquetesQuirofano/agregarMaquinas');


let app = express();

app.post('/agregar/maquina', (req, resp) => {

    let body = req.body;


    let newMaquinaQuirofano = new maquinasQuirofano({


        nombreDeLaMaquina: body.nombreMaquina,
        costoMaquina: body.costoDeLaMaquinaPorHora

    });


    newMaquinaQuirofano
        .save()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se guardo la maquina',
                    data
                })
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo guadar',
                    error: error
                })
        })



});


app.get('/ver/maquinas', (req, resp) => {

    maquinasQuirofano
        .find()
        .exec()
        .then(

            (data) => {
                console.log(data);
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se cargaron los datos',
                        data: data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: true,
                        message: 'No se puden mostrar',
                        error: error
                    });
            }
        )

});


module.exports = app;