const express = require('express');
const medicamentosNuevos = require('../../models/crearPaquetesQuirofano/agregarMedicamentos');


let app = express();


app.get('/ver/medicamentos', (req, resp) => {


    medicamentosNuevos
        .find()
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se encontraron los datos',
                        data: data
                    })
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo listar',
                        error
                    })
            }
        )

});


app.post('/agregar/medicamentos', (req, resp) => {

    let body = req.body;

    let newSchemaMedicamentos = new medicamentosNuevos({
        nombreDelMedicamento: body.nombreMedicamento,
        costoMedicamento: body.costoMedicamento
    });



    newSchemaMedicamentos
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se guardo el medicamento',
                        data: data
                    })
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar',
                        error
                    })
            }
        )

});

module.exports = app;