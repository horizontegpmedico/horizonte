 const express = require('express');
 const paciente = require('../../models/nuevoPaciente');
 const paquetesQuirofano = require('../../models/crearPaquetesQuirofano/guardarPaquetesQuirofano');

 let app = express();


 // este servicio agrega un paquete a un pacientes
 app.put('/agregar/paquete/quirofano/:id', (req, resp) => {

     let id = req.params.id;
     let body = req.body

     let paquete = { paquetesQuirofano: body }


     paciente.findByIdAndUpdate({ _id: id }, { $push: paquete })
         .exec()
         .then(

             (data) => {

                 return resp.status(200)
                     .json({
                         ok: true,
                         message: 'Se agrego el paquete',
                         data: data
                     })

             }



         )
         .catch(

             (error) => {

                 return resp.status(500)
                     .json({
                         ok: false,
                         message: 'No se pudo agregar',
                         error
                     });
             }
         )

 });


 app.post('/crear/paquete/quirofano', (req, resp) => {

     let body = req.body;


     console.log(body);

     let newPaquete = new paquetesQuirofano({
         nombrePaquete: body.nombrePaquete,
         participantes: body.participantes,
         maquinas: body.maquinas,
         medicamentos: body.medicamentos,
         horaRecuperacion: body.horaRecuperacion,
         costoHorasRecuperacion: body.costoRecuperacion
     });


     newPaquete
         .save()
         .then((data) => {
             return resp.status(200)
                 .json({
                     ok: true,
                     message: 'Se creó el paquete',
                     data
                 })
         })
         .catch((error) => {
             return resp.status(500)
                 .json({
                     ok: false,
                     message: 'No se pudo crear el paquete',
                     error
                 })
         });

 });

 app.get('/ver/paquetes/quirofano', (req, resp) => {


     paquetesQuirofano
         .find()
         .exec()
         .then(
             (data) => {
                 return resp.status(200)
                     .json({
                         ok: true,
                         message: 'Se encontraron los paquetes',
                         data: data
                     })
             }
         )
         .catch(
             (error) => {
                 return resp.status(500)
                     .json({
                         ok: false,
                         message: 'No se encontraron',
                         error
                     })
             }
         )


 });


 app.get('/ver/paquete/:id', (req, resp) => {


     let id = req.params.id;


     paquetesQuirofano
         .findById({ _id: id })
         .exec()
         .then(
             (data) => {
                 return resp.status(200)
                     .json({
                         ok: true,
                         message: 'Se encontro el paquete',
                         data
                     })
             }
         )
         .catch(
             (error) => {
                 return resp.status(500)
                     .json({
                         ok: false,
                         message: 'No se encontró el id',
                         error
                     });
             }
         )



 });
// app.put('/agregar/excedentes/:id', (req, resp) => {
    
//     let id = req.params.id;


//     let excedentes =  { paquetesQuirofano:  req.body } 

//     paciente
//     .findByIdAndUpdate( { _id: id }, { $push :  excedentes } )
//     .exec()
//     .then((data)  => {

//         console.log(data);
//         return  resp.status(200)
//         .json({
//             ok:true,
//             message: 'Se agrego el excedente',
//             data
//         })

//     } )
//     .catch(  
//         error => {
//             return resp.status(500)
//             .json({
//                 ok:false,
//                 message: 'No se pudo agregar',
//                 error
//             })
//         }
//      )

// })

 module.exports = app;