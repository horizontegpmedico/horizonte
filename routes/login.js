const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const ModelPersonal = require('../models/personal');

const serveResp = require('../functions/functions');
const { Error } = require('mongoose');
const app = express();
require('dotenv').config()
// mdAuth.verificaToken
app.post('/login/:sede', (req, res) => {
 
    const { sede } = req.params;
    const { body } = req;
 
    ModelPersonal
        .findOne({ nombre: body.nombre, sede })
        .populate('sedes')
        .exec()
        .then(data => {

            if (data) {


                if (!bcrypt.compareSync(body.password, data.password)) {
                    return res.status(400).json({
                        ok: false,
                        err: {
                            message: 'El usuario o la contraseña no son correctos'
                        }

                    });

                } else {

                    // si el usuario se logea correctamente nos regresa el objeto  PersonalDB


                    let token = jwt.sign({
                        nombre: data.nombre,
                        role: data.role,
                        id: data.id,
                        sede: data.sedeId
                    }, process.env.SEED, { expiresIn: process.env.CADUCIDADTOKEN })

                    data.password = ':)';

                    return res.json({
                        ok: true,
                        personal: data,
                        token
                    });

                }

            }

            throw new Error('No tienes credenciales');



        })
        .catch((error) => {
            /* console.log( error); */
            serveResp(null, error, 'No se encontro el usuario', res);
        });
});

app.post('/login/socios/prueba', (req, res) => {

    /* const { sede } = req.params; */
    const { body } = req;
 
    ModelPersonal
        .findOne({ nombre: body.nombre })
        /* .populate('sedes') */
        .exec()
        .then(data => {

            if (data) {


                if (!bcrypt.compareSync(body.password, data.password)) {
                    return res.status(400).json({
                        ok: false,
                        err: {
                            message: 'El usuario o la contraseña no son correctos'
                        }

                    });

                } else {

                    // si el usuario se logea correctamente nos regresa el objeto  PersonalDB


                    let token = jwt.sign({
                        nombre: data.nombre,
                        role: data.role,
                        id: data.id,
                        sede: data.sedeId
                    }, process.env.SEED, { expiresIn: process.env.CADUCIDADTOKEN })

                    data.password = ':)';

                    return res.json({
                        ok: true,
                        personal: data,
                        token
                    });

                }

            }

            /* throw new Error('No tienes credenciales'); */



        })
        .catch((error) => {
            /* console.log( error); */
            serveResp(null, error, 'No se encontro el usuario', res);
        });
});


module.exports = app;