const express = require('express');
const serveResp = require('../../functions/functions');
const roles = require('../../models/roles/modelroles');

let app = express();

app.get('/roles', async (req, resp ) => {
    try {
       const rolesDB = await roles.find();
       serveResp(rolesDB, null, 'Se encontraron los roles', resp);
    } catch (error) {
        console.log(error); 
        serveResp(null, error, 'No se pudo conectar', resp)  
    }

});

module.exports = app;