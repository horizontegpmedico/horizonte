const express = require('express');
const Paquetes = require('../models/paquetes');
const PaquetesPacientes = require('../models/paquetes-paciente');
const Pacientes = require('../models/nuevoPaciente');

const { v4: uuidv4 } = require('uuid');

 
const serveResp  = require('../functions/functions');

// const expressFileUpload = require('express-fileupload');
const paquetes = require('../models/paquetes');

const app = express();

app.get('/paquetes', (req, resp) => {



    Paquetes.find({status:true})
        .exec((error, paquetes) => {

            if (error) {
                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'Algo ocurrió',
                        error
                    })
            }

            if (!paquetes) {
                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No se encontraron paquetes'
                    })
            }



            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Los paquetes son',
                    paquetes: paquetes
                })


        })

});

app.get('/paquete/:id', (req, resp) => {
    let id = req.params.id;


    Paquetes.findById(id, (error, paqueteDB) => {

        if (error) {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'Hubo un error',
                    error
                })
        }


        if (!paqueteDB || paqueteDB === null) {
            return resp.status(401)
                .json({
                    ok: false,
                    message: 'No se encontró el paquete'
                })
        }



        return resp.status(200)
            .json({
                ok: true,
                message: 'El paquete es:',
                paquete: paqueteDB
            });

    })


});

// encontramos los paquete que el paciente tine por el id 

app.get('/ver/paquetes/paciente/:id', async (req, resp) => {

    const id = req.params.id;
   
    // console.log(id);

    try{
        let paquetesid = await PaquetesPacientes
        .find({ paciente: id, membresiaActiva:true })
        .populate('paquete')
        .populate('paciente')
        serveResp(paquetesid,null,"no actualizo el adelanto",resp)
    }catch(error){
        serveResp(null,error,"no actualizo el adelanto",resp)
    }
});

app.get('/contar/paquetes/folios', (req, resp) => {

    PaquetesPacientes.find().countDocuments()
    .then((data) => {
        serveResp(data, null, 'folio de paquetes', resp)
    }).catch((err) => {
        serveResp(null, err, 'No se encontro ningun folio', resp)
    });

})
//==========================================================================================
// VISITAS
// este servicio agrega las consultas y los examenes a la tabla de los pacientes-paquetesPacientes

app.post('/paciente/paquete/:tipo/:id', async (req, resp) => {

    const {id} = req.params;
    const {tipo} = req.params;
    const {body} = req;
    let insertData = [];
    let respuesta;
    if (tipo === "pagos") {
        insertData = { pagos: body }
        respuesta = await PaquetesPacientes.findByIdAndUpdate({ _id: id }, { $push: insertData },{new:true});
    }else{
        body.forEach(async element => {
            if (element.tipo === "consultas") {
                insertData = { consultas: element }
                
            } else if (element.tipo === "pagos") {
        
                insertData = { pagos: element }
        
            } else if (element.tipo === "laboratorio") {
        
                insertData = { laboratorio: element }
        
            } else if (element.tipo === "ultrasonido") {
        
                insertData = { ultrasonido: element }
        
            } else if (element.tipo === "rayosX") {
        
                insertData = { rayosX: element }
        
            } else if (element.tipo === "tomografia") {
        
                insertData = { tomografia: element }
        
            }else if (element.tipo === "farmacia") {
        
                insertData = { farmacia: element }
        
            }else if (element.tipo === "otros") {
        
                insertData = { otros: element }
        
            }else if (element.tipo === "hospitalizacion") {
        
                insertData = { hospitalizacion: element }
        
            } else if (element.tipo === "" || element.tipo === null) {
        
                serveResp( null, 'Se encontro la accion', 'Accion no valida', resp);
            }
            respuesta = await PaquetesPacientes.findByIdAndUpdate({ _id: id }, { $push: insertData },{new:true});
        });
    }
    const final = await PaquetesPacientes.findById(id).populate('paquete');
    if(respuesta != null){
        serveResp( respuesta, null, 'Se encontro el paquete', resp);
    }else{
        serveResp( null, 'no se encontro el paquete', 'Accion no valida', resp);
    }

});


//=======================================================================
//   ESTE SERVICIO NOS MUESTRA LOS PAGOS DEL PACIENTE CON UN PAQUETE
//=======================================================================

app.get('/paciente/paquete/pagos/:id', async(req, resp) => {

    const {id} = req.params;
    
    
    try {
        const paquetesPacientes = await  PaquetesPacientes.findById(id);
        serveResp(paquetesPacientes, null, 'Se encontro el paquete', resp);
    } catch (error) {
        /* console.log(error); */
        serveResp(null, error, 'No se encontro el paquete', resp);
    }

   
})

app.get('/paquetes/pacientes/visitas', (req, resp) => {


    PaquetesPacientes
        .find()
        .populate('pacientes')
        .populate('paquetes')
        .exec()
        .then(
            (data) => {

                if (!data) {
                    return resp.status(200)
                        .json({
                            ok: false,
                            message: 'No se encontraron prrrm datos',
                            data
                        })
                }
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Todas las tablas del control de los paquetes',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontró',
                        error
                    });
            });
});

app.get('/paciente/paquete/visitas/:id', (req, resp) => {


    let id = req.params.id;

    // console.log(id);

    PaquetesPacientes.findById(id)
        .exec((error, pacientePaquetesDB) => {

            if (error) {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'Algo pasó',
                        error
                    });
            }

            if (!pacientePaquetesDB) {

                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No se encontraron visitas',
                        paquete: pacientePaquetesDB
                    });
            }



            return resp.status(200)
                .json({
                    ok: true,
                    massage: 'Se encontró el paquete',
                    paquete: pacientePaquetesDB.visitas
                });

        });


});


// este servicio muestra a los pacientes y los paquetes de manera informativa
app.get('/paquete/usuario/:id', (req, resp) => {
    let id = req.params.id;

    Pacientes.findById(id)
        .populate('paquetes')
        .populate('paquetesPacientes')
        .exec((error, paquetsPacientesDB) => {

            // console.log(paquetsPacientesDB);

            if (error) {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo encontrar',
                        error
                    })
            }

            if (!paquetsPacientesDB) {
                return resp.status(400)
                    .json({
                        ok: false,
                        mesage: 'No se encontró el usuario'
                    })

            }


            return resp.status(200)
                .json({
                    ok: true,
                    message: 'El usuario y paquetes',
                    pacientesPaquetes: paquetsPacientesDB
                });
        });

});

// En este servicio se obtiene el listado de los paquetes que el paciente tiene

app.get('/paquete/paciente/:id', (req, resp) => {

    let id = req.params.id;
    // console.log(id);
    PaquetesPacientes
        .findById(id)
        .populate('paciente')
        .populate('paquete')
        .exec()
        .then(
            (paquesYpacientes) => {

                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Los paquetes son: ',
                        paquetes: paquesYpacientes
                    });

            }
        )
        .catch((error) => {
            /* console.log( error ) */
            return resp.status(300)
                .json({
                    ok: false,
                    message: 'Ocurrió algo',
                    error
                });

        })

});



app.put('/actualizar/estado/paquete/:id', async(req, resp) => {

    const { id } = req.params;
    const { body } = req;

    const validatePaquete = await PaquetesPacientes.findById(id);

    if(validatePaquete){
       
        try {
            
            const paqueteToUpdate = await PaquetesPacientes.findByIdAndUpdate(id, body );
            serveResp(paqueteToUpdate, null, 'Se actualizo el estado', resp);

        } catch (error) {
            /* console.log(error) */
            serveResp(null, error, 'No se actualizo', resp);
        }
        
    }

});


app.put('/subir/contratos', async(req, resp) => {
    let body = req.body;

    // validamos que el archivo exista
    if (!req.files) {
        return resp.status(500)
            .json({
                ok: false,
                message: 'No hay archivo'
            });
    }


    const files = req.files.contrato;

    const nombreCortado = files.name.split('.');
    const extension = nombreCortado[nombreCortado.length - 1];

    // Validamos el tipo de extension 

    if (!extension === 'pdf') {
        return resp.status(500)
            .json({
                ok: false,
                message: 'Extension no valida',
                extension
            });
    }

    const nombreArchivo = `${uuidv4()}.${extension}`;


    const path = `./contratos/${nombreArchivo}`;


    await files.mv(path)
        .then(
            () => {


                PaquetesPacientes
                    .findByIdAndUpdate({ _id: body.idPaciente }, { contrato: nombreArchivo })
                    .exec()
                    .then(
                        (data) => {
                            return resp.status(200)
                                .json({
                                    ok: true,
                                    message: 'Se subió el archivo',
                                    data
                                });
                        }
                    )
                    .catch(
                        (error) => {
                            return resp.status(500)
                                .json({
                                    ok: false,
                                    message: 'No se pudo subir el archivo',
                                    error
                                });
                        })

            })
        .catch(
            (error) => {
                /* console.log(error) */
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo subir archivo',
                        error
                    });
            });

});

app.get('/contratos/pendientes', async(req, resp) => {

    // este servicio nos devuelve los contratos de los pacientes que estan pendientes por subir


    await PaquetesPacientes
        .find({ contrato: "", identificacion: "", comprobanteDireccion: "", actaDeNacimiento: "" })
        .populate('paciente')
        .populate('paquete')
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Contratos pendientes por subir',
                        data
                    })
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se econtraron pendientes',
                        error
                    });
            })
});

app.put('/actualizar/acumulado/pago/:id', (req, resp) => {
    const body = req.body;
    const id = req.params.id;

    PaquetesPacientes
    .findByIdAndUpdate({_id:id},{acumulado:body.acumulado})
    .exec()
    .then(data =>  {
        /* console.log(data); */
        serveResp(data,null,"ya se actualizo el adelanto",resp) })
    .catch(error => serveResp(null,error,"no actualizo el adelanto",resp)); 

});

app.put('/finalizar/paquete/:id',async (req,resp)=>{
    const body = req.body
    const id = req.params.id
    try{
        let cancelacion = await PaquetesPacientes
        .findByIdAndUpdate({_id:id},{membresiaActiva:body.membresiaActiva},{new:true})
        serveResp(cancelacion,null,"ya se actualizo el adelanto",resp)
    }catch(error){
        serveResp(null,error,"no actualizo el adelanto",resp)
    }
})

module.exports = app;