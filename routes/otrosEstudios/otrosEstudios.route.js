const express = require('express');
const otrosEstudios = require('../../models/otrosEstudios/otrosEstudios.model');
const serveResp = require('../../functions/functions');


const app = express();

app.post('/agregar/otros/estudios', (req, res) => {
    const {body} = req;
    const otherStudy = new otrosEstudios({
        idPaciente: body.idPaciente,
        fechaEstudios: body.fechaEstudios,
        fechaCargaEsudio: body.fechaCargaEsudio,
        vendedor: body.vendedor,
        imageUrl: body.imageUrl,
        tipoEstudio: body.tipoEstudio
    });
    otherStudy
    .save()
    .then(data => serveResp(data, null, 'Se agregó estudio de manera Correcta', res))
    .catch(error => serveResp(null, error, 'Error al cargar', res));
});

app.get('/obtener/otros/estudios/:id/:estudio', async(req, res) =>{
    let {id, estudio} = req.params;
    await otrosEstudios
        .find({ idPaciente: id, tipoEstudio: estudio })
        .then(data => serveResp(data, null, 'Se encontraron Estudios', res))
        .catch(error => serveResp(null, error, 'No se encontraron Estudios', res));
})


app.get('/otros/estudios/paciente/:id', async(req, res) =>{
    let {id} = req.params;
    await otrosEstudios
        .find({ idPaciente: id })
        .then(data => serveResp(data, null, 'Se encontraron Estudios', res))
        .catch(error => serveResp(null, error, 'No se encontraron Estudios', res));
})


module.exports = app;