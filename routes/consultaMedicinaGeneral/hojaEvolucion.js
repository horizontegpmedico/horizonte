const express = require('express');
const hojaEvolucion = require('../../models/consultasGeneralesBitacoras/hojaEvolucion');
const serveResp = require('../../functions/functions');

let app = express();


app.post('/hoja/evolucion', (req, resp) => {

    let body = req.body

    let NewHojaEvolucion = new hojaEvolucion({
        diagnosticos: body.diagnosticos,
        motivoDeConsulta: body.motivoDeConsulta,
        fechaHojaEvolucion: body.fechaHojaEvolucion,
        horaHojaEvolucion: body.horaHojaEvolucion,
        plan: body.plan,
        evolucion: body.evolucion,
        idPaciente: body.idPaciente,
        idConsulta: body.idConsulta,
        idMedico: body.idMedico
    });

    NewHojaEvolucion
        .save()
        .then((data) => serveResp(data, null, 'Se guardo la hoja de evolucion', resp))
        .catch(error => serveResp(null, error, 'No se pudo guardar la hoja de evolucion', resp))

});

app.get('/ver/hoja/evolucion/:id', (req, resp) => {

    const { id } = req.params;

    hojaEvolucion
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se econtro la hoja de evolucion', resp))
        .catch(error => serveResp(null, error, 'No se econtro la hoja de evolucion', resp));

});

app.get('/ver/hoja/historial/evolucion/:id', (req, resp) => {

    const { id } = req.params;

    hojaEvolucion
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se econtro la hoja de evolucion', resp))
        .catch(error => serveResp(null, error, 'No se econtro la hoja de evolucion', resp));

});


module.exports = app;