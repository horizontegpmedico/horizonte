const express = require('express');
const antecedentesPersonalesNoPatologicos = require('../../models/signosVitales/antecedentesPersonalesNoPatologicos')

let app = express();




app.post('/agregar/antecedentes/no/patologicos', (req, resp) => {


    let body = req.body;
    console.log(body);

    let newAntecedentesNoPatologicos = new antecedentesPersonalesNoPatologicos({

        tabaquismoPorDia: body.tabaquismoPorDia,
        aniosconsumo: body.aniosconsumo,
        exFumadorPasivo: body.exFumadorPasivo,
        copasPorDia: body.copasPorDia,
        aniosDeconsumoAlcohol: body.aniosDeconsumoAlcohol,
        exAlcoholicoUOcasional: body.exAlcoholicoUOcasional,
        alergias: body.alergias,
        tipoAlergias: body.tipoAlergias,
        tipoSanguineo: body.tipoSanguineo,
        desconoceTipoSanguineo: body.desconoceTipoSanguineo,
        drogadiccionTipo: body.drogadiccionTipo,
        aniosConsumoDrogas: body.aniosConsumoDrogas,
        exDrogadicto: body.exDrogadicto,
        alimentacionAdecuada: body.alimentacionAdecuada,
        viviendaConServiciosBasicos: body.viviendaConServiciosBasicos,
        otrosAntecedentesNoPatologicos: body.otrosAntecedentesNoPatologicos,
        idPaciente: body.idPaciente

    });


    newAntecedentesNoPatologicos
        .save()
        .then((data) => serveResp(data, null, 'Se agregaron los antecedentes no patologicos', resp))
        .catch((error) => serveResp(null, error, 'No se pudieron guardar', resp));
});



app.get('/ver/antecedentes/no/patologicos/:id', (req, resp) => {


    let id = req.params.id;

    console.log(id);

    antecedentesPersonalesNoPatologicos
        .find({ idPaciente: id })
        .exec()
        .then((data) => serveResp(data, null, 'Se encontraron los antecedentes', resp))
        .catch((error) => serveResp(null, error, 'No se encontraron', resp));


});


app.put('/actualizar/antecedentes/no/patologicos/paciente/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;

    antecedentesPersonalesNoPatologicos
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes no patologicos',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes no patologicos',
                        error
                    });
            });
});


app.post('/ver/antecedentes/no/patologicos', (req, resp) => {

    let { idPaciente } = req.body;

    console.log(idPaciente);

    antecedentesPersonalesNoPatologicos
        .find({ idPaciente: idPaciente })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los antecedentes', resp))
        .catch(error => serveResp(error, null, 'No se encontraron antecedentes', resp))

});


const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}



module.exports = app;