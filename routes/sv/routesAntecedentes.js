const express = require('express');
const antecedentes = require('../../models/signosVitales/antecedentes');

let app = express();

app.post('/agregar/AgregarAntecedentes', (req, resp) => {


    let body = req.body;

    let newSchemaAntecedentes = new antecedentes({

        idPaciente: body.idPaciente,
        enfermedadesPielPersonal: body.enfermedadesPielPersonal,
        enfermedadesPielFecha: body.enfermedadesPielFecha,
        enfermedadesPielFamiliares: body.enfermedadesPielFamiliares,
        enfermedadesPielNotas: body.enfermedadesPielNotas,
        desnutricionPersonal: body.desnutricionPersonal,
        desnutricionFecha: body.desnutricionFecha,
        desnutricionFamiliares: body.desnutricionFamiliares,
        desnutricionNotas: body.desnutricionNotas,
        obesidadPersonal: body.obesidadPersonal,
        obesidadFecha: body.obesidadFecha,
        obesidadFamiliares: body.obesidadFamiliares,
        obesidadNotas: body.obesidadNotas,
        defectosPostularesPersonal: body.defectosPostularesPersonal,
        defectosPostularesFecha: body.defectosPostularesFecha,
        defectosPostularesFamiliares: body.defectosPostularesFamiliares,
        defectosPostularesNotas: body.defectosPostularesNotas,
        fracturasPersonal: body.fracturasPersonal,
        fracturasFecha: body.fracturasFecha,
        fracturasFamiliares: body.fracturasFamiliares,
        fracturasNotas: body.fracturasNotas,
        hospitalizacionesPersonal: body.hospitalizacionesPersonal,
        hospitalizacionesFecha: body.hospitalizacionesFecha,
        hospitalizacionesFamiliares: body.hospitalizacionesFamiliares,
        hospitalizacionesNotas: body.hospitalizacionesNotas,
        transfucionesPersonal: body.transfucionesPersonal,
        transfucionesFecha: body.transfucionesFecha,
        transfucionesFamiliares: body.transfucionesFamiliares,
        transfucionesNotas: body.transfucionesNotas,
        cardiopatiasPersonal: body.cardiopatiasPersonal,
        cardiopatiasFecha: body.cardiopatiasFecha,
        cardiopatiasFamiliares: body.cardiopatiasFamiliares,
        cardiopatiasNotas: body.cardiopatiasNotas,
        cirugiasPersonal: body.cirugiasPersonal,
        cirugiasFecha: body.cirugiasFecha,
        cirugiasFamiliares: body.cirugiasFamiliares,
        cirugiasNotas: body.cirugiasNotas,
        cancerLeucemiaPersonal: body.cancerLeucemiaPersonal,
        cancerLeucemiaFecha: body.cancerLeucemiaFecha,
        cancerLeucemiaFamiliares: body.cancerLeucemiaFamiliares,
        cancerLeucemiaNotas: body.cancerLeucemiaNotas,
        alergiasPersonal: body.alergiasPersonal,
        alergiasFecha: body.alergiasFecha,
        alergiasFamiliares: body.alergiasFamiliares,
        alergiasNotas: body.alergiasNotas,
        vihPersonal: body.vihPersonal,
        vihFecha: body.vihFecha,
        vihFamiliares: body.vihFamiliares,
        vihNotas: body.vihNotas,
        tabaquismoFecha: body.tabaquismoFecha,
        tabaquismoFamiliares: body.tabaquismoFamiliares,
        tabaquismoNotas: body.tabaquismoNotas,
        tabaquismoPersonal: body.tabaquismoPersonal,
        diabetesPersonal: body.diabetesPersonal,
        diabetesFecha: body.diabetesFecha,
        diabetesFamiliares: body.diabetesFamiliares,
        diabetesNotas: body.diabetesNotas,
        tuberculosisPersonal: body.tuberculosisPersonal,
        tuberculosisFecha: body.tuberculosisFecha,
        tuberculosisFamiliares: body.tuberculosisFamiliares,
        tuberculosisNotas: body.tuberculosisNotas,
        alcoholismoPersonal: body.alcoholismoPersonal,
        alcoholismoFecha: body.alcoholismoFecha,
        alcoholismoFamiliares: body.alcoholismoFamiliares,
        alcoholismoNotas: body.alcoholismoNotas,
        deportesPersonal: body.deportesPersonal,
        deportesFecha: body.deportesFecha,
        deportesFamiliares: body.deportesFamiliares,
        deportesNotas: body.deportesNotas,
        idPaciente: body.idPaciente,
        otrasEnfPersonales: body.otrasEnfPersonales,
        otrasEnfFecha: body.otrasEnfFecha,
        otrasEnfFamiliares: body.otrasEnfFamiliares,
        otrasEnfNotas: body.otrasEnfNotas,
        enfermedadesDeLosSentidosPersonales: body.enfermedadesDeLosSentidosPersonales,
        enfermedadesDeLosSentidosFecha: body.enfermedadesDeLosSentidosFecha,
        enfermedadesSentidosFamiliares: body.enfermedadesSentidosFamiliares,
        enfermedadesSentidosNotas: body.enfermedadesSentidosNotas,
        expoLaboralPersonales: body.expoLaboralPersonales,
        expoLaboralFecha: body.expoLaboralFecha,
        expoLaboralFamiliares: body.expoLaboralFamiliares,
        expoLaboralNotas: body.expoLaboralNotas,
        postQuirurgicoPersonales: body.postQuirurgicoPersonales,
        postQuirurgicoFecha: body.postQuirurgicoFecha,
        postQuirurgicoFamiliares: body.postQuirurgicoFamiliares,
        postQuirurgicoNotas: body.postQuirurgicoNotas
    });


    newSchemaAntecedentes
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({

                        ok: true,
                        message: 'Se guardaron los antecedentes',
                        data
                    });
            })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar',
                        error
                    });
            });
});


// app.get('/ver/antecedentes/:id', (req, resp) => {

//     let id = req.params.id;

//     antecedentes
//         .findById({ _id: id })
//         .exec()
//         .then((data) => serveResp(data, null, 'Se encontraron los antecdentes', resp))
//         .catch((error) => serveResp(null, error, 'No se encontraro antecedentes', resp));
// });

app.get('/ver/antecedentes/:id', (req, resp) => {

    let id = req.params.id;

    console.log(id);
    antecedentes
        .find({ idPaciente: id })
        .exec()
        .then((data) => serveResp(data, null, 'Se encontraron los antecedentes del paciente', resp))
        .catch((error) => serveResp(null, error, 'No se ecnotraro los antecedentes del paciente', resp));
});

app.put('/actualizar/antecedentes/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    antecedentes
        .findByIdAndUpdate(id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });

});



const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;