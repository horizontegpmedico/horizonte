const express = require('express');
const antecedentesPersonalesPatologicos = require('../../models/signosVitales/antecedentesPersonalesPatologicos');
let app = express();


app.post('/agregar/antecedentes/patologicos', (req, resp) => {

    let body = req.body;


    let newAntecedentesPatologicos = new antecedentesPersonalesPatologicos({
        accidentes: body.accidentes,
        accidentesEsp: body.accidentesEsp,
        antecedentesQuirurgicos: body.antecedentesQuirurgicos,
        antecedentesQuirurgicosEsp: body.antecedentesQuirurgicosEsp,
        enfermedadesInfancia: body.enfermedadesInfancia,
        enfermedadesInfanciaSecuelas: body.enfermedadesInfanciaSecuelas,
        fracturasEsp: body.fracturasEsp,
        fractutas: body.fractutas,
        hospitalizacionesPrevias: body.hospitalizacionesPrevias,
        hospitalizacionesPreviasEsp: body.hospitalizacionesPreviasEsp,
        medicamentosActuales: body.medicamentosActuales,
        medicamentosActualesEsp: body.medicamentosActualesEsp,
        otrasEnfermedades: body.otrasEnfermedades,
        otrasEnfermedadesEsp: body.otrasEnfermedadesEsp,
        transfucionesPrevias: body.transfucionesPrevias,
        transfucionesPreviasEsp: body.transfucionesPreviasEsp,
        idPaciente: body.idPaciente
    })


    newAntecedentesPatologicos
        .save()
        .then(data => serveResp(data, null, 'Se guardaron los antecedentes personales patologicos', resp))
        .catch(error => serveResp(null, error, 'No se pudieron guardar los antecedente personales no patologicos', resp))


});


app.get('/ver/antecedentes/personales/patologicos/:id', (req, resp) => {

    let id = req.params.id;

    antecedentesPersonalesPatologicos
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los antecedentes personales patologicos', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los antecdentes personale patologicos', resp))


});

app.put('/actualizar/personales/patologicos/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    antecedentesPersonalesPatologicos
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });
});

const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


app.get('/ver/antecedentes/personales/patologicos/:id', (req, resp) => {

    let id = req.params.id;

    antecedentesPersonalesPatologicos
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los antecedentes personales patologicos', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los antecdentes personale patologicos', resp))


});

// const serveResp = (data, error, message, resp) => {

//     if (data != null) {
//         return resp.status(200)
//             .json({
//                 ok: true,
//                 message,
//                 data
//             })
//     } else if (error != null) {
//         return resp.status(500)
//             .json({
//                 ok: false,
//                 message,
//                 error
//             });
//     }

// }


module.exports = app;