const express = require('express');
const antecedentesHeredoFamiliares = require('../../models/signosVitales/heredoFamiliares')

let app = express();


app.post('/agregar/antecedentes/herodo/familiares', (req, resp) => {

    let body = req.body;

    let antecedentesHeredoNew = new antecedentesHeredoFamiliares({
        diabetes: body.diabetes,
        cancer: body.cancer,
        cardiopatias: body.cardiopatias,
        malformaciones: body.malformaciones,
        hipertension: body.hipertension,
        hipertensionTipo: body.hipertensionTipo,
        nefropatias: body.nefropatias,
        nefropatiasTipo: body.nefropatiasTipo,
        idPaciente: body.idPaciente,
    });

    antecedentesHeredoNew
        .save()
        .then(
            (data) => {
                console.log("data AHF", data);
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se guardaron los antecdentes herodo familiares',
                        data
                    });
            })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudieron guardar los antecedentes',
                    error
                });
        });
});



app.get('/ver/antecedentes/heredo/familiares/:id', (req, resp) => {

    let id = req.params.id;

    console.log('obtener HF', req.body);

    antecedentesHeredoFamiliares
        .find({ idPaciente: id })
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontraron los antecedentes herodo familiares',
                    data
                });
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'no se encontarron antecedentes',
                    error
                });
        });

});


// update de los antecedentes heredo familiares

app.put('/actualizar/antecedentes/heredo/familiares/paciente/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;

    antecedentesHeredoFamiliares
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });

});


app.get('/ver/antecedentes/familiares/:id', (req, resp) => {

    let id = req.params.id;

    antecedentesHeredoFamiliares
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los antecedentes heredo familiares', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los antecedentes heredo familiares', resp));

});

const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}

module.exports = app;