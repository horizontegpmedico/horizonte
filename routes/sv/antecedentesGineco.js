const express = require('express');
const antecedentesGineco = require('../../models/signosVitales/antecedentesGineco');
const antecednentesGineco = require('../../models/signosVitales/antecedentesGineco')


let app = express();



app.post('/agregar/antecedentes/gineco', (req, resp) => {


    let body = req.body;
    console.log("Antecedentes Gineco Obstetricos por Visita", body);

    let newAntecedentes = new antecednentesGineco({
        embarazoActual: body.embarazoActual,
        embarazoAltoRiesgo: body.embarazoAltoRiesgo,
        administracionHierroAcidoF: body.administracionHierroAcidoF,
        fechaAdmonHierroAcidoF: body.fechaAdmonHierroAcidoF,
        fechaAdmonHierro: body.fechaAdmonHierro,
        fechaAdmonAcido: body.fechaAdmonAcido,
        metodoAntiConceptivoPostParto: body.metodoAntiConceptivoPostParto,
        incioVidaSexualActiva: body.incioVidaSexualActiva,
        fechaUltimoParto: body.fechaUltimoParto,
        crecimientoBellosPubicos: body.crecimientoBellosPubicos,
        tipoMetodoPlanFamiliar: body.tipoMetodoPlanFamiliar,
        gestas: body.gestas,
        partos: body.partos,
        cesareas: body.cesareas,
        abortos: body.abortos,
        ultimoPapanicolao: body.ultimoPapanicolao,
        ultimaColposcopia: body.ultimaColposcopia,
        ultimaRevisionMamaria: body.ultimaRevisionMamaria,
        bebesNacidos: body.bebesNacidos,
        bebesMuertos: body.bebesMuertos,
        primeraMenstruacion: body.primeraMenstruacion,
        crecimientoDePechos: body.crecimientoDePechos,
        primerEmbarazo: body.primerEmbarazo,
        fechaPrimerEmbarazo: body.fechaPrimerEmbarazo,
        idPaciente: body.idPaciente
    });

    newAntecedentes
        .save()
        .then(
            (data) => {
                // console.log("data AGOxV", data); 
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se guardaron los Gineco x Visita',
                        data
                    });
            })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudieron guardar los antecedentes',
                    error
                });
        });

    // newAntecedentes
    //     .save()
    //     .then(data => serveResp(data, null, 'Se guardaron los antecedentes gineco obstetricos', resp))
    //     .catch((error) => serveResp(null, error, 'No se guardaron los antecedentes', resp))

});


app.get('/ver/antecedentes/gineco/visita/:id', (req, resp) => {

    let id = req.params.id;

    antecednentesGineco
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron los antecedentes gineco obstetricos', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los antecedentes gineco obstetricos', resp))

});


//Método PUT
app.put('/actualizar/gineco/visita/paciente/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    console.log('Data Gineco x Visita', body)
    antecednentesGineco
        .findByIdAndUpdate(id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los Gineco x Visita',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo actualizar los Gineco x Visita',
                        error
                    });
            });
})



const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;