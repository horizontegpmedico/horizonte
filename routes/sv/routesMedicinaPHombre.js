const express = require('express');
const medicinaPreventiva = require('../../models/signosVitales/medicinaPreventiva');


let app = express();


app.post('/agregar/medicina/preventiva', (req, resp) => {


    let body = req.body;
    console.log(body);

    const newSchemaMH = new medicinaPreventiva({
        cancerEstomagoFechaToma: body.cancerEstomagoFechaToma,
        cancerEstomagoFechaEntrega: body.cancerEstomagoFechaEntrega,
        cancerEstomagoResultado: body.cancerEstomagoResultado,
        cancerEstomagoObservaciones: body.cancerEstomagoObservaciones,
        cancerColonFechaToma: body.cancerColonFechaToma,
        cancerColonFechaEntrega: body.cancerColonFechaEntrega,
        cancerColonResultado: body.cancerColonResultado,
        cancerColonObservaciones: body.cancerColonObservaciones,
        cancerPulmonFechaToma: body.cancerPulmonFechaToma,
        cancerPulmonFechaEntrega: body.cancerPulmonFechaEntrega,
        cancerPulmonResultado: body.cancerPulmonResultado,
        cancerPulmonObservaciones: body.cancerPulmonObservaciones,
        cancerMamaFechaToma: body.cancerMamaFechaToma,
        cancerMamaFechaEntrega: body.cancerMamaFechaEntrega,
        cancerMamaResultado: body.cancerMamaResultado,
        cancerMamaObservaciones: body.cancerMamaObservaciones,
        cancerCervicoUterinoFechaToma: body.cancerCervicoUterinoFechaToma,
        cancerCervicoUterinoFechaEntrega: body.cancerCervicoUterinoFechaEntrega,
        cancerCervicoUterinoResultado: body.cancerCervicoUterinoResultado,
        cancerCervicoUterinoObservaciones: body.cancerCervicoUterinoObservaciones,
        cancerProstataFechaToma: body.cancerProstataFechaToma,
        cancerProstataFechaEntrega: body.cancerProstataFechaEntrega,
        cancerProstataResultado: body.cancerProstataResultado,
        cancerProstataObservaciones: body.cancerProstataObservaciones,
        leucemiaFechaToma: body.leucemiaFechaToma,
        leucemiaFecheEntrega: body.leucemiaFecheEntrega,
        leucemiaResultado: body.leucemiaResultado,
        leucemiaObservaciones: body.leucemiaObservaciones,
        agudezaAuditivaFehaToma: body.agudezaAuditivaFehaToma,
        agudezaAuditivaFechaEntrega: body.agudezaAuditivaFechaEntrega,
        agudezaAuditivaResultado: body.agudezaAuditivaResultado,
        agudezaAuditivaObservaciones: body.agudezaAuditivaObservaciones,
        desnutricionFehaToma: body.desnutricionFehaToma,
        desnutricionFechaEntrega: body.desnutricionFechaEntrega,
        desnutricionResultado: body.desnutricionResultado,
        desnutricionObservaciones: body.desnutricionObservaciones,
        saludBucalFechaToma: body.saludBucalFechaToma,
        saludBucalFechaEntrega: body.saludBucalFechaEntrega,
        saludBucalResultado: body.saludBucalResultado,
        saludBucalObservaciones: body.saludBucalObservaciones,
        diabetesFechaToma: body.diabetesFechaToma,
        diabetesFechaEntrega: body.diabetesFechaEntrega,
        diabetesResultado: body.diabetesResultado,
        diabetesObservaciones: body.diabetesObservaciones,
        agudezaVisualFechaToma: body.agudezaVisualFechaToma,
        agudezaVisualFechaEntrega: body.agudezaVisualFechaEntrega,
        agudezaVisualResultado: body.agudezaVisualResultado,
        AgudezaVisualObservaciones: body.agudezaVisualObservaciones,
        enfermedadesRespiratoriasFechaToma: body.enfermedadesRespiratoriasFechaToma,
        enfermedadesRespiratoriasFechaEntrega: body.enfermedadesRespiratoriasFechaEntrega,
        enfermedadesRespiratoriasResultado: body.enfermedadesRespiratoriasResultado,
        enfermedadesRespiratoriasObservaciones: body.enfermedadesRespiratoriasObservaciones,
        transtornoMarchaFechaToma: body.transtornoMarchaFechaToma,
        transtornoMarchaFechaEntrega: body.transtornoMarchaFechaEntrega,
        transtornoMarchaResultado: body.transtornoMarchaResultado,
        transtornoMarchaObservaciones: body.transtornoMarchaObservaciones,
        otrosFechaToma: body.otrosFechaToma,
        otrosFechaEntrega: body.otrosFechaEntrega,
        otrosResultado: body.otrosResultado,
        desnutricionSobrePesoObesidadFechaToma:body.desnutricionSobrePesoObesidadFechaToma,
        agudezaAuditivaFechaToma:body.agudezaAuditivaFechaToma,
        desnutricionSobrePesoObesidadFechaEntrega:body.desnutricionSobrePesoObesidadFechaEntrega,
        desnutricionSobrePesoObesidadResultado:body.desnutricionSobrePesoObesidadResultado,
        desnutricionSobrePesoObesidadObservaciones:body.desnutricionSobrePesoObesidadObservaciones,
        trastornoMarchaFechaToma:body.trastornoMarchaFechaToma,
        trastornoMarchaFechaEntrega:body.trastornoMarchaFechaEntrega,
        trastornoMarchaResultado:body.trastornoMarchaResultado,
        trastornoMarchaObservaciones:body.trastornoMarchaObservaciones,
        otrosObservaciones: body.otrosObservaciones,
        idPaciente: body.idPaciente
    });

    newSchemaMH
        .save()
        .then(
            (data) => {
                console.log(data);
                return resp.status(200)
                    .json({

                        ok: true,
                        message: 'Se creo guardaron los datos de medicina preventiva',
                        data
                    });
            })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar Medicina Preventiva',
                        error
                    })
            }
        )


});

// recibir el id del paciente

app.get('/ver/medicina/preventiva/:id', (req, resp) => {

    let id = req.params.id;


    medicinaPreventiva
        .find({ idPaciente: id })
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Medicina preventiva del paciente',
                        data
                    });
            })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontraron',
                        error
                    });
            });
});

app.put('/actualizar/medicina/preventiva/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body

    console.log("eeeeeg", body)
    medicinaPreventiva
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });

});
module.exports = app;