'use strict'
const express = require('express');
const antecedentesGinecoObstetricosHistoria = require('../../models/signosVitales/antecedentesGinecoObstetricosH');

let app = express();

app.post('/agregar/gineco/obstetricos/historia', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaAntecedentesGinecoHistoria = new antecedentesGinecoObstetricosHistoria({
        menarcaAniosEdad: body.menarcaAniosEdad,
        fechaUltimaMenstruacion: body.fechaUltimaMenstruacion,
        dismenorrea: body.dismenorrea,
        ciclosRegulares: body.ciclosRegulares,
        polimenorrea: body.polimenorrea,
        ivsaAnios: body.ivsaAnios,
        fechaUltimaCitologia: body.fechaUltimaCitologia,
        ritmo: body.ritmo,
        hipermenorrea: body.hipermenorrea,
        numParejasSexuales: body.numParejasSexuales,
        resultado: body.resultado,
        gestas: body.gestas,
        partos: body.partos,
        abortos: body.abortos,
        cesareas: body.cesareas,
        idPaciente: body.idPaciente
    });

    newSchemaAntecedentesGinecoHistoria
        .save()
        .then(
            (data) => {
                console.log("Data AGOHistoria", data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Se guardaron los Antecedentes Gineco Obstetricos de la Historia Clinica',
                        data
                    });
            }
        );
});

app.get('/ver/antecedentes/gineco/historia/:id', (req, resp) => {

    let id = req.params.id;

    antecedentesGinecoObstetricosHistoria
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los antecedentes Gineco Obs Historia', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los AGOH', resp))
});

app.put('/actualizar/gineco/historia/paciente/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;
    antecedentesGinecoObstetricosHistoria
        .findByIdAndUpdate(id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los Gineco en Historia',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes Gineco Historia',
                        error
                    });
            });
});

const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            });
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}

module.exports = app;