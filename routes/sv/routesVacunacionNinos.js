const express = require('express');
const vninos = require('../../models/signosVitales/vacunacionNinos');


let app = express();


app.post('/agregar/esquema/vacunacion', (req, resp) => {


    let body = req.body;

    console.log("Esquema de Vacunación", body);

    let newSchemaVN = new vninos({

        tuberculosisNinoDosis: body.tuberculosisNinoDosis,
        tuberculosisNinoFechaUno: body.tuberculosisNinoFechaUno,
        tuberculosisNinoFechaDos: body.tuberculosisNinoFechaDos,
        tuberculosisNinoFechaTres: body.tuberculosisNinoFechaTres,
        tuberculosisNinoFechaCuatro: body.tuberculosisNinoFechaCuatro,
        tuberculosisNinoFechaUltima: body.tuberculosisNinoFechaUltima,
        hepatitisNinoDosis: body.hepatitisNinoDosis,
        hepatitisNinoFechaUno: body.hepatitisNinoFechaUno,
        hepatitisNinoFechaDos: body.hepatitisNinoFechaDos,
        hepatitisNinoFechaTres: body.hepatitisNinoFechaTres,
        hepatitisNinoFechaCuatro: body.hepatitisNinoFechaCuatro,
        hepatitisNinoFechaUltima: body.hepatitisNinoFechaUltima,
        pentavalenteNinoDosis: body.pentavalenteNinoDosis,
        pentavalenteNinoFechaUno: body.pentavalenteNinoFechaUno,
        pentavalenteNinoFechaDos: body.pentavalenteNinoFechaDos,
        pentavalenteNinoFechaTres: body.pentavalenteNinoFechaTres,
        pentavalenteNinoFechaCuatro: body.pentavalenteNinoFechaCuatro,
        pentavalenteNinoFechaUltima: body.pentavalenteNinoFechaUltima,
        dptNinoDosis: body.dptNinoDosis,
        dptNinoFechaUno: body.dptNinoFechaUno,
        dptNinoFechaDos: body.dptNinoFechaDos,
        dptNinoFechaTres: body.dptNinoFechaTres,
        dptNinoFechaCuatro: body.dptNinoFechaCuatro,
        dptNinoFechaUltima: body.dptNinoFechaUltima,
        rotavirusNinoDosis: body.rotavirusNinoDosis,
        rotavirusNinoFechaUno: body.rotavirusNinoFechaUno,
        rotavirusNinoFechaDos: body.rotavirusNinoFechaDos,
        rotavirusNinoFechaTres: body.rotavirusNinoFechaTres,
        rotavirusNinoFechaCuatro: body.rotavirusNinoFechaCuatro,
        rotavirusNinoFechaUltima: body.rotavirusNinoFechaUltima,
        neumococoNinoDosis: body.neumococoNinoDosis,
        neumococoNinoFechaUno: body.neumococoNinoFechaUno,
        neumococoNinoFechaDos: body.neumococoNinoFechaDos,
        neumococoNinoFechaTres: body.neumococoNinoFechaTres,
        neumococoNinoFechaCuatro: body.neumococoNinoFechaCuatro,
        neumococoNinoFechaUltima: body.neumococoNinoFechaUltima,
        influenzaNinoDosis: body.influenzaNinoDosis,
        influenzaNinoFechaUno: body.influenzaNinoFechaUno,
        influenzaNinoFechaDos: body.influenzaNinoFechaDos,
        influenzaNinoFechaTres: body.influenzaNinoFechaTres,
        influenzaNinoFechaCuatro: body.influenzaNinoFechaCuatro,
        influenzaNinoFechaUltima: body.influenzaNinoFechaUltima,
        sprNinoDosis: body.sprNinoDosis,
        sprNinoFechaUno: body.sprNinoFechaUno,
        sprNinoFechaDos: body.sprNinoFechaDos,
        sprNinoFechaTres: body.sprNinoFechaTres,
        sprNinoFechaCuatro: body.sprNinoFechaCuatro,
        sprNinoFechaUltima: body.sprNinoFechaUltima,
        sabinNinoDosis: body.sabinNinoDosis,
        sabinNinoFechaUno: body.sabinNinoFechaUno,
        sabinNinoFechaDos: body.sabinNinoFechaDos,
        sabinNinoFechaTres: body.sabinNinoFechaTres,
        sabinNinoFechaCuatro: body.sabinNinoFechaCuatro,
        sabinNinoFechaUltima: body.sabinNinoFechaUltima,
        srNinoDosis: body.srNinoDosis,
        srNinoFechaUno: body.srNinoFechaUno,
        srNinoFechaDos: body.srNinoFechaDos,
        srNinoFechaTres: body.srNinoFechaTres,
        srNinoFechaCuatro: body.srNinoFechaCuatro,
        srNinoFechaUltima: body.srNinoFechaUltima,
        otrasVacunasNinoDosis: body.otrasVacunasNinoDosis,
        otrasVacunasNinoFechaUno: body.otrasVacunasNinoFechaUno,
        otrasVacunasNinoFechaDos: body.otrasVacunasNinoFechaDos,
        otrasVacunasNinoFechaTres: body.otrasVacunasNinoFechaTres,
        otrasVacunasNinoFechaCuatro: body.otrasVacunasNinoFechaCuatro,
        otrasVacunasNinoFechaUltima: body.otrasVacunasNinoFechaUltima,
        alergia: body.alergia,
        idPaciente: body.idPaciente
    });

    newSchemaVN
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({

                        ok: true,
                        message: 'Se guardo el esquema de vacunacion',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar',
                        error
                    });
            }
        )


});

app.get('/ver/esquema/vacunacion/:id', (req, resp) => {

    let id = req.params.id;
    console.log(id);
    vninos
        .find({ idPaciente: id })
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Sigonos del paciente',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontraron',
                        error
                    });
            }
        )
});

app.put('/actualizar/esquema/vacunacion/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    vninos
        .findByIdAndUpdate(id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });


});


// app.put('/agregar/sv/:id', (req, resp) => {

//     let id = req.params.id;

//     sv
//     .findByIdAndUpdate({ _id: id })
//     .exec()
//     .then(
//         (data) => {
//             return
//         }
//     )
//     .catch()
// });

module.exports = app;