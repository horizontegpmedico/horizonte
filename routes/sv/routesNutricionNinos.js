const express = require('express');
const nNinos = require('../../models/signosVitales/nutricionNinos');


let app = express();


app.post('/agregar/AgregarNutricionNinos', (req, resp) => {


    let body = req.body;

    console.log("Nutrición Niños", body);

    let newSchemaNN = new nNinos({
        alimentarSanamenteNinosMesUno: body.alimentarSanamenteNinosMesUno,
        alimentarSanamenteNinosMesDos: body.alimentarSanamenteNinosMesDos,
        alimentarSanamenteNinosMesTres: body.alimentarSanamenteNinosMesTres,
        alimentarSanamenteNinosMesCuatro: body.alimentarSanamenteNinosMesCuatro,
        alimentarSanamenteNinosMesCinco: body.alimentarSanamenteNinosMesCinco,
        alimentarSanamenteNinosMesSeis: body.alimentarSanamenteNinosMesSeis,
        desparacitacionIntestinalNinosMesUno: body.desparacitacionIntestinalNinosMesUno,
        desparacitacionIntestinalNinosMesDos: body.desparacitacionIntestinalNinosMesDos,
        desparacitacionIntestinalNinosMesTres: body.desparacitacionIntestinalNinosMesTres,
        desparacitacionIntestinalNinosMesCuatro: body.desparacitacionIntestinalNinosMesCuatro,
        desparacitacionIntestinalNinosMesCinco: body.desparacitacionIntestinalNinosMesCinco,
        desparacitacionIntestinalNinosMesSeis: body.desparacitacionIntestinalNinosMesSeis,
        vitaminaANinosMesUno: body.vitaminaANinosMesUno,
        vitaminaANinosMesDos: body.vitaminaANinosMesDos,
        vitaminaANinosMesTres: body.vitaminaANinosMesTres,
        vitaminaANinosMesCuatro: body.vitaminaANinosMesCuatro,
        vitaminaANinosMesCinco: body.vitaminaANinosMesCinco,
        vitaminaANinosMesSeis: body.vitaminaANinosMesSeis,
        hierroNCBPNinosMesUno: body.hierroNCBPNinosMesUno,
        hierroNCBPNinosMesDos: body.hierroNCBPNinosMesDos,
        hierroNCBPNinosMesTres: body.hierroNCBPNinosMesTres,
        hierroNCBPNinosMesCuatro: body.hierroNCBPNinosMesCuatro,
        hierroNCBPNinosMesCinco: body.hierroNCBPNinosMesCinco,
        hierroNCBPNinosMesSeis: body.hierroNCBPNinosMesSeis,
        hierroPPNinosMesUno: body.hierroPPNinosMesUno,
        hierroPPNinosMesDos: body.hierroPPNinosMesDos,
        hierroPPNinosMesTres: body.hierroPPNinosMesTres,
        hierroPPNinosMesCuatro: body.hierroPPNinosMesCuatro,
        hierroPPNinosMesCinco: body.hierroPPNinosMesCinco,
        hierroPPNinosMesSeis: body.hierroPPNinosMesSeis,
        otrosNinosMesUno: body.otrosNinosMesUno,
        otrosNinosMesDos: body.otrosNinosMesDos,
        otrosNinosMesTres: body.otrosNinosMesTres,
        otrosNinosMesCuatro: body.otrosNinosMesCuatro,
        otrosNinosMesCinco: body.otrosNinosMesCinco,
        otrosNinosMesSeis: body.otrosNinosMesSeis,
        notasMesUno: body.notasMesUno,
        notasMesDos: body.notasMesDos,
        notasMesTres: body.notasMesTres,
        notasMesCuatro: body.notasMesCuatro,
        notasMesCinco: body.notasMesCinco,
        notasMesSeis: body.notasMesSeis,


        idPaciente: body.idPaciente
    });

    newSchemaNN
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({

                        ok: true,
                        message: 'Se creo nuevo sv',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo crear',
                        error
                    });
            }
        )


});

app.get('/ver/nutricion/:id', (req, resp) => {

    let id = req.params.id;

    nNinos
        .find({ idPaciente: id })
        .exec()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Sigonos del paciente',
                        data
                    });
            }
        )
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontraron',
                        error
                    });
            }
        )
});

app.put('/actualizar/nutricion/:id', (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    console.log('ID', id)
    nNinos
        .findByIdAndUpdate(id, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes',
                    data
                });
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes',
                        error
                    });
            });

})

module.exports = app;