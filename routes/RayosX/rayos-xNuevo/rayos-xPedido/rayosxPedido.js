const express = require('express');
const newRayosX = require('../../../../models/Rayos-x/Nuevo/New-Pedido/newRayosX');
// es la tabla de los pedidos

const serveResp = require('../../../../functions/functions')

const app = express();


app.post('/nuevo/pedido/rayosX', (req, resp) => {

    const { body } = req;

    const schemaPedidos = new newRayosX({
        estudios: body.estudios,
        idPaciente: body.idPaciente,
        sede: body.sede,
        medicoQueSolicito: body.medicoQueSolicito,
        fechaDePedidoDeLosExamenes: body.fecha
    });

    schemaPedidos
        .save()
        .then(data => serveResp(data, null, 'Se creo el  nuevo pedido rayos X', resp))
        .catch(error => serveResp(null, error, 'No se creo el pedido', resp));

});




app.get('/ver/pedidos/rayosX', (req, resp) => {


    newRayosX
        .find()
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los Rayos X', resp))
        .catch(error => serveResp(null, error, 'No se encontraron', resp));

});


app.get('/obtener/rayosX/pedido/:id', (req, resp) => {

    let id = req.params.id;
    newRayosX
        .findById(id)
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'se encontraron los pedidos Rayos X ', resp))
        .catch(error => serveResp(null, error, 'no se encontraron pedidos X', resp))
});

module.exports = app;