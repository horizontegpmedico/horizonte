const express = require('express');
const xrayRegresos = require('../../../../models/Rayos-x/Nuevo/Regreso/regresoX');
const serveResp = require('../../../../functions/functions');


const app = express();

const multer = require('../../../../Libs/storage')

/// lo comete para enviar a los socios

/***Note
 * 
 * , multer.array('imageUrl', 12)
 * next
 *     req.files.forEach(img => {

        imagenesXray.push(img.filename);

    })


        const imagenesXray = [];
 */
app.post('/agregar/regresos/xray', async (req, resp) => {
    
    const {body} = req;
   
    try {
        
        const newReturnUsg = new xrayRegresos({

            idPaciente: body.idPaciente,
            idPedido: body.idPedido,
            machoteEdit: body.machoteEdit,
            diagnostico: body.diagnostico,
            idEstudio: body.idEstudio,
            imageUrl: body.img,
            observaciones: body.observaciones,
            fecha: body.fecha,
            usuario: body.usuario
    
        });
    
        const dataSaved = await newReturnUsg.save();
        serveResp( dataSaved, null, 'Se guardarons los regresos', resp);

    } catch (error) {
        
        console.error(error);
        serveResp( null,error, 'No se pudieron guardar', resp);
    }

});


app.get('/ver/servicio/regreso/xray/:id', (req, resp) => {

    let id = req.params.id;
    // console.log(id)
    xrayRegresos
        .find({ idPaciente: id })
        .populate('idPaciente')
        .populate('idEstudio')
        .exec()
        .then(data => serveResp(data, null, 'se encontro los regresos de xray', resp))
        .catch(error => serveResp(null, error, 'no se encontro regreso xray', resp))
})


app.get('/ver/regreso/xray/recepcion/:id', (req, resp) => {

    let id = req.params.id;
    // console.log(id)
    xrayRegresos
        .find({ idPedido: id })
        .populate('idPaciente')
        .populate('idEstudio')
        .exec()
        .then(data => serveResp(data, null, 'se encontro los regresos de xray', resp))
        .catch(error => serveResp(null, error, 'no se encontro regreso xray', resp))
})



module.exports = app;