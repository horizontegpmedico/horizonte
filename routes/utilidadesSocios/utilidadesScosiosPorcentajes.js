const express = require('express');
const utilidadesServicios = require('../../models/utilidades/utilidadesService');
const sedes = require('../../models/sedes/sedes');
const serveResp = require('../../functions/functions');

const app = express();


app.post('/utilidad/socio/agregar', async(req, resp) => {
    const body = req.body;
    // console.log(body);
    // pasar estos valores al generar este json desde el front 
    // se deben de pasar con el mismo nombre 
    
    //! ESTA FUNCION SE ENCARGA DE ASIGNAR LOS MISMO PORCENTAJES A TODOS LOS SOCIOS
//TODO: QUITAR LA FUNCION DESPUES DE 3 SEMANAS  
    try {
    
        // obtenemos todas las sedes
        const sociosDates = await sedes.find();
        //console.log(   sociosDates  )
        sociosDates.forEach( async(element) => {
            // console.log(element)
           // recorremos las sedes y les seteamos los ids de los socios
           if(element.nomenclatura != "TLYC01"){

            const newModelUtilidades = new utilidadesServicios({
                // mandar el id de la sede y del servicio
                rangoUtilidad: body.rangoUtilidad,
        
                preciosRangoA: body.preciosRangoA,
                preciosRangoB: body.preciosRangoB,
                preciosRangoC: body.preciosRangoC,
                idServicio: body.idServicio,
                idSede: element._id,
                name: body.name
            });
            //guardamos los datos que se envian desde el front
           const dataNew = await newModelUtilidades.save();
           }
           
        });

        serveResp(sociosDates, null, 'No se creo', resp)
    } catch (error) {
        console.log(error);
        serveResp(null, error, 'Error', resp)
    }
   
    /*
   

    newModelUtilidades
        .save()
        .then(data => serveResp(data, null, 'Se guradaron las utilidades', resp))
        .catch(error => serveResp(null, error, 'No se guardaron', resp));
        */
       
});

app.get('/ver/utilidades/:id', (req, resp) => {

    const { id } = req.params;

    utilidadesServicios
        .find({ _id: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraro el servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontro el servicio', resp));

});


app.post('/ver/utilidades/item/sedes', (req, resp) => {

    const { idSede, name, idItem } = req.body;
    // console.log( idSede, name, idItem )

    utilidadesServicios
        .find({ name, idSede, idServicio: idItem })
        .populate('idSede')
        .populate('idServicio')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraro el servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontro el servicio', resp));

});

app.post('/obtener/servicios/por/idSede', (req, resp) => {

    const { idSede } = req.body;
    utilidadesServicios
        .find({ idSede })
        .populate('idSede')
        .populate('idServicio')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraro el servicios', resp))
        .catch(error => serveResp(null, error, 'No se encontro el servicio', resp));

});


app.get('/ver/servicio/utilidad/:idServicio/:idSede', (req, resp) => {

    const { idServicio, idSede } = req.params;

    utilidadesServicios
        .find({ idServicio, idSede })
        .populate('idServicio')
        .exec()
        .then(data => serveResp(data, null, 'Se encontró el serivicio', resp))
        .catch(error => serveResp(null, error, 'No se encontró el servicios', resp));
});

app.get('/ver/utilidades/sede/:name/:idSede', async(req, resp) => {
    
    try {

            var utilidades;
            const { name, idSede } = req.params;

             utilidades = await utilidadesServicios
                                .find({ name, idSede })
                                .populate( 'idServicio' , 'PRECIO_PUBLICO PRECIO_PUBLICO_URGENCIA PRECIO_MEMBRESIA PRECIO_MEMBRESIA_URGENCIA name ESTUDIO')
           
        //    sort del populate que acomoda los estudios en orden alfabetico 
            /* utilidades.sort( (a, b) => {
                if(a.idServicio.ESTUDIO != null || b.idServicio.ESTUDIO != null){
                    if(a.idServicio.ESTUDIO < b.idServicio.ESTUDIO) return -1  
                    return 1
                }
            }); */
                
            serveResp(utilidades, null, 'Se encontraro el servicios', resp)
        }
         catch (error) {
            console.log(error)
            serveResp(null, error, 'No se encontro el servicio', resp)
        }

});


app.put('/editar/utilidad/servicio/:id', (req, resp) => {

    const { id } = req.params;
    const { body } = req;


    utilidadesServicios
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then(data => serveResp(data, null, 'Se creó la utilidad', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear la utilidad', resp));
});


app.delete('/eliminar/utilidad/servicio/:id', (req, resp) => {

    const { id } = req.params;
    utilidadesServicios
        .findByIdAndRemove({ _id: id })
        .exec()
        .then(data => serveResp(data, null, 'Se elimino la utilidad', resp))
        .catch(error => serveResp(null, error, 'No se puedo eliminar', resp));

});


app.put('/actualizar/rango/:sede/:name', (req, resp) => {
    // console.log(req.body);
    const { sede, name } = req.params;
    const { rangoUtilidad } = req.body;

    utilidadesServicios
        .updateMany({ idSede: sede, name }, { rangoUtilidad })
        .then(data => serveResp(data, null, 'Se actualizo el rango', resp))
        .catch(error => serveResp(null, error, 'No se pudo actualizar', resp));

});


app.get('/ver/utilidad/:nomenclatura/:name', async(req, resp) => {

    try {
// iniciamlizamos el objeto 
 /*        let rangos = { 
            porcentajePrecioPublicoA: 0,
            porcentajePrecioMembresiaA: 0,
            porcentajePrecioPublicoUrgenciaA: 0,
            porcentajePrecioMembresiaUrgenciaA: 0,
            porcentajePrecioPublicoHospitalizacionA: 0,
            porcentajePrecioMembresiaHospitalizacionA: 0
            */
            const {nomenclatura, name} = req.params;
            const dataSedes = await  sedes.find({nomenclatura});       
            const dataUtilidad = await utilidadesServicios.findOne({idSede: dataSedes[0]._id,  name})
            .populate('idServicio');
            serveResp( dataUtilidad, null, 'Rangos encontrados' , resp);
        } catch (error) {
        console.error(error);
        serveResp( null, error, 'No se pudo conectar', resp);
    }
    
        // destructuring de los datos del la URL

        // find de la nomeclatura para obtener el ID

        // find para obtener el % de los servicios
        //const rango = dataUtilidad.rangoUtilidad;
    
        //seteamos los % de los servicios
 /*        if(rango === "A"){
           rangos  = dataUtilidad.preciosRangoA[0]            
        }else if( rango === "B") {
           rangos =   dataUtilidad.preciosRangoB[0]
        }else if(  rango === "C") {
           rangos = dataUtilidad.preciosRangoC[0]
        } */
        

    
});


//! SOLO USAR EN CASO ESPECIALES
// esta funcion se encarga de actualizar los porcentajes de los socios de manera global
//TODO: PORCENATJES SOCIOS
app.get("/obtener/porcentajes/socios", async (req, resp) => {

    try {
        
        
        const porcentajesResp = await utilidadesServicios.updateMany({},{preciosRangoA:[{
            "porcentajePrecioPublicoA": 8,
            "porcentajePrecioMembresiaA": 8,
            "porcentajePrecioPublicoUrgenciaA": 8,
            "porcentajePrecioMembresiaUrgenciaA": 8,
            "porcentajePrecioPublicoHospitalizacionA": 8,
            "porcentajePrecioMembresiaHospitalizacionA": 8
        }],
        preciosRangoB :  [{
                "porcentajePrecioPublicoB": 10,
                "porcentajePrecioMembresiaB": 10,
                "porcentajePrecioPublicoUrgenciaB": 10,
                "porcentajePrecioMembresiaUrgenciaB": 10,
                "porcentajePrecioPublicoHospitalizacionB": 10,
                "porcentajePrecioMembresiaHospitalizacionB": 10
            }],
            preciosRangoC: [
                {
                    "porcentajePrecioPublicoC": 11,
                    "porcentajePrecioMembresiaC": 11,
                    "porcentajePrecioPublicoUrgenciaC": 11,
                    "porcentajePrecioMembresiaUrgenciaC": 11,
                    "porcentajePrecioPublicoHospitalizacionC": 11,
                    "porcentajePrecioMembresiaHospitalizacionC": 11
                }
            ]
    
    }, { new: true});

        const newDataSocios = await utilidadesServicios.find();

        serveResp(newDataSocios, null, 'Se econtaron', resp);

    } catch (error) {
        console.log( error );
        serveResp( null, error, 'No se pudo conectar', resp)
    }
    

}); 

module.exports = app;