const express = require('express');
const utilidadesAmbulancia = require('../../models/utilidades/utilidadesAmbulancia');
const serveResp = require('./../../functions/functions')

const app = express();

app.post('/utilidad/ambulancia/agregar', (req, resp) => {

    const { body } = req;

    const newAmbulancia = new utilidadesAmbulancia({
        rangoUtilidad: body.rangoUtilidad,
        preciosRangoA: body.preciosRangoA,
        preciosRangoB: body.preciosRangoB,
        preciosRangoC: body.preciosRangoC,
        name: body.name,
        idServicio: body.idServicio,
        idSede: body.idSede,
    });


    newAmbulancia
        .save()
        .then(data => serveResp(data, null, 'Se creo el modelo', resp))
        .catch(error => serveResp(null, error, 'No se creo la utilidad', resp));

});


app.get('/ver/utilidades/ambulancia/:sede/:servicio', (req, resp) => {

    const { sede, servicio } = req.params;


    utilidadesAmbulancia
        .find({ idServicio: servicio, idSede: sede })
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las utilidades', resp))
        .catch(error => serveResp(null, error, 'No se encontraron las utilidades', resp));
});


app.get('/ver/utilidad/ambulancia/:name/:idSede', (req, resp) => {
    const { name, idSede } = req.params;

    utilidadesAmbulancia
        .find({ name, idSede })
        .populate('idServicio')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las utilidades', resp))
        .catch(error => serveResp(null, error, 'No se encontraron las utilidades', resp));
});

app.get('/ver/servicio/utilidad/ambulancia/:idServicio/:idSede', (req, resp) => {

    const { idServicio, idSede } = req.params;

    utilidadesAmbulancia
        .find({ idServicio, idSede })
        .exec()
        .then(data => serveResp(data, null, 'Se encontró el serivicio', resp))
        .catch(error => serveResp(null, error, 'No se encontró el servicios', resp));
});

app.put('/actualizar/utilidad/ambulancia/:id', (req, resp) => {

    const { id } = req.params;
    const { body } = req;

    utilidadesAmbulancia
        .findByIdAndUpdate(id, body)
        .then(data => serveResp(data, null, 'Se actualizaron las utilidades', resp))
        .catch(error => serveResp(null, error, 'No se pudo actualizar las utilidades', resp));
});


app.delete('/eliminar/utilidad/:id', (req, resp) => {
    const { id } = req.params;

    utilidadesAmbulancia
        .findByIdAndRemove(id)
        .then(data => serveResp(data, null, 'Se elimino la utilidad', resp))
        .catch(error => serveResp(null, error, 'No se pudo eliminar la utilidad', resp))
});

app.put('/update/rango/ambulancia/:sede', (req, resp) => {
    const { sede } = req.params;
    const { rangoUtilidad } = req.body
        // console.log(req.body);

    utilidadesAmbulancia
        .updateMany({ idSede: sede }, { rangoUtilidad })
        .then(data => serveResp(data, null, 'Se actualizaron los rangos', resp))
        .catch(error => serveResp(null, error, 'No se actualizaron los rangos', resp))
});


app.post('/ver/utilidades/ambulancia/item/sede', (req, resp) => {

    const { idSede, name, idItem } = req.body;

    utilidadesAmbulancia
        .find({ name, idSede, idServicio: idItem })
        .populate('idSede')
        .populate('idServicio')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron las utilidades', resp))
        .catch(error => serveResp(null, error, 'No se encontraron las utilidades', resp));

});

module.exports = app;