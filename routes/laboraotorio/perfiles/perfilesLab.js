const express = require('express');
const perfilesLaboratorio = require('../../../models/laboratorio/perfiles/modelPerfiles');

let app = express();

//===================================================
//      LISTADO DE LOS PERFILES
//===================================================

app.get('/perfiles', async(req, resp) => {


    await perfilesLaboratorio
        .find()
        .exec()
        .then((data) => {

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontraron los perfiles',
                    perfiles: data
                })
        })
        .catch((error) => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'no se pudieron encontrar',
                    error
                });
        })

});


//===================================================
//      LISTA UN SOLO PERFIL
//===================================================


app.get('/perfiles/:id', (req, resp) => {


    let id = req.params.id;

    perfilesLaboratorio
        .findOne({ _id: id })
        .exec()
        .then((data) => {
            console.log(data);
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontro la data',
                    data
                });
        })
        .catch((error) => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se encontro el perfil',
                    error
                });
        });

});

//===================================================
//     CREACION DE UN NUEVO PERFIL
//===================================================


app.post('/perfiles', async(req, resp) => {


    let body = req.body;

    let newPerfil = new perfilesLaboratorio({
        ESTUDIOS: body.ESTUDIOS,
        MUESTRA: body.MUESTRA,
        ENTREGA: body.ENTREGA,
        METODO: body.METODO,
        PUBLICO: body.PUBLICO,
        URGENCIA_PUBLICO: body.URGENCIA_PUBLICO,
        MEMBRESIA: body.MEMBRESIA,
        URGENCIA_MEMBRESIA: body.URGENCIA_MEMBRESIA,
        PROMOCION: body.PROMOCION,
        NOCTURNO: body.NOCTURNO,
        H6: body.H6,
        B5: body.B5
    });


    newPerfil
        .save()
        .then((data) => {
            console.log(data)
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se guardo el perfil',
                    perfil: data
                })
        })
        .catch(
            (error) => {
                console.log(error);
                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar el perfil',
                        error
                    });
            }
        )


});

//===================================================
//          ELIMINACIÓN DE UN PERFIL
//===================================================


app.delete('/perfiles/:id', (req, resp) => {


    let id = req.params.id;

    perfilesLaboratorio
        .findOne({ _id: id })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se elimino el perfil',
                    data
                });
        })
        .catch(error => {
            return resp.status(400).json({
                ok: false,
                message: 'No se pudo eliminar el perfil',
                error



            })
        })


});
//===================================================
//          ACTUALIZACION DE UN PERFIL
//===================================================

app.put('/perfiles/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;

    perfilesLaboratorio
        .findOne({ _id: id }, body)
        .exec()
        .then((dataUpdated) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actulizo la ifnromacion',
                    data: dataUpdated
                });
        })
        .catch(error => {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo actualizar',
                    error
                });
        })


});


module.exports = app;