const express = require('express');
const estudiosPedidos = require('../../../models/pedidosSinMem/pedidoLab');
const estudiosLaboratorio = require('../../../models/pedidosSinMem/estudiosLaboratorio');

let app = express();




app.post('/crear/estudio/obtenido', (req, resp) => {

    let body = req.body;

    let newEstudio = new estudiosLaboratorio({

        estudios: body.estudios,
        ventas: body.ventas,
        fecha: body.fecha,
        hora: body.hora,

    });

    newEstudio
        .save()
        .then(data => serveResp(data, null, 'Se creó el nuevo estudio', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear el estudio', resp));

});


app.get('/nuevo/estudio', (req, resp) => {

    estudiosLaboratorio
        .find()
        .populate('ventas')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron estudios nuevos', resp))
        .catch(error => serveResp(null, error, 'No se pudieron encontrar', resp));
});



app.get('/pedidos/pendientes', (req, resp) => {

    estudiosPedidos
        .find()
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encontraron los pedidos', resp))
        .catch(error => serveResp(null, error, 'No se pudieron encontrar', resp));
});


app.post('/crear/pedido', (req, resp) => {

    let body = req.body;

    let newPedido = new estudiosPedidos({
 
        estudios: body.estudios,
        idPaciente: body.idPaciente,
        fecha: body.fecha,
        hora: body.hora,
        medicoQueSolicita: body.medicoQueSolicita,
        sede: body.sede,
        prioridad: body.prioridad,
        estado: body.estado


    });

    newPedido
        .save()
        .then(data => serveResp(data, null, 'Se creó el pedido', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear el pedido', resp));

});



app.get('/obtener/pedido/:id', (req, resp) => {


    let id = req.params.id;

    estudiosPedidos
        .findById(id)
        .populate('idPaciente')

    .exec()
        .then(data => serveResp(data, null, 'Se encontro la consultas', resp))
        .catch(error => serveResp(null, error, 'No se encontro la consultas', resp));

});


app.put('/actualizar/pedido/:id/:idEstudio', (req, resp) => {
    const body = req.body;
    const {id, idEstudio} = req.params;
    console.log(id, idEstudio);
    let update = {
        $set: { 
          "estudios.$[elem].estado": body.estado
        }
    }
      
      let options = {
        new: true,
        arrayFilters: [{ "elem.idEstudio": idEstudio}]
      }
    // let id = req.params.id;
    // estudiosPedidos
    //     .findByIdAndUpdate(id)
    //     .exec()
    //     .then(data => serveResp(data, null, 'Se actualizaron los estudios', resp))
    //     .catch(error => serveResp(null, error, 'No se pudo actualizar', resp));

    estudiosPedidos
    .findByIdAndUpdate(id,update,options)
    .exec()
    .then(data =>  {
        console.log(data);
        serveResp(data,null,"ya se actualizo el estudio :D",resp) })
    .catch(error => serveResp(null,error,"no se puede aun",resp)); 

});

app.put('/actualizar/prioridad/pedido/:id', (req, resp) => {
    const body = req.body;
    const id = req.params;

    estudiosPedidos
    .findByIdAndUpdate(id,{prioridad: body.prioridad})
    .exec()
    .then(data =>  {
        console.log(data);
        serveResp(data,null,"ya se actualizo el estudio :D",resp) })
    .catch(error => serveResp(null,error,"no se puede aun",resp)); 

});


const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;