
const express = require('express');

const estudiosUnicos = require('../../../models/laboratorio/nuevos/estudioUnico');

let app = express();


/* app.post('/nuevo/estudio', (req, resp) => {

    let body = req.body;

    let nuevoEstudio = new estudiosUnicos({

        nombre: body.nombre,
      
        valoresDeReferencia: body.valoresDeReferencia,
        tipoExamen: body.tipoExamen

    });

    nuevoEstudio
        .save()
        .then(data => serveResp(data, null, 'Se creo el estudio', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear', resp));


}); */



app.get('/ver/estudios', (req, resp) => {

    estudiosUnicos
        .find()
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron los estudios', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los estudios', resp));

});

app.post("/ver/estudios", (req, resp) => {


    console.log( req.body );

    estudiosUnicos
    .find({ nombre: req.body.nombre })
    .exec()
     .then(   data => serveResp(data, null, 'Estudios encontrados', resp) )
    .catch( error => serveResp( null, error, 'No e encontraron estudios', resp ) )

});


const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;