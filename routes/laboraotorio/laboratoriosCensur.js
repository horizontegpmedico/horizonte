//libs
const express = require('express');
const app = express();

//models 
const LaboratoriosCensur = require('../../models/laboratorio/modelLaboratoriosCensur')
const pedidosServicios  = require('../../models/pedidosServicios/pedidosServicios');
//funcitons
const serveResp = require('../../functions/functions');




//creacion del formulario 
app.post('/nuevo/estudio', (req, resp) => {

    const body = req.body

    const estudios = new LaboratoriosCensur({

        nombre: body.nombreEstudio,
        tipo_de_examen: body.tipoExamen,
        valoresDeReferencia: body.valores,
        metodo: body.metodo,
        observaciones : body.observaciones,
        idEstudio:body.idEstudio
        
    })
    estudios
    .save()
    .then((data) => serveResp(data, null, 'Se guardaron los estudios correctamente', resp))
    .catch((error) => serveResp(null, error, 'No se guardaron los estudios' + error, resp));
});
//obtiene el nombre del estudio de laboratorio por el id de servicio
app.post('/editar/estudios/laboratorio/censur', (req, resp) => {

    const body = req.body

    console.log(body);

    LaboratoriosCensur.find({ 'idEstudio': body.id })
    .then((data) => serveResp(data, null, 'Se guardaron los estudios correctamente', resp))
    .catch((error) => serveResp(null, error, 'No se guardaron los estudios' + error, resp));
})
//update de los valores de referencia de los laboratorios de censur
app.put('/actualizar/valores/referencia/censur/:id', async (req, resp) => {

    try {
    
        const { id } = req.params;
        const { body } = req;

        const respEstudios = await LaboratoriosCensur.findById( id );
        
        if(respEstudios === []  || respEstudios === null) {
            serveResp(null,'','No existe el ID del estudio', resp);

        }else {
            if( body === null || body == [] ) {

                serveResp(null, 'Los valores estan vacios', 'No hay valores', resp );
            }else {
                const updatedValuesRef = await LaboratoriosCensur.findByIdAndUpdate( id, body );
                serveResp(updatedValuesRef, null, 'Se actulizaros los valores de referencia', resp);
            }
 
        }

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se puede conectar', resp);
    }
    
});

//update el proceso del pedido y obtiene los valores de referencia de los laboratorios
app.post('/obtener/valores/referencia/censur/', async (req, resp) => {

    try {
    
        const { id } = req.body;
        const updateStatus = await pedidosServicios.findOneAndUpdate({'_id':id},{status: 'EN PROCESO'}, {new: true}).populate('idPaciente');
        const valoresRef = await LaboratoriosCensur.find({idEstudio: updateStatus.idServicio})
        let valores = {
            paciente: updateStatus?.idPaciente,
            venta: updateStatus?.idVenta,
            servicio: updateStatus?.nombreServicio,
            metodo: valoresRef[0]?.metodo,
            nombre: valoresRef[0]?.nombre,
            tipo_de_examen: valoresRef[0]?.tipo_de_examen,
            valoresDeReferencia: valoresRef[0]?.valoresDeReferencia,
            imgQrCode: updateStatus?.imgQrCode,
            newEstudio: updateStatus.newEstudio
        }
        serveResp(valores, null, 'los valores de referencia son', resp);

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se puede conectar', resp);
    }
    
});

//update el proceso del pedido y obtiene los valores de referencia de los laboratorios
app.get('/hoja/reporte/resultados/:id', async (req, resp) => {

    try {
    
        const { id } = req.params;
        const updateStatus = await pedidosServicios.findOne({'_id':id}).populate('idPaciente').populate('idServicio', 'ESTUDIO');

        const valoresRef = await LaboratoriosCensur.find({idEstudio: updateStatus.idServicio})
        // console.log(valoresRef);
        let valores = {
            paciente: updateStatus?.idPaciente,
            metodo: valoresRef[0]?.metodo,
            nombre: valoresRef[0]?.nombre,
            tipo_de_examen: valoresRef[0]?.tipo_de_examen,
            valoresDeReferencia: valoresRef[0]?.valoresDeReferencia,
            resultados: updateStatus?.resultados,
            imgQrCode: updateStatus?.imgQrCode,
            nombreEstudio: updateStatus?.nombreServicio,
            servicio: updateStatus.idServicio,
            solicito: updateStatus?.doctorQueSolicito,
            fechaPedido: updateStatus.fechaPedido,
            pdf: updateStatus.pdf,
            link: updateStatus.link
        }

        serveResp(valores, null, 'los valores de referencia son', resp);

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se puede conectar', resp);
    }
    
});


module.exports = app