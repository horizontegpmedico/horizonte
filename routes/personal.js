const express = require('express');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const serveResp = require('../functions/functions');
// const mdAuth = require('../middlewares/auth');

const ModelPersonal = require('../models/personal');
const Sedes = require('../models/sedes/sedes');

const app = express();


//==================================
// verificacion del middleware
//==================================

app.post('/personal/nuevo', async(req, resp) => {
    let body = req.body;
    try {
        const sede = await Sedes.findById(body.sedeId);
        if(sede){ 
           body.sede =  sede.nomenclatura
        }
        body.password = bcryptjs.hashSync(body.password, 10);
        if(body.checkMedico){
            body.medico = body.nombre;
        }
        const personalN = new ModelPersonal(body);
        const personalSaved = await personalN.save();
        return serveResp(personalSaved, null, 'EMPLEADO REGISTRADO CORRECTAMENTE', resp);
    } catch (error) {
        console.log(error);
        return serveResp(null, error, "NO SE PUDO REGISTRAR EL EMPLEADO", resp);
    }
});

function validateToken(req, res, next) {
    const accessToken = req.body.token;
    if(!accessToken){
        // res.sendStatus(403);
        res.status(403).json({ok: false, message: 'Acceso denegado' });
    }else{
        jwt.verify(accessToken, process.env.SEED, (err, user) => {
            if(err){
                res.status(403).json({ok: false, message: 'Acceso denegado o token expirado' });
            }else{
                next();
            }
        });
    }
}

app.get('/personal', async(req, resp) => {

    await ModelPersonal
        .find()
        .exec((error, personalDB) => {

            if (error) {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'Algo pasó',
                        error
                    });
            }
            if (!personalDB) {
                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No se encontró',
                        personal: personalDB
                    });
            }

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontró personal',
                    personal: personalDB
                });

        });

});

app.get('/ver/personal', (req, resp) => {

    let nombre = req.params.nombre;

    ModelPersonal
        .find()
        .sort()
        .exec()
        .then(data => serveResp(data, null, "Se encotro el colaborado", resp))
        .catch(error => serveResp(null, error, 'No se pudo encontrar', resp));


});

app.put('/actualizar/password/:id', async (req, resp) => {

    const { id} = req.params;
    const { body } = req;

    
        
        
        try {
            const personal = await ModelPersonal.findById( id );
            if(personal) {

                const passwordHash = bcryptjs.hashSync(body.nuevaPassword, 10);
                const updatedPass = await ModelPersonal.findByIdAndUpdate( id, {  password: passwordHash } );
                serveResp(updatedPass, null, 'Se actulizo la contraseña', resp);
            }
    } catch (error) {

        serveResp(null, error, 'No se pudo actualizar la contraseña' , resp);        
    }


});


app.get('/obtener/medicos/por/sede/:sede', async(req, resp) => {

    try {
        const { sede } = req.params
        const medicosDB = await ModelPersonal.find({sede, role:'Medico', status:'Activo'}).sort({"NOMBRE" : 1});
        serveResp(medicosDB, null, 'Se encontraron los medicos', resp);
     } catch (error) {
         serveResp(null, error, 'No se pudo conectar', resp)  
     }
});


module.exports = app;