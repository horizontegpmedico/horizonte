const express = require('express');
const ambulancia = require('../../models/servicioIntegrados/modelAmbulancia');
const rayosX = require('../../models/servicioIntegrados/modelRayoX');

let app = express();


app.get('/todos/servicios/busqueda/:termino', async(req, resp) => {

    let termino = req.params.termino;
    // se genera la expresion regular que es sensible a las mayusculas
    let regexp = new RegExp(termino, 'i');

    // ejecutamos todas las promesas que se han hecho para obtener todos los resultados
    Promise.all(
            [
                busquedAmbulancia(termino, regexp),
                busquedaRayosX(termino, regexp),
                busquedaUltrasonido(termino, regexp),
                buquedaOtrosServicios(termino, regexp),
                busquedaEndoscopia(termino, regexp),
                busquedaExamenesLaboratorio(termino, regexp),
                busquedaTomogrfia(termino, regexp),
                busquedaPatolgia(termino, regexp)
            ]
        )
        .then((data) => {

            // listamos los resultados de las promesas 
            resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontró',
                    ambulancia: data[0],
                    rayosX: data[1],
                    ultrasonido: data[2],
                    otrosServicio: data[3],
                    endoscopia: data[4],
                    laboratorios: data[5],
                    tomografia: data[6],
                    patologia: data[7]

                });

        })
        .catch((error) => {
            resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo conectar',
                    error
                });

        })

});


const busquedAmbulancia = (termino, regexp) => {

    return new Promise((resolve, reject) => {


        ambulancia.find({ DESTINO: regexp }, (error, destinoDB) => {

            if (error) {
                reject('Error con ambulancia', error);
            } else {
                resolve(destinoDB);
            }

        });


    });

}

const busquedaRayosX = (termino, regexp) => {

    return new Promise((resolve, reject) => {


        rayosX.find({ ESTUDIO: regexp }, (error, destinoDB) => {

            if (error) {
                reject('Error con ambulancia', error);
            } else {
                resolve(destinoDB);
            }

        });


    });

}

const busquedaUltrasonido = (termino, regexp) => {

    return new Promise((resolve, reject) => {

        ultrasonido.find({ ESTUDIO: regexp }, (error, ultrasonidoDB) => {

            if (error) {
                reject('Error en ultrasonido', error);
            } else {
                resolve(ultrasonidoDB);
            }
        });

    });
}

const busquedaPatolgia = (termino, regexp) => {


    return new Promise((resolve, reject) => {


        patologia
            .find({ ESTUDIO: regexp }, (error, patologiaDB) => {

                if (error) {
                    reject('Error con patologias', error)
                } else {
                    resolve(patologiaDB);
                }

            });

    });

}


const buquedaOtrosServicios = (termino, regexp) => {

    return new Promise((resolve, reject) => {

        otrosServicios.find({ ESTUDIO: regexp }, (error, servicioDB) => {

            if (error) {
                reject('Error en Otros servicios', error);
            } else {
                resolve(servicioDB);
            }
        });

    });
}

const busquedaEndoscopia = (termino, regexp) => {
    return new Promise((resolve, reject) => {

        endoscopia.find({ ESTUDIO: regexp }, (error, endoscopiaDB) => {
            if (error) {
                reject('Error en endoscopia', error);
            } else {
                resolve(endoscopiaDB);
            }
        });
    })

}


const busquedaExamenesLaboratorio = (termino, regexp) => {


    return new Promise((resolve, reject) => {

        examenesLaboratorio.find({ ESTUDIO: regexp }, (error, estudioDB) => {
            if (error) {
                reject('Error en Otros servicios', error);
            } else {
                resolve(estudioDB);
            }
        });
    })

}


const busquedaTomogrfia = (termino, regexp) => {
    return new Promise(
        (resolve, reject) => {
            tomografia.find({ ESTUDIO: regexp }, (error, tomografiaDB) => {

                if (error) {
                    reject('Error en tomografia', error);
                } else {
                    resolve(tomografiaDB);
                }

            });
        }
    );
}

module.exports = app;