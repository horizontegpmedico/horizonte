'use strict'
const express = require('express');
const aparatosYsistemas = require('../../models/aparatosYsistemas/aparatosYsistemas');

let app = express();

app.post('/agregar/aparatos/sistemas', (req, resp) => {
    let body = req.body;
    /* console.log("Aparatos y sistmas", body); */

    let newAparatoSistemas = new aparatosYsistemas({
        motivoDeConsulta: body.motivoDeConsulta,
        evolucionDelPadecimiento: body.evolucionDelPadecimiento,
        medicoTrante: body.medicoTrante,
        diagnostico: body.diagnostico,
        plan: body.plan,
        respiratorioCardiovascular: body.respiratorioCardiovascular,
        digestivo: body.digestivo,
        endocrino: body.endocrino,
        musculoEsqueletico: body.musculoEsqueletico,
        genitourinario: body.genitourinario,
        hematopoyeticoLinfatico: body.hematopoyeticoLinfatico,
        pielAnexos: body.pielAnexos,
        neurologicoPsiquiatricos: body.neurologicoPsiquiatricos,
        piel: body.piel,
        cabezaCuello: body.cabezaCuello,
        torax: body.torax,
        abdomen: body.abdomen,
        genitales: body.genitales,
        extremidades: body.extremidades,
        sistemaNervioso: body.sistemaNervioso
    });

    newAparatoSistemas
        .save()
        .then(
            (data) => {
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Se guardaron Aparatos y Sistemas',
                        data
                    });
            })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudieron guardar los antecedentes',
                    error
                });
        });
});

app.get('/ver/aparatos/sistemas/:id', (req, resp) => {

    let id = req.params.id;

    aparatosYsistemas
        .find({ idPaciente: id })
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron los antecedentes gineco obstetricos', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los antecedentes gineco obstetricos', resp))

});



const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}

app.put('/actualizar/aparatos/sistemas/paciente/:id', (req, resp) => {

    let id = req.params.id;
    let body = req.body;

    aparatosYsistemas
        .findByIdAndUpdate({ _id: id }, body)
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se actualizaron los antecedentes Aparatos y Sitemas',
                    data
                })
        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudieron actualizar los antecedentes Aparatos y Sistemas',
                        error
                    });
            });


});





module.exports = app;