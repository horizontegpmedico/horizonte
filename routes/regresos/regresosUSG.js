const express = require('express');
const usgRegresos = require('../../models/Regresos/regresosUSG');
const serveResp = require('../../functions/functions');


const app = express();

// const multer = require('../../Libs/storage')

app.post('/agregar/regresos/usg',  (req, resp) => {
    
    const {body} = req;

    const newReturnUsg = new usgRegresos({
        idPaciente: body.idPaciente,
        idPedido: body.idPedido,
        machoteEdit: body.machoteEdit,
        diagnostico: body.diagnostico,
        idEstudio: body.idEstudio,
        imageUrl: body.img,
        observaciones: body.observaciones,
        fecha: body.fecha,
        usuario: body.usuario

    });

    newReturnUsg
        .save()
        .then(data => serveResp(data, null, 'Se envió', resp))
        .catch(error => serveResp(null, error, 'No se encontró', resp));

});


app.get('/ver/estudio/regreso/ultrasonido/:idEstudio', (req, resp) => {

    const { idEstudio } = req.params;
    // console.log(id)

    usgRegresos
        .findById({ idEstudio })
        .populate('idPaciente')
        .populate('idEstudio')
        // // .populate('idPedido')
        // .populate('imageUrl')
        .exec()
        .then(data => serveResp(data, null, 'se encontro los regresos', resp))
        .catch(error => serveResp(null, error, 'no se encontro regreso usg', resp))
});


app.get('/ver/estudio/regreso/ultrasonido/hc/:id', (req, resp) => {

    const { id } = req.params;
    // console.log(id)

    usgRegresos
        .find({ idPaciente: id })
        .populate('idPaciente')
        .populate('idEstudio')
        // // .populate('idPedido')
        // .populate('imageUrl')
        .exec()
        .then(data =>serveResp(data, null, 'se encontro los regresos', resp))
        .catch(error => serveResp(null, error, 'no se encontro regreso usg', resp))
})


app.get('/ver/usg/regresos/:idPaciente/:idPedido', async (req, resp) => {
    
    const {
        idPaciente,
        idPedido
    } = req.params;

    try {
     const dataPedidoId =  await  usgRegresos.find({ idPaciente: idPaciente , idPedido: idPedido}).populate('idEstudio');
        // console.log( dataPedidoId )
     serveResp(dataPedidoId, null, 'Se encontro el pedido', resp);
    } catch (error) {
        console.error(error);
        serveResp(null, error, 'No se encontro', resp);
    }

});


module.exports = app;