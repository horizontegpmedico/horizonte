const express = require('express');
const laboratorioRegresos = require('../../models/Regresos/laboratorioRegresos');
const serveResp = require('../../functions/functions');

const app = express();



app.post('/agregar/regresos/lab', (req, resp) => {

    const body = req.body;
    console.log(body)
    const newRegreso = new laboratorioRegresos({
        idPaciente: body.idPaciente,
        idPedido: body.idPedido,
        obtenidos: body.obtenidos,
        idEstudio: body.idEstudio,
        quimico: body.quimico
    });

    newRegreso
        .save()
        .then(data => serveResp(data, null, 'Se envió', resp))
        .catch(error => serveResp(null, error, 'No se encontró', resp));
});





app.get('/ver/estudio/laboratorio/:id/:idPedido', async (req, resp) => {

    const {id, idPedido } = req.params;


    laboratorioRegresos
        .find({ idPaciente: id, idPedido })
        .populate(' idPaciente')
        .populate('idEstudio')
        .populate('idPedido')
        .exec()
        .then(data => serveResp(data, null, 'se encontro los regresos', resp))
        .catch(error => serveResp(null, error, 'no se encontro regreso laboratorio', resp))
});



app.get('/ver/laboratorios/hoja/evolucion/:id', async(req, resp) => {

    const { id } = req.params;


    try {

        const laboratoriosPacienteDB = await laboratorioRegresos.find({ idPaciente: id }).populate('idEstudio').populate('idPedido')
        serveResp(laboratoriosPacienteDB, null, 'Se econtaron los laboratorios', resp);
    
    } catch (error) {
    
        console.log(error);
        serveResp(null, error, 'Algo pasó', resp);
    }
});



module.exports = app;