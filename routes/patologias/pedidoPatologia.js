const patologiaPedido = require('../../models/Regresos/patologias');
const patologiaregresos = require('../../models/Regresos/regresosPatologias');

const express = require('express');
const serveResp = require('../../functions/functions');
const app = express();

// creacion de los pedidos que se hacen por medio de las ventas de los servicios
app.post('/agregar/pedido/patologia', async(req, resp) => {

    const { body} = req;

    try {

        const pedidoToSave = new  patologiaPedido(body);
        const pedidoSaved = await pedidoToSave.save();

        serveResp(pedidoSaved, null, 'Se guardo el pedido', resp);

    } catch (error) {
        console.error(error);
        serveResp(null, error, 'Algo paso', resp);
    }

});


//obtenmos los pedidos que estan pendientes

app.get('/ver/pedidos/:status', async (req, resp) => {

    const {status} = req.params;

    if(status == 'PEDIDO' || status == "FINALIZADO") {

        try {
            
            const pedidosSaved = await patologiaPedido.find({ status })
            .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero')
            .sort({ fechaDePedidoDeLosExamenes :1 })
    
            serveResp(pedidosSaved, null, 'Se encontraron los pedidos', resp);
            
        } catch (error) {
            console.error(error);
            serveResp(null, error, 'Algo paso', resp);
        }
        
        
    }else {
        
        serveResp(null, 'No es valido el status', 'Status no valido', resp);
    
    }
});

app.get('/ver/pedido/:id', async (req, resp) => {

    try {
        
        const { id} = req.params;

        const pedidosFinded = await patologiaPedido.findById(id)
                                .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero curp telefono fechaNacimientoPaciente')
        
        if(pedidosFinded) {


            serveResp( pedidosFinded, null, 'Se encontro el pedido', resp);

        }else {
            const errorResp = "El id no existe";
            serveResp(null, errorResp, 'No encontró el ID', resp);
        }

    } catch (error) {
        console.error(error);
        serveResp(null, error, 'Algo paso', resp);
    }

});

app.post('/agregar/regresos/patologia', async (req, resp) => {

    const { body } = req;

    try {
        
        const regresosPatologiaSaved = new patologiaregresos(body);
        const regresos = await regresosPatologiaSaved.save();

        if( !regresos ) {
            serveResp(null, 'No se pudo guardar', 'No se pudo guardar', resp);
        }

        const patologiaStatus = await patologiaPedido.findByIdAndUpdate( body.idPedido, { status: 'FINALIZADO', idObtenido: regresosPatologiaSaved  }, {new: true});


        serveResp( patologiaStatus, null, 'Se creo el regreso', resp);

    } catch (error) {
        console.error(error);
        serveResp(null, error, 'No se pudo conectar', resp);
    }

});


app.get("/ver/obtenidos/patologia/:id", async (req, resp) => {
    
    const  { id } = req.params;
    
    
    try {
        
        if(id == ""){
    
            serveResp(null, 'No es in ID valido', 'No es un ID valido', resp);
        }

        const dataObtenido = await patologiaPedido.findById( id )
                                    .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero curp telefono fechaNacimientoPaciente religion')
                                    .populate('idObtenido');

        if(!dataObtenido) {

            serveResp(null, 'No se enoctro el regreso', 'No es un ID valido', resp);
        }

        serveResp( dataObtenido, null, 'Se encontró el regreso', resp);

    } catch (error) {
        console.error(error);
        serveResp(null, error, 'No se pudo ver', resp);
    }

});
module.exports = app;