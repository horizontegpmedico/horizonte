const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const csv = require('csvtojson');
var fs = require('fs');
var path = require('path');

const paciente = require('../models/nuevoPaciente');
const codigoPostal = require('../models/codigoPostal');
const serveResp = require('../functions/functions');
const paquetePacientes = require('../models/paquetes-paciente')
const hojaEvolucion = require('../models/consultasGeneralesBitacoras/hojaEvolucion');
const consecutivoExpediente = require('../models/consecutivoExpediente/consecutivoExpediente');
const app = express();

app.post("/paciente/:sede", async (req, resp) => {
  const { sede } = req.params;
  const body = req.body;

  try {
    const encontrarConsecutivo = await consecutivoExpediente.findOne({ sede });
    if (+body.numeroExpediente > encontrarConsecutivo.consecutivo) {
      return resp.status(500).json({
        ok: false,
        message: 'No se puede agregar un expediente mayor al consecutivo'
      });
    }
    const pacienteN = new paciente(body);
    const pacienteSaved = await pacienteN.save();
    if (pacienteSaved) {
      if (encontrarConsecutivo?.consecutivo === +body.numeroExpediente && body.activeExpediente) {
        await consecutivoExpediente.findByIdAndUpdate(encontrarConsecutivo._id, {
          consecutivo: encontrarConsecutivo.consecutivo + 1
        });
      }
      return respuestaServer(pacienteSaved, null, "Se creó el paciente", resp);
    }
    return respuestaServer(null, 'Hubo un error', "No se creó el paciente", resp);
  } catch (error) {
    console.log(error);
    return respuestaServer(null, error, "No se pudo crear el paciente", resp);
  }
});


app.put("/cambiar/estado/membresia/:id", async(req, resp) => {
  let id = req.params.id;
  let body = req.body;
  console.log(id);
  console.log(body);
  const encontrarPac = await paciente.findById(id);
  console.log(encontrarPac);
  if(!encontrarPac){
    respuestaServer(
           null,
           error,
           "No se pudo actualizar el estado de la mebresia",
          resp
        )
  }
  const pacienteActualiza = await paciente.findByIdAndUpdate(id, {membresiaActiva: body.membresiaActiva},{new:true});
  if(!pacienteActualiza){
    respuestaServer(
      null,
      error,
      "No se pudo actualizar el estado de la mebresia",
     resp
   )
  }
  respuestaServer(
          pacienteActualiza,
          null,
          "Se actualizo el estado de la membresia",
          resp
        )
  // paciente
  //   .findByIdAndUpdate(id, {membresiaActiva: body.membresiaActiva},{new:true})
  //   .exec()
  //   .then((data) =>
  //     respuestaServer(
  //       data,
  //       null,
  //       "Se actualizo el estado de la membresia",
  //       resp
  //     )
  //   )
  //   .catch((error) =>
  //     respuestaServer(
  //       null,
  //       error,
  //       "No se pudo actualizar el estado de la mebresia",
  //       resp
  //     )
  //   );
});

app.get("/pacientes/repetidos", (req, resp) => {
  paciente
    .find({}, {"nombrePaciente":1, "apellidoPaterno":1, "apellidoMaterno":1})
    .sort({"nombrePaciente":1})
    .then((data) =>
      respuestaServer(data, null, "Se encontraron los pacientes", resp)
    )
    .catch((error) =>
      respuestaServer(null, error, "No se encotraron pacientes", resp)
    );
});

app.get("/pacientes", (req, resp) => {
  paciente
    .find({ status: 'Activo' })
    .populate("paquetes")
    .populate("paquetesVisitas")
    .limit(56)
    .sort({_id:-1})
    // .sort({fechaRegistro: -1})
    .then((data) =>{
      respuestaServer(data, null, "Se encontraron los pacientes", resp)
    }
    )
    .catch((error) =>
      respuestaServer(null, error, "No se encotraron pacientes", resp)
    );
});

// app.get("/pacientes/repetidos/tabla", (req, resp) => {
//   paciente
//     .find()
//     .then((data) =>{
//       respuestaServer(data, null, "Se encontraron los pacientes", resp)
//     }
//     )
//     .catch((error) =>
//       respuestaServer(null, error, "No se encotraron pacientes", resp)
//     );
// });

//=====================================================================
// ESTE SERVICIO RETORNA TODOS LOS PACIENTES CON LA PAGINACION
//=====================================================================
app.get("/ultimo/expediente/pacientes/:sede",async (req, resp) => {
  const sede = req.params.sede;
  try{
    const encontrarConsecutivo = await consecutivoExpediente.findOne({sede:sede});
    /* console.log(totalExpediente[0].numeroExpediente) */
    serveResp(encontrarConsecutivo.consecutivo, null, 'se encontro el ultimo expediente', resp)
  }catch (error) {
    /* console.log("error",error); */
    serveResp(null, error, 'No se encontro el paciente', resp)
  }
});
//=====================================================================
// ESTE SERVICIO RETORNA TODOS LOS PACIENTES CON LA PAGINACION
//=====================================================================

app.get("/paciente/:intervalo", async (req, resp) => {
  let intervalo = req.params.intervalo | 0;
  // if(intervalo < 0) {
  //     intervalo -= intervalo;
  // }

  await paciente
    .find()
    .skip(intervalo)
    .limit(8)
    .populate("paquetes")
    .exec()
    .then((users) => {
      if (users === null || users.length === 0) {
        return resp.status(200).json({
          ok: false,
          message: "No hay pacientes",
        });
      }

      return resp.status(200).json({
        ok: true,
        message: "Los usuarios son:",
        users: users,
      });
    })
    .catch((error) => {
      return resp.status(400).json({
        ok: false,
        message: "Algo pasó con la DB",
        error,
      });
    });
});

//=============================================
//        Obtener paciente por el ID
//=============================================

app.get("/pacientes/:id", (req, resp) => {
  let id = req.params.id;
  if (id < 2) {
    return resp.status(300).json({
      ok: false,
      message: "No se introdujo un ID",
    });
  }
  paciente
    .findById({_id: id})
    .populate("paquetes")
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "se encontro el usuario", 
        paciente: data,
      });
    })
    .catch((error) => {
      return resp.status(400).json({
        ok: false,
        message: "No se encontró el usuario",
        error,
      });
    });
});

/*     let id = req.params.id;


    if (id < 2) {
        return resp.status(300)
            .json({
                ok: false,
                message: 'No se introdujo un ID'
            })
    }

    paciente.findById({ _id: id })
        .populate('paquetes')
        .then(
            (data) => {

                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'se encotro el usuario',
                        paciente: data
                    });

            })
        .catch(
            (error) => {

                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No se encontró el usuario',
                        error
                    })

            });
 */

//=============================================
//        Obtener direccion por codigo postal
//=============================================

app.post('/codigo/postal', async (req, resp) => {

    const body = req.body;
    /* console.log(req.body); */

    try {
        if (body.codigoPostal != '') {
            const jsonArrayCode = await codigoPostal.find({codigoPostal: body.codigoPostal})
           // console.log(jsonArrayCode);
            serveResp(jsonArrayCode, null, 'todos los datos del banco de sangre son', resp)   
        } 
        if (body.estado != '') {
            const jsonArrayCode = await codigoPostal.find({cpEstado: body.estado})
        //    console.log(jsonArrayCode);
            serveResp(jsonArrayCode, null, 'todos los datos del banco de sangre son', resp)   
        }
    } catch (error) {
        /* console.log("error",error); */
        serveResp(null, error, 'No se pudo agregar el banco de sagre', resp)
    }
    
});



//=============================================
//        Obtener paciente por nombre
//=============================================

app.post("/pacientes/nombre", async(req, resp) => {

  try{

    const {nombre} = req.body;
    //oconsole.log( "Buscar por nombre completo", nombre.toUpperCase() )
    // var nombreBuscar = "";
    // var nombreBuscar_arr = "";
    // const arrNombres = nombre.trim().split(" ");
    
    // for(var i = 0;  i <= 1;  i++){

    //     if(arrNombres[i] == "" || arrNombres[i] == " " || arrNombres[i] == undefined){
    //         console.log("Entra")
    //     }else{
    //         arrNombres[i].trim();
    //         if( nombreBuscar != " "  || nombreBuscar.length == 0){
    //             nombreBuscar = arrNombres[0].trim();
    //         }else {

    //             return
    //         }
    //     }
    // }
    //         //creamos el nombre completo por el nombre + apellido paterno + apelllido materno
    // for(var i = 0;  i <= arrNombres.length;  i++){

    //     if(arrNombres[i] == "" || arrNombres[i] == " " || arrNombres[i] == undefined){
    //     }else{
    //         arrNombres[i].trim();
    //         if( nombreBuscar == "" ){
    //             nombreBuscar = arrNombres[i].trim();
    //         }else {
    //             nombreBuscar_arr += " " +arrNombres[i]; 
    //         }
    //     }
    // }

    if (nombre.length < 2) {
        return resp.status(400)
            .json({
                ok: false,
                message: 'No se ha introducido un nombre'
            })
    }
    let regexp = new RegExp(nombre.trim(), 'i');
    const pacientesFilter =  await paciente.find({ nombreCompletoPaciente: {$regex:  regexp   }});
    console.log( pacientesFilter.length )
    //OBED CONTRERAS  FERNDÁNDEZ
    // for( var j = 0; j < pacientesFilter.length; j++){
    //     // var nombreCompleto = "";
    //     // nombreCompleto = pacientesFilter[j].nombrePaciente.trim() + " " + pacientesFilter[j].apellidoPaterno.trim() + " " + pacientesFilter[j].apellidoMaterno.trim()
    //     if( nombreCompleto.includes(nombreBuscar_arr.trim().toUpperCase())  ){
    //         arrReponse.push( pacientesFilter[j] );
    //     }   

    // };

    respuestaServer(pacientesFilter, null , 'Se encontraron los pacientes', resp);
}catch ( error) {

    console.log( error);
    respuestaServer(null, error, 'No se econtraron pacientes', resp);
}

  // const nombre = req.params.nombre;

  // if (nombre.length < 2) {
  //   return resp.status(400).json({
  //     ok: false,
  //     message: "No se ha introducido un nombre",
  //   });
  // }
  // let regexp = new RegExp(nombre, "i");

/*   paciente
    .find({nombrePaciente:  })
    .populate("paquetes")
    .exec()
    .then((data) =>
      respuestaServer(data, null, "Se encontraron pacientes", resp)
    )
    .catch((error) =>
      respuestaServer(null, error, "No se econtraron pacientes", resp)
    ); */
});


//=============================================
//        Obtener paciente por expediente
//=============================================

app.post("/pacientes/expediente", async(req, resp) => {
  try{
    const {expediente} = req.body;
    if (expediente.length == 0) {
        return resp.status(400)
            .json({
                ok: false,
                message: 'No se ha introducido un expediente'
            })
    }
    let regexp = new RegExp(expediente.trim(), 'i');
    const pacientesFilter =  await paciente.find({ numeroExpediente: expediente});
    respuestaServer(pacientesFilter, null , 'Se encontraron los pacientes', resp);
  }catch ( error) {
    console.log( error);
    respuestaServer(null, error, 'No se econtraron pacientes', resp);
  }
});

//=============================================
//        Obtener paciente por nombre y sede
//=============================================

app.post("/pacientes/nombre/:sede", async(req, resp) => {
  try{
    const {nombre} = req.body;
    const sede = req.params.sede;
    if (nombre.length < 2) {
        return resp.status(400)
            .json({
                ok: false,
                message: 'No se ha introducido un nombre'
            })
    }
    let regexp = new RegExp(nombre.trim(), 'i');
    const pacientesFilter =  await paciente.find({ nomenclaturaRegistro: sede ,nombreCompletoPaciente: {$regex:  regexp   }});
    respuestaServer(pacientesFilter, null , 'Se encontraron los pacientes', resp);
}catch ( error) {
    console.log( error);
    respuestaServer(null, error, 'No se econtraron pacientes', resp);
}
});

app.put("/pacientes/:id", (req, res) => {
  const pacienteId = req.params.id;
  const body = req.body;
  paciente
    .findByIdAndUpdate(pacienteId, body)
    .exec()
    .then((data) =>
      respuestaServer(data, null, "Se guardo la informacion", res)
    )
    .catch((error) =>
      respuestaServer(null, error, "No se pudo guardar la informacion", res)
    );
});

// se agrega el paquete al paciente, por medio del update

// update de los datod del paciente
app.put("/actualizar/paciente/:id", async (req, resp) => {
  const { id } = req.params;
  const body = req.body;
  try {
    if (body.sede) {
      const consecutivo = await consecutivoExpediente.findOne({ sede: body.sede });
      if (+body.numeroExpediente > consecutivo.consecutivo) {
        return resp.status(500).json({
          ok: false,
          message: 'No se puede agregar un expediente mayor al consecutivo'
        });
      }
      if (+body.numeroExpediente === consecutivo.consecutivo) {
        await consecutivoExpediente.findByIdAndUpdate(consecutivo._id, {
          consecutivo: consecutivo.consecutivo + 1
        });
      }
    }
    const pacienteActualizado = await paciente.findByIdAndUpdate(id, body, { new: true });
    if (pacienteActualizado) {
      return respuestaServer(pacienteActualizado, null, "Se guardó la información", resp);
    } else {
      return respuestaServer(null, 'No se encontró el paciente', "No se pudo actualizar la información", resp);
    }
  } catch (error) {
    console.log(error);
    return respuestaServer(null, error, "No se pudo guardar la información", resp);
  }
});


// agregamos los paquetes al paciente

// se recibe el id del paciente
app.post("/agregar/paquetes/:id", async (req, resp) => {
  const id = req.params.id;
  const body = req.body;
  console.log(id);
  const paquetes = {
    paquetes: body._id,
  };

  try {
    let x;
    const pacientes = await paciente.findById(id)
    console.log(pacientes);
    if(pacientes.paquetes.length > 0){
      /* let encontrar = pacientes.paquetes.find(element => element == body._id);
      if(encontrar == null){ */
        await paciente.findByIdAndUpdate(id, {$push: paquetes},{new:true}).exec()
        const paquetePaciente = new paquetePacientes({
          paquete:body._id,
          paciente:id,
          pagos:body.pago
        })
        x = await paquetePaciente.save()
      /* }else{
        x = "Repetido"
      } */
        /* if(body._id != pacientes.paquetes[index]){
          
        }else{
          
        } */
      
      respuestaServer(x, null, "Se agrego el paquete", resp)
    }else{
      await paciente.findByIdAndUpdate(id, {$push: paquetes},{new:true}).exec()
      const paquetePaciente = new paquetePacientes({
        paquete:body._id,
        paciente:id,
        pagos:body.pago
      })
      const x = await paquetePaciente.save()
      respuestaServer(x, null, "Se agrego el paquete", resp)
    }
  }catch (error) {
    /* console.log(error); */
    respuestaServer(null, error, "No se pudo agregar el paquete", resp)
  }

});

// agregar la historia clinica al paciente

app.put("/historia-clinica/:id", (req, res) => {
  const id = req.params.id;
  const body = req.body;

  const historia = {historiaClinica: body};

  paciente
    .findByIdAndUpdate({_id: id}, {$push: historia})
    .exec()
    .then((data) => {
      return res.status(200).json({
        ok: true,
        message: "Se agrego la historia clinica",
        data,
      });
    })
    .catch((error) => {
      return res.status(400).json({
        ok: false,
        message: "No se pudo guardar la historia clinica",
        error,
      });
    });
});

// servicio para ver la histori clinica

app.get("historia-clinica/:id", (req, resp) => {
  let id = req.params.id;

  paciente
    .findById({_id: id})
    .exec()
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "prmmm datos!",
        data: data,
      });
    })
    .catch((error) => {
      return resp.status(400).json({
        ok: false,
        message: "No se pueden mostar los prmm datos!!!",
        error,
      });
    });
});

/**
 * 
 * helper para actualizar la edad de los pacientes

app.get("/actualizar/edad/paciente", async (req, resp)=>{

  const dateToday = new Date();

  const pacientes = await paciente.aggregate([
    {
        $project:{
          dateBirth: { $subtract: [ paciente.fechaNacimientoPaciente, dateToday.toISOString() ]}
         } 
    }
   ]);

   console.log(pacientes)

});

*/

app.put("/actualizar/paciente/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;

  paciente
    .findByIdAndUpdate({_id: id}, {body})
    .exec()
    .then((pacienteUpdated) => {
      return resp.status(200).json({
        ok: true,
        message: "Se actualizo la informacio",
        paciente: pacienteUpdated,
      });
    })
    .catch((error) => {
      return resp.status(200).json({
        ok: false,
        message: "No se pudo guardar",
        error,
      });
    });
});

// agregar un paquete a un paciente

app.put("/agregar/paquete/paciente/referencia/:id", (req, resp) => {
  let id = req.params.id;

  let body = req.body;

  paciente
    .findByIdAndUpdate({_id: id}, {$push: {paquetes: body.idPaquete}})
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Se actualizaron los paquetes",
        data,
      });
    });
});

// servicio que actualiza el modelo de la medicina preventiva
app.put("/agregar/medicina/preventiva/paciente/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;
  
  
  paciente
    .findByIdAndUpdate(
      {_id: id},
      {medicinaPreventiva: body.idMedicinaPreventiva}
    )
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Se actualizó",
        data,
      });
    })
    .catch((error) => {
      return resp.status(500).json({
        ok: false,
        message: "No se pudo guardar",
        error,
      });
    });
});

// este servicio agrega al modelo del paciente el modelo de los antecedentes (Recibir ID Paciente)

app.put("/agregar/antecedentes/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;


  paciente
    .findByIdAndUpdate({_id: id}, {antecedente: body.idAntecedentes})
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Se agrego el modelo",
        data,
      });
    })
    .catch((error) => {
      return resp.status(500).json({
        ok: false,
        message: "No se pudo guardar",
        error,
      });
    });
});

// ver los antecedentes del paciente

app.get("/ver/antecedentes/paciente/:id", async (req, resp) => {
  let id = req.params.id;

  paciente
    .findById({_id: id})
    .populate("antecedente")
    .exec()
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Antecedentes paciente",
        data,
      });
    })
    .catch((error) => {
      return resp.status(500).json({
        ok: false,
        error,
        message: "No se pudo encontrar",
      });
    });
});

// esta seccion nos da los antecedentes no patologicos del usuario, primero hay que agregar el ID

// model para diferenciar entre las rutas de los demas
app.put(
  "/actualizar/antecedentes/no/patologicos/model/paciente/:id",
  (req, resp) => {
    let id = req.params.id;
    let body = req.body;

    paciente
      .findByIdAndUpdate(
        {_id: id},
        {antecedentesPersonalesNoPatologicos: body.id}
      )
      .exec()
      .then((data) => {
        return resp.status(200).json({
          ok: true,
          message: "Se actualizo el id",
          data,
        });
      })
      .catch((error) => {
        return resp.status(500).json({
          ok: false,
          message: "No se pudo actualizar el id",
          error,
        });
      });
  }
);

// SERVICIO QUE AGREGA AL MODELO DE LOS PACIENTES LOS ANTECEDENTES HEREDOFAMILIARES

app.put("/agregar/antecedentes/heredo/familiares/paciente/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;

  paciente
    .findById(
      {_id: id},
      {antecedentesHeredoFamiliares: body.idAntecedentesHeredoFam}
    )
    .exec()
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Se actualizaron los antecdentes heredo familiares",
        data,
      });
    })
    .catch((error) => {
      return resp.status(500).json({
        ok: false,
        message: "No se pudieron actualizar",
        error,
      });
    });
});

app.put("/actualizar/historia/clinica/:id", (req, resp) => {
  let id = req.params.id;

  let body = req.body;

  paciente
    .findByIdAndUpdate({_id: id}, {historiaClinica: body.idHistoriaClinica})
    .exec()
    .then((data) => {
      return resp.status(200).json({
        ok: true,
        message: "Se actulizo en el paciente",
        data,
      });
    })
    .catch((error) => {
      return resp.status(500).json({
        ok: false,
        message: "No se pudo actualizar",
        error,
      });
    });
});

app.put("/actualizar/conteo/consultas/:id", (req, resp) => {

  
  let id = req.params.id;
  let body = req.body;


  paciente
    .findByIdAndUpdate({_id: id}, {consultas: body.consultas})
    .exec()
    .then((data) =>
      respuestaServer(
        data,
        null,
        "Se actualizaron las consultas del paciente",
        resp
      )
    )
    .catch((error) =>
      respuestaServer(
        null,
        error,
        "No se pudieron actualizar las consultas",
        resp
      )
    );
});

app.put("/actualizar/password", async (req, resp) => {
  let body = req.body;
  let errorr = { x:'no se pudo actualiza la contraseña'};
  try{
    const respuesta = await paciente.findOne({$or:[
      {correoPaciente: body.correoPaciente},
      {telefono: body.correoPaciente}
    ]});
    console.log(respuesta)
    const respues = await paciente.findByIdAndUpdate({_id : respuesta._id},
      {password: bcrypt.hashSync(body.password, 10)},
      {new:true}
    );
    console.log(resp)
    if(respues != null){
      serveResp(respues, null, 'Se cambio la contraseña', resp);
    }else{
      serveResp( null, errorr.x , 'Accion no valida', resp);
    }
  }catch(error){
    serveResp(null, error, 'No se actualizo', resp);
  }

  /* paciente
    
    .then((data)=>{
      
    })
    .then((data) =>
      respuestaServer(data, null, "Se actualizo la constraseña", resp)
    )
    .catch((error) =>
      respuestaServer(null, error, "No se pudo actualizar la constraseña", resp)
    ); */
});

// const findByNumber = (body) => {

//     return new Promise((resolve, reject) => {

//         paciente
//             .findOne({ telefono: body.telefono })
//             .exec()
//             .then(data => resolve(data))
//             .catch(error => reject(error));
//     });

// }

// const findByEmail = (body) => {
//     // console.log(body);
//     return new Promise((resolve, reject) => {
//         paciente
//             .findOne({ correoPaciente: body.correoPaciente })
//             .exec()
//             .then(data => resolve(data))
//             .catch(error => reject(error));
//     });
// }

app.post("/log/login/app", async (req, resp) => {
  let body = req.body;


  paciente
    .findOne({$or:[
      {correoPaciente: body.correo},
      {telefono: body.correo}
    ]})
    .then((data) => {

      if (bcrypt.compareSync(body.password, data.password)) {
        let token = jwt.sign(
          {
            nombre: data.nombrePaciente,
            id: data._id,
          },
          process.env.SEED,
          {expiresIn: process.env.CADUCIDADTOKEN}
        );

        return resp.status(200).json({
          ok: true,
          message: "Se encotro el usuario",
          token,
          usuario: data,
        });
      } else {
        /* console.log("no entro"); */
        return resp.status(200).json({
          ok: false,
          message: "La contraseña es incorrecta",
          data: null,
        });
      }
    })
    .catch((error) =>
      respuestaServer(null, error, "No se encontro el usuario", resp)
    );
});

//==============================================================================================================
//                                          SOCIO DE CANAL
//==============================================================================================================

app.get("/listado/pacientes/:sede", (req, resp) => {
  const {sede} = req.params;

  paciente
    .find({nomenclaturaRegistro: sede})
    .populate("paquetes")
    .populate("paquetesVisitas")
    .limit(56)
    .sort({_id:-1})
    .exec()
    .then((data) =>
      respuestaServer(
        data,
        null,
        "Se encontraron los pacientes de la sede",
        resp
      )
    )
    .catch((error) =>
      respuestaServer(null, error, "Aun no hay pacientes", resp)
    );
});

app.get("/paciente/:sede/:id", (req, resp) => {
  const {sede, id} = req.params;

  // console.log({ sede, id });
  paciente
    .findOne({nomenclaturaRegistro: sede, _id: id})
    .exec()
    .then((data) =>
      respuestaServer(data, null, "Se encontró el paciente", resp)
    )
    .catch((error) =>
      respuestaServer(null, error, "No se encontró paciente", resp)
    );
});

const respuestaServer = (data, error, message, resp) => {
  if (data != null || data != undefined) {
    return resp.status(200).json({
      ok: true,
      message,
      data,
    });
  } else if (error != null || error != undefined) {
    return resp.status(500).json({
      ok: false,
      message,
      error,
    });
  }
};

app.get("/listado/receptores/:sede", (req, resp) => {
  const {sede} = req.params;
  paciente
    .find({sede:sede,disponente:'RECEPTOR'})
    .exec()
    .then((data) =>
      respuestaServer(
        data,
        null,
        "Se encontraron los receptores de la sede",
        resp
      )
    )
    .catch((error) =>
      respuestaServer(null, error, "Aun no hay receptores", resp)
    );
});

app.post("/paciente/repetidos/list", (req, resp) => {
  const  body  = req.body;

  // console.log({ sede, id });
  hojaEvolucion
/*     .aggregate([{ $match : {
      $and : [
        { nombrePaciente: body.nombre },
        { apellidoPaterno: body.paterno},
        { apellidoMaterno : body.materno}
      ]
      
       } } ]) */
                                                 
       .then((data) =>
      respuestaServer(data, null, "Se encontró el paciente", resp)
    )
    .catch((error) =>{

        console.log(error)
        respuestaServer(null, error, "No se encontró paciente", resp)
    }
    );
});


app.post('/agregar/nombre/completo/paciente', async (req, resp) => {

  var pacientesCount = 0;
  
  try{
    
    const pacientes = await paciente.find().sort({fechaRegistro: 1});
    //traemos todos los pacientes para iterarlos
    const totalPacientes = await paciente.find().count();
    //count de los pacientes
    console.log( "pacientes totales: " +totalPacientes);
    
    var pacienteFor;
    //pacientes.forEach(async (element, index) => {
      pacienteFor = {} ;
      
      for await ( element of pacientes ){
        pacienteActual = element;
        nombreCompletoPaciente = (element.nombrePaciente.trim() + " "+ element.apellidoPaterno +" " + element.apellidoMaterno).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      
        /*nombreCompletoPaciente.replace('É', 'E' );
        nombreCompletoPaciente.replace('Í', 'I' );
        nombreCompletoPaciente.replace('Ó', 'O' );
        nombreCompletoPaciente.replace('Ú', 'u' );*/
        console.log(nombreCompletoPaciente, "nombreCompletoPaciente")
        //encontremos el paciente por el ID  y hacemos el update de los nombres
    pacienteFor = await paciente.findByIdAndUpdate(element._id, { nombreCompletoPaciente: nombreCompletoPaciente },  {new:true});
    console.log(pacienteFor, "pacienteFor")
    // await paciente.updateOne({ _id:element._id }, { nombreCompletoPaciente });
    // await pacienteFor.save();
     const pacienteGuardado = await paciente.findById(element._id);

    console.log(pacienteGuardado, "Pacinete guardado");
    pacientesCount++
    console.log( "pacientes actulizados: " + pacientesCount );
    console.log( "pacientes faltantes: ",  (totalPacientes - pacientesCount ) );
    }
    //creamos el valor de nombreCompleto
  //});

    resp.status(200);
    resp.json({
      ok: true,
      message: 'Terminó'
    });

 }catch(error){
  console.log(error);
  resp.status(500);
  resp.json({
    ok: false,
    message: 'Algo paso', 
    error
  })
 }

});

module.exports = app;
