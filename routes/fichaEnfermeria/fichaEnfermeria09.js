const express = require('express');
const fichaEnfermeria09 = require('../../models/enfermeria/fichaEnfemeria09');

let app = express();

app.post('/enfermeria09', (req, resp) => {

    let body = req.body;

    console.log("Signos Vitales", body);

    let newFicha = new fichaEnfermeria09({

        Talla: body.Talla,
        Peso: body.Peso,
        Imc: body.Imc,
        Fc: body.Fc,
        fr: body.fr,
        Temp: body.Temp,
        Tamiz: body.Tamiz,
        Pc: body.Pc,
        Pa: body.Pa,
        Pt: body.Pt,
        Saor: body.Saor,
        pao: body.pao,
        sistolica: body.sistolica,
        diastolica: body.diastolica,
        Notas: body.Notas
    });



    newFicha.save()
        .then(ficha => {

            resp.status(200)
                .json({
                    ok: true,
                    message: 'Se guardaron los datos',
                    data: ficha
                })

        })
        .catch(

            error => {

                resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar',
                        error
                    });

            });
});





module.exports = app;