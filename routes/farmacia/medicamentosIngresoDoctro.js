// importamos la libería express 

const express = require('express');

// importamos el modelo de la DB, para hacer uso, en las peticiones siguiente y tener un 
// registro de los medicamentos y agregarlos a la DB
const medicamentosDoctor = require('../../models/farmacia/ingresoMedicamentosDoctor');
const serveResp  = require('../../functions/functions');
const almacenProductos = require('../../models/almacen/modelProductos')
let app = express();
// endpoint de creacion de un nuevo medicamento

app.post('/agregar/medicamento/', (req, resp) => {


    // obtenemos el cuerpo de la peticion y esos valores se almacenan en la variable
    // body para hacer uso de estos valores más adelante

    let body = req.body;
    // hacemos uso de la variable body en la que se guardaron los datos de la peticion
    // y creamos un nuevo modelo para ingresarlos a la DB

    let newSchemaMedicamentoDoctor = new medicamentosDoctor({

        nombreComercial: body.nombreComercial,
        nombreDeSalOsustanciaActiva: body.nombreDeSalOsustanciaActiva,
        presentacio: body.presentacio,
        contenidoFrasco: body.contenidoFrasco,
        viaDeAdministracion: body.viaDeAdministracion,
        lote: body.lote,
        fechaCaducidad: body.fechaCaducidad,
        laboratorio: body.laboratorio
            // fechaCaducidad: body.fechaCaducidad

    });



    newSchemaMedicamentoDoctor
    // se guadra el modelos nuevo dentro de la DB
        .save()
        .then((data) => respuestaServer(data, null, 'Se guardo el medicamento', resp))
        .catch((error) => respuestaServer(null, error, 'No se pudo guardar el medicamento', resp));

});


app.get('/medicamentos/doctor', (req, resp) => {


    medicamentosDoctor
        .find()
        .exec()
        .then((data) => respuestaServer(data, null, 'Se encontraron los medicamnetos', resp))
        .catch((error) => respuestaServer(null, error, 'No se encontro', resp))


});


app.get('/medicamento/:nombre', (req, resp) => {

    let nombre = req.params.nombre;

    let regx = new RegExp(nombre, 'i');

    Promise.all([
            buscarPorNombre(regx),
            buscarPorSalOsustancia(regx)
        ])
        .then(data => respuestaServer(data, null, 'Se encontraron los medicamentos', resp))
        .catch(error => respuestaServer(null, error, 'No se enontraron', resp));

});

//! GET punto de venta de farmacia
app.get('/farmacia/venta/productos/materiales',async (req,resp)=>{
    try { 
        const productos = await almacenProductos.find()
        serveResp(productos, null, 'Se encontro el disponente del banco', resp)
    } catch (error) {
        console.log(error)
    }
})

// actualizacion de los campos de los medicamentos
app.put('/actualizar/medicamento/:id', (req, resp) => {


    let id = req.params.id;
    let body = req.body;


    // obtenemos el cuerpo de la peticion, que contine los campos ya actualizados


    medicamentosDoctor
        .findByIdAndUpdate({ _id: id }, body)
        // busqueda y actualizacion de los campos por medio del ID
        .exec()
        .then((data) => respuestaServer(data, null, 'Se actualizo el medicamento', resp))
        .catch((error) => respuestaServer(null, error, 'No se pudo actualizar', resp));

});


app.delete('/eliminar/medicamento/:id', (req, resp) => {


    // elimincacion de un medicamento por el ID que se obtiene por el la url

    let id = req.params.id;

    medicamentosDoctor
        .findByIdAndDelete({ _id: id })
        .exec()
        .then((data) => respuestaServer(data, null, 'Se elimino el medicamento', resp))
        .catch((error) => respuestaServer(null, error, 'No se pudo eliminar el medicamento', resp));

});


const buscarPorNombre = (nombre) => {

    return new Promise((resolve, reject) => {
        medicamentosDoctor

            .find({ nombreComercial: nombre })
            .exec()
            .then(data => resolve(data))
            .catch(error => reject(error));

    });
}


const buscarPorSalOsustancia = (salosustancia) => {

    return new Promise((resolve, reject) => {

        medicamentosDoctor
            .find({ nombreDeSalOsustanciaActiva: salosustancia })
            .exec()
            .then(data => resolve(data))
            .catch(error => reject(error));
    });

}


const respuestaServer = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}
module.exports = app;