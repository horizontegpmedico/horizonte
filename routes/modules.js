//libs
const express = require('express');
//models
const acceso = require('../models/modulos/modelos');
const modules = require('../models/modules');

//functions 
const serveResp = require('../functions/functions');


const app = express();


//======================================================
// Litado de todos los modulos
//======================================================

app.get('/modules', async (req, resp) => {

    try {
        
        // listamos todos los modulos que hay registrado
        const modulesResp = await modules.find().sort({ nameModule: -1 });
        serveResp( modulesResp, null, 'Se encontraron los modulos', resp);

    } catch (error) {   

        /* console.log(error); */
        serveResp(null, error, 'Algo paso', resp);
    }

});

// Estos servicios se usarán para asignar el usuario a los modulos que puede usar


// se el asigna un nuevo modulo a un usuario, por medio de los ids
app.post('/nuevo/usuario/modulo', async(req, resp) => {
    let body = req.body;
    try {
        let newAccesoModel = new acceso({
            ModelPersonal: body.id_user,
            modules: body.id_modulo
        });
        const moduloSaved = await newAccesoModel.save();
        return serveResp(moduloSaved, null, 'MODULO AGREGADO CORRECTAMENTE', resp);
    } catch (error) {
        return serveResp(null, error, "NO SE PUDO AGREGAR EL MODULO", resp);
    }
    // moduloSaved.save()
    // .then((result) => {
    //     return resp.status(201)
    //         .json({
    //             ok: true,
    //             message: 'Se guardo el acceso',
    //             modulo: result
    //         });

    // }).catch((err) => {
    //     return resp.status(500)
    //             .json({
    //                 ok: false,
    //                 message: 'Algo paso',
    //                 err
    //             });

    // });


});


// ESTE SERVICIO NOS LISTA LOS MODULOS DEL USUARIO
app.post('/modulos/:id', async (req, resp) => {

    
    try {

        const {id} = req.params;

        const modulesUser =   await acceso.find({ ModelPersonal: id }).populate('modules');
        modulesUser.sort( (a, b) => {
            if(a.modules.nameModule < b.modules.nameModule)  return -1
            return 1
        });

        if( modulesUser != [] ) {
            serveResp(modulesUser, null, 'Se encontraron los modulos', resp);
        }else{

            serveResp(null, 'No se ecnotraron modulos', resp);
        }

    } catch (error) {
        /* console.log(error); */
        serveResponse(null, error, 'No se pudo conectar', resp);
    }


});

//=================================================================
//        Muestra todos los modulos
//=================================================================


app.post('/modulos', (req, resp) => {



    acceso.find()
        //.populate('ModelPersonal')
        .populate('modules')
        .exec((error, modulos) => {

            if (error) { 
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se encontraron los modulos',
                        error
                    })
            }

            if (!modulos) {
                return resp.status(400)
                    .json({
                        ok: false,
                        message: 'No hay más modulos'
                    })
            }


            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Los modulos son',
                    modulos
                })
        });

});


//==========================================================
//               CREACIÓN DE UN NUEVO MODULO
//==========================================================



app.post('/agregar/modulo', (req, resp) => {

    let {body} = req;

    let newModule = new modules({
        nameModule: body.nombreModulo,
        route: body.route,
        role: body.role,
        icon: body.icon
    });


    newModule.save((error, moduloDB) => {

        if (error) {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se encontro',
                    error
                });



        }

        if (!moduloDB) {
            return resp.status(400)
                .json({
                    ok: false,
                    message: 'No se pudo crear'

                })
        }

        return resp.status(201)
            .json({
                ok: true,
                message: 'Se creo el modulo',
                modulo: moduloDB
            });

    });


});
module.exports = app;