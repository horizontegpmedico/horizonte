const express = require('express');
const cie10 = require('../../models/cie-10/modelCie10');
let app = express();



app.get('/cie10', (req, resp) => {


    cie10
        .find()
        .exec()
        .then((data) => {

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Todo Cie10',
                    data
                })
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se encontró',
                    error
                })
        })

});


app.get('/cie10/:nombre', (req, resp) => {

    let nombre = req.params.nombre;



    let regexp = new RegExp(nombre, 'i');

    cie10
        .find({ NOMBRE: regexp })
        .exec()
        .then((data) => {
            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontró',
                    data
                })
        })
        .catch((error) => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se encontró',
                    error
                })
        })

});

app.post('/cie10', (req, resp) => {


    let body = req.body;

    let newCie10 = new cie10({

        CONSECUTIVO: body.CONSECUTIVO,
        LETRA: body.LETRA,
        CATALOG_KEY: body.CATALOG_KEY,
        NOMBRE: body.NOMBRE
    });

    newCie10
        .save()
        .then((data) => {

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se guardo',
                    data
                })

        })
        .catch(
            (error) => {
                return resp.status(500)
                    .json({
                        ok: false,
                        message: 'No se pudo guardar',
                        error
                    });
            }
        )



});

module.exports = app;