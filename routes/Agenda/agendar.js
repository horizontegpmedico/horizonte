// libs
const express = require("express");
const app = express();
var mongoose = require("mongoose");

const Agenda = require("../../models/agenda/ModelAgenda");
const AgendaServicios = require("../../models/agenda/ModelAgendaServicios");
const modeloServicios = require("../../models/servicios/modeloServicios");

const serveResp = require("../../functions/functions");

app.post("/agenda/agendar/servicios", (req, resp) => {
  const body = req.body;

  const agendaServicios = new AgendaServicios({
    agendo: body.agendoser,
    color: body.colorser,
    titulo: body.tituloser,
    Servicio: body.servicioser,
    horaInicio: body.horainicioser,
    horaFin: body.horafinser,
    fechaInicio: body.fechainicioser,
    fechaFin: body.fechafinser,
    sede: body.sedeser,
    idPersonal: body.Doctor,
  });
  agendaServicios
    .save()
    .then(async (data) => {
      const datas = await AgendaServicios.find(data._id).populate("agendo");
      serveResp(datas, null, "Se guardaron las citas correctamente", resp);
    })
    .catch((error) => {
      console.log(error);
      serveResp(null, error, "No se puedieron guardar los servicios", resp);
    });
});
app.post("/agenda/agendar", (req, resp) => {
  const { body } = req;
  const { id } = req.params;
  const agenda = new Agenda({
    idPaciente: body.Paciente,
    idPersonal: body.Doctor,
    agendo: body.agen,
    status: body.stat,
    color: body.color,
    idServicio: body.Servicios,
    descrpicion: body.Descripcion,
    motivo: body.Motivo,
    horaInicio: body.HoraEnt,
    horaFin: body.HoraSal,
    fechaInicio: body.FechaEnt,
    fechaFin: body.FechaSal,
    sede: body.sedecon,
  });
  agenda
    .save()
    .then(async (data) => {
      const datas = await Agenda.find(data._id)
        .populate("idServicio")
        .populate("idPersonal")
        .populate("idPaciente");
      serveResp(datas, null, "Se guardaron las citas correctamente", resp);
    })
    .catch((error) =>
      serveResp(null, error, "No se guardó la cita" + error, resp)
    );
});
app.get("/agenda", (req, resp) => {
  Agenda.find()
    .populate("idServicio")
    .populate("idPersonal")
    .populate("idPaciente")
    .then((data) =>
      serveResp(data, null, "Se encontraron las citas medicas", resp)
    )
    .catch((error) => {
      console.log(error);
      serveResp(null, error, "No se puede encontraron las citas medicas", resp);
    });
});
app.get("/agenda/servicios", (req, resp) => {
  AgendaServicios.find()
    .populate("agendo")
    .populate("idPersonal")
    .then((data) =>
      serveResp(data, null, "Se encontraron  servicios disponibles", resp)
    )
    .catch((error) => {
      console.log(error);
      serveResp(null, error, "No se encontraron  servicios disponibles", resp);
    });
});

app.put("/agenda/update/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;
  Agenda.findByIdAndUpdate(
    { _id: id },
    {
      horaInicio: body.HoraEnt,
      horaFin: body.HoraSal,
      fechaInicio: body.FechaEnt,
      fechaFin: body.FechaSal,
    }
  )
    .exec()
    .then(async (data) => {
      const datas = await Agenda.find(data._id)
        .populate("idServicio")
        .populate("idPersonal")
        .populate("idPaciente");
      serveResp(datas, null, "Se guardaron las citas correctamente", resp);
    })
    .catch((error) =>
      serveResp(null, error, "No se guardó la cita" + error, resp)
    );
});
app.put("/agenda/cancelar/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;
  Agenda.findByIdAndUpdate(
    { _id: id },
    {
      status: body.status,
    }
  )
    .exec()
    .then(async (data) => {
      const datas = await Agenda.find(data._id)
        .populate("idServicio")
        .populate("idPersonal")
        .populate("idPaciente");
      serveResp(datas, null, "Se guardaron las citas correctamente", resp);
    })
    .catch((error) =>
      serveResp(null, error, "No se guardó la cita" + error, resp)
    );
});
app.put("/agenda/updateAgendar/:id", (req, resp) => {
  let id = req.params.id;
  let body = req.body;
  Agenda.findByIdAndUpdate(
    { _id: id },
    {
      color: body.color,
      descrpicion: body.Descripcion,
      motivo: body.Motivo,
      horaInicio: body.HoraEnt,
      horaFin: body.HoraSal,
      fechaInicio: body.FechaEnt,
      fechaFin: body.FechaSal,
      sede: body.sedecon,
      idPersonal: body.Doctor,
    }
  )
    .exec()
    .then(async (data) => {
      const datas = await Agenda.find(data._id)
        .populate("idServicio")
        .populate("idPersonal")
        .populate("idPaciente");
      serveResp(datas, null, "Se guardaron las citas correctamente", resp);
    })
    .catch((error) =>
      serveResp(null, error, "No se guardó la cita" + error, resp)
    );
});

app.get("/agenda/paciente/:id", (req, resp) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  console.log(id);

  Agenda.find({ idPaciente: id })
    .populate("idServicio")
    .populate("idPaciente")

    .then((data) =>
      serveResp(data, null, "Se encontraron las citas medicas", resp)
    )
    .catch((error) => {
      console.log(error);
      serveResp(null, error, "No se puede encontraron las citas medicas", resp);
    });
});

app.get("/agenda/personal/:id", (req, resp) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  console.log(id);

  Agenda.find({ idPersonal: id })
    .populate("idServicio")
    .populate("idPaciente")
    .then((data) =>
      serveResp(data, null, "Se encontraron las citas medicas", resp)
    )
    .catch((error) => {
      console.log(error);
      serveResp(null, error, "No se puede encontraron las citas medicas", resp);
    });
});
module.exports = app;
