const express = require('express');

const usgNew = require('../../../models/ultrasonidos/nuevos/usgNew');

let app = express();


app.post('/nuevo/usg', (req, resp) => {

    let body = req.body;

    console.log(body);
    let nuevoUltra = new usgNew({

        nombre: body.nombre,

        machote: body.machote,

    });

    nuevoUltra
        .save()
        .then(data => serveResp(data, null, 'Se creo el USG', resp))
        .catch(error => serveResp(null, error, 'No se pudo crear USG', resp));


});



app.get('/ver/estudios', (req, resp) => {

    usgNew
        .find()
        // .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'Se encotraron los USG', resp))
        .catch(error => serveResp(null, error, 'No se encontraron los USG', resp));

});

app.post("/ver/estudios", (req, resp) => {


    console.log(req.body);

    usgNew
        .find({ nombre: req.body.nombre })
        .exec()
        .then(data => serveResp(data, null, 'USG encontrados', resp))
        .catch(error => serveResp(null, error, 'No se encontraron USG', resp))

});


const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}


module.exports = app;