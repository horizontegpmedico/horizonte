const express = require('express');
const usgNew = require('../../../models/ultrasonidos/nuevos/usgNew');
// es la tabla de los pedidos

const serveResp = require('../../../functions/functions')

const app = express();



app.post('/nuevo/pedido/ultrasonido', (req, resp) => {

    const { body } = req;

    const schemaPedidos = new usgNew({
        estudios: body.estudios,
        idPaciente: body.idPaciente,
        sede: body.sede,
        medicoQueSolicito: body.medicoQueSolicito,
        prioridad: body.prioridad,
        fechaDePedidoDeLosExamenes: body.fecha
    });

    schemaPedidos
        .save()
        .then(data => serveResp(data, null, 'Se creo el pedido', resp))
        .catch(error => serveResp(null, error, 'No se creo el pediso', resp));

});




app.get('/ver/pedidos/ultrasonido', async(req, resp) => {

    try {
      
      const pedidosUltra =  await usgNew.find().sort({fecha:1}).populate('idPaciente');
        serveResp(  pedidosUltra, null, 'Se enctraron los pedidos', resp);

    } catch (error) {
        console.log( error)
        serveResp(null, error, 'No se pudo obtener', resp);
    }

});

/// agregre nuevas consultas de ultrsonido
app.get('/obtener/ultrasonido/pedido/:id', (req, resp) => {

    let id = req.params.id;
    usgNew
        .findById(id)
        .populate('idPaciente')
        .exec()
        .then(data => serveResp(data, null, 'se encontraron los pedidos ultra', resp))
        .catch(error => serveResp(null, error, 'no se encontraron pedidos ultra', resp))
});

module.exports = app;