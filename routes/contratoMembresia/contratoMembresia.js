const express = require('express');
const paciente = require('../../models/membresiaContrato/membresiaContrato');


let app = express();



app.post('/agregar/membresia', (req, resp) => {

    let body = req.body;


    let pacienteNew = new paciente({
        fechaAfiliacion: body.fechaAfiliacion,
        fechaFinaliza: body.fechaFinaliza,
        metodoPago: body.metodoPago,
        atendio: body.atendio
    });



    pacienteNew.save()
        .then(
            data => {
                console.log(data)
                return resp.status(200)
                    .json({
                        ok: true,
                        message: 'Seguardo la informacion',
                        data
                    })

            }
        )
        .catch(error => {
            console.log(error);


            return resp.status(500)
                .json({
                    ok: false,
                    message: 'No se pudo guardar',
                    error
                });
        })


});


app.get('/ver/membresia/:id', (req, resp) => {

    let id = req.params.id;


    paciente.findById({ id })
        .exec()
        .then((data) => {

            return resp.status(200)
                .json({
                    ok: true,
                    message: 'Se encontró la membresia',
                    data
                })

        })
        .catch(error => {
            return resp.status(500)
                .json({
                    ok: false,
                    message: 'no se pudo conectar',
                    error
                });
        })

})

module.exports = app;