const express = require('express');
const app = express();
const multer = require('multer')
    // negocio
app.use(require('./inicio'));
app.use(require('./login'));
app.use(require('./personal'));
app.use(require('./modules'));
app.use(require('./paquetes'));
// app.use(multer)

// servicios integrados
app.use(require('./serviciosInt/ambulancia'));


// pacientes y familias

app.use(require('./pacientesNuevos'));
app.use(require('./families/families'));
//aqui van las nuevas tablas de servicios integrados/////

app.use(require('./serviciosIntegrados/servicios'));


///  no sabemos
app.use(require('./medicos/medicos'));
app.use(require('./maternidad/tabuladorPagos'));


app.use(require('./busquedaDepartamentos/todosDepartamentos'));
app.use(require('./cotizacion/cotizacion'));
app.use(require('./laboraotorio/pedidosLab/pedidosLab'));
app.use(require('./laboraotorio/perfiles/perfilesLab'));
app.use(require('./laboraotorio/laboratoriosCensur'));

app.use(require('./fichaEnfermeria/fichaEnfermeria09'));
app.use(require('./enfermeria/enfermeriaVisitas'));
app.use(require('./contratoMembresia/contratoMembresia'));
app.use(require('./historilaConsultas/consultasHstorial'));
app.use(require('./paquetesQuirofanos/agregarMaquinas'));
app.use(require('./paquetesQuirofanos/agregarMedicamentos'));
app.use(require('./paquetesQuirofanos/agregarPaquete'));

app.use(require('./historilaConsultas/ingresoHospitalizacion'));
app.use(require('./cie10/cie10'));


// agregamos el modulo de farmacia a la metodo principal
app.use(require('./farmacia/medicamentosIngresoDoctro'));



// hisotria clinica
app.use(require('./sv/routesAntecedentes'));
app.use(require('./sv/routesNutricionNinos'));
app.use(require('./sv/routesVacunacionNinos'));
app.use(require('./sv/routesMedicinaPHombre'));
app.use(require('./sv/antecedentesHeroFamiliares'));
app.use(require('./sv/antecedentesNoPatologicos'));
app.use(require('./sv/antecedentesPersonalesPatologicos'));
app.use(require('./sv/antecedentesGinecoObstetricosH'));
app.use(require('./sv/antecedentesGineco'));
app.use(require('./aparatosYsistemas/aparatosYsistemas'));
// consultas 
app.use(require('./historilaConsultas/historiaClinica'));
app.use(require('./historilaConsultas/receta'));

// hoja de evolucion
app.use(require('./consultaMedicinaGeneral/hojaEvolucion'));

// pago de servicios 
// consultas 
app.use(require('./historilaConsultas/historiaClinica'));
app.use(require('./historilaConsultas/receta'));

// hoja de evolucion
app.use(require('./consultaMedicinaGeneral/hojaEvolucion'));


// ventas de los servicios

app.use(require('./ventas/ventasServicios'));

//Otros Estudios
app.use(require('./otrosEstudios/otrosEstudios.route'))


// ventas de sedes 

app.use(require('./ventas/pedidosSedes'));

// laboratorios, ultrasonido y rayosX nuevos 

app.use(require('./laboraotorio/estudiosNuevos/estudiosNuevos'));
app.use(require('./ultrasonidos/usgNuevo/usgNuevo'));
app.use(require('./ultrasonidos/usgPedido/usgPedido'));

app.use(require('./RayosX/rayos-xNuevo/rayos-xPedido/rayosxPedido'));
// agenda
app.use(require('./Agenda/agendar'));

// sedes

app.use(require('./sedes/sedes'));

// regresos 
app.use(require('./regresos/regresosLab'));
app.use(require('./regresos/regresosUSG'));
app.use(require('./RayosX/rayos-xNuevo/rayosxRegresos/regresosRayosX'))


// Socios 
app.use(require('./utilidadesSocios/utilidadesScosiosPorcentajes'));
// utilidades ambulancias 
app.use(require('./utilidadesSocios/utilidadesAmbulancia'));

//Banco de sangre
app.use(require('./BancoDeSangre/disponente'))
app.use(require('./BancoDeSangre/bancosangre'))
app.use(require('./BancoDeSangre/signosvitales'))
app.use(require('./BancoDeSangre/historiaclinica'))
app.use(require('./BancoDeSangre/hojaevolucion'))
app.use(require('./BancoDeSangre/laboratorio'))
/// sedes de banco de sangre
app.use(require('./BancoDeSangre/sedes'))

app.use(require('./BancoDeSangre/flebotomia'))
    // ruta de almacen
app.use(require('./BancoDeSangre/almacen'));
app.use(require('./BancoDeSangre/autoexclusion'));
app.use(require('./BancoDeSangre/bitacoras'));
app.use(require('./BancoDeSangre/receptor'));
//app.use(require('./bancoDeSangre/jefeabanco'));

// rutas para los enpoints de proveedores de BS
app.use(require('./BancoDeSangre/proveedores'));

// endpoints de los servicios de censur
app.use(require('./BancoDeSangre/ServiciosBS/serviciosBS'));
app.use(require('./BancoDeSangre/routesSeguimientos'));
app.use(require('./BancoDeSangre/ServiciosBS/serviciosBS'));

// rutas para el carrito de censur
app.use(require('./BancoDeSangre/carritoCensur'));

//helpers del sistema
app.use(require('./BancoDeSangre/helpers/helpers'));


// servicios en bs
app.use( require('./serviciosIntegrados/seriviciosList'));

// Ruta para descargar archivos PDF
app.use(require('./descargasPDF'));

//pedidos de patologias
app.use(require('./patologias/pedidoPatologia'));

//ruta para los nuevos regresos de los servicios
app.use( require('./pedidosServicios/pedidiosServicios'));

//rutas posada
app.use(require('./posada/huespedesNuevos'));

app.use( require('./historilaConsultas/hospitalizacion') );

//roles
app.use( require('./roles/roles') );

module.exports = app;