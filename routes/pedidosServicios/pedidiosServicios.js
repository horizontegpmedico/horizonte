//libs

const express = require('express');
const QRcode = require('qrcode');

//modelos de la DB
const pedidosServicios  = require('../../models/pedidosServicios/pedidosServicios');
const modeloservicios = require('../../models/servicios/modeloServicios')
const Paciente = require('../../models/nuevoPaciente');
const ventas = require('../../models/ventasServicios/serviciosVentas');
const LaboratoriosCensur = require('../../models/laboratorio/modelLaboratoriosCensur');
const Sedes = require('../../models/sedes/sedes');


//funcitons 
const serveResp = require('../../functions/functions');
const  logMemory  = require('../../config/logs');

// express app 
const app = express();

//creacion de un nuevo pedido
app.post('/nuevo/pedido/estudios', async(req, resp) => {
    const servicios = {
        laboratorio: 'laboratorio',
        ultrasonido: 'ultrasonido',
        endoscopia: 'endoscopia',
        ambulancia: 'ambulancia',
        xray: 'xray',
        patologia: 'patologia',
        consultas: 'consulta',
        tomografia: 'tomografia'
    };
    try {
        const { body } = req;

        const validIdService = await modeloservicios.findById( body.idServicio );

        //validar el id del servicio
        if( validIdService ===  [] || validIdService === null  ) {
            serveResp( null, 'No existe el ID del servicio', 'Ingresa el ID del servico valido' ,resp );
        }else{
                    //valida el id del paciente 
        const validPacienteId = await Paciente.findById(body.paciente);
        // console.log( validPacienteId )

        if( validPacienteId === [] || validPacienteId === null ) {
            serveResp( null, 'No existe el paciente', 'Ingresa el ID del paciente' ,resp);
        }else {

            //validamos el ID de la venta
            const validVentas = await ventas.findById( body.idVenta ); 
            
            if( validVentas=== []  || validVentas === null){

                serveResp( null, 'No existe la venta', 'Ingresa el ID de al venta',resp);
            }else {
                if(body.socioSede != 'TLYC01'){
                    const encontrarSede = await Sedes.findOne({ nomenclatura: body.socioSede });
                    if(encontrarSede){
                        const campoAIncrementar = servicios[body.nombreServicio];
                        await Sedes.findByIdAndUpdate(
                            encontrarSede._id, 
                            { $inc: { [campoAIncrementar]: 1 } },
                            { new: true }
                        );
                    }
                }
                if(body.nombreServicio == 'laboratorio' || body.nombreServicio == 'patologia' || body.nombreServicio == 'tomografia'){
                    body.newEstudio = true;
                }
                const nuevoPedido = new  pedidosServicios(body);
                const pedidoSaved  = await nuevoPedido.save();
                serveResp(pedidoSaved, null, 'Se creo el nuevo pedidio', resp);
            }
           
        }
    }

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se pudo conecar', resp);
    }

});

//obtener todos los pedidos
app.get('/ver/servicios/pedidos', async(req, resp) => {
try {

        const serviciosPedidos = await pedidosServicios.find()
        
        if( serviciosPedidos === []  || serviciosPedidos === null ) {

            serveResp( serviciosPedidos , null, 'No hay pedidio', resp)
        
        }else {
            serveResp( serviciosPedidos, null, 'Se creó el peddio' , resp);

        }
        

    } catch (error) {
        console.log(error);
        serveResponse(null, error, 'No se pudo conectar', resp);
    }
});

// actualiar los datos de la tabla de los pedidos
app.put('/actualizar/pedidios/:id', async(req, resp) => {
        
    try {
        
        const { id  } = req.params;
        const { body  } = req;
        //  console.log(id);
        // console.log(body);
        const validPedido = await pedidosServicios.findOne({'_id': id});
            // console.log( validPedido )
            if( validPedido.status === 'FINALIZADO' ) {
                serveResp( null, 'PEDIDO REALIZADO', 'PEDIDO NO ECONTRADO' , resp);
            }else {
                if( validPedido === null || validPedido === [] ) {
                    serveResp( null, 'No existe el ID del pedido', resp);
                }else {
                    if(validPedido.newEstudio){
                        let data = { 
                            'pdf': body.pdf, 
                            'link':body.link, 
                            status:'FINALIZADO' 
                        }
                        const updatedPedidoData =await pedidosServicios.findByIdAndUpdate({ '_id': id }, data, {new: true});
                        return serveResp( updatedPedidoData, null , 'Se actulizaron los datos', resp);
                    }
                    // if(body.manzo || body.pdf){
                    //     delete body.manzo;
                    //     const updatedPedidoData =await pedidosServicios.findByIdAndUpdate({ '_id': id }, { 'pdf': body.pdf, status:'FINALIZADO' }, {new: true});
                    //     return serveResp( updatedPedidoData, null , 'Se actulizaron los datos', resp);
                    // }
                    const updatedPedidoData = await pedidosServicios.findByIdAndUpdate({ '_id': id }, { 'resultados': body, status:'FINALIZADO' }, {new: true});

                    const dataPaciente = await  pedidosServicios.findById( id ).populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad genero fechaNacimiento');

                    var validCensur = "Laboratorio CENSUR ";
                    if( dataPaciente.nombreServicio === 'ULTRASONIDO' || dataPaciente.nombreServicio === 'ENDOSCOPIA'){
                            
                        validCensur = "Grupo Medico Horizonte";
                    }
                    const writteCode = "Nombre: "+dataPaciente.idPaciente.nombrePaciente + " " + dataPaciente.idPaciente.apellidoPaterno + " " +dataPaciente.idPaciente.apellidoMaterno+ "\n" + "Años: " +dataPaciente.idPaciente.edad +" " + "Género: " + dataPaciente.idPaciente.genero  + "\n" + "\n" + validCensur+ " avala la veracidad de los resultados" + "\n" + "\n" + "Teléfono: 735 357 7564" + "\n" + "\n" + "Dirección:  Francisco I. Madero 27-N, Barrio del Rosario, 62540 Tlayacapan, Mor." ;


                    QRcode.toDataURL(   writteCode , async(error, code) => {
                        if(error) {
                            serveResp(null, error, 'No se crearon los productos', resp);
                        }
                        await pedidosServicios.findByIdAndUpdate({ '_id': id}, {imgQrCode: code}, {new: true} )
                            serveResp( code, null , 'Se actulizaron los datos', resp);
                    });
                    
                       

                }
            }

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se pudo conectar', resp);
    }

});

// nuevo endpoint de la bitacora de los pedidos de los servicios
app.get('/ver/bitacora/estudios/pedidos/:servicio', async (req, resp) => {
    //global variables
    var arrResponse = [];
    try { 
        const { servicio } = req.params;
        servicio.toUpperCase();
        //verificamos que el servicio exista
        const servicesResp = await modeloservicios.find({name: servicio}).limit(1);
        if( servicesResp == [] || servicesResp === null  ){
            serveResp(null, 'No es un servicio valido', 'Servicio no valido', resp);
        }else {
            //si existe  continuamos con la consulta
            //consulta con la que obtenemos los servicios obtenidos
            const requestServicesResp = await pedidosServicios.find({nombreServicio: servicio, $and: [{ $or: [{ status: 'PEDIDO' }, { status: 'EN PROCESO' }] }] }).populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero').populate('idServicio', 'ESTUDIO').populate('idVenta', 'fecha')
            //  console.log( requestServicesResp )
            //verifacmos que el servicio retorne algo
            if( requestServicesResp === [] || requestServicesResp === null ){
                serveResp('No hay pedidos pendientes', null, 'No hay pedidos', resp);
            }else{
                //iteramos los resultados y hacemos el count de los servicios 
                for (let i = 0; i < requestServicesResp.length; i++) {
                    let lastIdVenta = "";
                    if( i == 0) {
                        lastIdVenta = requestServicesResp[i].idVenta._id;
                    }else {
                        lastIdVenta = requestServicesResp[i -1 ].idVenta._id;
                    }
                    const countServicesResp = await pedidosServicios.find( { idVenta: requestServicesResp[i].idVenta._id, nombreServicio: servicio,  $and: [{ $or: [{ status: 'PEDIDO' }, { status: 'EN PROCESO' }] }]  } );
                    //comparamos los ids anteriores 
                    requestServicesResp[i].countServices = countServicesResp.length;
                    if( arrResponse.length === 0 ) {
                        arrResponse.push(requestServicesResp[i]);
                    }else if(lastIdVenta !==  requestServicesResp[i]?.idVenta._id){
                        arrResponse.push(requestServicesResp[i]);
                    }
                }
                serveResp( arrResponse, null, 'Pedidos encontrados ', resp);
            }
        }
    } catch (error) {
        serveResp(null, error,'No se pudo conectar' ,resp);
    }
});

app.get('/ver/bitacora/estudios/pedidos/lab/:servicio', async (req, resp) => {
    let arrResponse = [];
    try {
        let { servicio } = req.params;
        servicio = servicio;
        const servicesResp = await modeloservicios.find({ name: servicio }).limit(1);
        if (!servicesResp.length) {
            return serveResp(null, 'No es un servicio valido', 'Servicio no valido', resp);
        }
        servicio = servicio.toUpperCase();
        // Realizamos la consulta para obtener los servicios con el estado de PEDIDO o EN PROCESO
        let filtros = {
            nombreServicio: servicio, 
            status: { $in: ['PEDIDO', 'EN PROCESO'] },
        }
        if(servicio == 'LABORATORIO' || servicio == 'PATOLOGIA' || servicio == 'TOMOGRAFIA'){
            filtros.newEstudio = true;
        }
        const requestServicesResp = await pedidosServicios.find(filtros)
        .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero')
        .populate('idServicio', 'ESTUDIO')
        .populate('idVenta', 'fecha');

        // Si no se encuentran pedidos
        if (!requestServicesResp.length) {
            return serveResp([], null, 'No hay pedidos', resp);
        }

        // Hacemos un "map" para evitar el ciclo y optimizar la obtención de datos
        const groupedRequests = requestServicesResp.reduce((acc, request) => {
            const idVenta = request.idVenta._id.toString(); // Obtener el ID de la venta
            if (!acc[idVenta]) {
                acc[idVenta] = { ...request.toObject(), countServices: 0 };
            }
            acc[idVenta].countServices += 1;
            return acc;
        }, {});

        // Convertimos el objeto agrupado en un arreglo
        arrResponse = Object.values(groupedRequests);

        // Respondemos con los resultados
        serveResp(arrResponse, null, 'Pedidos encontrados', resp);
        
    } catch (error) {
        serveResp(null, error, 'No se pudo conectar', resp);
    }
});


//get arreglo estudios por paciente
app.post('/hoja/servicio/pedido/estudios', async(req, resp) => {
    
    try {

    const  body  = req.body
    const query = await pedidosServicios.find({ idVenta:body.id, nombreServicio : body.nameService ,  $and: [{ $or: [{ status: 'PEDIDO' }, { status: 'EN PROCESO' }] }],
    }).populate('idPaciente').populate('idServicio','ESTUDIO')
    //    console.log(query);
        serveResp(query, null, 'Pedido del servicio', resp);
    } catch (error) {
        console.log(error);
        serveResp(null, error, 'Error con la consulta', resp);
    }

});


//get arreglo de obtenidos por estudios para resultados
app.get('/hoja/reporte/resultados/:id', async(req, resp) => {
    
    try {

        const date = new Date();
        const new_date = new Date(date);

        const hours = new_date.getHours()
        const minutes = new_date.getMinutes()

        console.log(query, "/hoja/reporte/resultados/"+ hours+":"+minutes )

    const query = await pedidosServicios.find({_id: id}).populate('idPaciente').populate('idServicio');
    const valoresRef = await LaboratoriosCensur.find({idEstudio: query.idServicio})
    // console.log(query, "Query");
    // console.log( valoresRef , "valoresRef");
    let valoresReferencia = {
            paciente: query?.idPaciente,
            venta: query?.idVenta,
            servicio: query?.nombreServicio,
            metodo: valoresRef[0]?.metodo,
            nombre: valoresRef[0]?.nombre,
            tipo_de_examen: valoresRef[0]?.tipo_de_examen,
            valoresDeReferencia: valoresRef[0]?.valoresDeReferencia,
            imgQrCode: updateStatus?.imgQrCode,
            // fechaRealizo: valoresRef.fechaRealizo
        }
        
        serveResp(valoresReferencia, null, 'Pedido del servicio', resp);
    } catch (error) {
        console.log(error);
        serveResp(null, error, 'Error con la consulta', resp);
    }

});

var respArrResultados = [];
//buscamos los resultados por el nombre del paciente y el servicio
app.post('/ver/hoja/reporte/resultados/por/nombre/paciente', async (req, resp) => {

    try{

        const { body } = req;

        const date = new Date();
        const new_date = new Date(date);

        const hours = new_date.getHours()
        const minutes = new_date.getMinutes()

        // console.log( body , "/ver/hoja/reporte/resultados/por/nombre/paciente " + hours+":"+minutes);

        const { nombre} = body;
        console.log(nombre);

        const servicesResp = await modeloservicios.find({name: body.servicio}).limit(1);

        if( !servicesResp ){
            serveResp(null, 'No es un servicio valido', 'Servicio no valido', resp);
        }

        const requestServicesResp = await pedidosServicios.find({nombreServicio: body.servicio.toUpperCase(), status: 'FINALIZADO' })
                                                .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero nombreCompletoPaciente')
                                                .populate('idServicio', 'ESTUDIO')
                                                .populate('idVenta', 'fecha');
                                                

        console.log(requestServicesResp);
        if(!requestServicesResp){
            serveResp(null, 'No se pudo encontrar', 'Servicio no valido', resp);
        }
        respArrResultados = [];
        //resetamos la variable 
        

        requestServicesResp.forEach(  elements => {
            if( elements.idPaciente.nombreCompletoPaciente.includes(nombre.toUpperCase())){
                console.log('entro');
                if( respArrResultados.length === 0 ){
                    console.log('entro1');
                    respArrResultados.push(elements);
                }
                
                if( respArrResultados.some( element =>  element.idPaciente.nombreCompletoPaciente === nombre.toUpperCase() )  ){
                    console.log('entro2');
                    if( body.servicio.toUpperCase() !==  'LABORAOTORIO' ){
                        console.log('entro3');
                        elements.resultados  = [];
                    }

                    elements.imgQrCode = "";
                    respArrResultados.push(elements);
                }

            };
        });

        // console.log(respArrResultados)
        serveResp( respArrResultados, null, 'Pedidos encontrados ', resp);

        //console.log( respArrResultados )

    }catch (error) {
        console.log(error);
        serveResp(null, error,'No se pudo conectar' ,resp);
    }
});
// get bitacora de resultados 
app.get('/ver/hoja/reporte/resultados/:servicio', async (req, resp) => {
    //global variables
    var arrResponse = [];
    try { 
        
        const { servicio } = req.params;
        const date = new Date();
        const new_date = new Date(date);

        const hours = new_date.getHours()
        const minutes = new_date.getMinutes();

        console.log("/ver/hoja/reporte/resultados/"+servicio + " " + hours + ":" + minutes);
        servicio.toUpperCase();
        //verificamos que el servicio exista
        const servicesResp = await modeloservicios.find({name: servicio}).limit(1);
        
        if( servicesResp == [] || servicesResp === null  ){
            serveResp(null, 'No es un servicio valido', 'Servicio no valido', resp);
            
        }else {
            
            //si existe  continuamos con la consulta

            //consulta con la que obtenemos los servicios obtenidos
            const requestServicesResp = await pedidosServicios.find({nombreServicio: servicio, status: 'FINALIZADO'})
                                                .populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero')
                                                .populate('idServicio', 'ESTUDIO').populate('idVenta', 'fecha')
                                                .limit(5)
                                                .sort({fechaPedido:-1});                                   
            //verifacmos que el servicio retorne algo
            if( requestServicesResp === [] || requestServicesResp === null ){
                
                logMemory('requestServicesResp /ver/hoja/reporte/resultados/', false,requestServicesResp )
                serveResp('No hay pedidos pendientes', null, 'No hay pedidos', resp);
                
            }else{

                requestServicesResp.forEach( (element) => {
                    element.imgQrCode = '';
                    // console.log(element)
                });

                // console.log("Resp requestServicesResp /ver/hoja/reporte/resultados/", false, requestServicesResp);
                serveResp( requestServicesResp, null, 'Pedidos encontrados ', resp);
            }
        }
        
    } catch (error) {
        console.log(error);
        serveResp(null, error,'No se pudo conectar' ,resp);
    }

});

//get bitacora de resultados sedes
app.get('/ver/hoja/sede/reporte/resultados/:servicio/:sede', async (req, resp) => {
    //global variables
    var arrResponse = [];

    try { 

        const { servicio, sede } = req.params;
        console.log(servicio);
        servicio.toUpperCase();
        //verificamos que el servicio exista
        const servicesResp = await modeloservicios.find({name: servicio}).limit(1);
        
        if( servicesResp == [] || servicesResp === null  ){
        
            serveResp(null, 'No es un servicio valido', 'Servicio no valido', resp);
            
        }else {

            //si existe  continuamos con la consulta

            //consulta con la que obtenemos los servicios obtenidos
            const requestServicesResp = await pedidosServicios.find({nombreServicio: servicio,sede:sede, status: 'FINALIZADO'}).populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero').populate('idServicio', 'ESTUDIO').populate('idVenta', 'fecha')
            //  console.log( requestServicesResp )
            //verifacmos que el servicio retorne algo
            if( requestServicesResp === [] || requestServicesResp === null ){

                serveResp('No hay pedidos pendientes', null, 'No hay pedidos', resp);
                
            }else{
                console.log( requestServicesResp )

                serveResp( requestServicesResp, null, 'Pedidos encontrados ', resp);
            }
        }
        
    } catch (error) {
        console.log(error);
        serveResp(null, error,'No se pudo conectar' ,resp);
    }

});

app.get('/obtener/resultaos/por/paciente/servicio/:id/:servicio', async(req, resp) => {
    try {   
        const { id } = req.params;
        const { servicio } = req.params;
        
        const validIdPaciente = await Paciente.findById( id );

        if( validIdPaciente  == null ) {
            serveResp(null, 'ID no valido', 'No es un ID valido', resp);
        }else {
            const respServicesSells = await pedidosServicios.find({ idPaciente: id, status:'FINALIZADO', nombreServicio: servicio }).populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero').populate('idServicio', 'ESTUDIO').populate('idVenta', 'fecha')
            if( respServicesSells != null || respServicesSells != [] ){
                serveResp( respServicesSells, null, '', resp);
            }else {
                serveResp(null, 'ID no valido', 'No es un ID valido', resp);
            }
        }

    } catch (error) {
        console.log(error);
        serveResp(null, error, 'No se pudo conectar', resp);
    }
}); 


app.get('/obtener/resultaos/por/paciente/servicio/optimizado/:id/:servicio', async(req, resp) => {

    var resutadosRegresos = [];

    try {   
        console.log('/obtener/resultaos/por/paciente/servicio/optimizado/:id/:servicio');   
        const { id } = req.params;
        const { servicio } = req.params;
        
        const validIdPaciente = await Paciente.findById( id );

        if( validIdPaciente  == null ) {
            serveResp(null, 'ID no valido', 'No es un ID valido', resp);
        }else {
            const respServicesSells = await pedidosServicios.find({ idPaciente: id, status:'FINALIZADO', nombreServicio: servicio }).populate('idPaciente', 'nombrePaciente apellidoPaterno apellidoMaterno edad sede genero').populate('idServicio', 'ESTUDIO').populate('idVenta', 'fecha')
            
           
            if( respServicesSells != null || respServicesSells != [] ){
                
                respServicesSells.forEach( (element ) =>{
                    element.imgQrCode = '';
                    element.resultados = [];
                    console.log( element );
                    resutadosRegresos.push( element );
                });
                
                serveResp( respServicesSells, null, '', resp);
            }else {
                serveResp(null, 'ID no valido', 'No es un ID valido', resp);
            }
        }

    } catch (error) {
        console.log(error);
        logMemory( '/obtener/resultaos/por/paciente/servicio/optimizado/:id/:servicio', true, error );
        serveResp(null, error, 'No se pudo conectar', resp);
    }
});

app.get('/ver/hoja/reporte/resultados/por/nombre/paciente/:id', async (req, resp) => {

    try{
        const id = req.params.id;
        const requestServicesResp = await pedidosServicios.find({idPaciente: id, status: 'FINALIZADO' })
                                                            .populate('idServicio', 'ESTUDIO').sort({_id:-1});
        if(!requestServicesResp){
            serveResp(null, 'No se pudo encontrar', 'Servicio no valido', resp);
        }
        serveResp( requestServicesResp, null, 'Pedidos encontrados ', resp);
    }catch (error) {
        console.log(error);
        serveResp(null, error,'No se pudo conectar' ,resp);
    }
});

module.exports = app;