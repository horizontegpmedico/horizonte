const express = require('express');
const pdf = require('handlebars-pdf');
const consultaHistorico = require('../models/consultasGeneralesBitacoras/consultaHistorial');

const app = express();

app.get('/descarga/pdf/historialPacientes', async ( req, res ) => {
    let users = [];
    try {
        const response = await consultaHistorico.find().sort({
            fechaIngreso: 1
        }).populate('paciente').exec();
        users = response;
    } catch (error) {
        console.log(error);
    }
    let document = {
        template: `
        <!DOCTYPE html>
        <html>
          <head>
            <style>
              .clearfix:after {
                content: "";
                display: table;
                clear: both;
              }
              a {
                color: #5D6975;
                text-decoration: underline;
              }
              body {
                position: relative;
                margin: 0 auto; 
                color: #001028;
                background: #FFFFFF; 
                font-family: Arial, sans-serif; 
                font-size: 12px; 
                font-family: Arial;
              }
              header {
                padding: 10px 0;
                margin-bottom: 10px;
              }
              #logo {
                text-align: center;
                margin-bottom: 10px;
              }
              #logo img {
                width: 180px;
              }
              h1 {
                border-top: 1px solid  #2978B5;
                border-bottom: 1px solid  #2978B5;
                color: white;
                font-size: 2.4em;
                line-height: 1.4em;
                font-weight: normal;
                text-align: center;
                margin: 0 0 20px 0;
                background-color: #2978B5;
              }
              #project {
                float: left;
              }
              #project div,
              #company div {
                white-space: nowrap;        
              }
              table {
                width: 100%;
                border-collapse: collapse;
                border-spacing: 0;
                margin-bottom: 20px;
              }
              table tr:nth-child(2n-1) td {
                background: #F5F5F5;
              }
              table th,
              table td {
                font-size: 1.2em;
              }
              table th {
                padding: 5px 20px;
                color: #5D6975;
                border-bottom: 3px solid #125D98;
                white-space: nowrap;        
                font-weight: normal;
              }
              table .service,
              table .desc {
                text-align: left;
              }
              table td {
                padding: 20px;
                text-align: right;
              }
              table td.service,
              table td.desc {
                vertical-align: top;
              }
              table td.grand {
                border-top: 1px solid #125D98;
              }
              #notices .notice {
                color: #5D6975;
                font-size: 1.2em;
              }
            </style>
          </head>
          <body>
            <header class="clearfix">
              <div id="logo">
                <img src="http://localhost:3200/logo-horizonte.jpg">
              </div>
              <h1>{{ titulo }}</h1>
            </header>
            <main style="margin-top: 20px;">
              <table>
                <thead>
                  <tr>
                    <th class="service">Num.</th>
                    <th style="text-align: left;">Fecha</th>
                    <th class="desc">Nombre</th>
                    <th style="text-align: left;">Genero</th>
                    <th style="text-align: left;">Edad</th>
                    <th style="text-align: left;">Medico Tratante</th>
                  </tr>
                </thead>
                <tbody>
                    {{#each users}}
                        <tr>
                            <td class="service create_index">{{ @index }}</td>
                            <td class="unit" style="text-align: left;">{{ fechaIngreso }}</td>
                            <td class="desc">{{ paciente.nombrePaciente }} {{ paciente.apellidoPaterno }} {{ paciente.apellidoMaterno }}</td>
                            <td class="unit" style="text-align: left;">{{ paciente.genero }}</td>
                            <td class="desc">{{ paciente.edad }}</td>
                            <td class="desc">{{ medicoTrante }}</td>
                        </tr>
                    {{/each}}
                </tbody>
              </table>
            </main>
            <script>
                var divs = document.querySelectorAll('.create_index');
                for (var i = 0; i < divs.length; ++i) {
                    divs[i].innerHTML = i + 1;
                }
            </script>
          </body>
        </html>
        `,
        context: {
            titulo: 'Historial de Pacientes',
            users: users
        },
        path: './archivosPDF/historial_pacientes.pdf'
    }
    try {
        await pdf.create(document);
        res.download('./archivosPDF/historial_pacientes.pdf');
    } catch (error) {
        console.log(error);
    }
})

module.exports = app;