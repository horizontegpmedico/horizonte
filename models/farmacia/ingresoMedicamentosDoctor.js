// creacion de una modelo para DB usando la librería mongoose
// este modelo es de los medicamentos que el doctor va a agregar 
// desde la receta interna

const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let modelSchemaMedicamentos = new Schema({

    nombreComercial: { type: String },
    nombreDeSalOsustanciaActiva: { type: String },
    presentacio: { type: String },
    contenidoFrasco: { type: String },
    viaDeAdministracion: { type: String },
    lote: { type: String },
    laboratorio: { type: String },
    fechaCaducidad: { type: String }

});

module.exports = mongoose.model('medicamentosDoctor', modelSchemaMedicamentos);