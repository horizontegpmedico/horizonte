const mongoose = require('mongoose');

const schemaService = new mongoose.Schema({

    preciosRangoA: { type: Array },
    preciosRangoB: { type: Array },
    preciosRangoC: { type: Array },
    // id's
    idServicio: { type: mongoose.Schema.Types.ObjectId, ref: 'modeloservicios' },
    idSede: { type: mongoose.Schema.Types.ObjectId, ref: 'sedes' },
    // rango de utilidad 
    rangoUtilidad: { type: String, required: true, trim: true },
    // name del servicio
    name: { type: String, required: true, trim: true },
});

module.exports = mongoose.model('utilidadesServicios', schemaService)