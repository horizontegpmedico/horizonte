const mongoose = require('mongoose');

const schemaUtilidades = mongoose.Schema({

    rangoUtilidad: { type: String, required: true, trim: true },
    preciosRangoA: { type: Array },
    preciosRangoB: { type: Array },
    preciosRangoC: { type: Array },
    /* precio_publico_dia: { type: Number, require: true, trim: true },
    precio_publico_redondo_dia: { type: Number, require: true, trim: true },
    precio_publico_noche: { type: Number, require: true, trim: true },
    precio_publico_redondo_noche: { type: Number, require: true, trim: true },
    precio_membresia_dia: { type: Number, require: true, trim: true },
    precio_membresia_redondo_dia: { type: Number, require: true, trim: true },
    precio_membresia_noche: { type: Number, require: true, trim: true },
    precio_membresia_redondo_noche: { type: Number, require: true, trim: true }, */
    name: { type: String, required: true, trim: true },
    idServicio: { type: mongoose.Schema.Types.ObjectId, ref: 'ambulancia' },
    idSede: { type: mongoose.Schema.Types.ObjectId, ref: 'sedes' }
});


module.exports = mongoose.model('utilidadesAmbulancia', schemaUtilidades);