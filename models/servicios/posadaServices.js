const { Schema, model} = require('mongoose');

const schemaServicesPosada = Schema({
    SERVICIO: { type: String, trim: true , uppercase: true },
    INDICACIONES: { type: String },
    PRECIO_PUBLICO: { type: Number },
    status: {  type: Boolean },
    name: { type: String }
});

module.exports = model('posadaServices', schemaServicesPosada);