const { Schema, model  } = require('mongoose');


const schemaServicios = new Schema({
    nombre: {  type: String, trim: true, uppercase: true  }
}, { timestamps: true });



//TODO: EN ESTE MODELO SE PUEDE AGREGR EL NOMBRE DEL DOCTOR QUE IMPARTE ESTE SERVICIO
module.exports = model('serviciosDepartamento', schemaServicios);