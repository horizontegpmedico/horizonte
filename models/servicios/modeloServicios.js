const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const modelServicios = new Schema({
    
    tipoDeServicio: { type: mongoose.Types.ObjectId, ref:"horarioServicios" },
    ESTUDIO: { type: String, trim: true , uppercase: true },
    
    INDICACIONES: { type: String },
    PRECIO_PUBLICO: { type: String },
    PRECIO_PUBLICO_URGENCIA: { type: String },
    PRECIO_PUBLICO_HOSPITALIZACION: { type: String },

    PRECIO_MEMBRESIA: { type: String },
    PRECIO_MEMBRESIA_URGENCIA: { type: String },
    PRECIO_MEMBRESIA_HOSPITALIZACION: { type: String },

    // elementos que contiene los estudios 
    name: { type: String },
    status: { type: String, trim: true, uppercase: true, default: 'ACTIVO' }

}, { timestamps: true });


/*
!Socios
atlatlahucan
Felipe de Jesus rul ramirez
rigoberto hernande franco
seidy lucia martinaz

totolapan
jose fernando rubio paniaugua

la vivianas
guillermo arenas sandoval


tlalnepatlan
juan manuel vera montiel
*/
module.exports = mongoose.model('modeloservicios', modelServicios);