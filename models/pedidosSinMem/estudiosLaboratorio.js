const mongoose = require('mongoose');
let Schema = mongoose.Schema;
let nuevoEstudio = new Schema({

    estudios: { type: Array },
    fecha: { type: String },
    hora: { type: String },
    ventas:  {  type: mongoose.Types.ObjectId, ref: 'estudiosPedidos'  }


});

module.exports = mongoose.model('estudiosLaboratorio', nuevoEstudio);