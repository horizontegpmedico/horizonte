const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let pedidiosSchema = new Schema({

    estudios: { type: Array },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    fecha: { type: String },
    hora: { type: String },
    medicoQueSolicita: { type: String },
    estadoPedido: { type: String, default: 'En espera' },
    sede: { type: String },
    prioridad: { type: String },
    estado: { type: String }
});

module.exports = mongoose.model('estudiosPedidos', pedidiosSchema);