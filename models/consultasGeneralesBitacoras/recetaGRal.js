const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let schemaReceta = new Schema({

    medicamentos: { type: Array },
    otrasIndicaciones: { type: String },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    idConsulta: { type: Schema.Types.ObjectId, ref: 'consultaHistorico' },
    idMedico: { type: Schema.Types.ObjectId, ref: 'ModelPersonal' },
    fechaReceta: { type: String },
    horaReceta: { type: String },
    estudios: { type: Array },
    medicoQueAtendio: { type: String },
    cedula: { type: String },
    firma: { type: String },
    origen: { type: String },
    horaEgreso: { type: String },
    prioridad: { type: String },
    folio: { type: Number },
});

module.exports = mongoose.model('receta', schemaReceta);