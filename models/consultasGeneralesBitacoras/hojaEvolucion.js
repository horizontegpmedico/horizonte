const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let schemaHojaEvolucion = new Schema({
    diagnosticos: { type: Array },
    motivoDeConsulta: { type: String },
    fechaHojaEvolucion: { type: String },
    horaHojaEvolucion: { type: String },
    evolucion: { type: String },
    plan: { type: String },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    idConsulta: { type: Schema.Types.ObjectId, ref: 'consultaHistorico' },
    idMedico: { type: Schema.Types.ObjectId, ref: 'ModelPersonal' }
});

module.exports = mongoose.model('hojaEvolucion', schemaHojaEvolucion);