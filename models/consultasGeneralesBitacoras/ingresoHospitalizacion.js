const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let newHospitalizacion = new Schema({


    fechaIngreso: { type: String },
    horaIngreso: { type: String },
    enfermeraAtendio: { type: String },
    diagnosticoInicial: { type: String },
    diagnosticoActual: { type: String },
    medicoTrante: { type: String },
    genero: { type: String },
    nombre: { type: String },
    apellidoPaterno: { type: String },
    apellidoMaterno: { type: String },
    tipoDeServicio: { type: String, default: 'consulta general' },
    membretesLegible: { type: String },
    notificacionDeIdentificacion: { type: String },
    solicitudesDeEstudioDeGabinete: { type: String },
    cuentaConBrazaleteDeIdentificacion: { type: String },
    elMedicamentoDelPacienteCuantaConLaIdentificion: { type: String },
    solucionIntravenosaCuentaConIdentificacioCompleta: { type: String },
    cuentaConMembreteLaCabecera: { type: String },
    elHemoDervidadoSeencuentraConLosDatosDelPaciente: { type: String },
    lasSolucionesDeDotacionDeDietaEstanIdentificada: { type: String },
    cuentaConAlgunaTipoDeSondaOdrenage: { type: String },
    laSondaODrenageSeEncuntraConLosDatosDelPaciente: { type: String },
    estado: { type: String, default: 'En proceso' },
    solicitudesDeEstudio: { type: String },
    tipoConsulta: { type: String, default: "Consulta general" },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    excedentes: { type: Array },
    edad: { type: String },
    montoAcumulado: { type: String },
    abono: { type: String },
    paqueteQuirofano: { type: Schema.Types.ObjectId }
});


module.exports = mongoose.model('hospitalizacion', newHospitalizacion);