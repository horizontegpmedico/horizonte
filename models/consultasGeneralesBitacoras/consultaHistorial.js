const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let newConsulta = new Schema({

    // Datos que captura la encargada de recepcion
    fechaIngreso: { type: String },
    horaIngreso: { type: String },
    horaEgreso: { type: String },
    enfermeraAtendio: { type: String },
    motivoIngreso: { type: String },
    diagnosticoActual: { type: String },
    diagnosticos: { type: Array },
    medicoTrante: { type: String },
    notaRecepcion: { type: String },
    notaDeLaEnfermera: { type: String },
    tipoDeServicio: { type: String },
    horaHojaEvolucion: { type: String },
    doctorAPasar: { type: String },
    riesgoCaida: { type: String },

    // consultorio
    consultorio: { type: String },
    // sv
    talla: { type: String },
    peso: { type: String },
    imc: { type: String },
    fc: { type: String },
    fr: { type: String },
    temp: { type: String },
    pc: { type: String },
    pa: { type: String },
    pt: { type: String },
    apgar: { type: String },
    sistolica: { type: String },
    diastolica: { type: String },
    SaO: { type: String },
    // oximetria: { type: String },
    glucosa: { type: String },
    observaciones: { type: String },
    turno: { type: String },
    pao: { type: String },
    // certificacion
    membretesLegible: { type: String },
    notificacionDeIdentificacion: { type: String },
    solicitudesDeEstudioDeGabinete: { type: String },
    cuentaConBrazaleteDeIdentificacion: { type: String },
    elMedicamentoDelPacienteCuantaConLaIdentificion: { type: String },
    solucionIntravenosaCuentaConIdentificacioCompleta: { type: String },
    cuentaConMembreteLaCabecera: { type: String },
    elHemoDervidadoSeencuentraConLosDatosDelPaciente: { type: String },
    lasSolucionesDeDotacionDeDietaEstanIdentificada: { type: String },
    cuentaConAlgunaTipoDeSondaOdrenage: { type: String },
    laSondaODrenageSeEncuntraConLosDatosDelPaciente: { type: String },
    status: { type: String, default: 'En espera' },
    solicitudesDeEstudio: { type: String },

    // sedes 
    sede: { type: String },
    horaAtendioenfermeria: { type: String },

    // referencias
    paciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    idHojaEvolucion: { type: Schema.Types.ObjectId, ref: 'hojaEvolucion' },
    idRecetaMedica: { type: Schema.Types.ObjectId, ref: 'receta' },
    idHistoriaClinica: {type: Schema.Types.ObjectId, ref : 'historiaClinica'}


});


module.exports = mongoose.model('consultaHistorico', newConsulta);