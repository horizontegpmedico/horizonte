const mongoose = require('mongoose');

const schemaPedidosSedes = mongoose.Schema({

    paciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    vendedor: { type: String, trim: true },
    fecha: { type: Date },
    hora: { type: String, trim: true },
    estudios: { type: Array },
    efectivo: { type: Boolean },
    montoEfectivo: { type: Number },
    doctorQueSolicito: { type: String, trim: true },
    sede: { type: String, trim: true },
    totalCompra: { type: Number },
    compraConMembresia: { type: Boolean },
    anticipo: { type: Boolean },
    montoAnticipo: { type: Number },
    folio : { type: Number },
    status: { type: String, trim: true, default: 'Pendiente' },
    montoTarjteCredito: { type: Number, default: 0 },
    montoTarjetaDebito: { type: Number, default: 0 },
    montoTranferencia: { type: Number, default: 0 },
    tarjetCredito: { type: Boolean, default: false },
    tarjetaDebito: { type: Boolean, default: false },
    transferencia: { type: Boolean, default: false },
    sinpago:{ type: Boolean, default: false },
    montoSinpago:{ type: Number },
    ganancias: { type: Number }
});


module.exports = mongoose.model('pedidosSedes', schemaPedidosSedes)