const { Schema, model }= require('mongoose');


const schemaCancelVentas = Schema({
    idVenta: { type: Schema.Types.ObjectId, ref: 'ventasServicios' },
    idUser : { type: Schema.Types.ObjectId, ref:'ModelPersonal' },
}, 
{ timestamps: true }
);

module.exports = model('cancelVentas', schemaCancelVentas);