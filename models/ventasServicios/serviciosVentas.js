const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let SchemaVentas = new Schema({

    paciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    nombrePaciente: { type: String },
    vendedor: { type: mongoose.Types.ObjectId, ref: 'ModelPersonal' },
    fecha: { type: Date, default: Date.now()  },
    hora: { type: String },
    estudios: { type: Array },
    efectivo: { type: Boolean, default: false },
    montoEfectivo: { type: Number },
    tarjetCredito: { type: Boolean, default: false },
    montoTarjteCredito: { type: Number },
    tarjetaDebito: { type: Boolean, default: false },
    montoTarjetaDebito: { type: Number },
    doctorQueSolicito: { type: String },
    transferencia: { type: Boolean, default: false },
    montoTranferencia: { type: Number },
    facturacion:{ type: Boolean, default: false },
    factura:{ type: String },
    sede: { type: String, trim: true, default:'TLYC01' },
    socioSede: { type: String, trim: true, default:'TLYC01' },
    totalCompra: { type: Number },
    compraConMembresia: { type: Boolean },
    status: { type: String, default: 'Anticipo' },
    recibe: { type: String, default: ''},
    folio: { type: Number },
    montoAnticipo: { type: Number, default:0},
    motivoCancelacio : {  type: String, uppercase: true, trim: true },
    ganancia: { type: Number },
    gananciaPromedio: { type: Number, default: 0}
}, { timestamps: true});
module.exports = mongoose.model('ventasServicios', SchemaVentas)