const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let schemaExpedientes = new Schema({
    sede: { type: String },
    consecutivo: { type: Number },
});

module.exports = mongoose.model('expediente', schemaExpedientes);