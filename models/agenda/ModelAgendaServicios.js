const mongoose = require('mongoose');

let Schema = mongoose.Schema; 

let schemaAgendaServicios = new Schema({ 
agendo:	     { type: Schema.Types.ObjectId, ref:'ModelPersonal'},  
idPersonal:  { type: Schema.Types.ObjectId, ref:'medicos' },
color:	{type:String, uppercase: true}, 
titulo:	{type:String, uppercase: true}, 
Servicio:	{type:String, uppercase: true}, 
horaInicio:	{type:String, uppercase: true}, 
horaFin:	{type:String, uppercase: true}, 
fechaInicio:	{type:String, uppercase: true}, 
fechaFin:	{type:String, uppercase: true},
sede:	{type:String, uppercase: true}

})

module.exports = mongoose.model('AgendaServicios', schemaAgendaServicios)