const mongoose = require('mongoose');

let Schema = mongoose.Schema; 

let schemaAgenda = new Schema({ 
idPaciente:  { type: Schema.Types.ObjectId, ref:'Paciente' },
idPersonal:  { type: Schema.Types.ObjectId, ref:'medicos' },
agendo:	     { type: Schema.Types.ObjectId, ref:'ModelPersonal'},  
status:	{type:String, uppercase: true}, 
color:	{type:String, uppercase: true}, 
idServicio:	{type: Schema.Types.ObjectId, ref:'modeloservicios'}, 
descrpicion:	{type:String, uppercase: true}, 
motivo:	{type:String, uppercase: true}, 
horaInicio:	{type:String, uppercase: true}, 
horaFin:	{type:String, uppercase: true}, 
fechaInicio	:	{type:String, uppercase: true}, 
fechaFin	:	{type:String, uppercase: true},
sede	:	{type:String, uppercase: true}
})


module.exports = mongoose.model('Agenda', schemaAgenda)