const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let personalSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es requerido'], trim: true }, // nombre completo
    password: { type: String, required: [true, 'La constrasenia es requerida'], trim: true },
    RFC: { type: String, trim: true },
    curp: { type: String, trim: true }, // limitado a los 18
    role: { type: String, required: [true, 'El role es requerido'], trim: true },
    fechaNaciemiento: { type: Date },
    fechaInicio: { type: Date }, // esta sera una funcion que se dispare no necesita input
    turno: { type: String, trim: true },
    fechaREgistro: { type: Date, default: Date.now() },
    img: { type: String, trim: true },
    status: { type: String, default: 'Activo' },
    sede: { type: String, default: 'TLYC01', trim: true },
    sedeId: { type: mongoose.Types.ObjectId, ref: 'sedes', default:'6007a6872cda7c07dc593bc8'},
    cedulaProfesional: { type:String, trim: true },
    Especialidad: { type: String, trim: true},
    medico: { type: String, uppercase: true, trim: true },
    cedulaEsp: { type: String, trim: true },
    universidad: { type: String, trim: true, default:''}
});

// agregar mongoose-unique-validator

module.exports = mongoose.model('ModelPersonal', personalSchema);