const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let visitas = new Schema({

    horallegada: { type: String },
    horaSalida: { type: String },
    servicio: { type: String },
    pacienteId: { type: Schema.Types.ObjectId, fer: 'pacientes' },
    status: { type: String, default: 'En proceso' },
    atendio: { type: String },
});


module.exports = mongoose.model('serviciosPacientes', visitas)