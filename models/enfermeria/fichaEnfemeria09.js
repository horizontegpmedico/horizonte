const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let fichaEnfermeria09 = new Schema({

    Talla: { type: String },
    Peso: { type: String },
    Imc: { type: String },
    Fc: { type: String },
    fr: { type: String },
    Temp: { type: String },
    Tamiz: { type: String },
    Pc: { type: String },
    Pa: { type: String },
    Pt: { type: String },
    Saor: { type: String },
    Notas: { type: String },
    pao: { type: String },
    visitas: { type: Array },
    sistolica: { type: String },
    diastolica: { type: String }

});



module.exports = mongoose.model('fichaEnfermeria09', fichaEnfermeria09);