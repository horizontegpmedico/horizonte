const  mongooose = require('mongoose');
const Schemma = mongooose.Schema;

let ModuleSchema = new Schemma({
    nameModule:{ type: String, uppercase: true,  require:[true, 'El nombre del modulo es requerido'] },
    route: { type: String,   required: [true, 'La ruta del modulo es requerida'] },
    role: { type: String,   required: [true, 'El rol del modulo es requerida'] },
    icon: { type: String }
});

module.exports = mongooose.model('modules', ModuleSchema);
