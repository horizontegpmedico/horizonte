const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let paquetePaciente = new Schema({

    folio: { type: String }, 
    vendedor: { type: String },
    fecha: { type: Date, default: Date.now() },
    fechaTermino: { type: Date},
    documentos: { type: Array },
    paquete: { type: Schema.Types.ObjectId, ref: 'paquetes' },
    hora: { type: String },
    paciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    pagos: {type: Array},
    consultas: { type: Array },
    laboratorio: { type: Array },
    ultrasonido:{ type: Array },
    rayosX:{ type: Array },
    documentos:{ type: Array },
    hospitalizacion:{ type: Array },
    farmacia:{ type: Array },
    tomografia:{ type: Array },
    otros:{ type: Array },
    membresiaActiva: { type: Boolean, default: true }
});


module.exports = mongoose.model('paquetesPacientes', paquetePaciente);