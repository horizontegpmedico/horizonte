const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let modelCie10 = new Schema({

    CONSECUTIVO: { type: String },
    LETRA: { type: String },
    CATALOG_KEY: { type: String },
    NOMBRE: { type: String }
});

module.exports = mongoose.model('cie10', modelCie10);