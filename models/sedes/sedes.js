const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schemaSedes = new Schema({

    nomenclatura: { type: String },
    localizacion: { type: String },
    encargado: { type: String },
    rfc: { type: String },

    ambulancia: { type: Number },
    endoscopia: { type: Number },
    ultrasonido: { type: Number },
    laboratorio: { type: Number },
    rayosX: { type: Number },
    tomografia: { type: Number },
    patologia: { type: Number },
    consultas: { type: Number },
    serviciosGeneral: { type: Number }

});

module.exports = mongoose.model('sedes', schemaSedes);