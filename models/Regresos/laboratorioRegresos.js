const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const schemaSedes = new Schema({
    idPaciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    idPedido: { type: mongoose.Types.ObjectId, ref: 'estudiosPedidos' },
    obtenidos: { type: Array },
    idEstudio: { type: mongoose.Types.ObjectId, ref: 'modeloservicios' },
    quimico: { type: String, trim: true }
});


module.exports = mongoose.model('laboratorioRegresos', schemaSedes);