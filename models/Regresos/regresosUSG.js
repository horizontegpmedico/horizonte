const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const schemaUSG = new Schema({

    idPaciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    idPedido: { type: mongoose.Types.ObjectId, ref: 'estudiosPedidos' },
    machoteEdit: { type: String },
    diagnostico: { type: String,  trim: true, usuario: true },
    observaciones: { type: String, trim: true, usuario: true},
    fecha: { type: String },
    usuario: { type: String },
    idEstudio: { type: mongoose.Types.ObjectId, ref: 'modeloservicios' },
    imageUrl: { type: Array },
});


module.exports = mongoose.model('usgRegresos', schemaUSG);