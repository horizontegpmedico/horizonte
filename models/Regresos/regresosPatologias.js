const { Schema, model } = require('mongoose');

const patologiaRegresos = new Schema({
   
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    idPedido: { type: Schema.Types.ObjectId, ref: 'patologiaPedidos'  },
    sede: { type: String, trim: true },
    interpretacion: { type: String, trim: true, uppercase: true },
    fechaDePedidoDeLosExamenes: { type: Date },
    diagnostico: { type: String,  trim: true, uppercase: true },
    observaciones: { type: String, trim: true, uppercase: true },
    usuario: { type: String, trim: true, uppercase: true},
    idEstudio: { type: Schema.Types.ObjectId, ref: 'modeloservicios' },

});

module.exports = model('patologiaregresos',  patologiaRegresos);