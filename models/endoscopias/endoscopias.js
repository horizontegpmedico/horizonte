const {Schema, model, mongoose} = require("mongoose");

const endoscopiaSchema = new Schema({
  idPaciente: {type: Schema.Types.ObjectId, ref: "Paciente"},
  estudios: {type: Array},
  sede: {type: String, trim: true},
  medicoQueSolicito: {type: String, trim: true, upperCase: true},
  fechaDePedidoDeLosExamenes: {type: Date},
  prioridad: {type: String, default: "Programado"},
  status: {type: String, default: "PEDIDO", trim: true, uppercase: true},
  idObtenido: {type: Schema.Types.ObjectId, ref: "endoscopiaregresos"},
  video: {type: Buffer, trim: true, uppercase: true},
});

module.exports = model("endoscopiaPedidos", endoscopiaSchema);
