const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let log = new Schema({
    ruta:  { type: String} ,
    error:{ type: String },
    hora: { type: String},
    fecha: { type: Date, default: Date.now()  }
});


module.exports = mongoose.model('log', log);