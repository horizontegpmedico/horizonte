const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const schemaX = new Schema({

    idPaciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    idPedido: { type: mongoose.Types.ObjectId, ref: 'estudiosPedidos' },
    machoteEdit: { type: String },
    diagnostico: { type: String },
    observaciones: { type: String },
    fecha: { type: String },
    idEstudio: { type: mongoose.Types.ObjectId, ref: 'modeloservicios' },
    imageUrl: { type: Array },
    sede: { type: String },
    usuario: { type: String }
});


module.exports = mongoose.model('xrayRegresos', schemaX);