const mongoose = require('mongoose');

let schema = mongoose.Schema;


let newSchemaMembresia = new schema({

    fechaAfiliacion: { type: String },
    fechaFinaliza: { type: String },
    metodoPago: { type: String },
    atendio: { type: String },
    contratoPDF: { type: String }
});


module.exports = mongoose.model('membresiaContrato', newSchemaMembresia);