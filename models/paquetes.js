const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let SchemaPaquetes = new Schema({

    nombrePaquete: { type: String },
    costoTotal: { type: Number },
    contenidoPaquete: { type: Array },
    nomenclaturaPaciente: { type: Array },
    nomenclatura: { type: String },
    status: { type: Boolean },
    anticipo: { type: Number },
})

module.exports = mongoose.model('paquetes', SchemaPaquetes);