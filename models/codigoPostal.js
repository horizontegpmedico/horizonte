const mongoose = require('mongoose');
let Schema = mongoose.Schema; 
let schemacpPostal= new Schema({ 
    codigoPostal: {type: String},
    cpAsentamiento:{type: String},
    cpTipoAsentamiento:{type:String},
    cpMunicipios:{type:String},
    cpEstado:{type:String},
    cpCiudad:{type:String}
})


module.exports = mongoose.model('cpPostal', schemacpPostal,'cpPostal')