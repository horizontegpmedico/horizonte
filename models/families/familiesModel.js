const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// este modelo va agregar a los integrantes de las familias

const schemaFamilies = new Schema({
    nombre: { type: String },
    integrantes: [{ type: Schema.Types.ObjectId, ref: 'Paciente'}]
});

module.exports = mongoose.model('familias', schemaFamilies);