const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let newEstudio = new Schema({

    estudios: { type: Array },
    idPaciente: { type: mongoose.Schema.Types.ObjectId, ref: 'Paciente' },
    sede: { type: String },
    medicoQueSolicito: { type: String },
    fechaDePedidoDeLosExamenes: { type: Date },
    prioridad: { type: String, default: 'Programado' },
    // tipoExamen: { type: String }

});


module.exports = mongoose.model('usgNew', newEstudio);