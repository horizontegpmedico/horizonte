const { Schema, model} = require('mongoose');

// nombredeservicio es el name del departamento
const schemaServicios = Schema({

    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente'},
    idServicio: { type:Schema.Types.ObjectId, ref: 'modeloservicios' },
    pedidosSedes: { type: Boolean, default: false},
    status: { type: String, default: 'PEDIDO', uppercase: true ,trim: true },
    urgencia: { type: Boolean, default: false},
    sede: { type: String, default: 'TLYC01'},
    fechaPedido: { type: Date, default: Date.now()  },
    recibe: { type: String, trim: true, uppercase: true, default: ''},
    hora: { type: String, trim: true },
    fechaEntrega:{ type: Date },
    doctorQueSolicito: { type: String, trim: true, uppercase: true},
    resultados: { type: Array },
    nombreServicio: { type: String, trim: true, uppercase: true},
    idVenta: { type: Schema.Types.ObjectId, ref: "ventasServicios", required: true},
    diagnostico:{  type: String, trim: true, uppercase: true},
    countServices : { type:Number, default: 1},
    imgQrCode: { type: String },
    newEstudio: { type: Boolean, default:false },
    pdf: { type: String },
    link: { type: String }
},{ timestamps: true });

module.exports = model('pedidosServicios', schemaServicios );
