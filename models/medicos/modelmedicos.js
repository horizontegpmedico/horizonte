const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let newSchemaMedico = new Schema({

    nombre: { type: String, required:[ true, 'El nombre es requerido'] },
    cedulaProfesional: { type: String, required:[ true, 'La cedula profesional es requerida'] },
    sucursal: { type: String, required:[ true, 'La sucrusal es requerida'] }
});

module.exports = mongoose.model('medicos', newSchemaMedico)