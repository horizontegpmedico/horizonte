const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const paquetespagos = new Schema({
    paquete: { type: Schema.Types.ObjectId, ref: 'paquetes' },
    paciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    semagesta: { type: String},
    semapago: { type: String},
    pago:{type: String},
    acumulado: { type: String}
})
module.exports = mongoose.model('paquetespagos',paquetespagos)