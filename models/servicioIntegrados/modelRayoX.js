const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let rayosXSchema = new Schema({
    ESTUDIO: { type: String, required: [true, 'El nombre del estudio es requerido'] },

    PRECIO_MEMBRESIA: { type: String },
    PRECIO_MEMBRESIA_URGENCIA: { type: String },
    PRECIO_HOSPITALIZACION_URGENCIA_MEMBRESIA: { type: String },
    PRECIO_MEMBRESIA_HOSPITALIZACION: { type: String },

    PRECIO_PUBLICO: { type: String },
    PRECIO_PUBLICO_URGENCIA: { type: String },
    PRECIO_PUBLICO_HOSPITALIZACION: { type: String },
    PRECIO_HOSPITALIZACIO_URGENCIA: { type: String },

    name: { type: String, default: 'Rayos x' }
});

module.exports = mongoose.model('rayosX', rayosXSchema);