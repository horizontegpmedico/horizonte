const mongoose = require('mongoose');

let Schema = mongoose.Schema;

//=======================================================================================
//    ESTE MODELO DE ULTRASONIDOS ESTA BASADO EN EL EXCEL
//=======================================================================================


let newSchemaUltrasonido = new Schema({
    ESTUDIO: { type: String },
    INDICACIONES: { type: String },
    PRECIO_PUBLICO: { type: String },
    PRECIO_PUBLICO_URGENCIA: { type: String },
    PRECIO_PUBLICO_HOSPITALIZACION: { type: String },
    PRECIO_PUBLICO_HOSPITALIZACIO_URGENCIA: { type: String },
    PRECIO_MEMBRESIA: { type: String },
    PRECIO_MEMBRESIA_URGENCIA: { type: String },
    PRECIO_MEMBRESIA_HOSPITALIZACION: { type: String },
    PRECIO_MEMBRESIA_HOSPITALIZACION_URGENCIA: { type: String },
    name: { type: String, default: 'ultrasonido' }

});



module.exports = mongoose.model('ultrasonido', newSchemaUltrasonido);