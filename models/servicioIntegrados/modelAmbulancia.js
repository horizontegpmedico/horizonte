const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let newSchemaAmbulancia = new Schema({

    DESTINO: { type: String, required: [true, 'El se requiere el destino'] },
    PRECIO_PUBLICO_DIA: { type: String, required: [true, 'El precio de día es requerido'] },
    PRECIO_PUBLICO_REDONDO_DIA: { type: String, required: [true, 'el precio de redonde de día es requerido'] },
    PRECIO_PUBLICO_NOCHE: { type: String, required: [true, 'el precio de noche es requerido'] },
    PRECIO_PUBLICO_REDONDO_NOCHE: { type: String, required: [true, 'el precio redondo de noche es requerido'] },
    PRECIO_MEMBRESIA_DIA: { type: String, required: ['el precio membresia día es requerido'] },
    PRECIO_MEMBRESIA_REDONDO_DIA: { type: String, required: ['precio membresia redondo es requerido'] },
    PRECIO_MEMBRESIA_NOCHE: { type: String, required: ['El precio de membresia de noche es requerido'] },
    PRECIO_MEMBRESIA_REDONDO_NOCHE: { type: String, required: ['El precio membresia noche redondo'] },
    name: { type: String, default: 'ambulancia' }

});


module.exports = mongoose.model('ambulancia', newSchemaAmbulancia);