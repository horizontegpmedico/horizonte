const  mongoose = require('mongoose');
let Schema = mongoose.Schema;


let newSchemaAcceso = new Schema({

  fechaDeAltaAlModulo: { type: Date, default: Date.now() },
  ModelPersonal: {type: Schema.Types.ObjectId, ref: 'ModelPersonal'},
  modules:{ type: Schema.Types.ObjectId, ref: 'modules' } 
  
});


module.exports = mongoose.model('acceso', newSchemaAcceso);