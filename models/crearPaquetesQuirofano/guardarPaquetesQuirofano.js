const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let newSchemaPaqueteQuirofano = new Schema({

    nombrePaquete: { type: String },
    participantes: { type: Array },
    maquinas: { type: Array },
    medicamentos: { type: Array },
    horaRecuperacion: { type: String },
    costoHorasRecuperacion: { type: String }
});



module.exports = mongoose.model('paquetesQuirofano', newSchemaPaqueteQuirofano);