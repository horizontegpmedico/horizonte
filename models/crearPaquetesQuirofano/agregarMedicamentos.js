const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let newSchemaMedicamento = new Schema({
    nombreDelMedicamento: { type: String },
    costoMedicamento: { type: Number }
})


module.exports = mongoose.model('medicamentosNuevos', newSchemaMedicamento);