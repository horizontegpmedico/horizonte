const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let schemaEvento = new Schema({

    paciente: { type: mongoose.Types.ObjectId, ref: 'Paciente' },
    medico: { type: mongoose.Types.ObjectId, ref: 'ModelPersonal' },
    color: { type: String, trim: true, uppercase: true },
    descripcion : {  type: String, trim: true, uppercase: true },
    agendo : { type: String, trim: true, uppercase: true},
    fecha: { type: Date, default: Date.now()  },
    motivo: { type: String },
    horaInicio: { type: String, trim: true },
    horaFin: { type: String, trim: true},
    fechaInicio: { type: String },
    fechaFin: { type: String },
    sede: { type: String, trim: true, default:'TLYC01' },
    status: { type: String, default: 'DISPONIBLE', trim: true, uppercase: true},

}, { timestamps: true });

/**
 * CANCELADO 
 * DISPONIBLE
 * TERMINADO
 * 
 */

module.exports = mongoose.model('evento', schemaEvento)