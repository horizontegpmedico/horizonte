const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let schemaCorteServiciosSede = new Schema({
    sede: { type: String },
    fecha: { type: Date, default: Date.now() },
    texto: { type: String }
});

module.exports = mongoose.model('corteServiciosSede', schemaCorteServiciosSede);