const mongoose = require('mongoose');

let hospitalizacionSchema = new mongoose.Schema({

    paciente: {  type: mongoose.Types.ObjectId, ref: 'Paciente'},
    indicaciones: { type: Array},
    hora: { type: String, trim: true},
    status: { type: String, uppercase: true, default: "HOSPITALIZADO" },
    fechaRegistro: { type: Date, default: Date.now() }, 
    AltaPaciente: { type: Date },
    horaAltaPaciente: { type: String, truth: true},
}, { timestamps: true});

module.exports = mongoose.model('hospitalizacionNotas', hospitalizacionSchema);
