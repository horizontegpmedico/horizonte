const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaHojaEvolucion = mongoose.Schema({
    hojaevolucion:   { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    piel:     {type: String,uppercase: true},
    mucosas:     {type: String, uppercase: true},
    torax:     {type: String, uppercase: true},
    abdomen:     {type: String, uppercase: true},
    higado:     {type: String, uppercase: true},
    bazo:     {type: String, uppercase: true},
    observaciones_explofisi:     {type: String, uppercase: true},
    ganglios:     {type: String, uppercase: true},
    observaciones_explofisidos:     {type: String, uppercase: true},
    candidato_donacion:     {type: String, uppercase: true},
    observaciones_diagnostico:     {type: String, uppercase: true},
    cie10:     {type: Array, uppercase: true},
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('hojaEvolucionBS', schemaHojaEvolucion)