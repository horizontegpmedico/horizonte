const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaReaccionesDerivadas = mongoose.Schema({
    paciente:   { type: mongoose.Types.ObjectId, ref:'Paciente' },
    tiposangreado: {type: String, uppercase: true},
    contenidoporaferesis: {type: String, uppercase: true},
    iniciodereaccion: {type: String, uppercase: true},
    tipodonante: {type: String, uppercase: true},
    presentacionreacciones: {type: String, uppercase: true},
    antecedentesreacciones: {type: String, uppercase: true},
    reaccionvasovagalleve: {type: Array},
    reaccionvasovagalmoderado: {type: Array},
    reaccionvasovagalsevera: {type: Array},
    reaccionvasovagalleveotros: {type: String, uppercase: true},
    reaccionvasovagalmoderadootros: {type: String, uppercase: true},
    reaccionvasovagalseveraotros: {type: String, uppercase: true},
    reaccionvenopuncionleve: {type: Array},
    reaccionvenopuncionmoderado: {type: Array},
    reaccionvenopuncionsevera: {type: Array},
    reaccionvenopuncionleveotros: {type: String, uppercase: true},
    reaccionvenopuncionmoderadootros: {type: String, uppercase: true},
    reaccionvenopuncionseveraotros: {type: String, uppercase: true},
    reacciontoxicidadleve: {type: Array},
    reacciontoxicidadmoderado: {type: Array},
    reacciontoxicidadsevera: {type: Array},
    reacciontoxicidadleveotros: {type: String, uppercase: true},
    reacciontoxicidadmoderadootros: {type: String, uppercase: true},
    reacciontoxicidadseveraotros: {type: String, uppercase: true},
    tiempodepresentacion: {type: Array},
    tratamiento: {type: String, uppercase: true},
    resultadosLabCalcio:{type: String, uppercase: true},
    resultadosLabMagnesio:{type: String, uppercase: true},
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('ReaccionesDerivadasBS', schemaReaccionesDerivadas)