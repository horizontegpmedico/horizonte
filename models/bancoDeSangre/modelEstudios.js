const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaEstudiosLabBS = new Schema({
    nombre:        { type: String, uppercase: true },
    tipo_de_examen: { type: String, uppercase: true},
    valoresDeReferencia:     { type: Array}, 
    observaciones: { type: String, uppercase: true},
    fecha:         { type: Date, default: Date.now() },
})


module.exports = mongoose.model('EstudiosLabBS', schemaEstudiosLabBS)