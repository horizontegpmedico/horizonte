const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let schemaSedesbs = new Schema({
    imagencli: { type: Array 
    },
    clinica: { type: String, uppercase: true },
    razon: { type: String, uppercase: true },
    rfc: { type: String, uppercase: true },
    responsable_sa: { type: String, uppercase: true },
    contacto: { type: String, uppercase: true },
    genero: { type: String, uppercase: true },
    fecha: { type: String },
    telefono: { type: String, uppercase: true },
    usuario: { type: String, uppercase: true },
    contrasena: { type: String, uppercase: true },
    correo: { type: String, uppercase: true },
    calle: { type: String, uppercase: true },
    no_exterior: { type: String, uppercase: true },
    postal: { type: String, uppercase: true },
    pais: { type: String, uppercase: true },
    estado: { type: String, uppercase: true },
    municipio: { type: String, uppercase: true },
    ref_1: { type: String, uppercase: true },
    ref_2: { type: String, uppercase: true },
    opcion_1: { type: String, uppercase: true },
    opcion_2: { type: String, uppercase: true },
    razSoc: { type: String, uppercase: true },
    rfc_em: { type: String, uppercase: true },
    postal_em: { type: String, uppercase: true },
    estado_em: { type: String, uppercase: true },
    municipio_em: { type: String, uppercase: true },
    calle_em: { type: String, uppercase: true },
    exterior_em: { type: String, uppercase: true },
    correo_em: { type: String, uppercase: true },
    archivos    : { type: Array },
    codsede: { type: String, uppercase: true }
})
schemaSedesbs.method.setImgUrl = function setImgUrl (filename) {
    const {host,port} = appConfig
    this.imagencli = `${host}:${port}/public/${filname}`
}

module.exports = mongoose.model('Sedesbs', schemaSedesbs)