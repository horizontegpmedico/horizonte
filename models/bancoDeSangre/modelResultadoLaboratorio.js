const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaLaboratorio = new Schema({
    resultados: { type: mongoose.Types.ObjectId, ref:'LaboratorioBS' },
    socio:      { type: mongoose.Types.ObjectId, ref:'TipeyCruceBS' },
    metodo:     { type: Array },
    quimico:    { type: String, uppercase: true},
    tecnico:    { type: String, uppercase: true},
    cedula:     { type: String, uppercase: true},
    fecha:      { type: Date, default: Date.now() },
}) 


module.exports = mongoose.model('ResultadosLaboratorioBS', schemaLaboratorio)