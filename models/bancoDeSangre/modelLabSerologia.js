const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaLaboratorioSero = mongoose.Schema({
    serologia:   { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    estudios:   { type: Array},
    proceso:    { type: String, uppercase: true,  default: 'EN PROCESO'},
    quimico:    { type: String,uppercase: true},
    tecnico:    { type: String, uppercase: true},
    cedula:    { type: String, uppercase: true},
    clave_id_ce: { type: String, uppercase: true},
    clave_id_cp: { type: String, uppercase: true},
    clave_id_pl: { type: String, uppercase: true},
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('LaboratorioSerologiaBS', schemaLaboratorioSero)