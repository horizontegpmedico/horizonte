const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemacarritopedidos = mongoose.Schema({
    paciente:    { type: mongoose.Types.ObjectId, ref:'Paciente'},
    vendedor:    { type: String, trim: true, uppercase: true},
    fecha:       { type: Date, default: Date.now()},
    estudios:    { type: Array },
    metodo_pago: { type: String, uppercase: true },
    recibe:      { type: String, uppercase: true },
    totalCompra: {type: Number}
})

module.exports = mongoose.model('CarritopedidosBS', schemacarritopedidos)