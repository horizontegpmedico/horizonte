const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaDocumentos = mongoose.Schema({
    disponente:           { type: mongoose.Types.ObjectId, ref:'FlebotomiaBS' },
    receptor:             { type: mongoose.Types.ObjectId, ref: 'BancoSangreSocios'},
    documentosdisponente: { type: Array },
    documentosreceptor:   { type: Array },
    fecha:                { type: Date, default: Date.now() },
})
 

module.exports = mongoose.model('DocumentosBS', schemaDocumentos)