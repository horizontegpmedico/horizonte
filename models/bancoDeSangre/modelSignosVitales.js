const mongoose = require('mongoose');

let Schema = mongoose.Schema;
const schemaSignosvitales = mongoose.Schema({
    signosvitales:    { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    talla:            { type: Number },
    peso:             { type: Number },
    imc:              { type: Number },
    temp:             { type: Number },
    sistolica:        { type: Number },
    diastolica:       { type: Number },
    huellasMultiples: { type: String, uppercase: true },
    huellasRecientes: { type: String, uppercase: true },
    fecha:            { type: Date, default: Date.now() },
})


module.exports = mongoose.model('SignosVitalesBS', schemaSignosvitales)