const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaLaboratorio = new Schema({
    laboratorio:    { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    tiparycruzar:   { type: Schema.Types.ObjectId, ref:'BancoSangreSocios'},
    obtenidos:      { type: Array },
    pedido:         { type: Array },
    metodo:         { type: Array },
    quimico:        { type: String, uppercase: true },
    tecnico:        { type: String, uppercase: true },
    cedula:         { type: String, uppercase: true }, 
    proceso:        { type: String, uppercase: true,  default: 'EN PROCESO' },
    fecha:          { type: Date, default: Date.now() },
    seDesecha: { type: String, uppercase: true},
    motivoDesecho: { type: String, uppercase: true},
    procesoDesecha: { type: String, uppercase: true}
}) 


module.exports = mongoose.model('LaboratorioBS', schemaLaboratorio)