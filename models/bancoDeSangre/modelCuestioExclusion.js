const mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaCuestioExclusion = new Schema({
fechaDelDisponente : {type:Date, default: Date.now()},
cuestionarioautoexclusion:   { type: Schema.Types.ObjectId, ref:'BancoSangre'},
autoexclusion0:  { type: String, uppercase: true, required:[true,'La pregunta 1 es requerida']},
autoexclusion1:  { type: String, uppercase: true, required:[true,'La pregunta 2 es requerida']},
autoexclusion2:  { type: String, uppercase: true, required:[true,'La pregunta 3 es requerida']},
autoexclusion3:  { type: String, uppercase: true, required:[true,'La pregunta 4 es requerida']},
autoexclusion4:  { type: String, uppercase: true, required:[true,'La pregunta 5 es requerida']},
autoexclusion5:  { type: String, uppercase: true, required:[true,'La pregunta 6 es requerida']},
autoexclusion6:  { type: String, uppercase: true, required:[true,'La pregunta 7 es requerida']},
autoexclusion7:  { type: String, uppercase: true, required:[true,'La pregunta 8 es requerida']},
autoexclusion8:  { type: String, uppercase: true, required:[true,'La pregunta 9 es requerida']},
autoexclusion9:  { type: String, uppercase: true, required:[true,'La pregunta 10 es requerida']},
autoexclusion10: { type: String, uppercase: true, required:[true,'La pregunta 11 es requerida']},
autoexclusion11: { type: String, uppercase: true, required:[true,'La pregunta 12 es requerida']},
autoexclusion12: { type: String, uppercase: true, required:[true,'La pregunta 13 es requerida']},
})
module.exports = mongoose.model('modelCuestioExclusion',schemaCuestioExclusion) 