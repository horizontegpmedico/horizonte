const mongoose = require('mongoose');
let Schema = mongoose.Schema;
let schemaBancoSangreSocios = new Schema({
    paciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    receptor: { type: Schema.Types.ObjectId, ref: 'DocumentosBS' },
    pedido: { type: Schema.Types.ObjectId, ref: 'pedidosSedesBanco' },
    tiparycruzar: [{ type: Schema.Types.ObjectId, ref: 'LaboratorioBS' }],
    proceso_censur: { type: Schema.Types.ObjectId, ref: 'BancoSangreSociosProcesos' },
    ventaCensur: [{ type: Schema.Types.ObjectId, ref: 'carritoCensur' }],
    tipo_producto: { type: String, uppercase: true },
    fecha: { type: Date, default: Date.now() },
    reaccionestransfucionales: { type: Array },
    estatus: { type: String, uppercase: true, default: 'RECEPTOR'},
    huboReaccionAdversa    : { type: String, uppercase: true, trim: true,  default: 'NO'}
})


module.exports = mongoose.model('BancoSangreSocios', schemaBancoSangreSocios)