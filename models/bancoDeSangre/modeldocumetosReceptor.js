const mongoose = require('mongoose');

let Schema = mongoose.Schema; 

let schemaBancoSangre = new Schema({ 
    nombreDonante: { type: String, uppercase: true, trim: true},
    numeroProgresivoDonante:{ type: String, uppercase: true, trim: true},
    hemocomponente:{ type: String, uppercase: true, trim: true},
    grupoRh:{ type: String, uppercase: true, trim: true},
    fechaExtraccion:{ type: String, uppercase: true, trim: true},
    fechaCaducidad:{ type: String, uppercase: true, trim: true},
    volumenUnidad:{ type: String, uppercase: true, trim: true},
    hbHto:{ type: String, uppercase: true, trim: true},
    fechaSalida:{ type: String, uppercase: true, trim: true},
    horaSalida:{ type: String, uppercase: true, trim: true},
    unidadReceptora:{ type: String, uppercase: true, trim: true},
    domicilioUnidadReceptora:{ type: String, uppercase: true, trim: true},
    nombrePreparoUnidades:{ type: String, uppercase: true, trim: true},
    nombreQuienEnviaUnidades:{ type: String, uppercase: true, trim: true},
    fecha:      { type: Date, default: Date.now() },
    idCensur: { type: Schema.Types.ObjectId, ref: 'BancoSangreSocios' },
   documentos: { type: Array }
})


module.exports = mongoose.model('documentosReaccionesReceptor', schemaBancoSangre)