const mongoose = require('mongoose');

let Schema = mongoose.Schema; 

let schemaBancoSangre = new Schema({ 
    paciente:   { type: Schema.Types.ObjectId, ref:'Paciente' },
    cuestionarioautoexclusion: {type: Schema.Types.ObjectId, ref:'modelCuestioExclusion'},
    signosvitales: {type: Schema.Types.ObjectId, ref:'SignosVitalesBS'},
    historiaclinica: { type: Schema.Types.ObjectId, ref:'HistoriaClinicaBS'},
    hojaevolucion: {type: Schema.Types.ObjectId, ref:'hojaEvolucionBS'},
    reaccionesderivadas: {type: Schema.Types.ObjectId, ref:'ReaccionesDerivadasBS'},
    flebotomia: {type: Schema.Types.ObjectId, ref:'FlebotomiaBS'},
    laboratorio: [ {type: Schema.Types.ObjectId, ref:'LaboratorioBS'} ],
    almacen:   [{ type: mongoose.Types.ObjectId, ref:'productosAlmacenStock' }],
    clave_id: {type:String, uppercase: true},   
    proceso: {type: String, uppercase: true},
    estatus: { type: String, uppercase: true},
    motivo: {type: String, uppercase: true},
    tiempo_rechazo: {type: String},
    fecha_termino:  { type: Date },
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('BancoSangre', schemaBancoSangre)