const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemapedidosbanco = mongoose.Schema({
    pedido:      { type: mongoose.Types.ObjectId, ref:'BancoSangreSocios'},
    vendedor:    { type: String, trim: true},
    fecha:       { type: Date, default: Date.now()},
    estudios:    { type: Array },
    sede_banco:  { type: String, trim: true, uppercase: true },
    totalCompra: {type: Number},
    status:      {type:String, trim: true, uppercase: true, default: 'ordenar pedido' }
})

module.exports = mongoose.model('pedidosSedesBanco', schemapedidosbanco)