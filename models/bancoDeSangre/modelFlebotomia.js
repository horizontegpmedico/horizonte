const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaFlebotomia = mongoose.Schema({
    flebotomia:   { type: Schema.Types.ObjectId, ref:'BancoSangre' },
    disponente:   {type: Schema.Types.ObjectId, ref: 'DocumentosBS' },
    tipo_de_sangreado: {type:String, uppercase: true},
    tipo_de_donador: {type:String, uppercase: true},
    de_repeticion: {type:String, uppercase: true},
    notas: {type: String, uppercase: true},
    volumen: { type: String, uppercase: true},
    tiempo : { type: Number },
    fecha:      { type: Date, default: Date.now() }, 
});

module.exports = mongoose.model('FlebotomiaBS', schemaFlebotomia)