const mongoose = require('mongoose'); 
let Schema = mongoose.Schema;
let schemaBancoSangreSociosProcesos = new Schema({
    proceso_censur:     { type: Schema.Types.ObjectId, ref:'BancoSangreSocios' },
    documentos_medicos: {type: String, default:'DOCUMENTOS MÉDICOS' },
    url_doc:            {type: String, default:'/documentos/receptor' },
    estado_doc:         {type: String, default:'EN ESPERA' },
    tipar_cruzar:       {type: String, default:'TIPAR Y CRUZAR' },
    url_tipar:          {type: String, default:'/tiparcruza' },
    estado_tipar:       {type: String, default:'EN ESPERA' },
    consultar_historico:{type: String, default:'CONSULTA DE HISTÓRICO' },
    url_historico:      {type: String, default:'/historico' },
    estado_historico:   {type: String, default:'EN ESPERA' },
    entregar_productos: {type: String, default:'ENTREGA DE PRODUCTOS' },
    url_productos:      {type: String, default:'/productos/receptor' },
    estado_productos:   {type: String, default:'EN ESPERA' },
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('BancoSangreSociosProcesos', schemaBancoSangreSociosProcesos)