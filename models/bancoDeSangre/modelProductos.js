const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaProductos = mongoose.Schema({
    nombre:   { type: String, uppercase: true},
    precio:   { type: Number},
    clave:    { type: String, uppercase: true},
    fecha:    { type: Date, default: Date.now() },
})


module.exports = mongoose.model('ProductosBS', schemaProductos)