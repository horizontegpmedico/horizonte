const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const schemaTipeyCruce = mongoose.Schema({
    tipeycruce: { type: mongoose.Types.ObjectId, ref:'BancoSangreSocios' },
    socio:      { type: mongoose.Types.ObjectId, ref:'ResultadosLaboratorioBS' },
    obtenidos:  { type: Array },
    pedido:     { type: Array },
    proceso:    { type: String, uppercase: true,  default: 'EN PROCESO'},
    fecha:      { type: Date, default: Date.now() },
})


module.exports = mongoose.model('TipeyCruceBS', schemaTipeyCruce)