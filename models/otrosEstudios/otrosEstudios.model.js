const { Schema, model } = require('mongoose');

const schemaOtrosEstudios = Schema ({

    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' },
    fechaEstudios: { type: Date },
    fechaCargaEsudio: { type: String },
    vendedor: { type: Schema.Types.ObjectId, ref: 'ModelPersonal' },
    imageUrl: { type: Array },
    tipoEstudio: { type: String }

})

module.exports = model ('otrosEstudios', schemaOtrosEstudios)