const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let newEstudio = new Schema({
    nombre: { type: String },
    valoresDeReferencia: { type: String },
    tipoExamen: { type: String }

});


module.exports = mongoose.model('estudiosUnicos', newEstudio);