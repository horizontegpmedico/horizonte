const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let SchemaExamenesLaboratorio = new Schema({

    fechaDePedidoDeLosExamenes = { type: Date },
    paciente: { type: Schema.Types.ObjectId, ref: 'pacientes' },
    DoctorQuePidio: { type: String },
    diagnostico: { type: String },
    tratamiento: { type: String },
    estudios: { type: Schema.Types.Array },
    total: { type: Number },
    acumulado: { type: Number }
});


module.exports = mongoose.model('examenesLaboratorio', SchemaExamenesLaboratorio);