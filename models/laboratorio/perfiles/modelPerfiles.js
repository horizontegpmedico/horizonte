const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let newSchemaPerfilesLab = new Schema({

    ESTUDIOS: { type: Array },
    MUESTRA: { type: String },
    ENTREGA: { type: String },
    METODO: { type: String },
    PUBLICO: { type: String },
    URGENCIA_PUBLICO: { type: String },
    MEMBRESIA: { type: String },
    URGENCIA_MEMBRESIA: { type: String },
    PROMOCION: { type: String },
    NOCTURNO: { type: String },
    H6: { type: String },
    B5: { type: String }

});


module.exports = mongoose.model('perfilesLaboratorio', newSchemaPerfilesLab)