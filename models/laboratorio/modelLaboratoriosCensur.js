const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaLaboratoriosCensur = new Schema({
    nombre:                  { type: String, uppercase: true },
    idEstudio:               { type: Schema.Types.ObjectId, ref: 'modeloservicios' },
    tipo_de_examen:          { type: String, uppercase: true },
    tipo_de_muestra:         { type: String, uppercase: true, default: '' },
    metodo:                  { type: String, uppercase: true },
    valoresDeReferencia:     { type: Array }, 
    observaciones:           { type: String, uppercase: true },
    fecha:                   { type: Date, default: Date.now() },
})


module.exports = mongoose.model('laboratorioscensur', schemaLaboratoriosCensur)