const mongoose = require('mongoose');
const { schema } = require('./enfermeriaRecepcio');

let Schema = mongoose.Schema;

let ChatSchemas = new Schema({

    role1: { type: String },
    role2: { type: String },
    message: { type: String },
    fecha:{
        type:Date,
        default:Date.now
    }
});

module.exports = mongoose.model('MensajesChat', ChatSchemas);