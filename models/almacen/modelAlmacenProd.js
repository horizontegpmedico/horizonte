const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaAlmacen = mongoose.Schema({
    almacen:   { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    productosstock:   { type: mongoose.Types.ObjectId, ref:'productosAlmacenStock' },
    fecha:         { type: Date, default: Date.now() },
});

module.exports = mongoose.model('AlmacenstockBS', schemaAlmacen)