const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaAlmacen = mongoose.Schema({
    bancodesangre:   { type: Schema.Types.ObjectId, ref:'BancoSangre' },
    clave_id:       { type:String, uppercase: true },
    nombre:        { type:String, uppercase: true },
    cantidad:      { type:String, uppercase: true },
    caducidad:     { type:String, uppercase: true },
    fecha:         { type: Date, default: Date.now() },
    precioVenta: { type: Number } 
})


module.exports = mongoose.model('MiniAlmacenBS', schemaAlmacen)