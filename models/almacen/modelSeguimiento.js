const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let SchemaCreateSeguimiento = new Schema({
    dptoSolicitante: { type: String },
    dptoSolicitado: { type: String },
    nombre: { type: String },
    // id: { type: Schema.Types.ObjectId },
    categoria: { type: String },
    servicios: { type: String },
    solicitante: { type: String },
    estado: { type: String },
    descripcion: { type: String },
});

module.exports = mongoose.model('createSeguimientos', SchemaCreateSeguimiento);