// PENDIENTE DE SABER QUE VA A GUARDAR PUEDE VARIAR
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const schemaAlmacen = mongoose.Schema({
    almacen:   { type: mongoose.Types.ObjectId, ref:'BancoSangre' },
    procedencia:   {type:String, uppercase: true},
    producto:      {type:String, uppercase: true},
    caducidad:     {type:String, uppercase: true},
    fecha:         { type: Date, default: Date.now() },
});

module.exports = mongoose.model('AlmacenBS', schemaAlmacen)