const { Schema, model } = require('mongoose');

//tabla de productos y de materiales
// a esta tabla la llamo admin ya que se usa para tener un cntrol administrativo de los productos, es unicamnete para la FI del producto
const schemaAlmacen = Schema({
    
    nombre:        { type:String, uppercase: true },
    tipo_producto:  { type:String, uppercase: true },
    nombre_comercial:  { type:String, uppercase: true },
    descripcion:        { type:String, uppercase: true },
    proveedor:     { type: Array },
    estado:        { type:String, uppercase: true, default: 'ACTIVO' },
    medicamento:   { type:Boolean, default: false },
    fecha:         { type: Date, default: Date.now() },
    laboratorio : { type: String, uppercase: true},
    idProducto: { type: String, uppercase: true},
    productoMatriales: { type: String, uppercase: true, default: 'PRODUCTO'},
    precio:{ type: Number},
    presentacion:{type:String, uppercase: true},
    contenidoFrasco:{type:String, uppercase: true},
    viaAdministracion:{type:String, uppercase: true},
    fechaCaducidad:{type:Date},
    lote:{type:Number},
    cantidadMin: { type: Number },
});



module.exports = model('almacenProductos', schemaAlmacen);