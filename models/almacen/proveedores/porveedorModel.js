const mongoose = require('mongoose');

const schemaProveedores = mongoose.Schema({
    
    nombreEmpresa: { type: String, uppercase: true},
    idProveedor:  { type:String,  uppercase: true},
    razonSocial: { type: String,  uppercase: true },
    domicilio : {type: String, uppercase: true },
    contacto: { type: String,  uppercase: true }, 
    correo: { type: String },
    rfcEmpresa: { type: String,  uppercase: true },
    diasDistribucion: { type: String },
    status: { type: String, default: 'ACTIVO' , uppercase: true },
    descripcion: { type: String , uppercase: true },
});

module.exports = mongoose.model( 'proveedoresBS', schemaProveedores)