const { Schema, model } = require('mongoose');

const schemaService = new Schema({
    
    nomenclatura: { type: String, uppercase: true},
    nombreServicio: { type: String, uppercase: true},
});

module.exports = model('serviciosFi',  schemaService);