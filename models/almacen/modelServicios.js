const mongoose = require('mongoose');

const schemaServices = mongoose.Schema({

    nombre: { type: String, uppercase: true },
    idServicio: { type: String, uppercase: true },
    tipoServicio: { type: String, uppercase: true },
    costo: { type: Number},
    categoria: {  type: String, uppercase: true },
    profesionalAREalizar: { type: String, uppercase: true },
    horario: { type: Array },
    status: { type: String, uppercase: true, default: 'ACTIVADO'},
    descripcion: { type: String, uppercase: true},
    participante: { type: String, uppercase: true},
    productosOMateriales: {  type: Array}

});

module.exports = mongoose.model('serviciosCensur', schemaServices)