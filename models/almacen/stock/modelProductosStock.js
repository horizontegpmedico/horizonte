const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schemaAlmacen = mongoose.Schema({

    productosFI: { type: Schema.Types.ObjectId, ref: 'almacenProductos' },
    productosstock: { type: Schema.Types.ObjectId, ref: 'AlmacenstockBS' },
    fecha_caducidad: { type: Date },
    lote: { type: String, trim : true },
    proveedor: { type: String },
    factura: { type: String, trim: true },
    lote_unitario: { type: String, trim: true },
    codeQr_img: { type: String },
    destino: { type: String, trim: true },
    status: { type: String, default: 'STOCK' },
    downloaded: { type: Boolean, default: false },
    costoReal: { type: Number },
    precioVenta: { type: Number },
    seDesecha: {type: String, trim:true, uppercase: true, default: ''} ,
    motivoDesecho: {type: String, trim:true, uppercase: true, default: ''} ,
    procesoDesecha:{type: String, trim:true, uppercase: true, default: ''} 
}, { timestamps: true});

module.exports = mongoose.model('productosAlmacenStock', schemaAlmacen)