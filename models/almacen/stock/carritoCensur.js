const { Schema, model } = require('mongoose');

const schemaCarritoCensur = Schema({
    idBanco: { type: Schema.Types.ObjectId, ref: 'BancoSangre' },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente'},
    product: { type: Array },
    total: { type: Number },
    fechaDeCreacion: { type: Date },
    TarjetaCredito: { type: Boolean },
    TarjetaDebito: { type: Boolean },
    Efectivo: { type: Boolean },
    Transferencia: { type: Boolean },
    horaDeCreacion: { type: String },
    montoCredito: { type: Number },
    montoDebito: { type: Number },
    montoEfectivo: { type: Number },
    montoTranseferencia: { type: Number },
    quienRecibe: { type: String },
    sede: { type: String },
    vendedor: { type: String },
    folio: { type: Number }
});

module.exports = model('carritoCensur', schemaCarritoCensur);