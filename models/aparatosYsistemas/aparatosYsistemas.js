'use strict'
const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let SchemaAparatosYsistemas = new Schema({
    motivoDeConsulta: { type: String },
    evolucionDelPadecimiento: { type: String },
    medicoTrante: { type: String },
    diagnostico: { type: String },
    plan: { type: String },
    respiratorioCardiovascular: { type: String },
    digestivo: { type: String },
    endocrino: { type: String },
    musculoEsqueletico: { type: String },
    genitourinario: { type: String },
    hematopoyeticoLinfatico: { type: String },
    pielAnexos: { type: String },
    neurologicoPsiquiatricos: { type: String },
    piel: { type: String },
    cabezaCuello: { type: String },
    torax: { type: String },
    abdomen: { type: String },
    genitales: { type: String },
    extremidades: { type: String },
    sistemaNervioso: { type: String }
});

module.exports = mongoose.model('aparatosYsistemas', SchemaAparatosYsistemas)