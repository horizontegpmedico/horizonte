const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let huespedNuevoSchema = new Schema({
    nombre: { type: String, uppercase: true, trim: true, required: [true, 'El nombre del paciente es requerido'] },
    apellidoPaterno: { type: String, uppercase: true, trim: true, required: [true, 'El apellido paterno es requerido'] },
    apellidoMaterno: { type: String, uppercase: true  ,trim: true},
    genero: { type: String, uppercase: true },
    edad: { type: Number },
    telefono: { type: Number },
    correoHuesped: { type: String, trim: true },
    domicilio:{ type: String, uppercase: true },
    claveIne:{ type: String, uppercase: true },
    status: { type: String, uppercase: true, default: "Activo" },
}); 

module.exports = mongoose.model('Huesped', huespedNuevoSchema);