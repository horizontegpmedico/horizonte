const mongoose = require('mongoose');


let Schema = mongoose.Schema;


let SchemaAntecedenteGinceo = new Schema({

    embarazoActual: { type: String },
    embarazoAltoRiesgo: { type: String },
    administracionHierroAcidoF: { type: String },
    fechaAdmonHierroAcidoF: { type: String },
    fechaAdmonHierro: { type: String },
    fechaAdmonAcido: { type: String },
    metodoAntiConceptivoPostParto: { type: String },
    incioVidaSexualActiva: { type: String },
    fechaUltimoParto: { type: String },
    crecimientoBellosPubicos: { type: String },
    tipoMetodoPlanFamiliar: { type: String },
    gestas: { type: String },
    partos: { type: String },
    cesareas: { type: String },
    abortos: { type: String },
    ultimoPapanicolao: { type: String },
    ultimaColposcopia: { type: String },
    ultimaRevisionMamaria: { type: String },
    bebesNacidos: { type: String },
    bebesMuertos: { type: String },
    primeraMenstruacion: { type: String },
    crecimientoDePechos: { type: String },
    primerEmbarazo: { type: String },
    fechaPrimerEmbarazo: { type: String },
    idPaciente: { type: String }
});


module.exports = mongoose.model('antecednentesGineco', SchemaAntecedenteGinceo);