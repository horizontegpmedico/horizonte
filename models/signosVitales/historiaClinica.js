const mongoose = require('mongoose');
// esta es la parte de la historia clinica
let Schema = mongoose.Schema;

let schemaHojaEvolucion = new Schema({

    idPaciente: { type: mongoose.Types.ObjectId },
    diagnostico: { type: Array },
    fecha: { type: String },
    hora: { type: String },
    motivoDeConsulta: { type: String },
    evolucionDelPadecimiento: { type: String },
    medicoTrante: { type: String },
    plan: { type: String },
    // exploracion fisica
    piel: { type: String },
    cabezaCuello: { type: String },
    torax: { type: String },
    abdomen: { type: String },
    genitales: { type: String },
    extremidades: { type: String },
    sistemaNervioso: { type: String },

    // interrogacion por aparatos y sistemas
    respiratorioCardiovascular: { type: String },
    digestivo: { type: String },
    endocrino: "",
    musculoEsqueletico: { type: String },
    genitourinario: { type: String },
    hematopoyeticoLinfatico: { type: String },
    pielAnexos: { type: String },
    neurologicoPsiquiatricos: { type: String }
});


module.exports = mongoose.model('historiaClinica', schemaHojaEvolucion);