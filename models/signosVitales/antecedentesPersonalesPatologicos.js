const mongoose = require('mongoose');


let Schema = mongoose.Schema;


let newSchemaAntecedentesPersonalesPatologicos = new Schema({
    idPaciente: { type: mongoose.Types.ObjectId },
    accidentes: { type: String },
    accidentesEsp: { type: String },
    antecedentesQuirurgicos: { type: String },
    antecedentesQuirurgicosEsp: { type: String },
    enfermedadesInfancia: { type: String },
    enfermedadesInfanciaSecuelas: { type: String },
    fracturasEsp: { type: String },
    fractutas: { type: String },
    hospitalizacionesPrevias: { type: String },
    hospitalizacionesPreviasEsp: { type: String },
    medicamentosActuales: { type: String },
    medicamentosActualesEsp: { type: String },
    otrasEnfermedades: { type: String },
    otrasEnfermedadesEsp: { type: String },
    transfucionesPrevias: { type: String },
    transfucionesPreviasEsp: { type: String },
});


module.exports = mongoose.model('antecedentesPersonalesPatologicos', newSchemaAntecedentesPersonalesPatologicos);