const mongoose = require('mongoose');


let Schema = mongoose.Schema;


let SchemaNutricionNinos = new Schema({

    alimentarSanamenteNinosMesUno: { type: String },
    alimentarSanamenteNinosMesDos: { type: String },
    alimentarSanamenteNinosMesTres: { type: String },
    alimentarSanamenteNinosMesCuatro: { type: String },
    alimentarSanamenteNinosMesCinco: { type: String },
    alimentarSanamenteNinosMesSeis: { type: String },
    desparacitacionIntestinalNinosMesUno: { type: String },
    desparacitacionIntestinalNinosMesDos: { type: String },
    desparacitacionIntestinalNinosMesTres: { type: String },
    desparacitacionIntestinalNinosMesCuatro: { type: String },
    desparacitacionIntestinalNinosMesCinco: { type: String },
    desparacitacionIntestinalNinosMesSeis: { type: String },
    vitaminaANinosMesUno: { type: String },
    vitaminaANinosMesDos: { type: String },
    vitaminaANinosMesTres: { type: String },
    vitaminaANinosMesCuatro: { type: String },
    vitaminaANinosMesCinco: { type: String },
    vitaminaANinosMesSeis: { type: String },
    hierroNCBPNinosMesUno: { type: String },
    hierroNCBPNinosMesDos: { type: String },
    hierroNCBPNinosMesTres: { type: String },
    hierroNCBPNinosMesCuatro: { type: String },
    hierroNCBPNinosMesCinco: { type: String },
    hierroNCBPNinosMesSeis: { type: String },
    hierroPPNinosMesUno: { type: String },
    hierroPPNinosMesDos: { type: String },
    hierroPPNinosMesTres: { type: String },
    hierroPPNinosMesCuatro: { type: String },
    hierroPPNinosMesCinco: { type: String },
    hierroPPNinosMesSeis: { type: String },
    otrosNinosMesUno: { type: String },
    otrosNinosMesDos: { type: String },
    otrosNinosMesTres: { type: String },
    otrosNinosMesCuatro: { type: String },
    otrosNinosMesCinco: { type: String },
    otrosNinosMesSeis: { type: String },
    notasMesUno: { type: String },
    notasMesDos: { type: String },
    notasMesTres: { type: String },
    notasMesCuatro: { type: String },
    notasMesCinco: { type: String },
    notasMesSeis: { type: String },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' }


});

module.exports = mongoose.model('nutricionNinos', SchemaNutricionNinos);