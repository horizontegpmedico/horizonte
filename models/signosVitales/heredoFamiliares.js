const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let modelHeredoFamiliares = new Schema({

    diabetes: { type: String },
    cancer: { type: String },
    cardiopatias: { type: String },
    malformaciones: { type: String },
    hipertension: { type: String },
    hipertensionTipo: { type: String },
    nefropatias: { type: String },
    nefropatiasTipo: { type: String },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' }

});


module.exports = mongoose.model('antecedentesHeredoFamiliares', modelHeredoFamiliares);