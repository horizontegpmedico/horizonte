'use strict'

const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let SchemaAntecedentesGinecoObstetricosHistoria = new Schema({
    menarcaAniosEdad: { type: String },
    fechaUltimaMenstruacion: { type: String },
    dismenorrea: { type: String },
    ciclosRegulares: { type: String },
    polimenorrea: { type: String },
    ivsaAnios: { type: String },
    fechaUltimaCitologia: { type: String },
    ritmo: { type: String },
    hipermenorrea: { type: String },
    numParejasSexuales: { type: String },
    resultado: { type: String },
    gestas: { type: String },
    partos: { type: String },
    abortos: { type: String },
    cesareas: { type: String },
    idPaciente: { type: String }
});

module.exports = mongoose.model('antecedentesGinecoObstetricosHistoria', SchemaAntecedentesGinecoObstetricosHistoria)