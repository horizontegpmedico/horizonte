const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let modelAntecedentesNoPatologicos = new Schema({

    tabaquismoPorDia: { type: String },
    aniosconsumo: { type: String },
    exFumadorPasivo: { type: String },
    copasPorDia: { type: String },
    aniosDeconsumoAlcohol: { type: String },
    exAlcoholicoUOcasional: { type: String },
    alergias: { type: String },
    tipoAlergias: { type: String },
    tipoSanguineo: { type: String },
    desconoceTipoSanguineo: { type: String },
    drogadiccionTipo: { type: String },
    aniosConsumoDrogas: { type: String },
    exDrogadicto: { type: String },
    alimentacionAdecuada: { type: String },
    viviendaConServiciosBasicos: { type: String },
    otrosAntecedentesNoPatologicos: { type: String },
    idPaciente: { type: Schema.Types.ObjectId, ref: 'Paciente' }
});


module.exports = mongoose.model('antecedentesPersonalesNoPatologicos', modelAntecedentesNoPatologicos)