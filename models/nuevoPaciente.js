const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let pacienteNuevoSchema = new Schema({
    //datos generales
    nombrePaciente: { type: String, uppercase: true, trim: true, required: [true, 'El nombre del paciente es requerido'] },
    apellidoPaterno: { type: String, uppercase: true, trim: true, required: [true, 'El apellido paterno es requerido'] },
    apellidoMaterno: { type: String, uppercase: true  ,trim: true},
    fechaNacimientoPaciente: { type: Date },
    paisNacimiento: { type: String, uppercase: true },
    entidadNacimiento: { type: String, uppercase: true },
    municipioPaciente: { type: String, uppercase: true },
    localidadPaciente: { type: String, uppercase: true, trim : true },
    estadoCivilPaciente: { type: String, uppercase: true },
    genero: { type: String, uppercase: true },
    edad: { type: Number },
    curp: { type: String, uppercase: true },
    disponente: { type:String, uppercase:true},
    receptor: { type: String, uppercase: true },
    sede: { type: String, uppercase: true, default: 'TLYC01' },
    telefono: { type: Number },
    numeroExpediente: { type: String, uppercase: true },
    correoPaciente: { type: String, trim: true },
    nombreCompletoPaciente: { type: String, trim: true, uppercase: true},
    //direccion
    cpPaciente: { type: Number },
    estadoPaciente: { type: String, uppercase: true, trim: true },
    paisPaciente: { type: String, uppercase: true, trim: true },
    callePaciente: { type: String, uppercase: true, trim: true },
    colonia: { type: String, uppercase: true },
    numeroPaciente: { type: String, uppercase: true },
    numeroIntPaciente: { type: String, uppercase: true },
    status: { type: String, uppercase: true, default: "Activo" },
    referenciaPaciente: { type: String, uppercase: true },
    referenciaPaciente2: { type: String, uppercase: true },
    //datos de facturacion
    razonSocial1: { type: String, uppercase: true },
    razonSocial1RFC: { type: String, uppercase: true },
    correoRazonSocial1: { type: String, uppercase: true },
    razonSocial1Calle: { type: String, uppercase: true },
    razonSocial1NumInt: { type: String, uppercase: true },
    razonSocial1NumExt: { type: String, uppercase: true },
    razonSocialPais: { type: String, uppercase: true },
    cpRazonSocial: { type: String, uppercase: true },
    razonSocial1Estado: { type: String, uppercase: true },
    razonSocial1Municipio: { type: String, uppercase: true },
    razonSocial1colonia: { type: String, uppercase: true },
    razonsocial1Telefono: { type: String, uppercase: true },

    razonSocial2: { type: String, uppercase: true },
    razoncocial2RFC: { type: String, uppercase: true },
    cpRazonSocial2: { type: String, uppercase: true },
    razonSocial2Estado: { type: String, uppercase: true },
    razonSocial2Municipio: { type: String, uppercase: true },
    razonSocial2Estado: { type: String, uppercase: true },
    razonSocial2Calle: { type: String, uppercase: true },
    correoRazonSocial2: { type: String, uppercase: true },
    razonSocial2colonia: { type: String, uppercase: true },
    religion: { type: String, uppercase: true },

    contactoEmergencia1Nombre: { type: String, uppercase: true },
    contactoEmergencia1ApellidoPaterno: { type: String, uppercase: true },
    contactoEmergencia1ApellidoMaterno: { type: String, uppercase: true },
    contactoEmergencia1Edad: { type: String, uppercase: true },
    contactoEmergencia1Telefono: { type: String, uppercase: true },
    contactoEmergencia1Parentesco: { type: String, uppercase: true },
    contactoEmergencia1Genero: { type: String, uppercase: true },
    contactoEmergencia1EstadoCivil: { type: String, uppercase: true },
    contactoEmergencia1Correo: { type: String, uppercase: true },
    contactoEmergencia1Curp: { type: String, uppercase: true },


    contactoEmergencia2Nombre: { type: String, uppercase: true },
    contactoEmergencia2ApellidoPaterno: { type: String, uppercase: true },
    contactoEmergencia2ApellidoMaterno: { type: String, uppercase: true },
    contactoEmergencia2Curp: { type: String, uppercase: true },
    contactoEmergencia2Edad: { type: String, uppercase: true },
    contactoEmergencia2Telefono: { type: String, uppercase: true },
    contactoEmergencia2EstadoCivil: { type: String, uppercase: true },
    contactoEmergencia2Genero: { type: String, uppercase: true },
    contactoEmergencia2Correo: { type: String, uppercase: true },
    contactoEmergencia2Parentesco: { type: String, uppercase: true },

    tipoDeSangre: { type: String, uppercase: true, trim: true},
    consultas: { type: Number, default: 0 },
    fechaRegistro: { type: Date, default: Date.now() },



    paquetes: [{ type: Schema.Types.ObjectId, ref: 'paquetes' }],
    // lugar de inscripcion
    nomenclaturaRegistro: { type: String, uppercase: true, default: 'TLYC' },
    membresiaActiva: { type: Boolean, default: false },
    password: { type: String},
    // antecedentes del paciente 
    antecedente: { type: Schema.Types.ObjectId, ref: 'antecedentes' },
    paquetesPacientes: [{ type: Schema.Types.ObjectId, ref: 'paquetesPacientes' }],
    antecedentesGinecoobstetricos: { type: String, uppercase: true },
    esquemaVacunacion: { type: Schema.Types.ObjectId, ref: 'vacunacionNinos' },
    antecedentesPersonalesNoPatologicos: { type: Schema.Types.ObjectId, ref: 'antecedentesPersonalesNoPatologicos' },
    antecedentesHeredoFamiliares: { type: Schema.Types.ObjectId, ref: 'antecedentesHeredoFamiliares' },
    medicinaPreventiva: { type: Schema.Types.ObjectId, ref: 'medicinaPreventiva' },
    historiaClinica: { type: Schema.Types.ObjectId, ref: 'historiaClinica' },
    familia: { type: Array },
    // referencia al paquete que se tiene
    // referencia a las visitas y los pagos
    // Banco de sangre
    lugardeorigen: {type: String, default: 'Disponente Nuevo'},
    modelBancoSangre: [{type: Schema.Types.ObjectId, ref: 'BancoSangre'}],
    censur: [{type: Schema.Types.ObjectId, ref:'BancoSangreTercero'}],
    personalRegistro: {type: String, uppercase: true}
}, { timestamps: true}); 



module.exports = mongoose.model('Paciente', pacienteNuevoSchema);