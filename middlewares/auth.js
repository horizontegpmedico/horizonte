
const jwt = require('jsonwebtoken');




exports.verificaToken = function(req, res) {

    const {token} = req.params;

    jwt.verify(token, process.env.SEED, (error, decode) => {

        if (error) {
            return res.status(401).json({
                ok: false,
                message: 'Token invalido',
                errors: error
            })
        }   

        req.usuario = decode.nombre;
        req.role = decode.role;
        req._id = decode.id;
        return true;

    })


}