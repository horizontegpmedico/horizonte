const sortByDate = (a, b ) => {
    if (a.fecha_caducidad < b.fecha_caducidad) {
        return 1;
    }
    if (a.fecha_caducidad > b.fecha_caducidad) {
        return -1;
    }
    return 0;
}
module.exports = sortByDate