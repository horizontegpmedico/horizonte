const serveResp = (data, error, message, resp) => {

    if (data != null) {
        return resp.status(200)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(500)
            .json({
                ok: false,
                message,
                error
            });
    }

}

const responseServeeStatus = (data, error, message = "", status = 200 , resp) => {
    
    if (data != null) {
        return resp.status(status)
            .json({
                ok: true,
                message,
                data
            })
    } else if (error != null) {
        return resp.status(status)
            .json({
                ok: false,
                message,
                error
            });
    }
}

module.exports =  serveResp