const Chat = require('../models/chats/modelcahts');
const { Usuaurios } = require('./usuarios');
const { Notification } = require('./notification');
const notificacionesChat = new Notification();
let usuariosConectados = {};
let usuariosenLinea = [];
let room = {};
var rooms = [];

const usuarios = new Usuaurios();

module.exports = io => {



    io.on('connection', (client) => {
        //conectar a los sockets
        client.on('usuarioConectado', (data) => {
            //console.log(data); 
            client.room = data._id;
            usuariosConectados[data._id] = { id: client.id, data };
            usuarios.agregarPersonas(client.id, data.Id, data.role, data.nombre);
            this.usuariosenLinea = usuarios.getPersonas();
            //console.log(this.usuariosenLinea); 
            io.emit('usuarioEnLinea', this.usuariosenLinea);

        });
        //mensajes
        client.on('entrarChatPrivado', async(data) => {
            /* console.log(data); */
            if (data.role1 != data.role2) {

                client.username = data.role1
                room[data.role1] = { id: client.id }
                /* console.log(room); */
                if (!rooms.includes(data.room)) {

                    client.room = data.room
                    rooms.push(client.room)
                    client.join(client.room)
                   // console.log("diferentes")
                } else {
                   // console.log("entro aqui")
                    client.room = data.room
                    client.join(client.room);
                    /*                     var mensaje = await Chat.find({
                                             $and: [
                                                 { $or: [{ role1: data.role1 }, { role1: data.role2 }] },
                                                 { $or: [{ role2: data.role1 }, { role1: data.role2 }] }
                                             ]
                                         })
                                         io.to(client.id).emit('crearMensaje', mensaje)
                                         io.to(data.room).emit('crearMensaje', mensaje) */
                }
                var MensajesViejos = await Chat.find({
                    $and: [
                        { $or: [{ role1: data.role1 }, { role1: data.role2 }] },
                        { $or: [{ role2: data.role1 }, { role2: data.role2 }] }
                    ]
                })
                /* console.log(MensajesViejos); */
                io.to(client.id).emit('crearMensaje', MensajesViejos)
            }

        });

        client.on('enviarMensajePrivado', async(data) => {

            /* console.log(data); */
            var newChat = new Chat({
                role1: data.role1,
                role2: data.role2,
                message: data.mensaje
            });
            await newChat.save()
            var mensaje = await Chat.find({
                    $and: [
                            { $or: [{ role1: data.role1 }, { role1: data.role2 }] },
                            { $or: [{ role2: data.role1 }, { role2: data.role2 }] }
                        ]
                        //}).limit(1).sort({$natural: -1})
                })
                //io.in(data.room).emit('crearMensaje',{from:'enviarMensajePrivado',data: mensaje})
            io.in(data.room).emit('crearMensaje', mensaje);

            const mesaagePush = data.role1 + ":" + data.mensaje;

                console.log(data)
            var message = {
                app_id: "f1649780-711b-4045-ad71-af4664b89496",
                contents: { "es": mesaagePush, "en": mesaagePush },
                filters: [
                    {"field": "tag", "key": "user", "relation": "=", "value": data.role2}
                ]
            };


            notificacionesChat.sendNotification(message);

            /* 
            await newChat
                .save()
                .then(data => {
                    console.log(data);
                })
                .catch(
                    error => {
                        console.log(error);
                    }
                )

            mensaje = await Chat.find({
                $and: [
                    { $or: [{ role1: data.role1 }, { role1: data.role2 }] },
                    { $or: [{ role2: data.role1 }, { role1: data.role2 }] }
                ]
            }).limit(50).sort({ $natural: -1 })


            console.log('mensajes guardados ', mensaje);

            // io.to(client.id).emit('nuevosMensajes', { from: 'enviarMensajePrivado', data: mensaje })
            io.emit('crearMensaje', mensaje)

*/
        })


        client.on('consultaGeneral', (consultas) => {
            /* console.log(consultas); */

            io.emit('consultaNueva', consultas);


        });

        client.on('laboratorios', (consultas) => {
            io.emit('laboratorioNuevo', consultas);
        });
        client.on('patologia', (consultas) => {
            io.emit('patologiaNuevo', consultas);
        });
        client.on('tomografia', (consultas) => {
            io.emit('tomografiaNuevo', consultas);
        });
    //Procesos banco de sangre
        client.on('bancoDeSangre', (consultas) => {
            /* console.log(consultas); */

            io.emit('procesoDonacion', consultas);


        });

        // client.on('mensaje', (data) => {

        //     io.emit('mensajeLab', data);
        // });


        client.on('cerrarSesion', (userDisconect) => {
            let personasBorrada = usuarios.eliminarPersona(client.id)
            let usuarioDesconectado = usuariosenLinea.filter((element) => {
                return element.role !== userDisconect;
            })

            io.emit('usuarioEnLinea', userDisconect);

        });

        client.on('usuarioDesconectado', ()=>{
            let personasBorrada = usuarios.eliminarPersona()
            io.emit('personaEliminada', personasBorrada)
        })


        client.on('enfermeria2Doctor', (paciente) => {
            io.emit('recibirPaciente', { paciente })
        });

        client.on('enviarReceta', (receta) => {
            io.emit('enviarAfarmaciaLaReceta', receta)
        });

        client.on('disconnect', () => {

            let personasBorrada = usuarios.eliminarPersona(client.id)
            // console.log('persona borrada', personasBorrada);
            io.emit('usuarioEnLinea', usuarios.getPersonas());
        })
    });
}