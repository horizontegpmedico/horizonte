class Notification {
    constructor(){}
    sendNotification(data){
        var headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization":"Basic OTkyZmZkNGItMWUyNi00NTRiLTliNTUtZDY0NTRjOTQ0OGY1"
        };

        var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
        }
        const https = require('https');
        var req = https.request(options,function(res){
           res.on('data',function(data){
            /* console.log("Response:"); */
            console.log(JSON.parse(data));
           }); 
        });
        req.on('error', function(e) {
            console.log("ERROR:");
            console.log(e);
          });
        req.write(JSON.stringify(data));
        req.end();
    }
 }
 module.exports = {
    Notification   
   }